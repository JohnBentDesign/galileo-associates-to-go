<?php
	/*===========================================================================
	Template Name: Full Width Page
	=============================================================================
	Style of page with no sidebar
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');

	if(post_password_required()):
		get_template_part('parts/shared/password');

	else:
?>
	<div class="container full">
		<div class="row">

			<div class="main small-10 small-centered columns">

				<?php
					// Page Featured Image //
					$featured = get_field('page_featured');

					if($featured):
						echo '<div class="content-featured secondary">';
						pantheon_display_post_field_image(get_field('page_featured'), 'TTG Featured Image', 'image', true);
						echo '</div>';
					endif;
				?>
				
				<div class="inner primary">

					<?php 
						if(have_posts()):
							while(have_posts()): the_post();
					?>
							<div class="content">
								<?php the_content(); ?>
							</div>
					<?php
							endwhile;
						endif;
					?>	

				</div>			
			</div>

		</div>
	</div>

<?php
	endif;

	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>