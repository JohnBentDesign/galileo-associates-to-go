<?php
/**
 * Class SassWatcher
 *
 * This simple tool compiles all .scss files in folder A to .css files (with exactly the same name) into folder B.
 * To keep things as minimal as possible, this tool compiles every X seconds, regardless of changes within the files.
 * This seems weird, but makes sense as checking for changes in the files is more CPU-extensive than simply
 * re-compiling them. SassWatcher uses scssphp, the best SASS compiler in PHP available.
 *
 * SassWatcher is not a standalone compiler, it's just a little method that uses the excellent scssphp compiler written
 * by Leaf Corcoran (https://twitter.com/moonscript), which can be found here: http://leafo.net/scssphp/ and adds
 * automatic interval-compiling to it.
 *
 * The currently supported version of SCSS syntax is 3.2.12, which is the latest one.
 * To avoid confusion: SASS is the name of the language itself, and also the "name" of the "first" version of the
 * syntax (which was quite different than CSS). Then SASS's syntax was changed to "SCSS", which is more like CSS, but
 * with awesome additional possibilities and features.
 *
 * The compiler uses the SCSS syntax, which is recommended and mostly used. The old SASS syntax is not supported.
 *
 * @see SASS Wikipedia: http://en.wikipedia.org/wiki/Sass_%28stylesheet_language%29
 * @see SASS Homepage: http://sass-lang.com/
 * @see scssphp, the used compiler (in PHP): http://leafo.net/scssphp/
 *
 * How to use this tool:
 *
 * 1. Edit $sass_watcher->watch( ... ); in the last line of this file and put your stuff in here, see the parameter
 *    list below.
 * 2. Make sure PHP can write into your CSS folder.
 * 3. Run the script:
 *    a) simple way, from browser, just enter the URL to scss-compiler.php: http://127.0.0.1/folder/php-sass-watcher.php
 *       The script will run forever, even if you close the browser window.
 *    b) PHPStorm users can run the script by right-clicking the file and selecting "Run php-sass-watcher.php".
 * 4. To stop the script, stop/restart your Apache/Nginx/etc. or press the red "stop process button in PHPStorm.
 *
 * The parameters:
 *
 *  1. relative path to your SCSS folder
 *  2. relative path to your CSS folder (make sure PHP has write-rights here)
 *  3. the compiling interval (in seconds)
 *  4. relative path to the scss.inc.php file, which is the main file of the SASS compiler used
 *     here. Download the script manually from http://leafo.net/scssphp/ or "require" it via Composer:
 *     "leafo/scssphp": "0.0.9"
 *  5. optional: how the .css output should look like. See http://leafo.net/scssphp/docs/#output_formatting for more.
 *
 * How the tool works:
 *
 * Every X seconds ALL files in the scss folder will be compiled to same-name .css files in the css folder.
 * The tool does not stop when a .scss file is broken, has syntax error or similar.
 * The tool does not compile when .scss file is broken, has syntax error or similar. It will only compile next time
 * when there's a valid scss file.
 */

/*==========================================================================
CUSTOM COLORS DEFINED BY USER
==========================================================================*/
$primaryColor       = (get_field('color_primary', 'options'))       ? get_field('color_primary', 'options')     : '#ffffff';
$secondaryColor     = (get_field('color_secondary', 'options'))     ? get_field('color_secondary', 'options')   : '#565656';
$tertiaryColor      = (get_field('color_tertiary', 'options'))      ? get_field('color_tertiary', 'options')    : '#00aeef';
$quaternaryColor    = (get_field('color_quaternary', 'options'))    ? get_field('color_quaternary', 'options')  : '#691c33';

$user_colors = '
	@import "includes";

	// FUNCTIONS
	//-------------------------------------------------------------
	// More Accurate Color Contrastor (http://codepen.io/bluesaunders/pen/FCLaz)

	// Calculate brightness of a given color.
	@function brightness($color) {
		@return ((red($color) * .299) + (green($color) * .587) + (blue($color) * .114)) / 255 * 100%;
	}

	// Compares contrast of a given color to the light/dark arguments and returns whichever is most "contrasty"
	@function color-contrast($color, $dark: $dark-text-default, $light: $light-text-default) {
		@if $color == null {
		    @return null;
		}
		@else {
			$color-brightness: brightness($color);  
			$light-text-brightness: brightness($light);
			$dark-text-brightness: brightness($dark);
			@return if(abs($color-brightness - $light-text-brightness) > abs($color-brightness - $dark-text-brightness), $light, $dark);  
		}
	}


	// VARIABLES
	//-------------------------------------------------------------
	// Raw
	$primary: ' . $primaryColor . ';
	$secondary: ' . $secondaryColor . ';
	$tertiary: ' . $tertiaryColor . ';
	$quaternary: ' . $quaternaryColor . ';

	// PRIMARY
	//-------------------------------------------------------------
	body, .primary { background-color: $primary; }
	.overlay-primary { background-color: rgba($primary, .8); }
	body, .primary, .overlay-primary {
		h1, h1 a, h2, h3, h4, h5, h6, a {
			color: $tertiary;
		}
		p, ol, ul, li, blockquote, dl, dt, dd, table, tr, td, span, pre, label, input, textarea {
			color: lighten($secondary, 15%);
		}
		hr:after { border-top-color: $tertiary; }
		blockquote {
			background-color: $tertiary;
			p, ol, ul, li, span, cite, a {
				color: $primary;
			}
		}
		a.button, input[type=submit] {
			background-color: rgba($tertiary, .8);
			border-color: $tertiary;
			color: $primary;
			&:hover {
				background-color: rgba($secondary, .8);
				color: $primary;
				border-color: $secondary;
			}
		}
	}
	.section-block .primary {
		ul.list li:nth-child(odd) { background-color: color-contrast($primary, darken($primary, 15%), lighten($primary, 15%)); }
		ul.list li:nth-child(even) { background-color: color-contrast($primary, darken($primary, 25%), lighten($primary, 25%)); }
		.section-list .list {
			background-color: color-contrast($primary, darken($primary, 15%), lighten($primary, 15%));
		}
	}
	.primary.section-cta.cta-box {
		a.button {
			background-color: darken($primary, 10%);
			color: color-contrast($primary, darken($primary, 100%), lighten($primary, 100%));
			&:hover {
				background-color: darken($primary, 15%);
				color: $primary;
			}
		}
	}
	header {
		.primary {
			border-bottom:solid 5px $quaternary;
		}
	}

	// SECONDARY
	//-------------------------------------------------------------
	.secondary { background-color: $secondary; }
	.overlay-secondary { background-color: rgba($secondary, .8); }
	.secondary, .overlay-secondary {
		h1, h1 a, h2, h3, h4, h5, h6, a {
			color: $primary;
		}
		p, ol, ul, li, blockquote, dl, dt, dd, table, tr, td, span, pre, label, input, textarea {
			color: $primary;
		}
		hr:after { border-top-color: $tertiary; }
		blockquote {
			background-color: $tertiary;
			p, ol, ul, li, span, cite, a {
				color: $primary;
			}
		}
		a.button, input[type=submit] {
			background-color: rgba($primary, .8);
			border-color: $primary;
			color: $secondary;
			&:hover {
				background-color: rgba($tertiary, .8);
				color: $primary;
				border-color: $tertiary;
			}
		}
	}
	.section-block .secondary {
		ul.list li:nth-child(odd) { background-color: color-contrast($secondary, darken($secondary, 15%), lighten($secondary, 15%)); }
		ul.list li:nth-child(even) { background-color: color-contrast($secondary, darken($secondary, 25%), lighten($secondary, 25%)); }
		.section-list .list {
			background-color: color-contrast($secondary, darken($secondary, 15%), lighten($secondary, 15%));
		}
	}
	.secondary.section-cta.cta-box {
		a.button {
			background-color: darken($secondary, 10%);
			color: color-contrast($secondary, darken($secondary, 100%), lighten($secondary, 100%));
			&:hover {
				background-color: darken($secondary, 15%);
				color: $primary;
			}
		}
	}

	// TERTIARY
	//-------------------------------------------------------------
	.tertiary { background-color: $tertiary; }
	.overlay-tertiary { background-color: rgba($tertiary, .8); }
	.tertiary, .overlay-tertiary {
		h1, h1 a, h2, h3, h4, h5, h6, a {
			color: $primary;
		}
		p, ol, ul, li, blockquote, dl, dt, dd, table, tr, td, span, pre, label, input, textarea {
			color: $primary;
		}
		hr:after { border-top-color: $tertiary; }
		blockquote {
			background-color: $tertiary;
			p, ol, ul, li, span, cite, a {
				color: $primary;
			}
		}
		a.button, input[type=submit] {
			background-color: rgba($primary, .8);
			border-color: $primary;
			color: $tertiary;
			&:hover {
				background-color: rgba($quaternary, .8);
				color: $primary;
				border-color: $quaternary;
			}
		}
	}
	.section-block .tertiary {
		ul.list li:nth-child(odd) { background-color: color-contrast($tertiary, darken($tertiary, 15%), lighten($tertiary, 15%)); }
		ul.list li:nth-child(even) { background-color: color-contrast($tertiary, darken($tertiary, 25%), lighten($tertiary, 25%)); }
		.section-list .list {
			background-color: color-contrast($tertiary, darken($tertiary, 15%), lighten($tertiary, 15%));
		}
	}
	.tertiary.section-cta.cta-box {
		a.button {
			background-color: darken($tertiary, 10%);
			color: color-contrast($tertiary, darken($tertiary, 100%), lighten($tertiary, 100%));
			&:hover {
				background-color: darken($tertiary, 15%);
				color: $primary;
			}
		}
	}

	// QUARTERNARY
	//-------------------------------------------------------------
	.quaternary, .has-bg {
		background-color: $quaternary;
	}
	.overlay-quaternary {
		background-color: rgba($quaternary, .8);
	}
	.quaternary, .overlay-quaternary {
		h1, h1 a, h2, h3, h4, h5, h6, a {
			color: $primary;
		}
		p, ol, ul, li, blockquote, dl, dt, dd, table, tr, td, span, pre, label, input, textarea {
			color: $primary;
		}
		hr:after { border-top-color: $tertiary; }
		blockquote {
			background-color: $tertiary;
			p, ol, ul, li, span, cite, a {
				color: $primary;
			}
		}
		a.button, input[type=submit] {
			background-color: rgba($tertiary, .8);
			border-color: $tertiary;
			color: $primary;
			&:hover {
				background-color: rgba($secondary, .8);
				color: $primary;
				border-color: $secondary;
			}
		}
	}
	.section-block .quaternary {
		ul.list li:nth-child(odd) { background-color: color-contrast($quaternary, darken($quaternary, 15%), lighten($quaternary, 15%)); }
		ul.list li:nth-child(even) { background-color: color-contrast($quaternary, darken($quaternary, 25%), lighten($quaternary, 25%)); }
		.section-list .list {
			background-color: color-contrast($quaternary, darken($quaternary, 15%), lighten($quaternary, 15%));
		}
	}
	.quaternary.section-cta.cta-box {
		a.button {
			background-color: darken($quaternary, 10%);
			color: color-contrast($quaternary, darken($quaternary, 100%), lighten($quaternary, 100%));
			&:hover {
				background-color: darken($quaternary, 15%);
				color: $primary;
			}
		}
	}
	
	// CONTAINER & SHARED STYLES
	//-------------------------------------------------------------
	body {
		background-color: $primary;
		color: $secondary;
	}
	.container {
		background-color: darken($primary, 10%);
	}
	a.button, a.continue {
		background-color: $quaternary;
		color: color-contrast($quaternary, darken($quaternary, 100%), lighten($quaternary, 100%));
		&:hover { background-color: $quaternary; }
	}
	input, textarea { border-color: $quaternary; }
	input[type=submit] {
		background-color: $quaternary;
		color: $primary;
		&:hover, &:active {
			background-color: $primary;
			color: $quaternary;
		}
	}

	// HEADER
	//-------------------------------------------------------------
	header.main {
		a, span { color: $tertiary; }
		nav.main .menu ul, nav.main .menu ul li:hover {
			background-color: rgba(color-contrast($quaternary, darken($quaternary, 15%), lighten($quaternary, 15%)), .9);
		}
		nav.main ul li, nav.main ul {
			border-color: $quaternary;
		}
		.sub-menu a {
			color: color-contrast($tertiary, darken($tertiary, 100%), lighten($tertiary, 100%));
		}
		.upper {
			.social, nav, .custom, .vcard {
				border-color: rgba(color-contrast($quaternary, darken($quaternary, 100%), lighten($quaternary, 100%)), .5);
				span, p, a { color: $secondary; }
			}
		}
	}

	// BACKDROP
	//-------------------------------------------------------------
	.backdrop {
		h1 {
			&.primary {
				background-color: rgba($primary, .8);
				color: $tertiary;
			}
			&.secondary {
				background-color: rgba($secondary, .8);
				color: $primary;
			}
			&.tertiary {
				background-color: rgba($tertiary, .8);
				color: $primary;
			}
			&.quaternary {
				background-color: rgba($quaternary, .8);
				color: $secondary;
			}
		}
	}

	// SIDEBAR
	//-------------------------------------------------------------
	aside.sidebar {
		.widget { 
			background-color: $tertiary;
			h1, h2, h3, h4, h5, h6, p, ul, ol, li, span, a, label {
				color: $primary;
			}
			input {
				border-color: $primary;
				background-color: lighten($tertiary, .25);
				color: $primary;
			}
		}
		.widget:after { background-color: rgba($secondary, .5); }
		.widget-hours .hours p { border-top-color: rgba($secondary, .25); }
		li {
			border-color: rgba($primary, .1);
		}
		#wp-calendar caption { color: $quaternary; }
	}

	// FOOTER
	//-------------------------------------------------------------
	footer.main {
		p, a, span {
			color: $tertiary;
		}
		nav.footer li { border-left-color: $tertiary; }
	}


	// HOMEPAGE
	//-------------------------------------------------------------
	.home-slideshow .slide-content {
		&.overlay-primary {
			h2, h3, h4, h5, h6, p { color: $secondary; }
		}
		&.overlay-secondary {
			h2, h3, h4, h5, h6, p { color: $primary; }
		}
		&.overlay-tertiary {
			h2, h3, h4, h5, h6, p { color: $primary; }
		}
		&.overlay-quaternary {
			h2, h3, h4, h5, h6, p { color: $tertiary; }
		}
	}
	.home-slider {
		.slick-dots button {
			background-color: $primary;
			border-color: $tertiary;
			&:hover, .slick-active & { background-color: $tertiary; }
		}
		.slick-dots .slick-active button { background-color: $tertiary; }
	}


	// SECTIONS
	//-------------------------------------------------------------
	.important-primary { border-color: $primary; }
	.important-secondary { border-color: $secondary; }
	.important-tertiary { border-color: $tertiary; }
	.important-quaternary { border-color: $quaternary; }


	// BLOG
	//-------------------------------------------------------------
	.post {
		time {
			background-color: $quaternary;
			span { color: $primary; }
		}
	}
	.the-post {
		&:after { background-color: lighten($secondary, 50%); }
	}
	.comment-list .children, .comments {
		background-color: rgba($secondary, .1);
	}
	.comment-list .comment, .comment-intro, .the-post, footer.post {
		border-color: lighten($secondary, 50%);
	}
	.comment .fn { color: $quaternary; }


	// BUTTONS
	//-------------------------------------------------------------
	.button.button-customize {
		&.primary-bg { 
			background-color: $primary;
			color: color-contrast($primary, darken($primary, 100%), lighten($primary, 100%));
		}
		&.secondary-bg { 
			background-color: $secondary;
			color: color-contrast($secondary, darken($secondary, 100%), lighten($secondary, 100%));
		}
		&.tertiary-bg { 
			background-color: $tertiary;
			color: color-contrast($tertiary, darken($tertiary, 100%), lighten($tertiary, 100%));
		}
		&.quaternary-bg { 
			background-color: $quaternary;
			color: color-contrast($quaternary, darken($quaternary, 100%), lighten($quaternary, 100%));
		}

		&.primary-border { border-color: $primary; }
		&.secondary-border { border-color: $secondary; }
		&.tertiary-border { border-color: $tertiary; }
		&.quaternary-border { border-color: $quaternary; }

		&.primary-hover:hover { 
			background-color: $primary;
			color: color-contrast($primary, darken($primary, 100%), lighten($primary, 100%));
		}
		&.secondary-hover:hover { 
			background-color: $secondary;
			color: color-contrast($secondary, darken($secondary, 100%), lighten($secondary, 100%));
		}
		&.tertiary-hover:hover { 
			background-color: $tertiary;
			color: color-contrast($tertiary, darken($tertiary, 100%), lighten($tertiary, 100%));
		}
		&.quaternary-hover:hover {
			background-color: $quaternary;
			color: color-contrast($quaternary, darken($quaternary, 100%), lighten($quaternary, 100%));
		}

		&.primary-hover-border:hover { border-color: $primary; }
		&.secondary-hover-border:hover { border-color: $secondary; }
		&.tertiary-hover-border:hover { border-color: $tertiary; }
		&.quaternary-hover-border:hover { border-color: $quaternary; }
	}

	// RESPONSIVE
	//-------------------------------------------------------------
	@media only screen and (max-width: 40em) {
		// Header
		nav.main {
			background-color: $tertiary;
			span { color: color-contrast($tertiary, darken($tertiary, 100%), lighten($tertiary, 100%)); }
		}
	}

	// Medium screens
	@media only screen and (min-width: 40.063em) { } /* min-width 641px, medium screens */

	@media only screen and (min-width: 40.063em) and (max-width: 64em) { } /* min-width 641px and max-width 1024px, use when QAing tablet-only issues */

	// Large screens
	@media only screen and (min-width: 64.063em) { } /* min-width 1025px, large screens */

	@media only screen and (min-width: 64.063em) and (max-width: 90em) { } /* min-width 1025px and max-width 1440px, use when QAing large screen-only issues */

	// XLarge screens
	@media only screen and (min-width: 90.063em) { } /* min-width 1441px, xlarge screens */

	@media only screen and (min-width: 90.063em) and (max-width: 120em) { } /* min-width 1441px and max-width 1920px, use when QAing xlarge screen-only issues */

	// XXLarge screens
	@media only screen and (min-width: 120.063em) { } /* min-width 1921px, xxlarge screens */
';

$user_custom_font_style = pantheon_function_font_select(array(
		'header' 	=> 'header.main',
		'footer' 	=> 'footer.main',
		'sidebar' 	=> 'aside.sidebar',
		'widget' 	=> '.widget',
		'post' 		=> '.the-post',
		'page' 		=> '.main-content',
		'gallery' 	=> 'article.gallery',
		'product' 	=> 'li.product',
	)
);

$user_custom_text_style = pantheon_function_text_style(array(
		'header' 	=> 'header.main',
		'footer' 	=> 'footer.main',
		'sidebar' 	=> 'aside.sidebar',
		'widget' 	=> '.widget',
		'post' 		=> '.the-post',
		'page' 		=> '.main-content',
		'gallery' 	=> 'article.gallery',
		'product' 	=> 'li.product',
	)
);

$user_custom_styles = get_field('advanced_css', 'options');


/*==========================================================================*/
// echo get_template_directory() . '/stylesheets/sass/';
class SassWatcher
{
	/**
	 * Watches a folder for .scss files, compiles them every X seconds
	 * Re-compiling your .scss files every X seconds seems like "too much action" at first sight, but using a
	 * "has-this-file-changed?"-check uses more CPU power than simply re-compiling them permanently :)
	 * Beside that, we are only compiling .scss in development, for production we deploy .css, so we don't care.
	 *
	 * @param string $scss_folder source folder where you have your .scss files
	 * @param string $css_folder destination folder where you want your .css files
	 * @param int $interval interval in seconds
	 * @param string $scssphp_script_path path where scss.inc.php (the scssphp script) is
	 * @param string $format_style CSS output format, ee http://leafo.net/scssphp/docs/#output_formatting for more.
	 */
	public function watch($scss_folder, $css_folder, $scssphp_script_path, $format_style = "scss_formatter_compressed", $user_defined_colors = null, $user_defined_custom_font = null, $user_defined_styles = null)
	{
		// load the compiler script (scssphp), more here: http://www.leafo.net/scssphp
		require $scssphp_script_path;
		$scss_compiler = new scssc();

		// set the path to your to-be-imported mixins. please note: custom paths are coming up on future releases!
		$scss_compiler->setImportPaths($scss_folder);
		// set css formatting (normal, nested or minimized), @see http://leafo.net/scssphp/docs/#output_formatting
		$scss_compiler->setFormatter($format_style);

		// get all .scss files from scss folder
		$filelist = glob($scss_folder . "*.scss");

		// step through all .scss files in that folder
		foreach ($filelist as $file_path) {
			// get path elements from that file
			$file_path_elements = pathinfo($file_path);

			// get file's name without extension
			$file_name = $file_path_elements['filename'];

			// get .scss's content, put it into $string_sass
			$string_sass = file_get_contents($scss_folder . $file_name . ".scss");

			// Add our user defined colors to the compiled code
			$string_sass .= $user_defined_colors;

			// Add our user font styles to the compiled code
			$string_sass .= $user_defined_custom_font;

			// Add our user defined styles to the compiled code (so we can save it)
			$string_sass .= $user_defined_styles;

			// try/catch block to prevent script stopping when scss compiler throws an error
			try {
				// compile this SASS code to CSS
				$string_css = $scss_compiler->compile($string_sass);
				// write CSS into file with the same filename, but .css extension
				file_put_contents($css_folder . $file_name . ".css", $string_css);
			} catch (Exception $e) {
				echo 'There was an error compiling your CSS.';
			}
		}
	}
}

$sass_watcher = new SassWatcher();
$sass_watcher->watch(get_template_directory() . '/stylesheets/sass/', get_template_directory() . '/stylesheets/css/', get_template_directory() . '/stylesheets/scss.inc.php', 'scss_formatter', $user_colors, $user_custom_font_style, $user_custom_styles);
