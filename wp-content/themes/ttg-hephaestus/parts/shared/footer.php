<?php
	/*===========================================================================
	FOOTER- MAIN
	===========================================================================*/
	// Contains the copyright and footer navigation
?>	
	<footer class="primary main row">
		
		<div class="large-4 columns">
			<?php
				// Get Contact Information!
				$addressOne 		= get_field('general_address_one', 'options');
				$addressTwo 		= get_field('general_address_two', 'options');
				$addressCity 		= get_field('general_address_city', 'options');
				$addressState 		= get_field('general_address_state_abbr', 'options');
				$addressZip 		= get_field('general_address_zip', 'options');
				$phone 				= get_field('general_phone', 'options');
				$phoneFree			= get_field('general_phone_free', 'options');
				$fax 				= get_field('general_fax', 'options');
				$emailPrimary 		= get_field('general_email_primary', 'options');
				$emailSecondary 	= get_field('general_email_secondary', 'options');

				// Make an array with our details so we don't show empty containers
				$contactDetails = array($addressOne, $addressTwo, $addressCity, $addressState, $addressZip, $phone, $phoneFree, $fax, $emailPrimary, $emailSecondary);

				if(!empty($contactDetails)):
					echo '<div class="vcard contact-details">';
					echo '<div class="row">';
					
					echo '<div class="medium-6 columns">';
					// ADDRESS //
					if($addressOne || $addressCity || $addressState || $addressZip){
						echo '<div class="adr">';
						// Address: Company Name
						echo '<p class="fn org">' . get_bloginfo('title') . '</p>';
						// Address: Street
						if($addressOne) { echo '<p class="street-address">' . $addressOne . '</p>'; }
						if($addressTwo) { echo '<p class="extended-address">' . $addressTwo . '</p>'; }
						// Address: Locale
						if($addressCity || $addressState || $addressZip) {
							echo '<p class="locale">';
							if($addressCity) 									{ echo '<span class="locality">' . $addressCity . '</span>'; }
							if($addressCity && ($addressState || $addressZip)) 	{ echo ', '; }
							if($addressState) 									{ echo '<span class="region">' . $addressState . '</span> '; }
							if($addressZip) 									{ echo '<span class="postal-code">' . $addressZip . '</span>'; }
							echo '</p>';
						}
						echo '</div>';
					}
					echo '</div>';

					echo '<div class="medium-6 columns">';
					// NUMBERS //
					if($phone || $phoneFree || $fax){
						echo '<div class="numbers">';

						if($phone) 		{ echo '<p class="tel"><span class="prepend">Phone:</span> <a href="tel:' . $phone . '">' . $phone . '</a></p>'; }
						if($phoneFree) 	{ echo '<p class="tel" type="work"><span class="prepend">Toll Free:</span> <a href="tel:' . $phoneFree . '">' . $phoneFree . '</a></p>'; }
						if($fax)		{ echo '<p class="tel" type="fax"><span class="prepend">Fax:</span>' . $fax . '</p>'; }

						echo '</div>';
					}

					// EMAILS //
					if($emailPrimary || $emailSecondary){
						echo '<div class="emails">';

						if($emailPrimary) 	{ echo '<a href="mailto:' . $emailPrimary . '">' . $emailPrimary . '</a>'; }
						if($emailSecondary) { echo '<a href="mailto:' . $emailSecondary . '">' . $emailSecondary . '</a>'; }

						echo '</div>';
					}
					echo '</div>';

					echo '</div>';
					echo '</div>';		
				endif;
			?>
		</div>

		<div class="large-8 columns">
			<?php
				// FOOTER NAVIGATION
				//===================================
				$menuFooter = array(
					'theme_location'  	=> 'menu-footer',
					'container'       	=> 'nav',
					'container_class' 	=> 'footer',
					'menu_class'      	=> 'menu',
					'depth'           	=> 1,
					'fallback_cb'		=> 0
				);
				wp_nav_menu($menuFooter);
			?>
			<div class="copyright">
				<p>&copy;<?php echo date('Y') . ' '; bloginfo('name'); ?>. Site Powered by <a href="http://techtherapytogo.com/" target="_blank">Technology Therapy To Go</a>.</p>
			</div>
		</div>

	</footer>