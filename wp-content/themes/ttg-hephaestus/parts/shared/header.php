<?php
	/*===========================================================================
	HEADER- MAIN
	===========================================================================*/
	// Contains the logo and main navigation

	$headerTop = get_field('header_top', 'options');
?>
	<header class="primary main row">

		<?php // LOGO // ?>
		<div class="medium-4 columns">
			<a href="<?php echo get_bloginfo ( 'url' ); ?>" class="logo">
				<?php 
					if(get_field('header_logo', 'options')){
						pantheon_display_post_field_image(get_field('header_logo', 'options'), 'medium', 'image');
					}
					else {
						bloginfo('title');
					}
				?>
			</a>
		</div>

		<div class="medium-8 columns">

			<div class="upper">
				<?php
					// CUSTOM CONTENT
					//===================================
					if(is_array($headerTop) && in_array('custom', $headerTop)){
						echo '<div class="custom">';
						echo wpautop(get_field('header_custom', 'options'));
						echo '</div>';
					}
				?>

				<?php
					// CONTACT DETAILS
					//===================================
					get_template_part('parts/shared/contact');
				?>

				<?php
					// TOP NAVIGATION
					//===================================
					$menuTop = array(
						'theme_location' 	=> 'menu-top',
						'container'			=> 'nav',
						'container_class'	=> 'top-nav',
						'menu_class'		=> 'menu',
						'depth'				=> 1,
						'fallback_cb'		=> 0
					);
					wp_nav_menu($menuTop);
				?>
				<?php
					// SOCIAL- CONNECT
					//===================================
					pantheon_display_social_connect('header');
				?>
			</div>
			
			<?php
				// MAIN NAVIGATION
				//===================================
				$menuMain = array(
					'theme_location' 	=> 'menu-main',
					'container'			=> 'nav',
					'container_class'	=> 'main',
					'menu_class'		=> 'menu',
					'depth'				=> 3,
					'fallback_cb'		=> 0
				);
				wp_nav_menu($menuMain);
			?>
		</div>

	</header><?php // /header.main ?>
