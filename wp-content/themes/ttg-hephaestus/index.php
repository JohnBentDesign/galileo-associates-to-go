<?php
	/*===========================================================================
	INDEX
	=============================================================================
	Default fallback style. Default page for blog viewing page
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>

	<div class="container primary">
		<div class="row">

			<?php 
				// POST LOOP //
				if($post->post_type == 'gallery'){
					get_template_part('parts/posts/loop', 'gallery');	
					get_template_part('parts/shared/pagination');
				}
				else {
			?>
					<div class="columns">
						<h2 class="blog-title"><?= get_the_title(get_option('page_for_posts')); ?></h2>
					</div>
					
					<div class="main medium-7 columns">
						<?php get_template_part('parts/posts/loop'); ?>
						<?php get_template_part('parts/shared/pagination'); ?>
					</div>

					<?php
						// SIDEBAR //
						get_template_part('parts/shared/sidebar');
					?>

			<?php
				}
			?>

		</div>
	</div>

<?php
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>