<?php
	/*===========================================================================
	Template Name: Homepage
	=============================================================================
	Template specifically for making it easier to target ACF display
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>

	<?php
		// SLIDER //
		if(have_rows('home_slider')):
			// Slider Overall Settings
			$contentSide 		= get_field('home_slide_content_side');
			$overlayColor 		= get_field('home_slide_overlay');
			$sliderTime			= get_field('home_slide_time');
			$sliderTimeAnimate 	= get_field('home_slide_time_animate');
			$slideAnimate 		= get_field('home_slide_animate');
			$slideType 			= ($slideAnimate == 'fade') ? 1 : 0;

			// Slider Container
	?>
			<div class="home-slideshow side-<?= $contentSide; ?>">
				<div class="home-slider" data-slider-time="<?= $sliderTime; ?>" data-slider-animate="<?= $sliderTimeAnimate; ?>" data-slider-fade="<?= $slideType; ?>" data-slider-overlay="overlay-<?= $overlayColor; ?>">

					<?php 
						while ( have_rows('home_slider') ) : the_row();
							// Slide Information
							$image 			= get_sub_field('image');
							$title 			= htmlentities(get_sub_field('title'));
							$content 		= htmlentities(get_sub_field('content'));
							$ctaDisplay 	= get_sub_field('cta_display');
							$ctaText 		= get_sub_field('cta_text');
							$ctaType 		= get_sub_field('cta_type');
							$ctaInternal 	= get_sub_field('cta_internal');
							$ctaExternal 	= get_sub_field('cta_external');
							$ctaOutput 		= ($ctaDisplay) ? htmlentities('<a href="' . $ctaInternal . $ctaExternal . '" class="button">' . $ctaText . '</a>') : '';
					?>
							<div class="slide" style="background-image: url(<?= $image['sizes']['TTG Featured Image']; ?>)" data-slider-title="<?= $title; ?>" data-slider-content="<?= $content; ?>" data-slider-cta="<?= $ctaOutput; ?>"></div>
					<?php endwhile; ?>

				</div>

				<div class="slide-content overlay-<?= $overlayColor; ?>"><div class="inner"></div></div>
			</div>
	<?php endif; ?>

	<div class="container">

		<?php 
			if(have_posts()):
				while(have_posts()): the_post();

					$sections = get_field('section');

					if($sections):
						$i = 1;
						$rowing = 0;
						foreach($sections as $section):
							// Our columns need to be cleared at the right times... So let's row!
							$sectionWidth 	= $section['section_width'];
							$rowContainer 	= hephaestus_rowing($sectionWidth, $rowing, $i);
							$rowing 		= $rowContainer['rowing'];

							echo $rowContainer['open'];

							// Build our section
							$build = new hephaestus_section_build($section);

							// Output our column
							$build->section_construct($i);

							if($rowContainer['close']){
								echo $rowContainer['close'];
							}

							// Increment our section
							$i++;
						endforeach;
					endif;

					// var_dump($rowing);

				endwhile;
			endif;
		?>			

	</div>

<?php
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>