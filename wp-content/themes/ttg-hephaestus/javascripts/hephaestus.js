(function($){
	$(document).foundation();

	$(document).ready(function(){

		//===================================================
		// HOMEPAGE SLIDER
		//===================================================
		$slider = $('.home-slider');

		// Show only if homepage slider is available
		if($slider.length > 0){
			// Variables
			var $slideFade 		= $slider.attr('data-slider-fade'),
				$slideTime 		= $slider.attr('data-slider-time'),
				$slideAnimate 	= $slider.attr('data-slider-animate');

				if($slideFade == 1) { $slideFade = true; }
				else { $slideFade = false; }

			// Initialize Slideshow
			$('.home-slider').slick({
				autoplay: true,
				autoplaySpeed: $slideTime,
				arrows: false,
				dots: true,
				fade: $slideFade,
				infinite: true,
				lazyLoad: 'ondemand',
				speed: $slideAnimate,
				onInit : function(){
					slideChange();
				},
    			onAfterChange : function(index){
        			slideChange();
			    }
			});
		}

		// slideChange Function
		function slideChange(){
			var $activeSlide 	= $('.slick-active'),
				$activeTitle 	= $activeSlide.attr('data-slider-title'),
				$activeContent 	= $activeSlide.attr('data-slider-content'),
				$activeCTA 		= $activeSlide.attr('data-slider-cta'),
				$slideContent 	= $('.slide-content');

			// If content is present, we'll switch the content with it
			if($activeTitle || $activeContent || $activeCTA){
				// Show the slide content if it was hidden before
				if(!$slideContent.is(':visible')) {
					$slideContent.fadeIn('fast');
				}

				// Change dat content
				$slideContent.find('.inner').html('<h2>' + $activeTitle + '</h2>' + $activeContent + $activeCTA);
			}
			// ELSE, we'll remove the box
			else {
				$slideContent.fadeOut('fast');
			}
		}


		//===================================================
		// RESPONSIVE NAVIGATION
		//===================================================
		// Responsive Navigation
		//---------------------------
		$('nav.main ul.menu').tinyNav({
			active: 'selected',
			header: 'Navigation',
			indent: '- ',
		});
		$('.tinynav').customSelect();
		$('.customSelectInner').html('Navigation');
	});
})(jQuery);