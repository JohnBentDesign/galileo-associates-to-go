<?php
	if( function_exists('register_field_group') ):

	register_field_group(array (
		'key' => 'group_547f84d824eeb',
		'title' => 'Page Settings',
		'fields' => array (
			array (
				'key' => 'field_547f86dbfe92f',
				'label' => 'Hide Title From Header',
				'name' => 'page_header_title',
				'prefix' => '',
				'type' => 'true_false',
				'instructions' => 'If this is checked, you will need to provide an H1 tag in the content.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => 33,
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
			),
			array (
				'key' => 'field_547f875d8cf30',
				'label' => 'Header Overlay',
				'name' => 'page_header_overlay',
				'prefix' => '',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => 33,
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					'primary' => 'Primary',
					'secondary' => 'Secondary',
					'tertiary' => 'Tertiary',
					'quaternary' => 'Quaternary',
				),
				'default_value' => array (
					'secondary' => 'secondary',
				),
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'ajax' => 0,
				'placeholder' => '',
				'disabled' => 0,
				'readonly' => 0,
			),
			array (
				'key' => 'field_547f85fafe92e',
				'label' => 'Content Area Featured Image',
				'name' => 'page_featured',
				'prefix' => '',
				'type' => 'image',
				'instructions' => 'This image will appear across the top of the content area.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => 33,
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '!=',
					'value' => 'page-home.php',
				),
				array (
					'param' => 'post_type',
					'operator' => '!=',
					'value' => 'post',
				),
			),
		),
		'menu_order' => 5,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));

	endif;