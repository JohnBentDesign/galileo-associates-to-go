<?php
	/*==================================================================
	BUILD A SECTION
	
	Like build a bear, but not as cute
	=================================================================*/

	class hephaestus_section_build {
		
		//----------------------------------------------------------------
		// ⍟ CONSTRUCT THE SECTION
		//----------------------------------------------------------------
		public $section = NULL;
		public function __construct($section) {
			$this->section = $section;
		}


		//----------------------------------------------------------------
		// ⍟ CONTENT CONSTRUCTION
		//----------------------------------------------------------------
		// CONTENT: Title
		//--------------------------------
		private function content_title() {
			$sectionWidth 	= $this->section['section_width'];
			$sectionSmall 	= ( ($sectionWidth == 'third') || ($sectionWidth == 'half') ) ? true : false;
			$contentType 	= $this->section['content_type'];
			$contentTitle 	= $this->section['content_title'];
			$titleOverlay 	= $this->section['title_overlay_display'];
			$titleColor 	= $this->section['title_overlay_color'];


			// Wrap in Overlay if Allowed
			if($sectionSmall && $titleOverlay && $contentTitle){
				echo '<div class="overlay-' . $titleColor . ' title-overlay">';
			}

			// Custom Title
			if ( ($contentType != 'article') && $contentTitle ) {
				echo '<h3>' . $contentTitle . '</h3>';
			}
			// Article Title
			elseif( $contentType == 'article' ) {
				$contentArticle	= $this->section['content_article'];

				// Setup the post object to get the info
				setup_postdata( $contentArticle ); 
				echo '<h3>' . get_the_title($contentArticle->ID) . '</h3>';
				wp_reset_postdata();
			} 
			// Nanimo...
			else { return false; }

			// Wrap in Overlay if Allowed
			if($sectionSmall && $titleOverlay && $contentTitle){
				echo '</div>';
			}
		}

		// CONTENT: Content
		//--------------------------------
		private function content_content() {
			$contentType 	= $this->section['content_type'];
			$contentContent	= $this->section['content_content'];

			// Custom Content
			if ( ($contentType != 'article') && $contentContent ) {
				echo $contentContent;
			}
			// Article Content
			elseif( $contentType == 'article' ) {
				$contentArticle	= $this->section['content_article'];

				// Setup the post object to get the info
				setup_postdata( $contentArticle ); 
				echo wpautop($contentArticle->post_content);
				wp_reset_postdata();
			} 
			// Nanimo...
			else { return false; }
		}

		// CONTENT: List
		//--------------------------------
		private function content_list() {
			$contentType 	= $this->section['content_type'];
			$contentList	= $this->section['content_list'];

			// List Loop
			if ( ($contentType == 'list') && $contentList ) {
				$listWidth 	= $this->style_inner_width();

				echo '<div class="list-container ' . $listWidth['list'] . '" data-equalizer-watch>';
				echo '<ul class="list">';
				foreach($contentList as $li) {
					$listType 		= $li['list_type'];
					$listPadding 	= ($li['list_padding']) ? '' : ' padded';

					// Content
					if($listType == 'content'){
						echo '<li class="list-type-content' . $listPadding . '">';
						echo $li['list_item'];
						echo '</li>';
					}

					// Image
					if($listType == 'image'){
						$isBG 		= $li['list_image_bg'];
						$imageType 	= ($isBG) ? 'style-add' : 'image'; 
						$image 		= pantheon_display_post_field_image($li['list_image'], $size = 'large', $output = $imageType, $echo = false);
						$height 	= $li['list_image_bg_height'];
						$style 		= ($isBG) ? ' style="' . $image . 'height:' . $height . 'px"' : '';
						echo '<li class="list-type-image' . $listPadding . '" ' . $style . '>';
						if(!$isBG){
							echo $image;
						}
						echo '</li>';
					}

					// CTA
					if($listType == 'cta'){
						$ctaTitle 		= $li['cta_text'];
						$ctaBlank 		= $li['cta_blank'] ? ' target="_blank"' : '';
						$ctaType 		= $li['cta_type'];
						$ctaLink 		= ($ctaType == 'internal') ? $li['cta_internal'] : $li['cta_external'];
						$ctaColor 		= $li['cta_color'];
						$ctaBorder 		= $li['cta_border'];
						$ctaColorHover	= $li['cta_hover'];
						$ctaBorderHover	= $li['cta_hover_border'];
						
						// We need to build the class for this button. Wowie.
						$ctaClass 		= 'button';
						if($ctaColor != 'default') $ctaClass .= ' ' . $ctaColor;
						if( ($ctaBorder != 'default') ) $ctaClass .= ' ' . $ctaBorder;
						if($ctaColorHover != 'default') $ctaClass .= ' ' . $ctaColorHover;
						if( ($ctaBorderHover != 'default') && ($ctaBorder != 'no-border') ) $ctaClass .= ' ' . $ctaBorderHover;

						echo '<li class="list-type-cta' . $listPadding . '">';
						echo '<a href="' . $ctaLink . '" class="button-customize ' . $ctaClass . '"' . $ctaBlank . '>' . $ctaTitle . '</a>';
						echo '</li>';
					}

				}
				echo '</ul>';
				echo '</div>';
			} 
			// Nanimo...
			else { return false; }
		}

		// CONTENT: Words
		//--------------------------------
		private function content_words() {
			// This combines the title, content, and cta button into one element
			$wordsWidth 		= $this->style_inner_width();

			echo '<div class="' . $wordsWidth['words'] . '" data-equalizer-watch><div class="padded clearfix">';
			$this->content_title();
			$this->content_content();
			$this->cta_btn();
			echo '</div></div>';
		}


		//----------------------------------------------------------------
		// ⍟ IMAGE CONSTRUCTION
		//----------------------------------------------------------------
		// IMAGE: Image
		//--------------------------------
		private function image_image() {
			$sectionWidth 	= $this->section['section_width'];
			$contentType 	= $this->section['content_type'];
			$imageDisplay 	= $this->section['image_display'];
			$imageImage 	= $this->section['image_image'];
			$imageSize 		= $this->style_inner_width();
			$equalize 		= ( ($sectionWidth == 'two-third') || ($sectionWidth == 'full') ) ? ' data-equalizer-watch' : '';
			$titleOverlay 	= $this->section['title_overlay_display'];

			if($imageDisplay){
				// Custom Image
				if($contentType != 'article') {
					$hasImage = pantheon_display_post_field_image($imageImage, 'large', 'image', false);

					if($hasImage){
						echo '<div class="' . $imageSize['image'] . '"' . $equalize . '>';
						echo '<div class="image-bg"' . pantheon_display_post_field_image($imageImage, 'large', 'style', false) . '>';
						if(!$equalize && $titleOverlay){
							$this->content_title();
						}
						else {
							echo '&nbsp;';
						}
						echo '</div>';
						echo '</div>';
					}
				}
				// Article Featured Image
				elseif($contentType == 'article') {
					$contentArticle	= $this->section['content_article'];

					// Setup the post object to get the info
					setup_postdata( $contentArticle ); 
					$hasImage = has_post_thumbnail($contentArticle->ID);
					if($hasImage){
						echo '<div class="' . $imageSize['image'] . '"' . $equalize . '>';
						echo '<div class="image-bg"' . pantheon_display_post_featured_image($contentArticle->ID, 'large', false, 'style') . '>';
						if(!$equalize && $titleOverlay){
							$this->content_title();
						}
						else {
							echo '&nbsp;';
						}
						echo '</div>';
						echo '</div>';
					}
					wp_reset_postdata();
				}
			}
			// Nanimo...
			else { return false; }
		}


		//----------------------------------------------------------------
		// ⍟ CTA CONSTRUCTION
		//----------------------------------------------------------------
		// CTA: Button
		//--------------------------------
		private function cta_btn() {
			$contentType 	= $this->section['content_type'];
			$ctaDisplay 	= $this->section['cta_display'];
			$ctaType 		= $this->section['cta_display_type'];

			// Custom CTA
			if ( ($contentType != 'article') && $ctaDisplay && ($ctaType == 'button') ) {
				// Get all the CTA settings
				$ctaTitle 		= $this->section['cta_text'];
				$ctaLinkType	= $this->section['cta_type'];
				$ctaLink 		= ($ctaLinkType == 'internal') ? $this->section['cta_internal'] : $this->section['cta_external'];
				$ctaBlank 		= ($this->section['cta_blank']) ? ' target="_blank"' : '';
				$ctaColor 		= $this->section['cta_color'];
				$ctaBorder 		= $this->section['cta_border'];
				$ctaColorHover	= $this->section['cta_hover'];
				$ctaBorderHover	= $this->section['cta_hover_border'];
						
				// We need to build the class for this button. Wowie.
				$ctaClass 		= 'button';
				if($ctaColor != 'default') $ctaClass .= ' ' . $ctaColor;
				if( ($ctaBorder != 'default') ) $ctaClass .= ' ' . $ctaBorder;
				if($ctaColorHover != 'default') $ctaClass .= ' ' . $ctaColorHover;
				if( ($ctaBorderHover != 'default') && ($ctaBorder != 'no-border') ) $ctaClass .= ' ' . $ctaBorderHover;

				// Output the link
				echo '<a href="' . $ctaLink . '" class="button-customize ' . $ctaClass . '"' . $ctaBlank . '>' . $ctaTitle . '</a>';
			}
			// Nanimo...
			else { return false; }
		}

		// CTA: Box
		//--------------------------------
		private function cta_box() {
			$contentType 	= $this->section['content_type'];
			$ctaDisplay 	= $this->section['cta_display'];
			$ctaType 		= $this->section['cta_display_type'];

			// Custom CTA
			if ( ($contentType != 'article') && $ctaDisplay && ($ctaType == 'box') ) {
				// Get all the CTA settings
				$ctaWidth 		= $this->style_inner_width();
				$ctaTitle 		= $this->section['cta_text'];
				$ctaLinkType	= $this->section['cta_type'];
				$ctaLink 		= ($ctaLinkType == 'internal') ? $this->section['cta_internal'] : $this->section['cta_external'];
				$ctaBlank 		= ($this->section['cta_blank']) ? ' target="_blank"' : '';
				$ctaBGColor 	= $this->section['cta_box_bgcolor'];
				$ctaSide 		= $this->section['cta_box_side'];
				$ctaContent 	= $this->section['cta_box_content'];
				$ctaColor 		= $this->section['cta_color'];
				$ctaBorder 		= $this->section['cta_border'];
				$ctaColorHover	= $this->section['cta_hover'];
				$ctaBorderHover	= $this->section['cta_hover_border'];
						
				// We need to build the class for this button. Wowie.
				$ctaClass 		= 'button';
				if($ctaColor != 'default') $ctaClass .= ' ' . $ctaColor;
				if( ($ctaBorder != 'default') ) $ctaClass .= ' ' . $ctaBorder;
				if($ctaColorHover != 'default') $ctaClass .= ' ' . $ctaColorHover;
				if( ($ctaBorderHover != 'default') && ($ctaBorder != 'no-border') ) $ctaClass .= ' ' . $ctaBorderHover;

				// Output the link
				echo '<div class="cta-box ' . $ctaWidth['cta'] . '" data-equalizer-watch><div class="inner-bg ' . $ctaBGColor . '""><div class="padded">';
				echo $ctaContent;
				echo '<a href="' . $ctaLink . '" class="button-customize ' . $ctaClass . '"' . $ctaBlank . '>' . $ctaTitle . '</a>';
				echo '</div></div></div>';
			}
			// Nanimo...
			else { return false; }
		}


		//----------------------------------------------------------------
		// ⍟ STYLING CONSTRUCTION
		//----------------------------------------------------------------
		// STYLING: Background Color
		//--------------------------------
		private function style_bgcolor() {
			$bgChange 		= $this->section['bg_change'];
			$bgChangeType 	= $this->section['bg_change_type'];

			// Custom Color
			if($bgChange && ($bgChangeType == 'color') ) {
				return $this->section['bg_color'];
			}
			// Defaults to Primary
			else {
				return 'primary';
			}
		}

		// STYLING: Background Image
		//--------------------------------
		private function style_bgimage() {
			$bgChange 		= $this->section['bg_change'];
			$bgChangeType 	= $this->section['bg_change_type'];
			$bgImage 		= $this->section['bg_image'];

			// Get background image
			if($bgChange && ($bgChangeType == 'image') && $bgImage) {
				pantheon_display_post_field_image($this->section['bg_image'], 'large', 'style');
			}
			// Nanimo...
			else { return false; }
		}

		// STYLING: Background Overlay
		//--------------------------------
		private function style_overlay() {
			$bgChange 		= $this->section['bg_change'];
			$bgChangeType 	= $this->section['bg_change_type'];
			$bgOverlay 		= $this->section['bg_overlay'];

			// Get background image
			if($bgChange && ($bgChangeType == 'image') && $bgOverlay) {
				return $bgOverlay;
			}
			// Nanimo...
			else { return false; }
		}

		// STYLING: Background Overlay Padding
		//--------------------------------
		private function style_overlay_padding() {
			$bgChange 		= $this->section['bg_change'];
			$bgChangeType 	= $this->section['bg_change_type'];
			$bgPadding 		= $this->section['bg_pad'];

			// Get background image
			if( ($bgChange && ($bgChangeType == 'image') && $bgPadding) || ($bgChangeType != 'image') ) {
				return ' padded';
			}
			// Nanimo...
			else { return false; }
		}

		// STYLING: Section Width
		//--------------------------------
		private function style_section_width() {
			$sectionWidth = $this->section['section_width'];

			// Translate our sizes into grids
			switch ($sectionWidth) {
				case 'third': 		$columnSize = 'large-4'; break;
				case 'half': 		$columnSize = 'medium-6'; break;
				case 'two-third': 	$columnSize = 'large-8'; break;
				case 'full': 		$columnSize = 'small-12'; break;
				default:			$columnSize = 'medium-6'; break;
			}
			return $columnSize;
		}

		// STYLING: Inner Grid Sizes
		//--------------------------------
		private function style_inner_width() {
			// This function helps us build our grid sizes. We have a few things to consider: content, image, lists, and CTA boxes
			// All of these things have the chance to change sides and sizes, and need to be strong armed at certain sizes to make sure the sizes aren't crazy
			// So to keep things neat, the front end code is done as follows:
			// [ row ]
			// 		[ column: image ]
			// 		[ column: all content ]
			// 			[ row ]
			// 				[ column: text content ]
			//				[ column: cta (box) ]
			//				[ column: list content ]

			// Section Width
			$sectionWidth 		= $this->section['section_width'];
			$sectionSmall 		= ( ($sectionWidth == 'third') || ($sectionWidth == 'half') ) ? true : false;

			// Image Width
			$imageDisplay 		= $this->section['image_display'];
			$imageSize 			= $this->section['image_size'];
			$imageSide 			= $this->section['image_side'];

			// Inner grid variables
			$contentType 		= $this->section['content_type'];
			$ctaDisplay 		= $this->section['cta_display'];
			$ctaType 			= $this->section['cta_display_type'];
			$ctaSide 			= $this->section['cta_box_side'];
			$listSide 			= $this->section['content_list_side'];
			$hasListCTA 		= (($contentType == 'list') && $ctaDisplay && ($ctaType == 'box')) ? true : false;
			$hasList 			= (($contentType == 'list')) ? true : false;
			$hasCTA 			= ($ctaDisplay && ($ctaType == 'box')) ? true : false;

			// Class Array
			$classes 			= array();
			$classes['image'] 	= $classes['content'] = $classes['words'] = $classes['cta'] = $classes['list'] = ' columns';
			$classes['image'] 	.= ' section-image';
			$classes['content'] .= ' section-content';
			$classes['words'] 	.= ' section-words';
			$classes['cta'] 	.= ' section-cta';
			$classes['list'] 	.= ' section-list';

			// Translate our sizes into grids
			if($imageDisplay){
				switch ($imageSize) {
					case 'third': 		
						$classes['image'] 	.= ' medium-3';
						$classes['content'] .= ' medium-9 here';
						// We need to change the positioning if the image is on the right
						if($imageSide == 'right') {
							$classes['image'] 	.= ' medium-push-9';
							$classes['content'] .= ' medium-pull-3';
						}
						break;
					case 'fourth': 		
						$classes['image'] 	.= ' medium-4';
						$classes['content'] .= ' medium-8';
						// We need to change the positioning if the image is on the right
						if($imageSide == 'right') {
							$classes['image'] 	.= ' medium-push-8';
							$classes['content'] .= ' medium-pull-4';
						}
						break;
					case 'sixth': 		
						$classes['image'] 	.= ' medium-6'; 
						$classes['content'] .= ' medium-6';
						// We need to change the positioning if the image is on the right
						if($imageSide == 'right') {
							$classes['image'] 	.= ' medium-push-6';
							$classes['content'] .= ' medium-pull-6';
						}
						break;
					case 'eighth': 		
						$classes['image'] 	.= ' medium-8'; 
						$classes['content'] .= ' medium-4';
						// We need to change the positioning if the image is on the right
						if($imageSide == 'right') {
							$classes['image'] 	.= ' medium-push-4';
							$classes['content'] .= ' medium-pull-8';
						}
						break;
					case 'full': 		
						$classes['image'] 	.= ' small-12'; 
						$classes['content'] .= ' small-12';
						// We need to change the positioning if the image is on the right
						if($imageSide == 'right') {
							$classes['image'] 	.= ' small-push-12';
							$classes['content'] .= ' small-pull-12';
						}
						break;
				}
			}
			else {
				$classes['content'] .= ' small-12';
			}

			// Content Section Sizes
			if( ($imageSize == 'third') || ($imageSize == 'fourth') ){
				// Change our inner grids depending on what we have going on
				if($hasListCTA){
					if( ($listSide != 'full') && ($ctaSide != 'full') ) {
						$classes['words']	.= ' large-6';
						$classes['cta']		.= ' large-3';
						$classes['list']	.= ' large-3';
						// Default is that both the list and the cta are to the right of the words,
						// so we only need to check if these elements are moving to the left
						if( ($listSide == 'left') && ($ctaSide == 'left') ){
							$classes['words'] 	.= ' large-push-6';
							$classes['cta'] 	.= ' large-pull-6';
							$classes['list'] 	.= ' large-pull-6';
						}
						elseif( ($listSide == 'right') && ($ctaSide == 'left') ) {
							$classes['words'] 	.= ' large-push-3';
							$classes['cta'] 	.= ' large-pull-9';
							$classes['list'] 	.= ' large-push-3';
						}
						elseif( ($listSide == 'left') && ($ctaSide == 'right') ) {
							$classes['words'] 	.= ' large-push-3';
							$classes['list'] 	.= ' large-pull-6';
						}
					}
				}
				elseif($hasList){
					if($listSide != 'full') {
						$classes['words']	.= ' large-8';
						$classes['list']	.= ' large-4';
						if($listSide == 'left') {
							$classes['words'] 	.= ' large-push-4';
							$classes['list'] 	.= ' large-pull-8';
						}
					}
				}
				elseif($hasCTA){
					if($listSide != 'full') {
						$classes['words']	.= ' large-8';
						$classes['cta']		.= ' large-4';
						if($ctaSide == 'left') {
							$classes['words'] 	.= ' large-push-4';
							$classes['cta'] 	.= ' large-pull-8';
						}
					}
				}
			}

			// Small Section Sizes
			if($sectionSmall) {
				// We don't care about sizes or sides for all these details on the smaller blocks
				$classes['image'] = $classes['content'] = $classes['cta'] = $classes['list'] = '';
			}

			return $classes;
		}


		//----------------------------------------------------------------
		// ⍟ MARK-UP CONSTRUCTION
		//----------------------------------------------------------------
		// SECTION: Output
		//--------------------------------
		public function section_construct($index){
			global $post; 
			// Check if this section will shift up (it'll do it only if it's one of the first three blocks)
			$sectionShifter 	= get_field('section_shift');
			$sectionShift 		= ( is_array($sectionShifter) && in_array($index, $sectionShifter) ) ? ' shift-up' : '';

			// Section width variables
			$sectionWidth 		= $this->section['section_width'];
			$sectionSmall 		= ( ($sectionWidth == 'third') || ($sectionWidth == 'half') ) ? true : false;

			// Section styling variables
			$sectionCenter 		= $this->section['content_center'] ? ' text-center' : '';
			$sectionBlockClass 	= 'section-block columns ' . $this->style_section_width() . $sectionShift . $sectionCenter;
			$sectionImpClass 	= ($this->section['content_type'] == 'important') ? ' important-' . $this->section['content_highlight'] : '';
			$sectionInnerClass 	= 'inside ' . $this->style_bgcolor() . $sectionImpClass;

			// Overlay Container
			$overlayOpen 		= ($this->style_overlay()) ? '<div class="overlay-' . $this->style_overlay() . '"><div class="inner">' : '';
			$overlayClose 		= ($this->style_overlay()) ? '</div></div>' : '';
			$overlayOpenLg 		= ($this->style_overlay()) ? '<div class="' . $this->style_overlay_padding() . '"><div class="overlay-' . $this->style_overlay() . '"><div class="inner">' : '';
			$overlayCloseLg		= ($this->style_overlay()) ? '</div></div></div>' : '';
?>

			<div class="<?= $sectionBlockClass; ?>" id="section-<?= $index; ?>" data-equalizer-watch>

				<?php 
					// SMALL SECTION
					//--------------------------------
					if($sectionSmall):
						$titleOverlay 	= $this->section['title_overlay_display'];
				?>
						<?php $this->image_image(); ?>

						<div class="<?= $sectionInnerClass; ?>" <?php $this->style_bgimage(); ?>>
							<div class="padded clearfix">
								<?= $overlayOpen; ?>
								<?php
									if(!$titleOverlay){
										$this->content_title();
									}
									$this->content_content();
									$this->cta_btn();
								?>
								<?= $overlayClose; ?>
							</div>
							<?php $this->content_list(); ?>
						</div>

				<?php
					// LARGE SECTION
					//--------------------------------
					else:
						$gridClass = $this->style_inner_width();
				?>

					<div class="section-larger <?= $sectionInnerClass . $this->style_overlay_padding(); ?>" <?php $this->style_bgimage(); ?>>

							<?= $overlayOpenLg; ?>
		
								<div class="row" data-equalizer>
					
									<?php $this->image_image(); ?>

									<div class="<?= $gridClass['content']; ?>" data-equalizer-watch>
										<div class="row" data-equalizer>
											<?php
												$this->content_words();
												$this->content_list();
												$this->cta_box();
											?>
										</div>
									</div>

								</div>

							<?= $overlayCloseLg; ?>
					</div>
				<?php endif; ?>
			</div>
<?php
		}

	}