<?php
	/*===========================================================================
	INCLUDE THE INCLUDES
	===========================================================================*/
	// Advanced Custom Fields
	include('includes/advanced-custom-fields/theme-options.php');
	include('includes/advanced-custom-fields/homepage-slider.php');
	include('includes/advanced-custom-fields/page-settings.php');
	include('includes/advanced-custom-fields/sections.php');

	include('includes/hephaestus_class_section_build.php');
	include('includes/function_rowing.php');



	/*===========================================================================
	ADD THEME SUPPORTS
	===========================================================================*/
	add_theme_support('post-thumbnails');
	add_theme_support('menus');



	/*===========================================================================
	CUSTOM THUMBNAIL SIZES
	===========================================================================*/
	add_image_size('TTG Featured Image', 1024, 500, true);
	add_image_size('TTG Gallery Normal Display', 637, 9999, false);
	add_image_size('TTG Medium Thumbnail', 350, 350, true);



	/*===========================================================================
	REGISTER MENUS
	===========================================================================*/
	if(!function_exists('ttg_menus')){
		function ttg_menus() {
			register_nav_menus(array(
				'menu-main' 		=> 'Main Menu',
				'menu-top' 			=> 'Top Menu',
				'menu-footer' 		=> 'Footer Menu'
			));
		}
		add_action('init', 'ttg_menus');
	}



	/*===========================================================================
	REGISTER SIDEBARS
	===========================================================================*/
	$sidebarGeneral = array(
		'name'          => 'General Sidebar',
		'id'            => 'sidebar-general',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	);
	register_sidebar($sidebarGeneral);
	$sidebarBlog = array(
		'name'          => 'Blog Sidebar',
		'id'            => 'sidebar-blog',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	);
	register_sidebar($sidebarBlog);



	/*===========================================================================
	ENQUEUE SCRIPTS AND STYLES
	===========================================================================*/
	// JAVASCRIPTS
	//---------------------------------------------------------------------------
	function ttg_scripts(){
		if(!is_admin()){
			// Plugins
			if(is_page_template('page-home.php')){
				wp_enqueue_script('slick', get_template_directory_uri() . '/javascripts/plugins/slick.min.js', array('jquery'), '', false);
			}
			wp_enqueue_script('responsive-navigation', get_template_directory_uri() . '/javascripts/responsive-navigation.js', array('jquery'), '', true);

			// Hephaestus Scripts
			wp_enqueue_script('hephaestus', get_template_directory_uri() . '/javascripts/hephaestus.js', array('jquery', 'foundation', 'responsive-navigation'), '', true);
		}
	}
	add_action('wp_enqueue_scripts', 'ttg_scripts');

	// STYLESHEETS
	//---------------------------------------------------------------------------
	function ttg_styles(){
		if(!is_admin()){
			// Hephaestus Stylesheets
			wp_enqueue_style('hephaestus', get_template_directory_uri() . '/stylesheets/css/style.css');
		}
	}
	add_action('wp_enqueue_scripts', 'ttg_styles');

	function ttg_styles_admin() {
		if(is_admin()){
			wp_enqueue_style('custom-admin', get_template_directory_uri() . '/stylesheets/css/admin.css');
		}
	}
	add_action('admin_enqueue_scripts', 'ttg_styles_admin');

	// RECOMPILES THE CSS ONLY WHEN THE OPTIONS PAGE IS SAVED
	//---------------------------------------------------------------------------
	function ttg_compile_css( $post_id ) {
		$page = htmlspecialchars($_GET["page"]);
		if($page == 'acf-options-theme-styling'){
			// Compile our CSS
			include(get_template_directory() . '/stylesheets/php-sass-watcher.php');
		}
	}
	// run after ACF saves the $_POST['fields'] data
	add_action('acf/save_post', 'ttg_compile_css', 20);


	/*===========================================================================
	ADDITIONAL FUNCTIONS
	===========================================================================*/	
	// REMOVE "PROTECTED" FROM PASSWORD PAGE'S TITLES
	//---------------------------------------------------------------------------
	add_filter('protected_title_format', 'blank');
	function blank($title) {
		return '%s';
	}

