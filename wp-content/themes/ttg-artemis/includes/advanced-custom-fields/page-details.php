<?php
	/*===========================================================================
	ARTEMIS: PAGE DETAILS
	===========================================================================*/
	// Page details, including settings for Call Out band and Location Band
	
	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_artemis-page-page-details',
			'title' => '[Artemis] Page: Page Details',
			'fields' => array (
				array (
					'key' => 'field_531a49826e302',
					'label' => 'Separator',
					'name' => '',
					'type' => 'message',
					'message' => '<hr />',
				),
				array (
					'key' => 'field_531a48f16e2fe',
					'label' => 'Call Out Band',
					'name' => '',
					'type' => 'tab',
				),
				array (
					'key' => 'field_5314dd82ba144',
					'label' => 'How to: Change Call Out Band',
					'name' => '',
					'type' => 'message',
					'message' => '<h4>Change the appearance and content of the band that is displayed toward the bottom of your website. Global settings for this can be set under <em>Options → Theme Styling</em>.</h4>',
				),
				array (
					'key' => 'field_5314de1cba145',
					'label' => 'Display call out band on this page?',
					'name' => 'cta_page_display',
					'type' => 'radio',
					'choices' => array (
						'default' => 'Yes, display the default text set under <strong>Options -> Styling: Theme</strong>',
						'yes' => 'Yes, but I want to display different content for this page',
						'no' => 'No, please remove it from this page',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => 'default',
					'layout' => 'vertical',
				),
				array (
					'key' => 'field_5314dd70ba143',
					'label' => 'Title',
					'name' => 'cta_page_title',
					'type' => 'text',
					'conditional_logic' => array (
						'status' => 1,
						'rules' => array (
							array (
								'field' => 'field_5314de1cba145',
								'operator' => '==',
								'value' => 'yes',
							),
						),
						'allorany' => 'all',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5314de5cba146',
					'label' => 'Content',
					'name' => 'cta_page_content',
					'type' => 'wysiwyg',
					'conditional_logic' => array (
						'status' => 1,
						'rules' => array (
							array (
								'field' => 'field_5314de1cba145',
								'operator' => '==',
								'value' => 'yes',
							),
						),
						'allorany' => 'all',
					),
					'default_value' => '',
					'toolbar' => 'full',
					'media_upload' => 'yes',
				),
				array (
					'key' => 'field_531a49006e2ff',
					'label' => 'Location Band',
					'name' => '',
					'type' => 'tab',
				),
				array (
					'key' => 'field_531a49506e301',
					'label' => 'Display location band on this page?',
					'name' => 'location_page_display',
					'type' => 'radio',
					'choices' => array (
						'yes' => 'Yes, display the default settings set under <strong>Options -> Styling: Theme</strong>',
						'no' => 'No, please remove it from this page',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => 'yes',
					'layout' => 'vertical',
				),
				array (
					'key' => 'field_531f2a5cf6b88',
					'label' => 'Featured Image',
					'name' => '',
					'type' => 'tab',
				),
				array (
					'key' => 'field_531f2a6bf6b89',
					'label' => 'How to: Change Call Out Band',
					'name' => '',
					'type' => 'message',
					'message' => '<h4>Choose how the featured image for this page should be displayed. Global settings for this can be set under <em>Options → Theme Styling</em>.</h4>',
				),
				array (
					'key' => 'field_531f2a71f6b8a',
					'label' => 'Display featured image for this page/post?',
					'name' => 'featured_image_display',
					'type' => 'radio',
					'choices' => array (
						'default' => 'Yes, display featured image for this page/post. (Defaults to featured image set in the Options page; overridden by the featured image set on this page)',
						'no' => 'No, please featured images from being displayed on this page.',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => 'default',
					'layout' => 'vertical',
				),
				array (
					'key' => 'field_533465f0d9a0d',
					'label' => 'Featured Image Background Color',
					'name' => 'featured_image_bgcolor',
					'type' => 'color_picker',
					'instructions' => 'Defaults to the background color of the header.',
					'default_value' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'post',
						'order_no' => 0,
						'group_no' => 1,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 5,
		));
	}
