<?php
	/*===========================================================================
	Template Name: Full Width
	=============================================================================
	Full width page with no sidebar
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>
	<div class="container">
		<div class="row">
			
			<div class="main columns">
				<?php 
					if(have_posts()):
						while(have_posts()): the_post();
							
							the_content();

						endwhile;
					endif;
				?>			
			</div><?php // /.content ?>

		</div>
	</div>

<?php
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>