<?php
	/*===========================================================================
	INDEX
	=============================================================================
	Default fallback style. Default page for blog viewing page
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>
	<div class="container primary">
		<div class="row">
			
			<div class="main medium-8 columns">
				<?php 
					if(have_posts()):
						while(have_posts()): the_post();
							
							the_content();

						endwhile;
					endif;
				?>			
			</div><?php // /.content ?>

			<?php
				// SIDEBAR //
				get_template_part('parts/shared/sidebar');
			?>

		</div>
	</div>

<?php
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>