<?php
	/*===========================================================================
	INDEX
	=============================================================================
	Default fallback style. Default page for blog viewing page
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>
	<div class="container primary">
		<div class="row">

			<?php 
				// POST LOOP //
				if($post->post_type == 'gallery'){
					get_template_part('parts/posts/loop', 'gallery');	
					get_template_part('parts/shared/pagination');
				}
				else {
					echo '<div class="main medium-8 columns">';
					get_template_part('parts/posts/loop');
					get_template_part('parts/shared/pagination');
					echo '</div>';
				}

				// SIDEBAR //
				get_template_part('parts/shared/sidebar');
			?>

		</div>
	</div>

<?php
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>