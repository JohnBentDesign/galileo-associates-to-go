<?php
	/*===========================================================================
	404
	=============================================================================
	Template automatically shown when a 404 error appears
	*/

	// Get 404 page content
	$fourohfourPage = get_field('404_content', 'options');
	$content = ($fourohfourPage[0]) ? apply_filters('the_content', get_post_field('post_content', $fourohfourPage[0])) : 'Sorry. It seems we can\'t find what you were looking for.';

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>
	<div class="container primary">
		<div class="row">

			<main class="the-page large-9 large-centered columns">
				<?php pantheon_display_post_featured_image($fourohfourPage[0], 'TTG Featured Image', false); ?>
				<?= $content; ?>
			</main>

		</div>
	</div>

<?php 
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>