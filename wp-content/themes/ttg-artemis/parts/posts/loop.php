<?php
	/*===========================================================================
	POST LOOP
	=============================================================================
	Blog post loop for single.php and index.php
	*/
	
	// Figure out where we are
	$listing = (is_home() || is_archive() || is_search()) ? true : false;
	if($listing){ $postOutput = ' excerpt'; }
	else if($listing == false){ $postOutput = ' full'; }

	// THE LOOP //
	if(have_posts()){

		// If it's the blog listing page, wrap our articles in a div
		if($listing){ echo '<div class="post-list">'; }

		// Start the loop
		while(have_posts()): the_post();
			if($listing){
				$title 		= '<h2><a href="' . get_permalink() . '">' . get_the_title() . '</a></h2>';
				$imageSize 	= 'thumbnail';
			}
			elseif($listing == false){
				$title 		= '<h1>' . get_the_title() . '</h1>';
			}
?>
			<article class="post<?= $postOutput; ?>">

				<?php // POST HEADER // ?>
				<header>
					<?= $title; ?>
					<?php if(get_post_type() == 'post'): ?>
						<time datetime="<?= get_the_date('Y-m-d'); ?>"><?= get_the_date('l, F j, Y'); ?></time> 
					<?php endif; ?>
				</header>

				<?php // POST CONTENT // ?>
				<div class="the-post row">

					<?php
						// Display featured image next to the excerpt if blog listing page 
						if($listing){ 
							echo '<div class="large-3 columns">';
							pantheon_display_post_featured_image($post->ID, $imageSize, get_permalink());
							echo '</div>';
						}
					?>
					<div class="content">
						<?php
							if($listing){
								$columnSize = (has_post_thumbnail($post->ID)) ? ' large-9' : '';

								echo '<div class="columns' . $columnSize . '">';
								the_excerpt();
								echo '</div>';
							}
							else {
								echo '<div class="columns">';
								the_content();
								echo '</div>';
							}
						?>
					</div>
				</div>

				<?php 
					// POST FOOTER //
					if(!is_search() && (get_post_type() != 'ai1ec_event')):
				?>
						<footer class="post">

							<div class="row">
								<div class="medium-6 columns">
									<?php
										// Display post comment links if blog listing page
										if($listing):
									?>
											<p class="comment-link"><a href="<?php comments_link(); ?>"><?php comments_number('No Comments', '1 Comment', '% Comments'); ?> &raquo;</a></p>
									<?php
										endif;

										// Display Post Categories if blog listing page
										pantheon_display_post_categories($post->ID);

										// Display Post Tags if blog listing page
										pantheon_display_post_tags($post->ID);
									?>
								</div>
								<div class="socialize medium-6 columns">
									<?php
										// ADD THIS BUTTONS //
										pantheon_display_social_share('blog', 'post', $post);
									?>
								</div>
							</div>

						</footer>
				<?php endif; ?>

			</article><?php // /.post ?>	

<?php 
			// COMMENTS //
			if( comments_open($post->ID) && ($listing == false) && (get_post_type() != 'ai1ec_event')){
				echo '<div class="comments">';
				comments_template( '', true );
				echo '</div>';
			}
		endwhile;

		// Close the blog listing div
		if($listing){ echo '</div>'; }
	}
	else{
		echo '<p>Sorry, but nothing matched your search criteria. Please try again with different keywords.</p>';
		get_template_part('searchform');
	}
?>