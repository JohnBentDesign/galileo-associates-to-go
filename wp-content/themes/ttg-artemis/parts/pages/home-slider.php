<?php
	/*===========================================================================
	HOMEPAGE: SLIDER
	===========================================================================*/
	// Homepage Slider
	
	$slideTransition 	= get_field('home_slideshow_transition');
	$slideSpeed 		= get_field('home_slideshow_speed');
	$slideTitle 		= (get_field('home_slideshow_title')) ? '<h1>' . get_field('home_slideshow_title') . '</h1>' : '';
	$slideContent 		= (get_field('home_slideshow_content')) ? '<p class="slide-content">' . get_field('home_slideshow_content') . '</p>' : '';
?>
		<div class="slider-info">
			<div class="row">
				<div class="slider-text-container large-6 columns">
					<div class="slider-text">
						<?= $slideTitle . $slideContent; ?>
					</div>
				</div>
			</div>
		</div>
		
		<div class="overlay"></div>
		
<?php
		echo '<ul class="homepage-slider" data-orbit data-options="animation: ' . $slideTransition . '; timer_speed: ' . $slideSpeed . '; pause_on_hover: false; navigation_arrows: false; bullets: false; slide_number: false;">';				

			while(have_rows('home_slideshow')): the_row();
				$imageObj = get_sub_field('slide_image');
				$imageURL = $imageObj['url'];
				$transition = get
?>
				<li class="orbit-item" style="background-image: url(<?= $imageURL; ?>)">
					<img src="<?= get_template_directory_uri(); ?>/images/spacer.png" alt="design element" height="1" width="1" />
				</li>
<?php
			endwhile;
		echo '</ul>';
?>