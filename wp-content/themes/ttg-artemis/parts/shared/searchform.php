<?php
	/*===========================================================================
	SEARCH FORM
	=============================================================================
	Search form to be used across the site
	*/
?>
	<form method="get" class="searchform clearfix" action="<?php bloginfo('home'); ?>/">
		<input type="text" size="put_a_size_here" name="s" id="s" value="" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;"/>
		<input type="submit" id="searchsubmit" value="&#xf1d9;" class="btn ion" />
	</form>