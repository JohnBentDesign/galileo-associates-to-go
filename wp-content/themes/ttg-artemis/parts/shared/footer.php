<?php
	/*===========================================================================
	FOOTER- MAIN
	===========================================================================*/
	// Contains the copyright and footer navigation

	// CTA //
	if(!is_front_page()){
		get_template_part('parts/shared/cta');
	}

	// LOCATION //
	get_template_part('parts/shared/location');
?>	
	<footer class="main quaternary">

		<?php
			//===================================
			// UPPER
			//===================================
		?>
		<div class="row upper">

			<?php 			
				// LOGO
				//===================================
			?>
			<div class="logo-footer medium-5 columns">
				<a href="<?php bloginfo('home'); ?>" class="logo">
					<?php 
						if(get_field('footer_logo', 'options')){
							$logoObj 	= get_field('footer_logo', 'options');
							pantheon_display_post_field_image($logoObj, 'TTG Logo');
						}
						else {
							bloginfo('title');
						}
					?>
				</a>
			</div>
		

			<?php
				// SOCIAL ICONS
				//===================================
			?>
			<div class="medium-7 columns">
				<?php
					// Connect
					//-----------------------------------
					pantheon_display_social_connect('footer');

					// Share
					//-----------------------------------
					pantheon_display_social_share('footer', 'post', $post);
				?>
				
				<div class="copyright">
					<p>&copy;<?php echo date('Y') . ' '; bloginfo('name'); ?>.</p>
					<p>Site Powered by <a href="http://techtherapytogo.com/" target="_blank">Technology Therapy To Go</a>.</p>
				</div><?php // /.copyright ?>
			</div>

		</div><?php // /.upper ?>


		<?php
			//===================================
			// LOWER
			//===================================
		?>
		<div class="row lower">
			<div class="columns">
				<?php
					// FOOTER NAVIGATION
					//===================================
					$menuFooter = array(
						'theme_location'  => 'menu-footer',
						'menu'            => 'Footer Menu',
						'container'       => 'nav',
						'container_class' => 'footer',
						'menu_class'      => 'menu',
						'depth'           => 1,
						'fallback_cb'		=> ''
					);
					wp_nav_menu($menuFooter);
				?>
			</div>
		</div>

	</footer><?php // /footer.main ?>