<?php
/**
 * Class SassWatcher
 *
 * This simple tool compiles all .scss files in folder A to .css files (with exactly the same name) into folder B.
 * To keep things as minimal as possible, this tool compiles every X seconds, regardless of changes within the files.
 * This seems weird, but makes sense as checking for changes in the files is more CPU-extensive than simply
 * re-compiling them. SassWatcher uses scssphp, the best SASS compiler in PHP available.
 *
 * SassWatcher is not a standalone compiler, it's just a little method that uses the excellent scssphp compiler written
 * by Leaf Corcoran (https://twitter.com/moonscript), which can be found here: http://leafo.net/scssphp/ and adds
 * automatic interval-compiling to it.
 *
 * The currently supported version of SCSS syntax is 3.2.12, which is the latest one.
 * To avoid confusion: SASS is the name of the language itself, and also the "name" of the "first" version of the
 * syntax (which was quite different than CSS). Then SASS's syntax was changed to "SCSS", which is more like CSS, but
 * with awesome additional possibilities and features.
 *
 * The compiler uses the SCSS syntax, which is recommended and mostly used. The old SASS syntax is not supported.
 *
 * @see SASS Wikipedia: http://en.wikipedia.org/wiki/Sass_%28stylesheet_language%29
 * @see SASS Homepage: http://sass-lang.com/
 * @see scssphp, the used compiler (in PHP): http://leafo.net/scssphp/
 *
 * How to use this tool:
 *
 * 1. Edit $sass_watcher->watch( ... ); in the last line of this file and put your stuff in here, see the parameter
 *    list below.
 * 2. Make sure PHP can write into your CSS folder.
 * 3. Run the script:
 *    a) simple way, from browser, just enter the URL to scss-compiler.php: http://127.0.0.1/folder/php-sass-watcher.php
 *       The script will run forever, even if you close the browser window.
 *    b) PHPStorm users can run the script by right-clicking the file and selecting "Run php-sass-watcher.php".
 * 4. To stop the script, stop/restart your Apache/Nginx/etc. or press the red "stop process button in PHPStorm.
 *
 * The parameters:
 *
 *  1. relative path to your SCSS folder
 *  2. relative path to your CSS folder (make sure PHP has write-rights here)
 *  3. the compiling interval (in seconds)
 *  4. relative path to the scss.inc.php file, which is the main file of the SASS compiler used
 *     here. Download the script manually from http://leafo.net/scssphp/ or "require" it via Composer:
 *     "leafo/scssphp": "0.0.9"
 *  5. optional: how the .css output should look like. See http://leafo.net/scssphp/docs/#output_formatting for more.
 *
 * How the tool works:
 *
 * Every X seconds ALL files in the scss folder will be compiled to same-name .css files in the css folder.
 * The tool does not stop when a .scss file is broken, has syntax error or similar.
 * The tool does not compile when .scss file is broken, has syntax error or similar. It will only compile next time
 * when there's a valid scss file.
 */

/*==========================================================================
CUSTOM COLORS DEFINED BY USER
==========================================================================*/
$primaryColor 		= (get_field('color_primary', 'options')) 		? get_field('color_primary', 'options') 	: '#ffffff';
$secondaryColor 	= (get_field('color_secondary', 'options')) 	? get_field('color_secondary', 'options') 	: '#565656';
$tertiaryColor 		= (get_field('color_tertiary', 'options')) 		? get_field('color_tertiary', 'options') 	: '#00aeef';
$quaternaryColor 	= (get_field('color_quaternary', 'options'))	? get_field('color_quaternary', 'options') 	: '#691c33';
$quinaryColor 		= (get_field('color_quinary', 'options'))		? get_field('color_quinary', 'options') 	: '#470e1d';
$senaryColor 		= (get_field('color_senary', 'options'))		? get_field('color_senary', 'options') 		: '#55cbf7';

$user_colors = '
	@import "includes";

	// FUNCTIONS
	//-------------------------------------------------------------
	@function contrast-color($color, $dark: $contrasted-dark-default, $light: $contrasted-light-default, $threshold: $contrasted-lightness-threshold) {
		@return if(lightness($color) < $threshold, $light, $dark)
	}

	// VARIABLES
	//-------------------------------------------------------------
	// Raw
	$primary: ' . $primaryColor . ';
	$secondary: ' . $secondaryColor . ';
	$tertiary: ' . $tertiaryColor . ';
	$quaternary: ' . $quaternaryColor . ';
	$quinary: ' . $quinaryColor . ';
	$senary: ' . $senaryColor . ';

	// Lightened
	$tertiaryLight: contrast-color($tertiary, lighten($tertiary, 15%), darken($tertiary, 15%), 30%);
	$quinaryLight: contrast-color($quinary, lighten($quinary, 15%), darken($quinary, 15%), 30%);

	// Darkened
	$primaryDark: darken($primary, 15%);

	// PRIMARY
	//-------------------------------------------------------------
	.primary {
		background-color: $primary;
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, blockquote p {
			color: $secondary;
		}
		a { 
			color: $quaternary;
			&:hover { color: darken($quaternary, 15%); }
		}
		h1, h2, h3, h4, h5, h6 { border-color: $quinary; }
	}
	.callout h4.primary:hover { background-color: darken($primary, 10%); }


	// SECONDARY
	//-------------------------------------------------------------
	.secondary {
		background-color: $secondary;
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, blockquote p {
			color: $primary;
		}
		a {
			color: $primary;	
			&:hover { color: darken($primary, 15%);	}
		}
	}
	.callout h4.secondary:hover { background-color: darken($secondary, 10%); }


	// TERTIARY
	//-------------------------------------------------------------
	.tertiary {
		background-color: $tertiary;
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, blockquote p {
			color: contrast-color($tertiary, lighten($tertiary, 50%), darken($tertiary, 50%), 30%);
		}
		&.lighter {
			background-color: $tertiaryLight;
		}
		a {
			color: contrast-color($tertiary, darken($tertiary, 25%), lighten($tertiary, 25%), 30%);	
			&:hover { color: contrast-color($tertiary, lighten($tertiary, 25%), darken($tertiary, 25%), 30%); }
		}
	}
	.callout h4.tertiary:hover { background-color: darken($tertiary, 10%); }


	// QUATERNARY
	//-------------------------------------------------------------
	.quaternary {
		background-color: $quaternary;
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, blockquote p {
			color: contrast-color($quaternary, lighten($quaternary, 50%), darken($quaternary, 50%), 30%);
		}
		a {
			color: contrast-color($quaternary, lighten($quaternary, 25%), darken($quaternary, 25%), 30%);	
			&:hover { color: contrast-color($quaternary, darken($quaternary, 25%), lighten($quaternary, 25%), 30%);	}
		}
	}
	.callout h4.quaternary:hover { background-color: darken($quaternary, 10%); }


	// QUINARY
	//-------------------------------------------------------------
	.quinary {
		background-color: $quinary;
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, blockquote p {
			color: contrast-color($quinary, lighten($quinary, 50%), darken($quinary, 50%), 30%);
		}
		a {
			color: contrast-color($quinary, darken($quinary, 25%), lighten($quinary, 25%), 30%);	
			&:hover {
				color: contrast-color($quinary, lighten($quinary, 25%), darken($quinary, 25%), 30%);	
			}
		}
	}
	.callout h4.quinary:hover { background-color: darken($quinary, 10%); }


	// SENARY
	//-------------------------------------------------------------
	.senary {
		background-color: $senary;
		h1, h2, h3, h4, h5, h6 { color: $quinary; }
		p, ul, ol, dt, dd, blockquote p { color: $secondary; }
		a {
			color: $quaternary;	
			&:hover { color: darken($quaternary, 15%); }
		}
	}
	.callout h4.senary:hover { background-color: darken($senary, 10%); }


	// CONTAINERS & SHARED STYLES
	//-------------------------------------------------------------
	body {
		background-color: $quinary;
	}
	.home .slider-text h2, h1 .title {
		background-color: $quinary;
		color: contrast-color($quinary, lighten($quinary, 50%), darken($quinary, 50%), 30%);
	}
	.breadcrumbs {
		span, a { color: $primary; }
	}
	a.button {
		background-color: $quinary;
		color: contrast-color($quinary, lighten($quinary, 50%), darken($quinary, 50%), 30%);
		&:hover {
			background-color: darken($quinary, 15%);
			color: contrast-color($quinary, lighten($quinary, 50%), darken($quinary, 50%), 30%); 
		}
	}


	// HEADER
	//-------------------------------------------------------------
	.announcement {
		background-color: darken($quaternary, 10%);
		border-color: contrast-color($quaternary, lighten($quaternary, 20%), darken($quaternary, 20%), 30%);
		color: contrast-color($quaternary, lighten($quaternary, 50%), darken($quaternary, 50%), 30%);
	}
	.info-band {
		.location .end-cap { border-right-color: $tertiaryLight; }	
		.phone .front-cap, .phone .end-cap { border-right-color: $quaternary; }
		.searchform {
			input[type=text]{
				background-color: rgba($primary, .65);
				color: $quaternary;
			}
			input[type=submit]{
				background-color: $primary;
				color: $quaternary;
			}
		}
	}
	.menu-top li { color: $quaternary; }
	.navigation {
		background-color: rgba($quinary, .8);
		a { color: contrast-color($quinary, lighten($quinary, 50%), darken($quinary, 50%), 30%); }
		li:hover, li ul, li.current-menu-item {
			> a { color: contrast-color($quinary, darken($quinary, 15%), lighten($quinary, 15%), 30%);  }
			background-color: rgba($quinaryLight, .8);	
		}
		li ul a { color: contrast-color($quinary, darken($quinary, 15%), lighten($quinary, 15%), 30%); }
	}


	// FOOTER
	//-------------------------------------------------------------
	nav.footer {
		border-top-color: contrast-color($quaternary, lighten($quaternary, 50%), darken($quaternary, 50%), 30%);
	}


	// SIDEBAR
	//-------------------------------------------------------------
	.primary .widget-custom.has-bg {
		background-color: $tertiary;
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, blockquote p {
			color: contrast-color($tertiary, lighten($tertiary, 50%), darken($tertiary, 50%), 30%);
		}
		&.lighter {
			background-color: $tertiaryLight;
		}
		a {
			color: contrast-color($tertiary, darken($tertiary, 25%), lighten($tertiary, 25%), 30%);	
			&:hover { color: contrast-color($tertiary, lighten($tertiary, 25%), darken($tertiary, 25%), 30%); }
		}
		a.button {
			background-color: $primary;
			color: $tertiary;
			&:hover {
				background-color: darken($tertiary, 15%);
				color: contrast-color($tertiary, lighten($tertiary, 50%), darken($tertiary, 50%), 30%); 
			}
		}
	}


	// HOMEPAGE
	//-------------------------------------------------------------
	.callout .cta-link-outside {
		color: $quinary;
		&:hover { color: contrast-color($quinary, darken($quinary, 15%), lighten($quinary, 15%), 30%); }
	}
	.slider-text {
		p  { color: $primary; }
	}


	// PAGES
	//-------------------------------------------------------------
	.backdrop {
		background-color: $primaryDark;
		.overlay.left {
			background: -moz-linear-gradient(left, rgba($primaryDark,1) 0%, rgba($primaryDark,0) 52%, rgba($primaryDark,0) 100%);
			background: -webkit-gradient(left top, right top, color-stop(0%, rgba($primaryDark,1)), color-stop(52%, rgba($primaryDark,0)), color-stop(100%, rgba($primaryDark,0)));
			background: -webkit-linear-gradient(left, rgba($primaryDark,1) 0%, rgba($primaryDark,0) 52%, rgba($primaryDark,0) 100%);
			background: -o-linear-gradient(left, rgba($primaryDark,1) 0%, rgba($primaryDark,0) 52%, rgba($primaryDark,0) 100%);
			background: -ms-linear-gradient(left, rgba($primaryDark,1) 0%, rgba($primaryDark,0) 52%, rgba($primaryDark,0) 100%);
			background: linear-gradient(to right, rgba($primaryDark,1) 0%, rgba($primaryDark,0) 52%, rgba($primaryDark,0) 100%);
		}
		.overlay.right {
			background: -moz-linear-gradient(left, rgba($primaryDark,0) 0%, rgba($primaryDark,0) 52%, rgba($primaryDark,1) 100%);
			background: -webkit-gradient(left top, right top, color-stop(0%, rgba($primaryDark,0)), color-stop(52%, rgba($primaryDark,0)), color-stop(100%, rgba($primaryDark,1)));
			background: -webkit-linear-gradient(left, rgba($primaryDark,0) 0%, rgba($primaryDark,0) 52%, rgba($primaryDark,1) 100%);
			background: -o-linear-gradient(left, rgba($primaryDark,0) 0%, rgba($primaryDark,0) 52%, rgba($primaryDark,1) 100%);
			background: -ms-linear-gradient(left, rgba($primaryDark,0) 0%, rgba($primaryDark,0) 52%, rgba($primaryDark,1) 100%);
			background: linear-gradient(to right, rgba($primaryDark,0) 0%, rgba($primaryDark,0) 52%, rgba($primaryDark,1) 100%);
		}
	}


	// BLOG
	//-------------------------------------------------------------
	.post {
		time { color: $quinary; }
	}


	// SIDEBAR
	//-------------------------------------------------------------
	aside.sidebar {
		h4.widgettitle {
			background-color: $tertiary;
			color: contrast-color($tertiary, lighten($tertiary, 50%), darken($tertiary, 50%), 30%);
		}
	}
	@media only screen and (max-width: 40em) {
		aside.sidebar {
			background-color: darken($primary, 10%);
		}
	}

	// GRAVITY FORMS
	//-------------------------------------------------------------
	.container .gform_wrapper {
		input[type=submit] {
			background-color: $quinary;
			color: $primary;
			&:hover {
				background-color: lighten($quinary, 10%);
			}
		}
	}
';

$user_custom_font_style = pantheon_function_font_select(array(
		'header' 	=> 'header.main',
		'footer' 	=> 'footer.main',
		'sidebar' 	=> 'aside.sidebar',
		'widget' 	=> '.widget',
		'post' 		=> '.the-post',
		'page' 		=> 'article.main, div.main',
		'gallery' 	=> 'article.gallery',
		'product' 	=> 'li.product',
	)
);

$user_custom_text_style = pantheon_function_text_style(array(
		'header' 	=> 'header.main',
		'footer' 	=> 'footer.main',
		'sidebar' 	=> 'aside.sidebar',
		'widget' 	=> '.widget',
		'post' 		=> '.the-post',
		'page' 		=> 'article.main, div.main',
		'gallery' 	=> 'article.gallery',
		'product' 	=> 'li.product',
	)
);

$user_custom_styles = get_field('advanced_css', 'options');

/*==========================================================================*/

class SassWatcher
{
	/**
	 * Watches a folder for .scss files, compiles them every X seconds
	 * Re-compiling your .scss files every X seconds seems like "too much action" at first sight, but using a
	 * "has-this-file-changed?"-check uses more CPU power than simply re-compiling them permanently :)
	 * Beside that, we are only compiling .scss in development, for production we deploy .css, so we don't care.
	 *
	 * @param string $scss_folder source folder where you have your .scss files
	 * @param string $css_folder destination folder where you want your .css files
	 * @param int $interval interval in seconds
	 * @param string $scssphp_script_path path where scss.inc.php (the scssphp script) is
	 * @param string $format_style CSS output format, ee http://leafo.net/scssphp/docs/#output_formatting for more.
	 */
	public function watch($scss_folder, $css_folder, $scssphp_script_path, $format_style = "scss_formatter", $user_defined_colors = null, $user_defined_custom_font = null, $user_defined_custom_text = null, $user_defined_styles = null)
	{
		// load the compiler script (scssphp), more here: http://www.leafo.net/scssphp
		require $scssphp_script_path;
		$scss_compiler = new scssc();

		// set the path to your to-be-imported mixins. please note: custom paths are coming up on future releases!
		$scss_compiler->setImportPaths($scss_folder);
		// set css formatting (normal, nested or minimized), @see http://leafo.net/scssphp/docs/#output_formatting
		$scss_compiler->setFormatter($format_style);

		// get all .scss files from scss folder
		$filelist = glob($scss_folder . "*.scss");

		// step through all .scss files in that folder
		foreach ($filelist as $file_path) {
			// get path elements from that file
			$file_path_elements = pathinfo($file_path);

			// get file's name without extension
			$file_name = $file_path_elements['filename'];

			// get .scss's content, put it into $string_sass
			$string_sass = file_get_contents($scss_folder . $file_name . ".scss");

			// Add our user defined colors to the compiled code
			$string_sass .= $user_defined_colors;

			// Add our user font styles to the compiled code
			$string_sass .= $user_defined_custom_font;

			// Add our user text styles to the compiled code
			$string_sass .= $user_defined_custom_text;

			// Add our user defined styles to the compiled code (so we can save it)
			$string_sass .= $user_defined_styles;

			// try/catch block to prevent script stopping when scss compiler throws an error
			try {
				// compile this SASS code to CSS
				$string_css = $scss_compiler->compile($string_sass);
				// write CSS into file with the same filename, but .css extension
				file_put_contents($css_folder . $file_name . ".css", $string_css);
			} catch (Exception $e) {
				echo 'There was an error in compiling your CSS. Please make sure there are no quotes, apostrophes, or deep nested pseudo classes.';
			}
		}
	}
}

$sass_watcher = new SassWatcher();
$sass_watcher->watch(get_template_directory() . '/stylesheets/sass/', get_template_directory() . '/stylesheets/css/', get_template_directory() . '/stylesheets/scss.inc.php', 'scss_formatter', $user_colors, $user_custom_font_style, $user_custom_text_style, $user_custom_styles);
