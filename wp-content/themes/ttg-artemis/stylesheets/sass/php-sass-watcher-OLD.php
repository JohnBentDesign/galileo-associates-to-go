<?php
/**
 * Class SassWatcher
 *
 * This simple tool compiles all .scss files in folder A to .css files (with exactly the same name) into folder B.
 * To keep things as minimal as possible, this tool compiles every X seconds, regardless of changes within the files.
 * This seems weird, but makes sense as checking for changes in the files is more CPU-extensive than simply
 * re-compiling them. SassWatcher uses scssphp, the best SASS compiler in PHP available.
 *
 * SassWatcher is not a standalone compiler, it's just a little method that uses the excellent scssphp compiler written
 * by Leaf Corcoran (https://twitter.com/moonscript), which can be found here: http://leafo.net/scssphp/ and adds
 * automatic interval-compiling to it.
 *
 * The currently supported version of SCSS syntax is 3.2.12, which is the latest one.
 * To avoid confusion: SASS is the name of the language itself, and also the "name" of the "first" version of the
 * syntax (which was quite different than CSS). Then SASS's syntax was changed to "SCSS", which is more like CSS, but
 * with awesome additional possibilities and features.
 *
 * The compiler uses the SCSS syntax, which is recommended and mostly used. The old SASS syntax is not supported.
 *
 * @see SASS Wikipedia: http://en.wikipedia.org/wiki/Sass_%28stylesheet_language%29
 * @see SASS Homepage: http://sass-lang.com/
 * @see scssphp, the used compiler (in PHP): http://leafo.net/scssphp/
 *
 * How to use this tool:
 *
 * 1. Edit $sass_watcher->watch( ... ); in the last line of this file and put your stuff in here, see the parameter
 *    list below.
 * 2. Make sure PHP can write into your CSS folder.
 * 3. Run the script:
 *    a) simple way, from browser, just enter the URL to scss-compiler.php: http://127.0.0.1/folder/php-sass-watcher.php
 *       The script will run forever, even if you close the browser window.
 *    b) PHPStorm users can run the script by right-clicking the file and selecting "Run php-sass-watcher.php".
 * 4. To stop the script, stop/restart your Apache/Nginx/etc. or press the red "stop process button in PHPStorm.
 *
 * The parameters:
 *
 *  1. relative path to your SCSS folder
 *  2. relative path to your CSS folder (make sure PHP has write-rights here)
 *  3. the compiling interval (in seconds)
 *  4. relative path to the scss.inc.php file, which is the main file of the SASS compiler used
 *     here. Download the script manually from http://leafo.net/scssphp/ or "require" it via Composer:
 *     "leafo/scssphp": "0.0.9"
 *  5. optional: how the .css output should look like. See http://leafo.net/scssphp/docs/#output_formatting for more.
 *
 * How the tool works:
 *
 * Every X seconds ALL files in the scss folder will be compiled to same-name .css files in the css folder.
 * The tool does not stop when a .scss file is broken, has syntax error or similar.
 * The tool does not compile when .scss file is broken, has syntax error or similar. It will only compile next time
 * when there's a valid scss file.
 */

// We need to be able to read our database and functions in Wordpress, so we'll find where wp-load.php is and load it
function find_wp_config_path() {
	$dir = dirname(__FILE__);
	do {
		if( file_exists($dir."/wp-config.php") ) {
			return $dir;
		}
	} while( $dir = realpath("$dir/..") );
	return null;
}
require_once( find_wp_config_path()  . '/wp-load.php' );

// In order to make this truely incredible, we need to check our colors so the theme can smartly change the color values
function check_color($hex, $modifier, $parentSame = null){
	//break up the color in its RGB components
	$hex 	= str_replace('#', '', $hex);
	$r 		= hexdec(substr($hex,0,2));
	$g 		= hexdec(substr($hex,2,2));
	$b 		= hexdec(substr($hex,4,2));

	// We're going to ignore some color values if they are too bright. It's making it difficult for our grays
	$r 		= ($r > 130) ? $r : 0;
	$g 		= ($g > 130) ? $g : 0;
	$b 		= ($b > 130) ? $b : 0;

	// Add it all together, weighting green because it can be tricky, and now we have our RGB value
	$rgb 	= $r + ($g * 1.8) + $b;

	if($parentSame){
		// If we're altering the color of something, where its parent container is the same color,
		// we need to override our logic and just make it darker or lighter
		if(($rgb > 0) && ($rgb > 0)) {
			return 'darken';
		}
		else if(($rgb == 0)) {
			return 'lighten';
		}
	}
	else if( ($modifier == 'lighten') && ( ($rgb > 0) && ($rgb <= 550) ) ){
		// Light Color being told to get lighter; Stop it
		return 'darken';
	}
	else if( ($modifier == 'darken') && ( ($rgb == 0) || ($rgb > 550) ) ){
		// Dark color being told to get darker; Stop it
		return 'lighten';
	}
	else {
		// This porridge is just right
		return $modifier;
	}
}



/*==========================================================================
CUSTOM COLORS DEFINED BY USER
==========================================================================*/
$primaryColor 		= (get_field('color_primary', 'options')) 		? get_field('color_primary', 'options') 	: '#ffffff';
$secondaryColor 	= (get_field('color_secondary', 'options')) 	? get_field('color_secondary', 'options') 	: '#565656';
$tertiaryColor 		= (get_field('color_tertiary', 'options')) 		? get_field('color_tertiary', 'options') 	: '#1e9ad8';
$quaternaryColor 	= (get_field('color_quaternary', 'options'))	? get_field('color_quaternary', 'options') 	: '#dd9933';


$user_colors = '
	@import "includes";

	// VARIABLES
	//-------------------------------------------------------------
	$primary: ' . $primaryColor . ';
	$secondary: ' . $secondaryColor . ';
	$tertiary: ' . $tertiaryColor . ';
	$quaternary: ' . $quaternaryColor . ';
	$quinary: ' . $quinaryColor . ';

	// Primary //
	header.main, .container, .search, #calendar_wrap table, .widget-cta .btn {
		background-color: ' . $primaryColor . ';
	}
	.info-band .phone .end-cap, .info-band .no-phone .location .end-cap {
		border-right-color: ' . $primaryColor . ';
	}
	footer.main .lower {
		border-top-color: ' . $primaryColor . ';
	}
	blockquote:before, blockquote:after, .cta h4, input[type=submit]:hover, .comment-reply-link:hover, h4.widgettitle,
	.callout:hover h4, .callout:hover h4 .cta-link,
	p.slide-content {
		color: ' . $primaryColor . ';
	}
	

	// Secondary //
	h4.widgettitle {
		background-color: ' . $secondaryColor . ';
	}
	footer.main, .breadcrumbs, blockquote, .comment-reply-link, #calendar_wrap thead, #calendar_wrap tfoot, 
	input[type=submit], .comment-reply-link, .callout h4 {
		background-color: ' . check_color($secondaryColor, 'lighten') . '(' . $secondaryColor . ', 50%);
	}
	.location-band {
		background-color: ' . check_color($secondaryColor, 'lighten') . '(' . $secondaryColor . ', 55%);
	}
	#calendar_wrap table tr.even, table tr.alt, table tr:nth-of-type(even), aside.sidebar {
		background-color: ' . check_color($secondaryColor, 'lighten') . '(' . $secondaryColor . ', 60%);
	}
	input, textarea, article.post, article.post footer, .children, aside.sidebar li, #calendar_wrap table, .location-map {
		border-color: ' . check_color($secondaryColor, 'lighten') . '(' . $secondaryColor . ', 50%);
	}
	body, p, footer.main nav a, footer.main .social-share a, footer.main .social-connect a, .breadcrumbs a, .breadcrumbs span,
	h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, blockquote p,
	.cta p, input, textarea, input[type=submit],
	.callout h4 .cta-link,
	.counter {
		color: ' . $secondaryColor . ';
	}
	footer.main p, footer.main span {
		color: ' . check_color($secondaryColor, 'lighten') . '(' . $secondaryColor . ', 20%);
	}
	footer.main .social-share a:hover, footer.main .social-connect a:hover {
		color: ' . check_color($secondaryColor, 'darken') . '(' . $secondaryColor . ', 5%);
	}
	.flex-direction-nav .flex-next {
		border-left: 30px solid ' . $secondaryColor . ';
	}
	.flex-direction-nav .flex-prev {
		border-right: 30px solid ' . $secondaryColor . ';
	}


	// Tertiary //
	html, body, .backdrop .half-band, .backdrop h1, .backdrop h1 .band, input[type=submit]:hover, .comment-reply-link:hover, .callout:hover h4 {
		background-color: ' . $tertiaryColor . ';	
	}
	.navigation {
		background-color: rgba(' . $tertiaryColor . ', .90);
	}
	nav.main ul.menu > li:hover, nav.main ul.menu li.current-menu-item, nav.main ul.menu ul {
		background-color: rgba( ' . check_color($tertiaryColor, 'lighten') . '(' . $tertiaryColor . ', 10%), .90 );
	}
	nav.main ul.menu ul ul {
		background-color: rgba( ' . check_color($tertiaryColor, 'lighten') . '(' . $tertiaryColor . ', 15%), .90 );
	}
	nav.main ul.menu ul li:hover {
		background-color: rgba(' . $tertiaryColor . ', .90);
	}
	nav.main ul.menu ul li {
		border-bottom: 1px solid rgba(' . $tertiaryColor . ', .90);
	}
	div.main {
		h1, h2, h3 {
			border-left-color: ' . $tertiaryColor . ';
		}
	}
	a, time, .location-band h3.location-title, .counter .current,
	.gform_confirmation_wrapper .gform_confirmation_message {
		color: ' . $tertiaryColor . ';
	}
	a:hover, footer.main nav a:hover {
		color: ' . check_color($tertiaryColor, 'darken') . '(' . $tertiaryColor . ', 20%);
	}
	header.main nav a, .backdrop h1 {
		color: ' . check_color($tertiaryColor, 'darken') . '(' . $tertiaryColor . ', 45%);
	}
	header.main nav a:hover {
		color: ' . check_color($tertiaryColor, 'darken') . '(' . $tertiaryColor . ', 50%);
	}
	

	// Quaternary //
	.ribbon .phone, .cta, .widget-cta {
		background-color: ' . $quaternaryColor . ';
	}
	.info-band .location .end-cap {
		border-right-color: ' . $quaternaryColor . ';
	}
	.container .gform_wrapper li.gfield.gfield_error, .container .gform_wrapper li.gfield.gfield_error.gfield_contains_required.gfield_creditcard_warning {
		border-color: ' . $quaternaryColor . ';
	}
	.info-band .phone .front-cap {
		border-right-color: ' . $quaternaryColor . ';
	}
	.half-band, .ribbon .location, .cta-content {
		background-color: ' . check_color($quaternaryColor, 'lighten') . '(' . $quaternaryColor . ', 10%);
	}
	.container .gform_wrapper li.gfield.gfield_error, .container .gform_wrapper li.gfield.gfield_error.gfield_contains_required.gfield_creditcard_warning, .widget-cta .btn:hover {
		background-color: ' . check_color($quaternaryColor, 'darken') . '(' . $quaternaryColor . ', 10%);
	}
	.widget-cta .btn {
		color: ' . $quaternaryColor . ';
	}
	.ribbon p, .cta-content p, .container .gform_wrapper div.validation_error, .widget-cta p,
	.container .gform_wrapper .ginput_container + .gfield_description.validation_message, .container .gform_wrapper .gfield_error .gfield_label {
		color: ' . check_color($quaternaryColor, 'darken', true) . '(' . $quaternaryColor . ', 40%);
	}


	// Header Overlay gradient
	.backdrop {
		background-color: ' . $overlayBg . ';
	}	
	.backdrop .overlay.left {
		background: -moz-linear-gradient(left, rgba(' . $overlayBg . ',1) 0%, rgba(' . $overlayBg . ',0) 52%, rgba(' . $overlayBg . ',0) 100%);
		background: -webkit-gradient(left top, right top, color-stop(0%, rgba(' . $overlayBg . ',1)), color-stop(52%, rgba(' . $overlayBg . ',0)), color-stop(100%, rgba(' . $overlayBg . ',0)));
		background: -webkit-linear-gradient(left, rgba(' . $overlayBg . ',1) 0%, rgba(' . $overlayBg . ',0) 52%, rgba(' . $overlayBg . ',0) 100%);
		background: -o-linear-gradient(left, rgba(' . $overlayBg . ',1) 0%, rgba(' . $overlayBg . ',0) 52%, rgba(' . $overlayBg . ',0) 100%);
		background: -ms-linear-gradient(left, rgba(' . $overlayBg . ',1) 0%, rgba(' . $overlayBg . ',0) 52%, rgba(' . $overlayBg . ',0) 100%);
		background: linear-gradient(to right, rgba(' . $overlayBg . ',1) 0%, rgba(' . $overlayBg . ',0) 52%, rgba(' . $overlayBg . ',0) 100%);
	}
	.backdrop .overlay.right {
		background: -moz-linear-gradient(left, rgba(' . $overlayBg . ',0) 0%, rgba(' . $overlayBg . ',0) 52%, rgba(' . $overlayBg . ',1) 100%);
		background: -webkit-gradient(left top, right top, color-stop(0%, rgba(' . $overlayBg . ',0)), color-stop(52%, rgba(' . $overlayBg . ',0)), color-stop(100%, rgba(' . $overlayBg . ',1)));
		background: -webkit-linear-gradient(left, rgba(' . $overlayBg . ',0) 0%, rgba(' . $overlayBg . ',0) 52%, rgba(' . $overlayBg . ',1) 100%);
		background: -o-linear-gradient(left, rgba(' . $overlayBg . ',0) 0%, rgba(' . $overlayBg . ',0) 52%, rgba(' . $overlayBg . ',1) 100%);
		background: -ms-linear-gradient(left, rgba(' . $overlayBg . ',0) 0%, rgba(' . $overlayBg . ',0) 52%, rgba(' . $overlayBg . ',1) 100%);
		background: linear-gradient(to right, rgba(' . $overlayBg . ',0) 0%, rgba(' . $overlayBg . ',0) 52%, rgba(' . $overlayBg . ',1) 100%);
	}
';
/*==========================================================================*/

class SassWatcher
{
	/**
	 * Watches a folder for .scss files, compiles them every X seconds
	 * Re-compiling your .scss files every X seconds seems like "too much action" at first sight, but using a
	 * "has-this-file-changed?"-check uses more CPU power than simply re-compiling them permanently :)
	 * Beside that, we are only compiling .scss in development, for production we deploy .css, so we don't care.
	 *
	 * @param string $scss_folder source folder where you have your .scss files
	 * @param string $css_folder destination folder where you want your .css files
	 * @param int $interval interval in seconds
	 * @param string $scssphp_script_path path where scss.inc.php (the scssphp script) is
	 * @param string $format_style CSS output format, ee http://leafo.net/scssphp/docs/#output_formatting for more.
	 */
	public function watch($scss_folder, $css_folder, $scssphp_script_path, $format_style = "scss_formatter", $user_defined_colors = null)
	{
		// load the compiler script (scssphp), more here: http://www.leafo.net/scssphp
		require $scssphp_script_path;
		$scss_compiler = new scssc();

		// set the path to your to-be-imported mixins. please note: custom paths are coming up on future releases!
		$scss_compiler->setImportPaths($scss_folder);
		// set css formatting (normal, nested or minimized), @see http://leafo.net/scssphp/docs/#output_formatting
		$scss_compiler->setFormatter($format_style);

		// get all .scss files from scss folder
		$filelist = glob($scss_folder . "*.scss");

		// step through all .scss files in that folder
		foreach ($filelist as $file_path) {
			// get path elements from that file
			$file_path_elements = pathinfo($file_path);

			// get file's name without extension
			$file_name = $file_path_elements['filename'];

			// get .scss's content, put it into $string_sass
			$string_sass = file_get_contents($scss_folder . $file_name . ".scss");

			// Add our user defined colors to the compiled code
			$string_sass .= $user_defined_colors;

			// try/catch block to prevent script stopping when scss compiler throws an error
			try {
				// compile this SASS code to CSS
				$string_css = $scss_compiler->compile($string_sass);
				// write CSS into file with the same filename, but .css extension
				file_put_contents($css_folder . $file_name . ".css", $string_css);
			} catch (Exception $e) {
				// here we could put the exception message, but who cares ...
				echo 'Gary was here; Ash is a loser!';
			}
		}
	}
}

$sass_watcher = new SassWatcher();
$sass_watcher->watch(get_template_directory() . '/stylesheets/sass/', get_template_directory() . '/stylesheets/css/', get_template_directory() . '/stylesheets/scss.inc.php', 'scss_formatter', $user_colors);
