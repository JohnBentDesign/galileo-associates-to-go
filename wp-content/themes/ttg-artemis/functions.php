<?php
	/*===========================================================================
	INCLUDE THE INCLUDES
	===========================================================================*/
	// Advanced Custom Fields
	include('includes/advanced-custom-fields/theme-styling.php');
	include('includes/advanced-custom-fields/page-details.php');
	include('includes/advanced-custom-fields/page-home.php');
	


	/*===========================================================================
	ADD THEME SUPPORTS
	===========================================================================*/
	add_theme_support('post-thumbnails');
	add_theme_support('menus');



	/*===========================================================================
	CUSTOM THUMBNAIL SIZES
	===========================================================================*/
	add_image_size('TTG Logo', 500, 125, false);
	add_image_size('TTG Featured Image', 1024, 500, true);
	add_image_size('TTG Gallery Normal Display', 637, 9999, false);
	add_image_size('TTG Medium Thumbnail', 350, 350, true);



	/*===========================================================================
	REGISTER MENUS
	===========================================================================*/
	if(!function_exists('ttg_menus')){
		function ttg_menus() {
			register_nav_menus(array(
				'menu-main' 		=> 'Main Menu',
				'menu-top' 			=> 'Header Top Menu',
				'menu-footer' 		=> 'Footer Menu'
			));
		}
		add_action('init', 'ttg_menus');
	}



	/*===========================================================================
	REGISTER SIDEBARS
	===========================================================================*/
	$sidebarBlog = array(
		'name'          => 'Blog Sidebar',
		'id'            => 'sidebar-blog',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>'
	);
	register_sidebar($sidebarBlog);

	$sidebarGeneral = array(
		'name'          => 'General Sidebar',
		'id'            => 'sidebar-general',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>'
	);
	register_sidebar($sidebarGeneral);



	/*===========================================================================
	ENQUEUE SCRIPTS AND STYLES
	===========================================================================*/
	// JAVASCRIPTS
	//---------------------------------------------------------------------------
	function ttg_scripts(){
		if(!is_admin()){
			// Plugins
			wp_enqueue_script('tinynav', get_template_directory_uri() . '/javascripts/tinynav.min.js', array('jquery'), '', true);
			wp_enqueue_script('slick', get_template_directory_uri() . '/javascripts/slick.min.js', array('jquery'), '', true);

			// Artemis Scripts
			wp_enqueue_script('artemis', get_template_directory_uri() . '/javascripts/artemis.js', array('jquery', 'foundation', 'tinynav'), '', true);
		}
	}
	add_action('wp_enqueue_scripts', 'ttg_scripts');

	// STYLESHEETS
	//---------------------------------------------------------------------------
	function ttg_styles(){
		if(!is_admin()){
			// Artemis Styles
			wp_enqueue_style('artemis', get_template_directory_uri() . '/stylesheets/css/style.css');
			wp_enqueue_style('responsive', get_template_directory_uri() . '/stylesheets/css/responsive.css');
		}
	}
	add_action('wp_enqueue_scripts', 'ttg_styles');

	// RECOMPILES THE CSS ONLY WHEN THE OPTIONS PAGE IS SAVED
	//---------------------------------------------------------------------------
	function ttg_compile_css( $post_id ) {
		$page = htmlspecialchars($_GET["page"]);
		if($page == 'acf-options-theme-styling'){
			// Compile our CSS
			include(get_template_directory() . '/stylesheets/php-sass-watcher.php');
		}
	}
	// run after ACF saves the $_POST['fields'] data
	add_action('acf/save_post', 'ttg_compile_css', 20);



	/*===========================================================================
	FIX SEARCH WIDGET TO USE SEARCHFORM.PHP
	===========================================================================*/
	function ttg_filter_searchform($args) {
		extract($args);
		echo $before_widget;
		include (TEMPLATEPATH . '/parts/shared/searchform.php');
		echo $after_widget;
	}
	$widgetOptions = array('classname' => 'widget_search', 'description' => __( "A search form for your blog"));
	wp_register_sidebar_widget('search',__('Search'),'ttg_filter_searchform',$widgetOptions);

