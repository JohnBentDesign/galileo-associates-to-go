<?php
namespace Pantheon\Hermes;
define( "THEME_NS", 'hermes' );


	/*===========================================================================
	INCLUDE THE INCLUDES
	===========================================================================*/
	$includes = get_dir_file_list( get_stylesheet_directory() . '/includes/functions' );

	foreach ( $includes as $include ) {
		include_once( $include );
	}

	function get_dir_file_list( $dir = __DIR__ ) {
		$result = array();

		$files = scandir( $dir );
		foreach ( $files as $file ) {
			if ( ! in_array( $file, array( ".", ".." ) ) ) {
				$file = $dir . DIRECTORY_SEPARATOR . $file;
				if ( is_dir( $file ) ) {
					$result = array_merge( $result, get_dir_file_list( $file ) );
				} else {
					$result[] = $file;
				}
			}
		}
		return $result;
	}

	/*===========================================================================
	SET ADVANCED CUSTOM FIELDS STORAGE
	===========================================================================*/
	function acf_json_save_point( $path ) {

		// update path
		$path = get_stylesheet_directory() . '/includes/advanced-custom-fields/';


		// return
		return $path;

	}
	add_filter('acf/settings/save_json', __NAMESPACE__ . '\acf_json_save_point');


	function acf_json_load_point( $paths ) {

		// append path
		$paths[] = get_stylesheet_directory() . '/includes/advanced-custom-fields/';


		// return
		return $paths;

	}
	add_filter('acf/settings/load_json', __NAMESPACE__ . '\acf_json_load_point');



	/*===========================================================================
	SET UP THEME
	===========================================================================*/
	function action__after_theme_setup() {



		/*===========================================================================
		ADD THEME SUPPORTS
		===========================================================================*/
		add_theme_support('post-thumbnails');
		add_theme_support('menus');



		/*===========================================================================
		CUSTOM THUMBNAIL SIZES
		===========================================================================*/
		add_image_size('TTG Featured Image', 1024, 500, true);
		add_image_size('Event Blocks', 450, 250, false);
		add_image_size('Event Blocks Large', 600, 300, false);



		/*===========================================================================
		REGISTER MENUS
		===========================================================================*/
		register_nav_menus(array(
			'menu-main' 		=> 'Main Menu',
			'menu-footer'		=> 'Footer Menu'
		));



		/*===========================================================================
		REQUIRE PANTHEON ADDITIONS
		===========================================================================*/
		do_action( 'pantheon/require_addition', 'event' );
	}

	add_action( 'after_setup_theme', __NAMESPACE__ . '\action__after_theme_setup' );



	/*===========================================================================
	REGISTER SIDEBARS - This theme doesn't use traditional sidebars, but here
	is how to do one for reference.
	===========================================================================*/
	function action__widgets_init() {
		register_sidebar( array(
			'name'          => 'Blog Sidebar',
			'id'            => 'sidebar-blog',
			'description'   => '',
			'class'         => '',
			'before_widget' => '<div class="widget">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widgettitle">',
			'after_title'   => '</h3>'
		) );
		register_sidebar( array(
			'name'          => 'Page Sidebar',
			'id'            => 'sidebar-page',
			'description'   => '',
			'class'         => '',
			'before_widget' => '<div class="widget">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widgettitle">',
			'after_title'   => '</h3>'
		) );
	}
	add_action( 'widgets_init', __NAMESPACE__ . '\action__widgets_init');



	/*===========================================================================
	ENQUEUE SCRIPTS AND STYLES
	===========================================================================*/
	function action__wp_enqueue_scripts() {
		// note: the wp_enqueue_scripts action only fires on the front end

		// set variable for readability and speed
		$dir = get_template_directory_uri();

		// Scripts
		wp_enqueue_script('requestAnimationFrame', $dir . '/javascripts/plugins/jquery.requestAnimationFrame.js', array('jquery'), '', true);
		wp_enqueue_script('mousewheel', $dir . '/javascripts/plugins/jquery.mousewheel.js', array('jquery'), '', true);
		wp_enqueue_script('ilightbox', $dir . '/javascripts/plugins/ilightbox.packed.js', array('jquery', 'mousewheel', 'requestAnimationFrame'), '', true);
		wp_enqueue_script('customselect', $dir . '/javascripts/plugins/customSelect.min.js', array('jquery'), '', true);
		wp_enqueue_script('tinynav', $dir . '/javascripts/plugins/tinynav.min.js', array('jquery'), '', true);
		wp_enqueue_script( THEME_NS, $dir . '/javascripts/' . THEME_NS . '.js', array('jquery', 'foundation', 'ilightbox', 'customselect', 'tinynav'), '', true);

		// Register Stylesheets
		wp_enqueue_style(  'ilightbox',       $dir . '/stylesheets/css/ilightbox.css');
		wp_enqueue_style(  THEME_NS,       $dir . '/stylesheets/css/style.css');

		// setup infinite scroll
		$do_infinite = get_field( 'do_infinite_scroll', 'options' );
		if ( $do_infinite && ( is_tax( 'pantheon_event_type' ) || is_post_type_archive( 'pantheon_event' ) ) ) {
			$taxonomy = get_queried_object();
			$taxonomy = isset( $taxonomy->slug ) ? $taxonomy->slug : 'all';
			do_action( 'pantheon/require_addition', 'infinite-scroll', array( 'event-type' => $taxonomy ) );
		}
	}
	add_action('wp_enqueue_scripts', __NAMESPACE__ . '\action__wp_enqueue_scripts');

	function per_page () {
		static $per_page = false;

		if ( false === $per_page ) {
			$per_page = get_field( 'events_per_page', 'options' ) ?: -1;
		}

		return $per_page;
	}
	add_filter( 'pantheon_event_archive_per_page', __NAMESPACE__ . '\per_page' );

	function action__pantheon_infinite_scroll_post () {
		get_template_part( 'parts/events/content', 'calendar' );
	}
	add_action( 'pantheon/infinite-scroll/post', __NAMESPACE__ . '\action__pantheon_infinite_scroll_post' );

	function action__wp_enqueue_scripts_admin() {
		if(is_admin()){
			wp_enqueue_style('custom-admin', get_template_directory_uri() . '/stylesheets/css/admin.css');
		}
	}
	add_action('admin_enqueue_scripts', __NAMESPACE__ . '\action__wp_enqueue_scripts_admin');


	// RECOMPILE THE CSS ONLY WHEN THE OPTIONS PAGE IS SAVED
	//---------------------------------------------------------------------------
	function ttg_compile_css( $post_id ) {
		$page = isset( $_GET["page"] ) ? htmlspecialchars( $_GET["page"] ) : '';
		if( $page == 'acf-options-theme-styling' ) {
			// Compile our CSS
			include( get_template_directory() . '/stylesheets/php-sass-watcher.php' );
		}
	}
	// run after ACF saves the $_POST['fields'] data
	add_action( 'acf/save_post', __NAMESPACE__ . '\ttg_compile_css', 20 );
