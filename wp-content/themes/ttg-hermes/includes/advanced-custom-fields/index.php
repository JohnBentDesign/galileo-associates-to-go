<?php
/*
 * This folder contains a new way of saving and retrieving Advanced Custom Fields (ACF) fields.
 *
 * All changes made with the UI will be saved here, but changes made in this folder or made by another user will not
 * show up in your UI. So after doing a git update you will need to (re)import any json files that you want to change
 * using the UI.
 *
 * Also all field groups will be saved here, including the ones that should be part of the Pantheon plugin. So if you
 * make changes to one of these, you will want to move them the plugin instead of the theme.
 */