<?php
namespace Pantheon\Hermes\Functions\Home;

function save_post( $post ) {
	static $run_once = false;

	// this is an expensive process don't do on autosave and only do it once per page load ever
	if ( $run_once || wp_is_post_autosave( $post ) || wp_is_post_revision( $post ) ) {
		return;
	}

	// call reset_hermes_theme_home_contents()
	reset_hermes_theme_home_contents();

	assign_homepage_widths();

	$run_once = true;
}
add_action( 'save_post', __NAMESPACE__ . '\save_post', 20 );

/**
 * Make sure we are updating the homepage at least once a day to adjust for events that are past. This runs on the first
 * visit to an admin page each day.
 */
function admin_init() {
	$option = get_option( 'pantheon-hermes-homepage-daily-reset', 'not set' );
	if ( current_time( 'Ymd' ) === $option ) {
		return;
	}

	reset_hermes_theme_home_contents();

	assign_homepage_widths();

	// checking for 'not set' allows us to use add_option() to make sure this autoloads.
	if ( 'not set' == $option ) {
		add_option ( 'pantheon-hermes-homepage-daily-reset', current_time( 'Ymd' ), '', 'yes' );
	} else {
		update_option( 'pantheon-hermes-homepage-daily-reset', current_time( 'Ymd' ) );
	}
}
add_action( 'admin_init', __NAMESPACE__ . '\admin_init' );

// called when posts are saved or related options are changed
function reset_hermes_theme_home_contents( $home_page_id = null ) {
	if ( null == $home_page_id ) {
		$home_pages = new \WP_Query(array(
			'post_type'  => 'page',
			'meta_key'   => '_wp_page_template',
			'meta_value' => 'page-home.php',
			'fields'     => 'ids'
		));

		if ( isset( $home_pages->posts[0] ) ) {
			foreach ( $home_pages->posts as $home_page ) {
				reset_hermes_theme_home_contents( $home_page );
			}
		}

		return;
	}

	$home_page = get_post( $home_page_id);

	// get number of events to show on home page
	$number_of_events = get_field( 'number_of_events', $home_page->ID );
	$found_events = array();

	if ( $number_of_events ) {
		// only put on homepage if specifically chosen
		$search_criteria = array( array(
			'key'     => 'display_on_home_page',
			'value'   => true,
			'fields'  => 'ids'
		) );

		// get all events with upcoming dates (could be start date, end date, performance date, etc.)
		$search_criteria[] = array(
			'value'   => current_time( 'mysql' ),
			'compare' => '>=',
			'cast'    => 'DATE'
		);

		$events = pantheon_acf_nested_field_query( array( 'agenda_items', 'date' ), $search_criteria, 'date' );

		// get only the nearest upcoming date for each event
		$events = array_map( 'min', $events );

		// sort events by their next upcoming date
		asort( $events );

		// get only published events up to the number specified in the homepage options
		$count = 0;
		foreach ( $events as $event_id => $date ) {
			if ( ($count <= $number_of_events) && ('publish' == get_post_status( $event_id )) && get_field('display_on_home_page', $event_id) ) {
				$found_event = get_homepage_post_content( $event_id );
				$found_events[$found_event['sort_priority']][] = $found_event;
				$count++;
			}
		}
		ksort( $found_events );
	}

	// get number of posts to show on home page
	$number_of_posts = get_field( 'number_of_posts', $home_page->ID );
	$found_posts = array();

	if ( $number_of_posts ) {
		// get most recent posts that have been selected for front page up to the number allowed
		$home_query = new \WP_Query( array(
			'posts_per_page' => $number_of_posts,
			'post_status'    => 'publish',
			'meta_query'     => array( array(
				'key'    => 'display_on_home_page',
				'value'  => true,
				'fields' => 'ids'
			) ),
		) );

		foreach ( $home_query->posts as $post ) {
			$found_post = get_homepage_post_content( $post );
			$found_posts[$found_post['sort_priority']][] = get_homepage_post_content( $post );
		}
		unset( $post );
		ksort( $found_posts );
	}

	// get curated content from homepage options
	$content = array();
	$rows = get_field( 'curated_content', $home_page->ID );
	if ( is_array( $rows ) ) {
		foreach( $rows as $row ) {
			$title       = isset( $row['title']             ) ? $row['title']             : '';
			$description = isset( $row['short_description'] ) ? $row['short_description'] : '';
			$size        = isset( $row['size']              ) ? $row['size']              : 'default';
			$image_size  = ( (int) $size > 5                ) ? 'Event Blocks Large'      : 'Event Blocks';
			$image = ( isset( $row['image'] ) ) ? pantheon_display_post_field_image( $row['image'], $image_size, 'image', false ) : '';

			if ( isset( $row['link_type'] ) && 'internal' == $row['link_type'] ) {
				$link_post = isset( $row['internal_link'][0] ) ? get_post( $row['internal_link'][0] ) : 0;
				$link = get_permalink( $link_post );
				$title = $title ?: $link_post->post_title;
				$image = $image ?: pantheon_display_post_featured_image( $link_post->ID, $image_size, null, 'image', false );
			} else {
				$link = isset( $row['external_link'] ) ? $row['external_link'] : '';
			}
			$content[] = array(
				'title'             => $title,
				'short_description' => $description,
				'images'            => $image,
				'link'              => $link,
				'size'              => $size,
				'class'             => '',
				'color'             => '',
			);
		}
	}

	// get sort order from homepage options
	$sort_order = get_field( 'sort_order', $home_page->ID );

	if ( 'priority' === $sort_order ) {
		// swapping dimensions puts all first priority content first with events before posts of the same
		// priority and sorted by date after that
		$content = swap_dimensions( array( array( $content ), $found_events, $found_posts ) );
		if ( isset( $content[0] ) ) {
			$content[] = $content[0];
			unset( $content[0] );
		}
	} else {
		// move events and posts without a sort priority - "0" is default - to the end before mixing in with the rest
		if ( isset( $found_events[0] ) ) {
			$found_events[] = $found_events[0];
			unset( $found_events[0] );
		}
		if ( isset( $found_posts[0] ) ) {
			$found_posts[] = $found_posts[0];
			unset( $found_posts[0] );
		}

		// add posts and events to $content in correct order
		switch ( $sort_order ) :
			case 'curated' :
				$content = array( $content, $found_events, $found_posts );
				break;
			case 'events' :
				$content = array( $found_events, $content, $found_posts );
				break;
			case 'posts' :
				$content = array( $found_posts, $found_events, $content );
				break;
			case 'alternating' :
				// swapping dimensions here puts the first of each type then the second, etc. Note that flattening each
				// array removes the priority info that was a separate dimension, but they stay in that order.
				$content = swap_dimensions( sort_by_size( array(
					flatten_array( $found_events,1 ),
					flatten_array( $found_posts, 1 ),
					$content
				) ) );
				break;
			case 'random' :
				$content = flatten_array( array( $content, $found_events, $found_posts ), -1 );
				shuffle( $content );
				break;
		endswitch;
	}

	$content = flatten_array( $content, -1 );

	update_option( 'hermes_theme_home_contents-' . $home_page->ID, $content );
	}

function get_homepage_post_content ( $post_id ) {
	// make sure we have a real post
	if ( ! $post = get_post( $post_id ) ) {
		return;
	}


	$options = (array) get_field( 'display_options', $post->ID );

	if ( in_array( 'show-description', $options ) ) {
		$description = get_field( 'short_description', $post->ID );
		if ( ! $description ) {
			setup_postdata( $post );
			$description = get_the_excerpt();
			wp_reset_postdata();
		}
	} else {
		$description = '';
	}

	$size = get_field( 'size', $post->ID ) ?: 'default';
	$image_size  = ( (int) $size > 5 ) ? 'Event Blocks Large' : 'Event Blocks';


	if ( in_array( 'show-image', $options ) ) {
		$image_objects = get_field( 'images', $post->ID );
		if( $image_objects ) {
			$images = '<div class="collage collage-' . count( $image_objects ) . '">';
			foreach ( $image_objects as $object ) {
				$orient = ( isset( $object['height'], $object['width'] ) && ( $object['height'] > $object['width'] ) ) ? 'tall' : 'wide';
				$images .= '<div class="image image-' . $orient . '"><div class="spacer"></div>';
				$images .= pantheon_display_post_field_image( $object, $image_size, 'image', false );
				$images .= '</div>';
			}
			$images .= '<div style="clear:both"></div></div>';
		} else {
			$images = pantheon_display_post_featured_image( $post->ID, $image_size, null, 'image', false );
		}
	} else {
		$images = '';
	}

	$class = $color = '';
	if ( 'pantheon_event' == $post->post_type ) {
		$terms = (array) get_the_terms( $post->ID, 'pantheon_event_type' );
		foreach ( $terms as $term ) {
			if ( is_object( $term ) ) {
				$class .= ' event_type-' . $term->slug;
				$class .= ' event_type_id-' . $term->term_id;
				$color = $color ?: get_field('color', 'pantheon_event_type_' . $term->term_id );
			}
		}
	}

	return array(
		'title'             => get_the_title( $post->ID ),
		'short_description' => $description,
		'images'            => $images,
		'link'              => get_permalink( $post->ID ),
		'size'              => $size,
		'date'              => ( 'post' === $post->post_type ) ? get_the_date( '', $post->ID ) : get_field('date', $post->ID),
		'class'             => $class,
		'color'             => $color,
		'sort_priority'     => get_field( 'sort_priority', $post->ID ) ?: 0,
	);
}

/*
 * Make sure all homepage content (posts, events, etc.) have a width assigned. Doing this for all once a day to make
 * sure we have a good mix of sizes as old events roll off etc.
 */
function assign_homepage_widths( $home_page_id = null, $content = null, $count = 1 ) {
	if ( null === $home_page_id ) {
		$home_pages = new \WP_Query(array(
			'post_type'  => 'page',
			'meta_key'   => '_wp_page_template',
			'meta_value' => 'page-home.php',
			'fields'     => 'ids'
		));

		if ( isset( $home_pages->posts[0] ) ) {
			foreach ( $home_pages->posts as $home_page ) {
				assign_homepage_widths( $home_page );
			}
		}

		return;
	}

	if ( null === $content ) {
		$home_contents = (array) get_option( 'hermes_theme_home_contents-' . $home_page_id );

		foreach ( $home_contents as $key => $home_content ) {
			$home_contents[$key]['class'] = 'w' . assign_homepage_widths( $home_page_id, $home_content, $count++ );
		}

		update_option( 'hermes_theme_home_contents-' . $home_page_id, $home_contents );
	} else {
		if ( isset( $content['size'] ) && 'default' != $content['size'] ) {
			return $content['size'];
		} elseif ( isset( $content['images'] ) && ( ! empty( $content['images'] ) ) ) {
			return ( $count % 2 ) ? '4' : '8' ;
		} else {
			return ( $count % 3 ) ? '3' : '6' ;
		}

	}
}

// A few helpful array manipulation functions. Maybe add to pantheon?

function flatten_array( $array, $depth = false ) {
	if ( ! is_array( $array ) ) {
		return $array;
	}

	$flattened_array = array();

	// if no depth is passed, let's do it the easy way
	if ( false === $depth ) {
		array_walk_recursive( $array, function( $a ) use ( &$flattened_array ) { $flattened_array[] = $a; } );
		return $flattened_array;
	}

	// if depth is negative, we are going to iterate until a $depth level short of full depth
	if ( $depth < 0 ) {
		$test_depth = 0;
		foreach ( $array as $test_array ) {
			if ( is_array( $test_array ) ) {
				$test_return = flatten_array( $test_array, $depth );
				if ( is_array( $test_return ) ) {
					$flattened_array = array_merge( $flattened_array, array_values( $test_return ) );
					$test_depth = $depth - 1;
				} else {
					$flattened_array[] = $test_array;
					$test_depth = min( $test_depth, $test_return );
				}
			} else {
				return -1;
			}
		}

		if ( $test_depth > $depth ) {
			return $test_depth - 1;
		} 
		elseif ( $test_depth == $depth ) {
			return $array;
		}
		else {
			return $flattened_array;
		}
	}

	// if depth is positive we will only flatten up to a certain depth
	if ( $depth > 0 ) {
		foreach ( $array as $sub_array ) {
			$return = flatten_array( $sub_array, $depth - 1 );
			if ( is_array( $return ) ) {
				$flattened_array = array_merge( $flattened_array, array_values( $return ) );
			} else {
				$flattened_array[] = $return;
			}
		}
		return $flattened_array;
	} else {
		return $array;
	}

}

function sort_by_size( $array, $sort = SORT_DESC ) {

	$_array = array();
	foreach ( $array as $key => $value ) {
		$_array[0][$key] = count( $value );
		$_array[1][$key] = $value;
	}

	array_multisort( $_array[0], $sort, $_array[1] );

	return $_array[1];
}

// depth will allow you to skip to a certain depth before performing the swap
function swap_dimensions( $array, $depth = 0 ) {

	if ( ! is_array( $array ) ) {
		return $array;
	}

	if ( $depth > 0 ) {
		$_array = array();
		foreach ( $array as $key => $value ) {
			$_array[$key] = swap_dimensions( $value, $depth - 1 );
		}
		return $_array;
	}

	$_array = array();
	foreach ( $array as $x => $xray ) {
		if ( is_array( $xray ) ) {
			foreach ( $xray as $y => $value ) {
				$_array[$y][$x] = $value;
			}
		} else {
			$_array[0][$x] = $xray;
		}
	}

	return $_array;
}