<?php
	/*===========================================================================
	PAGE TEMPLATE
	===========================================================================*/
	
	// HEADER //
	get_template_part( 'parts/shared/header' );
?>

	<div class="container row">

		<main class="medium-8 large-9 columns">

			<?php pantheon_display_post_featured_image($post->ID, $size = 'TTG Featured Image', $link = null, $output = 'image', $echo = true); ?>

			<div class="primary inner clearfix">
				<?php 
					if(have_posts()):
						while(have_posts()): the_post();
				?>
					
							<header>
								<div class="row">
									<div class="large-8 columns">
										<h1><?php the_title(); ?></h1>
									</div>
									<div class="large-4 columns">
										<?php pantheon_display_social_share($display = 'page', $type = 'post', $object = null, $content = null, $image = null, $url = null); ?>
									</div>
								</div>
							</header>

							<?php the_content(); ?>

				<?php
						endwhile;
					endif;
				?>
			</div>	

		</main>


		<?php
			// SIDEBAR // 
			get_template_part( 'parts/shared/sidebar' );
		?>

	</div>

<?php 
	// FOOTER //
	get_template_part( 'parts/shared/footer' );
