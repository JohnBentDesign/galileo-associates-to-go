<?php
/*===========================================================================
Template Name: Homepage
=============================================================================
Page template for Homepage. Displays events and posts in flexible boxes
powered by js.
*/

// enqueue correct masonry-type script depending on user choice
$layout_library = get_field( 'layout_library', 'options' );

switch ( $layout_library ) {
	case 'isotope' :
	case 'packery' :
	wp_enqueue_script( $layout_library, get_template_directory_uri() . '/javascripts/plugins/' . $layout_library . ".pkgd.min.js", array( 'jquery' ) );
		break;
	case 'masonry' :
	default :
		// all we have to do is enqueue masonry - it is already part of wp
		wp_enqueue_script( 'masonry' );
}

function hermes_homepage_layout_trigger() {
	$layout_library = get_field( 'layout_library', 'options' ) ?: 'masonry';
	?>
	<script type="text/javascript">
		jQuery(window).load(function() {
		<?php switch ( $layout_library ) :
			case 'nested' : ?>
				jQuery("#grid-container").nested({
					'selector' : '.item',
					'columnWidth' : '.grid-sizer'
				});
				<?php break;
			case 'masonry' : ?>
				jQuery("#grid-container").masonry({
					'itemSelector' : '.item',
					'columnWidth' : '.grid-sizer',
					'gutter' : 0
				});
				<?php break;
			case 'packery' : ?>
				jQuery("#grid-container").packery({
					'itemSelector' : '.item',
					'columnWidth' : '.grid-sizer'
				});
		<?php break; endswitch; ?>
		});
	</script>
	<?php
}
add_action( 'wp_footer', 'hermes_homepage_layout_trigger', 100 );

function hermes_homepage_body_class( $classes ) {
	$hover_actions = get_field( 'hover_actions', 'options' );

	if ( is_array( $hover_actions ) ) {
		foreach ( $hover_actions as $hover_action ) {
			$classes[] = "hover-$hover_action";
		}
	}

	return $classes;
}
add_filter( 'body_class', 'hermes_homepage_body_class' );

// get list of posts/events to show on homepage
$home_content = get_option( 'hermes_theme_home_contents-' . get_the_ID() );

get_template_part( 'parts/shared/header' ); ?>

	<main>

		<?php if ( is_array( $home_content ) ) : ?>

		<div id="container" class="container">
			<div id="grid-container">
			<div class="grid-sizer"></div>

			<?php foreach ( $home_content as $content ) :
				$content = wp_parse_args( $content, array(
					'title'             => '',
					'short_description' => '',
					'images'            => '',
					'link'              => '',
					'size'              => 'default',
					'date'              => '',
					'class'             => '',
					'color'             => '#777777',
				) );

				// Build our checks
				$eventImage 	= $content['images'];
				$eventDate 		= $content['date'];
				$eventContent 	= $content['short_description'];

				if($content['title'] || $eventImage || $eventDate || $eventContent):
			?>

					<div class="item <?php echo $content['class'] ?> tertiary" style="background-color: <?php echo $content['color']; ?>">

						<a href="<?php echo $content['link']; ?>">

							<?php if($eventImage): ?>
								<div class="images">
									<?php echo $eventImage ?>
								</div>
							<?php endif; ?>

							<div class="text">

								<?php if($eventDate): ?>
									<div class="date">
										<?php echo $eventDate; ?>
									</div>
								<?php endif; ?>

								<div class="title">
									<?php echo $content['title']; ?>
								</div>
								
								<?php if($eventContent): ?>
								<div class="excerpt">
									<?php echo $eventContent; ?>
								</div>
								<?php endif; ?>

								<div class="arrow <?php if ( strlen( $content['short_description'] ) > 50 ) echo 'below'; ?>">&rsaquo;</div>
							</div>

							<div class="overlay"></div>

						</a>

					</div>

			<?php 
				endif;
				endforeach;
			?>

			</div><!-- grid-container -->
		</div><!-- container -->

		<?php endif; ?>

	</main>

<?php get_template_part( 'parts/shared/footer' );