<?php
	/*===========================================================================
	Single Event
	===========================================================================*/

	// HEADER //
	get_template_part( 'parts/shared/header' );
?>
	<div class="event-single container row">

		<?php // TITLE // ?>
		<div class="small-12 columns">
			<h2 class="title-outside">Event Calendar</h2>
		</div>

		<?php // FILTERS // ?>
		<?php get_template_part( 'parts/events/sidebar', 'filters' ); ?>

		<?php // EVENT CONTENT // ?>
		<?php $columnEnd = (have_rows('event_sidebar')) ? '' : ' end'; ?>
		<main class="large-7 columns<?= $columnEnd; ?>">

			<?php // Featured Image // ?>
			<?php pantheon_display_post_featured_image($post->ID, $size = 'TTG Featured Image', $link = null, $output = 'image', $echo = true); ?>
			
			<?php // Content // ?>
			<?php
				// Event Color: We're only going to grab the color from the first event type we find... Then we're going to be hacky and add it through stylesheet :(
				// Note: for now, the text color that goes on top of the category color is the selected primary color... So the user will need to make sure there's manually enough contrast between the two
				$terms 		= wp_get_post_terms($post->ID, 'pantheon_event_type');
				$color 		= '';
				if ( ( isset( $terms[0] ) ) && is_object( $terms[0] ) ) {
					$termID = $terms[0]->term_id;
					$color = get_field('color', 'pantheon_event_type_' . $termID);
				}
				if(!$color && get_field('color_tertiary', 'options')){
					$color      = get_field('color_tertiary', 'options'); // if a term has not been assigned we will use the tertiary color
				}
				elseif(!$color) {
					$color      = '#7c7c7c'; // if a term has not been assigned we will use the tertiary color
				}
				$colorBG 	= ' style="background-color: ' . $color . '"';
				$colorTxt 	= ' style="color: ' . $color . '"';
			?>
			<div class="container"<?= $colorBG; ?>>

				<?php while ( have_posts() ) : the_post(); ?>

					<div class="row">

						<div class="infobox primary large-4 columns">
							<?php
								// Get our Variables
								$infoboxDisplay 	= get_field('infobox_overrides');
								$ticket 			= get_field('ticket_prices');
								$infoboxSection 	= get_field('additional_infobox_sections');
								$agendaItems 		= get_field('agenda_items');
								$ctaText 			= get_field('call_to_action_text');
								$ctaLink 			= esc_url(get_field('call_to_action_link'));
							?>

							<?php 
								// DATES //
								if( is_array($infoboxDisplay) && in_array('dates', $infoboxDisplay) ) {
							?>
									<div class="section-content ticket-dates">
										<h4<?= $colorTxt; ?>>Dates:</h4>
										<?php
											$dates 	= wp_list_pluck( $agendaItems, 'date' );
											$_dates = array();
											
											foreach ( $dates as $date ) {
												$month = substr( $date, 0, 6 );
												$day = substr( $date, 6, 2 );
												$_dates[$month][$day] = true;
											}

											$date_string = "";
											$serial = false;
											foreach ( $_dates as $month => $days ) {

												$date_string .= date( 'F ', strtotime( $month . '01' ) );
												foreach ( $days as $day => $unused ) {
													$tomorrow = sprintf( '%02d', $day + 1 );
													if ( $serial ) {
														$serial = isset( $days[$tomorrow] );
														$date_string .= $serial ? '' : date( 'jS', strtotime( $month . $day ) ) . ', ';
													} else {
														$serial = isset( $days[$tomorrow] );
														$date_string .= date( 'jS', strtotime( $month . $day ) );
														$date_string .= $serial ? '-' : ', ';
													}
												}
											}
											// $date_string always ends in a comma... remove it
											$date_string = substr( $date_string, 0, -2 );
											echo '<p>' . $date_string . '</p>';
										?>
									</div>
							<?php } ?>

							<?php 
								// TIMES //
								if( is_array($infoboxDisplay) && in_array('times', $infoboxDisplay) ) {
							?>
									<div class="section-content ticket-times">
										<h4<?= $colorTxt; ?>>Times:</h4>
										<?php
											$times = wp_list_pluck( $agendaItems, 'start_time' );
											$time_string = '';

											if ( 1 === count( array_unique( $times ) ) ) {
												$time_string = $times[0];
											} else {
												foreach ( $agendaItems as $item ) {
													if ( isset ( $item['start_time'] ) && $item['start_time'] ) {
														$date = strtotime( $item['date'] );
														$time_string .= "<p>" . date( 'jS', $date ) . " " . $item['start_time'] . "</p>";
													}
												}
											}
											echo $time_string;
										?>
									</div>
							<?php } ?>

							<?php 
								// TICKET PRICES //
								if ( is_array($infoboxDisplay) && in_array('prices', $infoboxDisplay) && $ticket ) {
							?>
									<div class="section-content ticket-price">
										<h4<?= $colorTxt; ?>>Ticket Prices:</h4>
										<?php 
											foreach ( $ticket as $price ) {
												echo '<p><strong>' . $price['ticket_type'] . ':</strong> <span class="price"' . $colorTxt . '>$' . $price['price'] . '</span></p>';
											}
										?>
									</div>
							<?php } ?>

							<?php 
								// CALL TO ACTION //
								if ( is_array($infoboxDisplay) && in_array('cta', $infoboxDisplay) ) {
							?>
									<div class="call-to-action">
										<a href="<?php echo $ctaLink; ?>" class="button"><?php echo $ctaText; ?></a>
									</div>
							<?php } ?>

							<?php
								// ADDITIONAL SECTIONS //
								if ( is_array($infoboxDisplay) && in_array('add', $infoboxDisplay) && !empty($infoboxSection) ) {
									foreach ( $infoboxSection as $section ) {
										if ( isset ( $section['title'] ) && isset ( $section['description'] ) ) {
							?>
											<div class="section-content">
												<h4<?= $colorTxt; ?>><?php echo $section['title']; ?>:</h4>
												<?php echo $section['description']; ?>
											</div>
							<?php 
										}
									}
								}

							?>

						</div>

						<?php // Descripiton // ?>
						<div class="event-content large-8 columns clearfix">
							<header class="event">
								<h1><?php the_title(); ?></h1>
								<?php
									$locations = get_field( 'event_location' );
									if(!empty($locations)){
										echo '<div class="locations"><strong>Location:</strong> ';
										$separator = '';
										foreach ( (array) $locations as $location ) {
											if ( isset( $location->post_title ) ) {
												echo '<a href="' . get_permalink($location->ID) . '">' . $separator . $location->post_title . '</a>';
											}
											$separator = ' | ';
										}
										echo '</div>';
									}
								?>
							</header>
							<?php the_content(); ?>
						</div>

					</div>

				<?php endwhile; ?>

			</div>
		</main>

		<?php // MEDIA // ?>
		<?php get_template_part( 'parts/events/sidebar', 'media' ); ?>

	</div>

<?php 
	// FOOTER //
	get_template_part( 'parts/shared/footer' );
