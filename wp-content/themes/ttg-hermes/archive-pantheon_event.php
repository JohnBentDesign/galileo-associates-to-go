<?php
/*===========================================================================
Main page for events - a.k.a. Event Calendar
=============================================================================
*/

	// HEADER //
	get_template_part( 'parts/shared/header' );
?>
	<div class="event-archive-type container row">

		<?php // TITLE // ?>
		<div class="small-12 columns">
			<h2 class="title-outside">Event Calendar</h2>
		</div>

		<?php // FILTERS // ?>
		<?php get_template_part( 'parts/events/sidebar', 'filters' ); ?>

		<?php // EVENT CONTENT // ?>
		<main class="large-10 columns">

			<?php

				// The Loop
				if ( have_posts() ) :
			?>
					<ul id="infinite-scroll-container" class="event-listing medium-block-grid-2 large-block-grid-3">
						<?php
							while ( have_posts() ) : the_post();
								get_template_part( 'parts/events/content', 'calendar' );
							endwhile;
						?>
					</ul>
			<?php						
				endif;

				if ( ! get_field( 'do_infinite_scroll', 'options' ) ) {
					get_template_part( 'parts/shared/pagination' );
				}
			?>
			
		</main>

	</div>

<?php
	// FOOTER //
	get_template_part( 'parts/shared/footer' );

