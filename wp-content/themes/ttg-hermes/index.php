<?php
	/*===========================================================================
	INDEX
	=============================================================================
	Default fallback style. Default page for blog viewing page
	*/

	// HEADER //
	get_template_part( 'parts/shared/header' );
?>

	<div class="container">
		<div class="row">

			<div class="columns">
				<h2 class="blog-title title-outside"><?= get_the_title(get_option('page_for_posts')); ?></h2>
			</div>
			<?php
				echo '<div class="main medium-8 columns">';
				get_template_part('parts/posts/loop');
				get_template_part('parts/shared/pagination');
				echo '</div>';
			?>

			<?php
				// SIDEBAR //
				get_template_part('parts/shared/sidebar');
			?>

		</div>
	</div>

<?php 
	// FOOTER //
	get_template_part( 'parts/shared/footer' );