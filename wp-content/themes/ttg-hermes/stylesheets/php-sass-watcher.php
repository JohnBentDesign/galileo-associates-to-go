<?php
/**
 * Class SassWatcher
 *
 * This simple tool compiles all .scss files in folder A to .css files (with exactly the same name) into folder B.
 * To keep things as minimal as possible, this tool compiles every X seconds, regardless of changes within the files.
 * This seems weird, but makes sense as checking for changes in the files is more CPU-extensive than simply
 * re-compiling them. SassWatcher uses scssphp, the best SASS compiler in PHP available.
 *
 * SassWatcher is not a standalone compiler, it's just a little method that uses the excellent scssphp compiler written
 * by Leaf Corcoran (https://twitter.com/moonscript), which can be found here: http://leafo.net/scssphp/ and adds
 * automatic interval-compiling to it.
 *
 * The currently supported version of SCSS syntax is 3.2.12, which is the latest one.
 * To avoid confusion: SASS is the name of the language itself, and also the "name" of the "first" version of the
 * syntax (which was quite different than CSS). Then SASS's syntax was changed to "SCSS", which is more like CSS, but
 * with awesome additional possibilities and features.
 *
 * The compiler uses the SCSS syntax, which is recommended and mostly used. The old SASS syntax is not supported.
 *
 * @see SASS Wikipedia: http://en.wikipedia.org/wiki/Sass_%28stylesheet_language%29
 * @see SASS Homepage: http://sass-lang.com/
 * @see scssphp, the used compiler (in PHP): http://leafo.net/scssphp/
 *
 * How to use this tool:
 *
 * 1. Edit $sass_watcher->watch( ... ); in the last line of this file and put your stuff in here, see the parameter
 *    list below.
 * 2. Make sure PHP can write into your CSS folder.
 * 3. Run the script:
 *    a) simple way, from browser, just enter the URL to scss-compiler.php: http://127.0.0.1/folder/php-sass-watcher.php
 *       The script will run forever, even if you close the browser window.
 *    b) PHPStorm users can run the script by right-clicking the file and selecting "Run php-sass-watcher.php".
 * 4. To stop the script, stop/restart your Apache/Nginx/etc. or press the red "stop process button in PHPStorm.
 *
 * The parameters:
 *
 *  1. relative path to your SCSS folder
 *  2. relative path to your CSS folder (make sure PHP has write-rights here)
 *  3. the compiling interval (in seconds)
 *  4. relative path to the scss.inc.php file, which is the main file of the SASS compiler used
 *     here. Download the script manually from http://leafo.net/scssphp/ or "require" it via Composer:
 *     "leafo/scssphp": "0.0.9"
 *  5. optional: how the .css output should look like. See http://leafo.net/scssphp/docs/#output_formatting for more.
 *
 * How the tool works:
 *
 * Every X seconds ALL files in the scss folder will be compiled to same-name .css files in the css folder.
 * The tool does not stop when a .scss file is broken, has syntax error or similar.
 * The tool does not compile when .scss file is broken, has syntax error or similar. It will only compile next time
 * when there's a valid scss file.
 */

/*==========================================================================
CUSTOM COLORS DEFINED BY USER
==========================================================================*/
$primaryColor       = (get_field('color_primary', 'options'))       ? get_field('color_primary', 'options')     : '#ffffff';
$secondaryColor     = (get_field('color_secondary', 'options'))     ? get_field('color_secondary', 'options')   : '#565656';
$tertiaryColor      = (get_field('color_tertiary', 'options'))      ? get_field('color_tertiary', 'options')    : '#00aeef';
$quaternaryColor    = (get_field('color_quaternary', 'options'))    ? get_field('color_quaternary', 'options')  : '#691c33';

$user_colors = '
	@import "includes";

	// FUNCTIONS
	//-------------------------------------------------------------
	// More Accurate Color Contrastor (http://codepen.io/bluesaunders/pen/FCLaz)

	// Calculate brightness of a given color.
	@function brightness($color) {
		@return ((red($color) * .299) + (green($color) * .587) + (blue($color) * .114)) / 255 * 100%;
	}

	// Compares contrast of a given color to the light/dark arguments and returns whichever is most "contrasty"
	@function color-contrast($color, $dark: $dark-text-default, $light: $light-text-default) {
		@if $color == null {
		    @return null;
		}
		@else {
			$color-brightness: brightness($color);  
			$light-text-brightness: brightness($light);
			$dark-text-brightness: brightness($dark);
			@return if(abs($color-brightness - $light-text-brightness) > abs($color-brightness - $dark-text-brightness), $light, $dark);  
		}
	}

	// VARIABLES
	//-------------------------------------------------------------
	// Raw
	$primary: ' . $primaryColor . ';
	$secondary: ' . $secondaryColor . ';
	$tertiary: ' . $tertiaryColor . ';
	$quaternary: ' . $quaternaryColor . ';

	// PRIMARY
	//-------------------------------------------------------------
	body, .primary {
		background-color: $primary;
		h1, h1 a, h2, h3, h4, h5, h6, a {
			color: $tertiary;
		}
		p, ol, ul, li, blockquote, dl, dt, dd, table, tr, td, span, pre, label, input, textarea {
			color: $secondary;
		}
		a { 
			color: $quaternary;
			&:hover { color: color-contrast($quaternary, darken($quaternary, 5%), lighten($quaternary, 5%)); }
		}
		hr { border-color: color-contrast($primary, darken($primary, 25%), lighten($primary, 25%)); }
		blockquote {
			p, ol, ul, li, span {
				color: $secondary;
			}
		}
		.social .prepend, .st-single-color a:before {
			color: $quaternary;
		}
		.button {
			background-color: $tertiary;
			border-color: $tertiary;
			color: $primary;
			&:hover {
				background-color: $secondary;
				color: $primary;
				border-color: $secondary;
			}
		}
	}
	body .button.primary {
		background-color: $primary;
		color: $tertiary;
		&:hover {
			background-color: $tertiary;
			color: $primary;
		}
	}

	// SECONDARY
	//-------------------------------------------------------------
	.secondary {
		background-color: $secondary;
		h1, h1 a, h2, h3, h4, h5, h6, a {
			color: color-contrast($tertiary, darken($tertiary, 100%), lighten($tertiary, 100%));
		}
		p, ol, ul, li, blockquote, dl, dt, dd, table, tr, td, span, pre, label, input, textarea {
			color: color-contrast($tertiary, darken($tertiary, 75%), lighten($tertiary, 75%));
		}
		.social .prepend, .st-single-color a:before {
			color: $quaternary;
		}
		.button {
			background-color: $primary;
			border-color: $primary;
			color: $secondary;
			&:hover {
				background-color: $tertiary;
				color: $primary;
				border-color: $tertiary;
			}
		}
	}
	body .button.secondary {
		background-color: $secondary;
		color: $primary;
		&:hover {
			background-color: $tertiary;
			color: $primary;
		}
	}

	// TERTIARY
	//-------------------------------------------------------------
	.tertiary {
		background-color: $tertiary;
		h1, h1 a, h2, h3, h4, h5, h6, a {
			color: color-contrast($tertiary, darken($tertiary, 100%), lighten($tertiary, 100%));
		}
		p, ol, ul, li, blockquote, dl, dt, dd, table, tr, td, span, pre, label, input, textarea {
			color: color-contrast($tertiary, darken($tertiary, 75%), lighten($tertiary, 75%));
		}
		.social .prepend, .st-single-color a:before {
			color: $quaternary;
		}
		.button {
			background-color: $primary;
			border-color: $primary;
			color: $tertiary;
			&:hover {
				background-color: $quaternary;
				color: $primary;
				border-color: $quaternary;
			}
		}
	}
	body .button.tertiary {
		background-color: $tertiary;
		color: $primary;
		&:hover {
			background-color: $quaternary;
			color: $primary;
		}
	}

	// QUARTERNARY
	//-------------------------------------------------------------
	.quaternary, .has-bg {
		background-color: $quaternary;
		h1, h1 a, h2, h3, h4, h5, h6, a {
			color: color-contrast($quaternary, darken($quaternary, 100%), lighten($quaternary, 100%));
		}
		p, ol, ul, li, blockquote, dl, dt, dd, table, tr, td, span, pre, label, input, textarea {
			color: color-contrast($quaternary, darken($quaternary, 75%), lighten($quaternary, 75%));
		}
		blockquote {
			p, ol, ul, li, span {
				color: $secondary;
			}
		}
		.button {
			color: $quaternary;
			background-color: color-contrast($quaternary, lighten($quaternary, 100%), darken($quaternary, 100%));	
			&:hover, &:active {
				color: $quaternary;
				background-color: color-contrast($quaternary, lighten($quaternary, 25%), darken($quaternary, 25%));	
			}
		}
		&.widget-hours {
			p { border-top-color: rgba(color-contrast($quaternary, lighten($quaternary, 25%), darken($quaternary, 25%)), .5); }
		}
		.button {
			background-color: $tertiary;
			border-color: $tertiary;
			color: $primary;
			&:hover {
				background-color: $secondary;
				color: $primary;
				border-color: $secondary;
			}
		}
	}
	body .button.quaternary {
		background-color: $quaternary;
		color: $primary;
		&:hover {
			background-color: $tertiary;
			color: $primary;
		}
	}

	// CONTAINERS
	//-------------------------------------------------------------
	body { background-color: $tertiary; }
	.container {
		background-color: color-contrast($primary, lighten($primary, 5%), darken($primary, 5%));
	}

	// HEADER
	//-------------------------------------------------------------
	header.main {
		.clickables div, .clickables form {
			border-color: rgba(color-contrast($tertiary, lighten($tertiary, 100%), darken($tertiary, 100%)), .25);
		}
	}
	nav.main a {
		color: $tertiary;
	}
	nav.main .sub-menu {
		background-color: $quaternary;
		a { color: color-contrast($quaternary, lighten($quaternary, 100%), darken($quaternary, 100%)); }
		a:hover { background-color: color-contrast($quaternary, lighten($quaternary, 5%), darken($quaternary, 5%)); }
		li { border-color: color-contrast($quaternary, lighten($quaternary, 5%), darken($quaternary, 5%)); }
		.sub-menu { border-color: color-contrast($quaternary, lighten($quaternary, 5%), darken($quaternary, 5%)); }
	}
	input:-moz-placeholder {
		color: rgba(color-contrast($tertiary, lighten($tertiary, 100%), darken($tertiary, 100%)), .75);
	}
	input::-webkit-input-placeholder {
		color: rgba(color-contrast($tertiary, lighten($tertiary, 100%), darken($tertiary, 100%)), .75);
	}
	#header-search input:focus {
		color: $primary;
	}

	// FOOTER
	//-------------------------------------------------------------
	footer.main .upper {
		p, a, li, span { color: $secondary; }
		h3 { color: $tertiary; }
		.details { color: $quaternary; }
	}
	a.back-top {
		background-color: $quaternary;
		color: color-contrast($quaternary, lighten($quaternary, 100%), darken($quaternary, 100%));
		&:hover {
			background-color: color-contrast($quaternary, lighten($quaternary, 5%), darken($quaternary, 5%));
			color: color-contrast($quaternary, lighten($quaternary, 80%), darken($quaternary, 80%));
		}
	}
	.footer-links {
		border-color: color-contrast($primary, lighten($primary, 5%), darken($primary, 5%));
	}

	// SIDEBAR
	//-------------------------------------------------------------
	.event-filter, .widget {
		h3, #wp-calendar caption {
			background-color: $tertiary;
			color: color-contrast($tertiary, lighten($tertiary, 100%), darken($tertiary, 100%));
		}
	}
	.widget {
		li { background-color: $primary; }
		.secondary { 
			background-color: $quaternary;
			color: color-contrast($quaternary, lighten($quaternary, 100%), darken($quaternary, 100%));
		}
		.hours p {
			border-color: rgba($secondary, .25);
		}
		&.widget-login {
			.button {
				background-color: rgba($tertiary, .8);
				border-color: $tertiary;
				color: $primary;
				&:hover {
					background-color: rgba($secondary, .8);
					color: $primary;
					border-color: $secondary;
				}
			}
			.button a { color: $primary; }
		}
		.hasBG {
			p, span, ul, li, ol { color: $primary; }
		}
	}

	// EVENTS
	//-------------------------------------------------------------
	.event-filter {
		p { color: $tertiary; }
		.event-types li a {
			background-color: $quaternary;
			color: $primary;
		}
		.event-loc, .event-month {
			li a {
				background-color: $quaternary;
				color: color-contrast($quaternary, lighten($quaternary, 100%), darken($quaternary, 100%));
			}
		}
	}
	.event-single .event-content {
		h1, h1 a, h2, h3, h4, h5, h6, a {
			color: $primary;
		}
		p, ol, ul, li, blockquote, dl, dt, dd, table, tr, td, span, pre, label, input, textarea, strong, em {
			color: $primary;
		}
		blockquote {
			p, ol, ul, li, span {
				color: $primary;
			}
		}
		header.event:after { background-color: rgba($primary, .5); }
	}
	.event-listing {
		.date {
			background-color: $primary;
		}
		a, p, span { color: $primary; }
		a:hover { color: $primary; }
	}
	.event-media {
		.summary, .review-info, .reviewer, p {
			color: color-contrast($secondary, lighten($secondary, 25%), darken($secondary, 25%));
		}
		h4 span { color: $tertiary; }
	}
	#container .item {
		p, a, a:hover, .arrow { color: $primary; }
	}
	.location-expander-content {
		background-color: color-contrast($primary, lighten($primary, 2%), darken($primary, 2%));
		border-color: color-contrast($primary, lighten($primary, 10%), darken($primary, 10%));
	}

	// BLOG
	//-------------------------------------------------------------
	.post {
		time {
			background-color: $quaternary;
			span { color: color-contrast($quaternary, lighten($quaternary, 100%), darken($quaternary, 100%)); }
		}
	}
	.the-post {
		&:after { background-color: lighten($secondary, 50%); }
	}
	.comment-list .children, .comments {
		background-color: rgba($secondary, .1);
	}
	.comment-list .comment, .comment-intro, .the-post, footer.post {
		border-color: lighten($secondary, 50%);
	}
	.comment .fn { color: $quaternary; }

	// RESPONSIVE
	//-------------------------------------------------------------
	@media only screen and (max-width: 40em) {
		aside.sidebar { background-color: rgba($tertiary, .15); }
		nav.main { 
			background-color: color-contrast($primary, lighten($primary, 15%), darken($primary, 15%));
			.customSelectInner .title, .customSelectInner .icon { color: color-contrast($primary, lighten($primary, 100%), darken($primary, 100%)); }
		}
		.search-mobile {
			border-top-color: color-contrast($primary, lighten($primary, 15%), darken($primary, 15%));
			padding: .5em 0;
		}
		#header-search {
			input, input:focus {
				color: $secondary;
			}
			input:-moz-placeholder {
				color: rgba(color-contrast($secondary, lighten($secondary, 100%), darken($secondary, 100%)), .75);
			}
			input::-webkit-input-placeholder {
				color: rgba(color-contrast($secondary, lighten($secondary, 100%), darken($secondary, 100%)), .75);
			}
		}
	}
	@media only screen and (min-width: 40.063em) and (max-width: 64em) {
		.clickables {
			border-top-color: rgba(color-contrast($tertiary, lighten($tertiary, 100%), darken($tertiary, 100%)), .25);
		}
	}

';

$user_custom_font_style = pantheon_function_font_select(array(
		'header' 	=> 'header.main',
		'footer' 	=> 'footer.main',
		'sidebar' 	=> 'aside.sidebar',
		'widget' 	=> '.widget',
		'post' 		=> '.the-post',
		'page' 		=> '.page main',
		'gallery' 	=> 'article.gallery',
		'product' 	=> 'li.product',
	)
);

$user_custom_text_style = pantheon_function_text_style(array(
		'header' 	=> 'header.main',
		'footer' 	=> 'footer.main',
		'sidebar' 	=> 'aside.sidebar',
		'widget' 	=> '.widget',
		'post' 		=> '.the-post',
		'page' 		=> '.page main',
		'gallery' 	=> 'article.gallery',
		'product' 	=> 'li.product',
	)
);

$user_custom_styles = get_field('advanced_css', 'options');


/*==========================================================================*/

class SassWatcher
{
	/**
	 * Watches a folder for .scss files, compiles them every X seconds
	 * Re-compiling your .scss files every X seconds seems like "too much action" at first sight, but using a
	 * "has-this-file-changed?"-check uses more CPU power than simply re-compiling them permanently :)
	 * Beside that, we are only compiling .scss in development, for production we deploy .css, so we don't care.
	 *
	 * @param string $scss_folder source folder where you have your .scss files
	 * @param string $css_folder destination folder where you want your .css files
	 * @param int $interval interval in seconds
	 * @param string $scssphp_script_path path where scss.inc.php (the scssphp script) is
	 * @param string $format_style CSS output format, ee http://leafo.net/scssphp/docs/#output_formatting for more.
	 */
	public function watch($scss_folder, $css_folder, $scssphp_script_path, $format_style = "scss_formatter", $user_defined_colors = null, $user_defined_custom_font = null, $user_defined_custom_text = null, $user_defined_styles = null)
	{
		// load the compiler script (scssphp), more here: http://www.leafo.net/scssphp
		require $scssphp_script_path;
		$scss_compiler = new scssc();

		// set the path to your to-be-imported mixins. please note: custom paths are coming up on future releases!
		$scss_compiler->setImportPaths($scss_folder);
		// set css formatting (normal, nested or minimized), @see http://leafo.net/scssphp/docs/#output_formatting
		$scss_compiler->setFormatter($format_style);

		// get all .scss files from scss folder
		$filelist = glob($scss_folder . "*.scss");

		// step through all .scss files in that folder
		foreach ($filelist as $file_path) {
			// get path elements from that file
			$file_path_elements = pathinfo($file_path);

			// get file's name without extension
			$file_name = $file_path_elements['filename'];

			// get .scss's content, put it into $string_sass
			$string_sass = file_get_contents($scss_folder . $file_name . ".scss");

			// Add our user defined colors to the compiled code
			$string_sass .= $user_defined_colors;

			// Add our user font styles to the compiled code
			$string_sass .= $user_defined_custom_font;

			// Add our user text styles to the compiled code
			$string_sass .= $user_defined_custom_text;

			// Add our user defined styles to the compiled code (so we can save it)
			$string_sass .= $user_defined_styles;

			// try/catch block to prevent script stopping when scss compiler throws an error
			try {
				// compile this SASS code to CSS
				$string_css = $scss_compiler->compile($string_sass);
				// write CSS into file with the same filename, but .css extension
				file_put_contents($css_folder . $file_name . ".css", $string_css);
			} catch (Exception $e) {
				echo '<div id="message" class="error below-h2">There was an error compiling your CSS. You may have an open selector, quotation mark, or too many nests.</div>';
			}
		}
	}
}

$sass_watcher = new SassWatcher();
$sass_watcher->watch(get_template_directory() . '/stylesheets/sass/', get_template_directory() . '/stylesheets/css/', get_template_directory() . '/stylesheets/scss.inc.php', 'scss_formatter', $user_colors, $user_custom_font_style, $user_custom_text_style, $user_custom_styles);
