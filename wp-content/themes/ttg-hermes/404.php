<?php
	/*===========================================================================
	400
	=============================================================================
	Display for 404 pages
	*/

	// HEADER //
	get_template_part( 'parts/shared/header' );

	// Get 404 page content
	$fourohfourPage = get_field('404_content', 'options');

	if($fourohfourPage){
		$title 		= get_the_title($fourohfourPage[0]);
		$content 	= apply_filters('the_content', get_post_field('post_content', $fourohfourPage[0]));
		$image 		= pantheon_display_post_featured_image($fourohfourPage[0], 'TTG Featured Image', false);
	}
	else {
		$title 		= 'This page does not exist';
		$content 	= 'Sorry. It seems we can\'t find what you were looking for.';
		$image 		= '';
	}
?>

	<div class="container">
		<div class="row">
			<div class="main large-8 large-centered columns">
				<?php echo $image; ?>
				<h1><?= $title; ?></h1>
				<?= $content; ?>
			</div>
		</div>
	</div>
	
<?php 
	// FOOTER //
	get_template_part( 'parts/shared/footer' );