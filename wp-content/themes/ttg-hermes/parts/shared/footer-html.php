<?php
/*
 * This file is called by footer.php and closes out the body and html. It should be a part of every page. If you
 * replace footer.php be sure to call this file using get_template_part( 'parts/shared/footer', 'html' );.
 */

			// WORDPRESS FOOTER //
			wp_footer();

			// CUSTOM JAVASCRIPT //
			$customScript = get_field('advanced_js', 'options');
			if($customScript){
				echo $customScript;
			}
		?>

	</body>
</html>