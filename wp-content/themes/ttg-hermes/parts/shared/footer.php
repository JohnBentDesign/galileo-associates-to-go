<?php
	//===================================
	// FOOTER MAIN
	//===================================
?>
	<footer class="main">

		<?php
			// UPPER BAR
			//===================================
		?>
		<div class="upper primary">
			
			<?php 
				// Footer Details
				//---------------------
			?>
			<div class="footer-details">
				<div class="row">
					<?php
					$link_text = 'Details';

					// Widgets
					//---------------------
					if( function_exists( 'have_rows' ) ) :
						// Widget Counting
						$widgetCount 	= count( get_field( 'footer_widgets', 'options' ) );
						$widgetCounter	= 0;

						// Widget Sizes
						switch ($widgetCount) {
							case 1:	$columnSize = 'small-12'; break;
							case 2: $columnSize = 'medium-6'; break;
							case 3: $columnSize = 'medium-4'; break;	
							case 4: $columnSize = 'medium-3'; break;	
						}

						// Loop through the widgets
						while ( have_rows( 'footer_widgets', 'options' ) ) : the_row();
							// Increment Widget Count
							$widgetCounter++;

							// Switch Between Content Type
							switch( get_row_layout() ) {

								// Upcoming Events //
								case 'upcoming_events':
									$upcoming_events = new WP_Query( array(
										'posts_per_page' => get_sub_field( 'number_of_events' ),
										'tax_query'      => array(
											'taxonomy'   => 'pantheon_event_type',
											'terms'      => get_sub_field( 'event_types' ),
										)
									) );
									$title = (get_sub_field('widget_title')) ? '<h3>' . get_sub_field('widget_title') . '</h3>' : '';

									if ( $upcoming_events->have_posts() ) { ?>
										<div class="<?= $columnSize; ?> columns">
											<?= $title; ?>
											<ul>
												<?php while ( $upcoming_events->have_posts() ) : $upcoming_events->the_post(); ?>
													<li>
														<?php the_title() ?>
														<a href="<?php the_permalink() ?>" class="details"><?php echo $link_text; ?>&raquo;</a>
													</li>
												<?php endwhile; ?>
											</ul>
										</div>
										<?php
									}
									wp_reset_postdata();
									break;

								// Featured Events //
								case 'featured_events' :
									$posts = get_sub_field( 'featured_events' );
									$title = (get_sub_field('widget_title')) ? '<h3>' . get_sub_field('widget_title') . '</h3>' : '';

									if( $posts ) { ?>
										<div class="<?= $columnSize; ?> columns">
											<?= $title; ?>
											<ul>
												<?php foreach( $posts as $post ) : ?>
													<?php setup_postdata( $post ); ?>
													<li>
														<?php the_title() ?>
														<a href="<?php the_permalink() ?>" class="details"><?php echo $link_text; ?>&raquo;</a>
													</li>
												<?php endforeach; ?>
											</ul>
										</div>
										<?php
										wp_reset_postdata();
									}
									break;

								// Call to Action //
								case 'call_to_action' :
									$title = (get_sub_field('widget_title')) ? '<h3>' . get_sub_field('widget_title') . '</h3>' : '';
									if ( $link = get_sub_field( 'link' ) ) { ?>
										<div class="<?= $columnSize; ?> columns">
											<?= $title; ?>
											<a href="<?php the_sub_field( 'link' ) ?>" class="cta_footer">
												<?php pantheon_display_post_field_image(get_sub_field( 'image' ), $size = 'medium', $output = 'image', $echo = true, $add = null); ?>
												<span class="cta_text"><?php the_sub_field( 'text' ) ?></span>
											</a>
										</div>
									<?php
									}
									break;

								// Link List //
								case 'link_list' :
									$title = (get_sub_field('widget_title')) ? '<h3>' . get_sub_field('widget_title') . '</h3>' : '';
									if( have_rows( 'links' ) ) :
							?>
										<div class="<?= $columnSize; ?> columns">
											<?= $title; ?>
											<ul>
												<?php while ( have_rows( 'links' ) ) : the_row(); ?>
													<li>
														<?php the_sub_field( 'description' ); ?>
														<a href="<?php the_sub_field( 'link' ) ?>" class="details"><?php echo $link_text; ?>&raquo;</a>
													</li>
												<?php endwhile; ?>
											</ul>
										</div>
										<?php
									endif;
									break;
							}
						endwhile;
					endif;
					?>
				</div>
			</div>

			<?php 
				// Footer Links
				//---------------------
			?>
			<div class="footer-links">
				<div class="row">
					<div class="small-12 medium-8 columns">
						<?php 
							// Footer Navigation
							wp_nav_menu( array(
								'theme_location' 	=> 'menu-footer',
								'menu'				=> 'Footer Menu',
								'container'			=> 'nav',
								'container_class' 	=> 'footer',
								'menu_class'		=> 'menu',
								'fallback_cb'		=> false,
								'depth' 			=> 1
							) );
						?>
					</div>

					<?php // Social Buttons // ?>
					<div class="small-12 medium-4 columns">
						<?php
							pantheon_display_social_connect( 'footer' );
						?>
					</div>

				</div>
			</div>
		</div>

		<?php
			// LOWER BAR
			//===================================
		?>
		<div class="lower tertiary">
			<div class="row">

				<?php // Copyright // ?>
				<div class="medium-8 columns">
					<p class="small-print">&copy; <?= date('Y') . ' ' . get_bloginfo('name'); ?>. All rights reserved. <a href="http://technologytherapy.com" target="_blank">Powered by Technology Therapy</a></p>
				</div>

				<?php // Back to Top Button // ?>
				<?php 
					$backToTop = ( get_field('footer_top_btn', 'options') ) ?: 'button';
					if ( 'button' == $backToTop ):
				?>
					<a data-scroll href="#top" class="back-top tiny button">Back to Top</a>
				<?php else: ?>
					<a data-scroll href="#top" class="back-top ion-chevron-up icon"></a>
				<?php endif; ?>

			</div>
		</div>

	</footer>
<?php get_template_part( 'parts/shared/footer', 'html' ); ?>