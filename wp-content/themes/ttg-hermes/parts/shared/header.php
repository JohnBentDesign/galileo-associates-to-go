<?php
	// First get the html header. It should be on every page, even if you bypass this file.
	get_template_part( 'parts/shared/header', 'html' );


	/*===========================================================================
	HEADER
	===========================================================================*/
	$topBarText       	= (get_field( 'top_bar_text', 'options' )) ? '<p>' . get_field( 'top_bar_text', 'options' ) . '</p>' : '';
	$taglinePosition   	= get_field( 'tagline_position', 'options' );

	$headerComponents 	= get_field('header_components', 'options');
	$headerDivider 		= get_field('header_divider', 'options');
	$headerSearch 		= get_field('header_search', 'options');
?>

	<header id="top" class="main primary">

		<?php
			// UPPER BAR
			//===================================
		?>
		<div class="upper tertiary">
			<div class="row medium-collapse" data-equalizer>

				<?php // Text // ?>
				<div class="text show-for-medium-up large-6 columns" data-equalizer-watch>
					<?php echo $topBarText; ?>
				</div>

				<?php // Clickables // ?>
				<div class="clickables large-6 columns clearfix" data-equalizer-watch>
					<?php 
						if(get_field('cta', 'options')):
							while(has_sub_field('cta', 'options')):
								$buttonColor = get_sub_field('home_top_cta_color');
								pantheon_display_post_cta( array( 'prefix' => 'home_top_', 'options' => true, 'repeater' => 'sub-field', 'class' => 'button ' . $buttonColor ) );
							endwhile;
						endif;
					?>
					<div class="show-for-medium-up">
						<?php get_template_part( 'parts/shared/searchform' ); ?>
						<?php pantheon_display_social_connect( 'header' ); ?>
					</div>
				</div>

			</div>
		</div>

		<?php
			// LOWER BAR
			//===================================
		?>
		<div class="lower">
			<div class="row">

				<div class="logo medium-4 large-3 columns">
					<a href="<?php echo get_bloginfo ( 'url' ); ?>">
						<?php 
							if( $header_logo = get_field( 'header_logo', 'options' ) ){
								pantheon_display_post_field_image( $header_logo, 'large', 'image' );
							}
							else {
								bloginfo( 'title' );
							}
						?>
					</a>
				</div>

				<?php
					// MOBILE CONTACT BUTTONS
					//===================================
					$phone 		= get_field('general_phone', 'options');
					$email 		= get_field('general_email_primary', 'options');
					$location	= get_field('mobile_google_link', 'options');

					// Count how many buttons
					$counter 	= 0;
					if($phone){ ++$counter; }
					if($email){ ++$counter; }
					if($location){ ++$counter; }

					if($phone || $email || $location){
						// Figure Out Button Size
						switch($counter){
							case 1:	$columnSize = '';			break;
							case 2:	$columnSize = 'small-6';	break;
							case 3:	$columnSize = 'small-4';	break;
						}

						// Get our Buttons
						$phone 		= ($phone) 		? '<a href="tel:' . $phone . '" class="ion-ios7-telephone-outline icon columns ' . $columnSize . '"></a>' : '';
						$email 		= ($email) 		? '<a href="mailto:' . $email . '" class="ion-ios7-email-outline icon columns ' . $columnSize . '"></a>' : '';
						$location 	= ($location) 	? '<a href="' . $location . '" target="_blank" class="ion-ios7-location-outline icon columns ' . $columnSize . '"></a>' : '';

						echo '<div class="mobile-buttons">' . $phone . $email . $location . '</div>';

					}
				?>

				<?php
					// TOP NAVIGATION
					//===================================
					wp_nav_menu( array(
						'theme_location' 	=> 'menu-main',
						'menu'				=> 'Main Menu',
						'container'			=> 'nav',
						'container_class' 	=> 'main small-6 medium-8 large-9 columns',
						'menu_class'		=> 'menu',
						'fallback_cb'		=> false,
						'depth' 			=> 3
					) );
				?>

				<?php
					// Mobile Search Bar - only in small/mobile
				?>
				<div class="search-mobile show-for-small-only small-6 columns">
					<?php get_template_part( 'parts/shared/searchform' ); ?>
				</div>

			</div>
		</div>
	</header>
