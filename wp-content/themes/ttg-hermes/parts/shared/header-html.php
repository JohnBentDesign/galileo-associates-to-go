<?php
/**
 * This file opens up the html structure and is called by header.php which contains the standard header for the theme.
 * If you replace header.php, call this file using get_template_part( 'parts/shared/header', 'html' );.
 */
?><!doctype html>
<html>
	<head>
		<?php // META TAGS // ?>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<?php // BLOG TITLE // ?>
		<title><?php wp_title(''); ?></title>

		<?php // FAVICON // ?>
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
	
		<?php 
			// WORDPRESS HEADER //
			wp_head();
		?>
	</head>

	<body <?php body_class(); ?>>
