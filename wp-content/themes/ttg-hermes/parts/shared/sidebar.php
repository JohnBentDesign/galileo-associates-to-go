<?php
	/*===========================================================================
	SIDEBAR: MAIN
	=============================================================================
	Right hand sidebar that appears on most pages.
	*/
	if( (get_post_type() != 'gallery') && !is_post_type_archive('gallery') ):
?>
		<aside class="sidebar medium-4 large-3 columns">

			<?php 
				if(is_page()){
					if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Page Sidebar')){}
				}
				else {
					if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar')){}
				}
			?>

		</aside><?php // /.sidebar ?>
<?php endif; ?>