<?php
	// EVENT POST MEDIA //
//can use the post object
?>

<?php
	// OUR WIDGETS //
	if( have_rows('event_sidebar') ):
?>
		<aside class="event-media large-3 columns">
			<div class="inner primary">
				<h3>Related Media</h3>

				<?php
					// Loop Through Widgets
					while ( have_rows('event_sidebar') ) : the_row();
						echo '<div class="widget">';

						// IMAGE WIDGET //
						if( get_row_layout() == 'image_gallery' ):
							$title = get_sub_field('title') ? '<h4><span class="ion-images"></span> ' . get_sub_field('title') . '</h4>' : '';
							echo $title;

							$images = get_sub_field('gallery');

							if( $images ):
				?>
								<ul class="gallery-images small-block-grid-3">
									<?php foreach( $images as $image ): ?>
										<li>
											<a href="<?php echo $image['url']; ?>" class="lightbox-image" data-caption="<?php echo $image['caption']; ?>">
												 <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
											</a>
										</li>
									<?php endforeach; ?>
								</ul>
				<?php 
						endif;

						// AUDIO WIDGET //
						elseif( get_row_layout() == 'audio' ): 
							$title = get_sub_field('title') ? '<h4><span class="ion-volume-medium"></span> ' . get_sub_field('title') . '</h4>' : '';
							echo $title;
							
							if( have_rows('audios') ):
								while ( have_rows('audios') ) : the_row();
									$audioTitle = (get_sub_field('audio_title')) ? '<p class="audio-title">' . get_sub_field('audio_title') . '</p>' : '';

									echo '<div class="audio-piece">';
									echo $audioTitle;
									echo do_shortcode('[audio src="' . get_sub_field('audio_embed') . '"]');
									echo '</div>';
								endwhile;
							endif;

						// VIDEO WIDGET //
						elseif( get_row_layout() == 'video' ): 
							$title = get_sub_field('title') ? '<h4><span class="ion-play"></span> ' . get_sub_field('title') . '</h4>' : '';
							echo $title;
							
							if( have_rows('videos') ):
								while ( have_rows('videos') ) : the_row();
									$thumbnail = get_sub_field('thumbnail');
									echo '<a href="' . get_sub_field('video_embed') . '" class="lightbox-video"  data-options="smartRecognition: true"><span class="icon ion-play"></span>' . pantheon_display_post_field_image($thumbnail, $size = 'medium', $output = 'image', $echo = false, $add = ' class="video-thumb"') . '</a>';
								endwhile;
							endif;

						// REVIEW WIDGET //
						elseif( get_row_layout() == 'reviews' ):
							$title = get_sub_field('title') ? '<h4><span class="ion-chatbubbles"></span> ' . get_sub_field('title') . '</h4>' : '';
							echo $title; 

							if( have_rows('reviews') ):
								while ( have_rows('reviews') ) : the_row();
									$summary 			= get_sub_field('review_summary');
									$link 				= get_sub_field('link');
									$publication 		= get_sub_field('link_text');
									$publicationLink	= ($link && $publication) ? '<a href="' . $link .'" target="_blank">' . $publication . '</a>' : $publication; 
									$reviewer 			= get_sub_field('reviewer_name');
									$reviewerName 		= ($publication) ? $reviewer . ', ' : $reviewer;
									$reviewerOutput		= ($reviewerName) ? '<span class="reviewer">- ' . $reviewerName . '</span>' : '';

									echo '<div class="review">';

									if($summary) { echo '<div class="summary">"' . $summary . '"</div>'; }
									if($reviewer || $publication) {
										echo '<p class="review-info">';
										echo $reviewerOutput . $publicationLink;
										echo '</p>';
									}

									echo '</div>';
								endwhile;
							endif;

						elseif( get_row_layout() == 'custom' ): 
							$title = get_sub_field('title') ? '<h4><span class="ion-star"></span> ' . get_sub_field('title') . '</h4>' : '';
							echo $title;
							the_sub_field('content');


						endif;

						echo '</div>';

					endwhile;
				?>

			</div>
		</aside>
<?php endif; ?>
