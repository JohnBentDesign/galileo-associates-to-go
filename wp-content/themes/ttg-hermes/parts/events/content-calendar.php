<?php
	// Event Color: We're only going to grab the color from the first event type we find... Then we're going to be hacky and add it through stylesheet :(
	// Note: for now, the text color that goes on top of the category color is the selected primary color... So the user will need to make sure there's manually enough contrast between the two
	$terms 		= wp_get_post_terms($post->ID, 'pantheon_event_type');
	$color 		= '';
	if ( ( isset( $terms[0] ) ) && is_object( $terms[0] ) ) {
		$termID = $terms[0]->term_id;
		$color = get_field('color', 'pantheon_event_type_' . $termID);
	}
	if(!$color && get_field('color_tertiary', 'options')){
		$color      = get_field('color_tertiary', 'options'); // if a term has not been assigned we will use the tertiary color
	}
	elseif(!$color) {
		$color      = '#7c7c7c'; // if a term has not been assigned we will use the tertiary color
	}
	$colorBG 	= ' style="background-color: ' . $color . '"';
	$colorTxt 	= ' style="color: ' . $color . '"';
?>

<li class="event">

	<div class="item tertiary <?php get_the_term_list( get_the_ID(), 'pantheon_event_type', 'event-type-', ' ' ); ?>"<?= $colorBG; ?>>

		<a href="<?php the_permalink(); ?>" class="event-link">

			<?php 
				// do featured image, first image from gallery, or placeholder
				if ( has_post_thumbnail() ):
			?>
				<div class="image" <?php pantheon_display_post_featured_image($post->ID, $size = 'Event Blocks', $link = null, $output = 'style', $echo = true); ?>></div>
			<?php endif; ?>

			<div class="text">
				<div class="date"<?= $colorTxt; ?>>
					<?php echo Pantheon\Addition\Event\get_upcoming_string() ?>
				</div>
				<div class="title-location">
					<p class="title">
						<?php the_title(); ?>
					</p>
					<p class="location">
						<?php
						$locations = get_field( 'event_location' );
						if ( isset( $locations[0]->post_title ) ) {
							echo $locations[0]->post_title;
						}
						?>
					</p>
				</div>
			</div>
			<div class="link-bar clearfix">
				<div class="media-types">
					<?php
						// Loop Through Widgets
						$widgetKinds = array();
						while ( have_rows('event_sidebar') ) : the_row();
							if( (get_row_layout() == 'image_gallery') && !in_array('image_gallery', $widgetKinds) ): 
								echo '<span class="ion-images"></span>';
								array_push($widgetKinds, 'image_gallery');
							elseif( (get_row_layout() == 'audio')  && !in_array('audio', $widgetKinds) ): 
								echo '<span class="ion-volume-medium"></span>';
								array_push($widgetKinds, 'audio');
							elseif( (get_row_layout() == 'video') && !in_array('video', $widgetKinds) ): 
								echo '<span class="ion-play"></span>';
								array_push($widgetKinds, 'video');
							elseif( (get_row_layout() == 'reviews') && !in_array('reviews', $widgetKinds) ): 
								echo '<span class="ion-chatbubbles"></span>';
								array_push($widgetKinds, 'reviews');
							endif;
						endwhile;
					?>
				</div>
				<div class="view-details">
					<?php echo 'View Details'; // doesn't have to be a link because all of this is wrapped in one big link ?>
				</div>
			</div>

			<div class="overlay"></div>

		</a>

	</div>

</li>