<?php
	// EVENT FILTERS SIDEBARS //
?>

	<aside class="event-filter large-2 columns">
		<p>Filter By:</p>
	
		<?php
			// Event Types
			//-------------------
			$eventTypes 	= get_terms( 'pantheon_event_type', array( 'hide_empty' => false ) );
			if(!empty($eventTypes)){
				echo '<h3 class="event-types">Event Types</h3>';
				echo '<ul class="event-types">';

				echo '<li><a href="' . get_post_type_archive_link('pantheon_event') . '">All</a></li>';

				foreach ( $eventTypes as $event_type ) {
					$link 	= get_term_link( $event_type, 'pantheon_event_type' );
					$color 	= get_field('color', 'pantheon_event_type_' . $event_type->term_id);
					echo '<li><a href="' . $link . '" class="event-type" style="background-color: ' . $color . '">' . $event_type->name . '</a></li>';
				}

				echo '</ul>';
			}


			// Event Locations
			//-------------------
			$eventLoc 		= array( 
				'post_type' 		=> 'pantheon_event_loc',
				'posts_per_page' 	=> -1,
				'order' 			=> 'DESC',
				'orderby' 			=> 'title',
			);
			$eventLocations	= new WP_Query( $eventLoc );
			
			if($eventLocations->have_posts()):
				echo '<h3 class="event-loc">Event Locations</h3>';
				echo '<ul class="event-loc">';
				
				while($eventLocations->have_posts()): $eventLocations->the_post();
					echo '<li><a href="' . get_permalink($post->ID) . '">' . get_the_title($post->ID) . '</a></li>';
				endwhile;

				echo '</ul>';
			endif;

			// Reset Post Data
			wp_reset_postdata();


			// Event Months
			//-------------------
			$eventArgs 		= array(
								'posts_per_page'   => -1,
								'post_type'        => 'pantheon_event',
								'post_status'      => 'publish',
							  );
			$eventArray		= get_posts($eventArgs);
			$monthArray 	= array();
			$todayMonth 	= date('Ym');

			// Loop through all of our events
			// We're going to check their months/years. Then we're going to compare all of that to today's month/year. Then we're going to make that a thing we care about.
			// Usage: add to end of url ... ?event_period_month=201412 or ?event_period_month=march or ?event_period_start=20141101&event_period_end=20150228
			foreach($eventArray as $event){
				$eventID 	= $event->ID;

				if(get_field('agenda_items', $eventID)):
					while(has_sub_field('agenda_items', $eventID)):
						$date 	= get_sub_field('date');
						$month 	= date('Ym', strtotime($date));

						if(!in_array($month, $monthArray) && ($month >= $todayMonth) ){
							array_push($monthArray, $month);
						}
					endwhile;
					sort($monthArray);
				endif;

			}
			
			// Output our Month Filter Widget
			if(!empty($monthArray)){
				echo '<h3 class="event-month">Monthly Calendar</h3>';
				echo '<ul class="event-month">';
				foreach($monthArray as $month){
					$thisMonth 	= substr($month, -2);
					$thisYear 	= substr($month, 0, -2);
					$dateObj   = DateTime::createFromFormat('!m', $thisMonth);
					$monthName = $dateObj->format('F');
					echo '<li><a href="' . get_post_type_archive_link('pantheon_event') . '?event_period_month=' . $month . '">' . $monthName . ' ' . $thisYear . '</a>';
				}
				echo '</ul>';
			}
		?>



	</aside>
