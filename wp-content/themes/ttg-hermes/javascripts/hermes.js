//---------------------------
// INITIALIZE FOUNDATION
//---------------------------
jQuery(document).foundation();


//---------------------------
// DOCUMENT READY
//---------------------------
jQuery(document).ready(function($) {

	// Media Lightboxes
	//---------------------------
	$('.lightbox-video').iLightBox({
		skin: 'dark'
	});
	$('.lightbox-image').iLightBox({
		skin: 'dark'
	});

	// Back to Top
	//---------------------------
	$('.back-top').click(function(e) {
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		e.preventDefault();
	});

	// Event Filters
	//---------------------------
	var clickAllowed = true;
	// Click Function
	$('.event-filter h3').click(function(){
		if(clickAllowed) {
			var thisClass = $(this).attr('class');
			$('ul.' + thisClass).slideToggle('fast');
		}
	}); 
	// Resize 
	onResize = function() {
		// Allow click if screen is small enough
		if($(window).width() <= 1024){
			clickAllowed = true;
		}
		// Else, disallow
		else{
			clickAllowed = false;
		}
	}
	$(document).ready(onResize);
	$(window).bind('resize', onResize);

	// Event Location Expander
	//---------------------------
	$('.location-expander').click(function(){
		$('.location-expander-content').slideToggle();
		return false;
	});

	// Responsive Navigation
	//---------------------------
	$('nav.main ul.menu').tinyNav({
		active: 'selected',
		header: 'Navigation',
		indent: '- ',
	});
	$('.tinynav').customSelect();
	$('.customSelectInner').html('Navigation');
		
});
