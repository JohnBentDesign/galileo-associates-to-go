<?php
/*===========================================================================
Main page for events - a.k.a. Event Calendar
=============================================================================
*/

	// HEADER //
	get_template_part( 'parts/shared/header' );

	the_post();
?>

	<div class="container"> 

		<?php // TITLE // ?>
		<div class="small-12 columns">
			<h1 class="title-outside"><?php the_title(); ?></h1>
		</div>
	
		<div class="event-archive-type row">

			<?php // FILTERS // ?>
			<?php get_template_part( 'parts/events/sidebar', 'filters' ); ?>

			<?php // EVENT CONTENT // ?>
			<main class="large-10 columns">

				<?php
					// Display Location Content
					if(get_the_content()):
						$postExcerpt 	= $post->post_excerpt;
						$postContent 	= $post->post_content;
						$hasThumbnail 	= has_post_thumbnail($post->ID);
						$contentWidth 	= ($hasThumbnail) ? 'large-9' : 'large-12';
				?>
					<div class="location-content row">
						<?php // TITLE // ?>
						<div class="small-12 columns">
							<div class="primary inner clearfix">
							
								<header class="header-location">
									<div class="row">
										<?php if($hasThumbnail): ?>
											<div class="large-3 columns">
												<?php pantheon_display_post_featured_image($post->ID, $size = 'TTG Medium Thumbnail', $link = null, $output = 'image', $echo = true); ?>
											</div>
										<?php endif; ?>
										<div class="<?= $contentWidth; ?> columns">
											<div class="row">
												<div class="large-8 columns">
													<h1><?php the_title(); ?></h1>
												</div>
												<div class="large-4 columns">
													<?php pantheon_display_social_share($display = 'page', $type = 'post', $object = null, $content = null, $image = null, $url = null); ?>
												</div>
											</div>
											<div class="excerpt">
												<?php 
													if($postExcerpt){
														echo wpautop($post->post_excerpt);
													}
													else {
														the_content(); 
													}
												?>
											</div>
										</div>
									</div>
								</header>

							</div>	
							<?php if($postContent && $postExcerpt): ?>
								<a href="#" class="button location-expander tertiary">Read More About Location</a>
								<div class="location-expander-content inner primary">
									<?php the_content(); ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				<?php endif; ?>
			

				<?php
					// get events in this location
					$events = new WP_Query( array(
						'post_type' => 'pantheon_event',
						'meta_query' => array(
							array(
								'key' => 'event_location',
								'value' => '"' . get_the_ID() . '"',
								'compare' => 'LIKE'
							)
						)
					));

					if($events):
				?>
						<div class="location-events row">
							<div class="columns">

								<?php if($events->have_posts()): ?>
									<ul class="event-listing medium-block-grid-2 large-block-grid-3">
										<?php
											while ( $events->have_posts() ) : $events->the_post();
												get_template_part( 'parts/events/content', 'calendar' );
											endwhile;
										?>
									</ul>
								<?php endif; ?>

							</div>
						</div>

				<?php
					endif;
						
					wp_reset_postdata();
				?>

			</main>

		</div>

	</div>

<?php 
	// FOOTER //
	get_template_part( 'parts/shared/footer' );
