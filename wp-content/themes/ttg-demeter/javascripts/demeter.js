jQuery(document).foundation();

(function($){
	var expander,
		auto_open_delay,
		auto_open_timeout;
	
	function isEmpty( el )
	{
		return !$.trim(el.html());
	}

	function on_document_ready()
	{
		expander 			= $('.expander');
		display_type 		= expander.attr('data-expander-display');
		auto_open_delay 	= expander.attr('data-expander-timer');

		// Click Event
		expander.on('click', '.control', on_click_expander_control);
		
		// Resize Event
		$(window).on('resize', on_window_resize);

		// Starting Content Display
		contentToggle();

		// Open the Expander After Delay
		if( ($(window).outerWidth(true) > 640) && (!$('.validation_error').length > 0) && (expander.attr('data-expander-display') == 'animate'))
		{
			auto_open_timeout = setTimeout(open_expander, auto_open_delay);
		}

		// Responsive Nav
		$('nav.main ul.menu').tinyNav({
			header: '[ Navigate ]'
		});
	}

	function on_click_expander_control(e)
	{
		if( ($(window).outerWidth(true) < 640) )
		{
			return false;
		}
		else
		{
			clearTimeout(auto_open_timeout);
			toggle_expander();
			return false;
		}
	}

	function on_window_resize(e)
	{
		contentToggle();
	}

	function on_expander_open()
	{
		expander.addClass('active');
		$('.arrow-up-large span').removeClass('ion-chevron-up').addClass('ion-close');
		contentHeightMatch();
		$('aside.sidebar .content').fadeIn('fast');
	}

	function on_expander_close()
	{
		expander.removeClass("active");
		$('.arrow-up-large span').removeClass('ion-close').addClass('ion-chevron-up');
		$('div.main, aside.sidebar').css('min-height', 0);
		$('div.main .content').slideUp('slow');
	}

	function open_expander()
	{
		if(!isEmpty($('div.main .content')))
		{
			$('div.main .content').slideDown('slow', on_expander_open);
		}
	}

	function close_expander()
	{
		$('aside.sidebar .content').fadeOut('fast', on_expander_close);
	}

	function toggle_expander()
	{
		if(expander.hasClass("active")){
			close_expander();
		}
		else
		{
			open_expander();
		}
	}

	// Toggle Hiding/Functionality Based on Browser Width
	function contentToggle()
	{
		var windowSize = $(window).outerWidth(true);

		if(windowSize > 640)
		{
			var heightBrowser 	= $(window).outerHeight(true),
				heightHeader 	= $('header.main').outerHeight(true),
				heightFooter 	= $('footer.main').outerHeight(true),
				heightContent 	= heightBrowser - ( (heightHeader * 3) + heightFooter);

			// If our content's height is taller than the available space, we'll give it a max height and mark that it needs a scroll
			if($('.expander .content, .expander aside.sidebar').outerHeight(true) > heightContent){
				$('.expander .content, .expander aside.sidebar').css('max-height', heightContent).addClass('scrolling');
			}

			// If we don't want to close our content on load OR if we have a validation error, we need to keep the expander open
			if( ($('.validation_error').length > 0) || (expander.attr('data-expander-display') == 'display') )
			{
				$('.expander div.main, .expander aside.sidebar').removeAttr('style');
				expander.addClass('active');
				$('.content', expander).show();
				contentHeightMatch();
			}
			// Hide content if it's not set as active and if our window is large enough
			else if(!$('.expander').hasClass('active') || (expander.attr('data-expander-display') == 'closed') )
			{
				$('.expander div.main, .expander aside.sidebar').removeAttr('style');
				$('.content', expander).hide();
			}
			// Hide content with animation if it was active and our window is large enough
			else if($('.expander').hasClass('active') )
			{
				// Reset our min-heights
				$('.expander div.main, .expander aside.sidebar').removeAttr('style');
				contentHeightMatch();
			}
		}
		// Show the content by default if the window is small enough
		else {
			expander.addClass('active');
			$('.content', expander).show();
			$('.expander div.main, .expander aside.sidebar, .expander .content').removeAttr('style').removeClass('scrolling');
		}
	}

	// Match main content and sidebar heights (important to bottom align content in sidebar)
	function contentHeightMatch()
	{
		var contentHeight 	= $('div.main').outerHeight(true),
			sidebarHeight 	= ($('aside.sidebar').outerHeight(true) + $('aside.sidebar .content').outerHeight(true)) + 10,
			heightArray 	= [contentHeight, sidebarHeight],
			biggestHeight 	= Math.max.apply(Math, heightArray);
		$('.expander div.main, .expander aside.sidebar').css('min-height', biggestHeight);
	}

	$(on_document_ready);
})(jQuery);