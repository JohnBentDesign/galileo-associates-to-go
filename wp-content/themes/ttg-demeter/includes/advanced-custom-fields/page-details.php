<?php
	/*===========================================================================
	DEMETER: PAGE FIELDS
	===========================================================================*/
	// Page custom fields
	
	if( function_exists('register_field_group') ):

	register_field_group(array (
	    'key' => 'group_5423132960220',
	    'title' => 'Backdrop',
	    'fields' => array (
	        array (
	            'key' => 'field_542180eed18eb',
	            'label' => 'Backdrop Type',
	            'name' => 'backdrop_type',
	            'prefix' => '',
	            'type' => 'radio',
	            'instructions' => '',
	            'required' => 1,
	            'conditional_logic' => 0,
	            'wrapper' => array (
	                'width' => 50,
	                'class' => '',
	                'id' => '',
	            ),
	            'choices' => array (
	                'none' => 'None',
	                'image' => 'Image',
	                'slider' => 'Slider',
	            ),
	            'other_choice' => 0,
	            'save_other_choice' => 0,
	            'default_value' => '',
	            'layout' => 'horizontal',
	        ),
	        array (
	            'key' => 'field_5421a7e0c5c97',
	            'label' => 'Backdrop Height',
	            'name' => 'backdrop_height',
	            'prefix' => '',
	            'type' => 'radio',
	            'instructions' => 'Choose the height of the backdrop',
	            'required' => 0,
	            'conditional_logic' => array (
	                array (
	                    array (
	                        'field' => 'field_542180eed18eb',
	                        'operator' => '!=',
	                        'value' => 'none',
	                    ),
	                ),
	            ),
	            'wrapper' => array (
	                'width' => 50,
	                'class' => '',
	                'id' => '',
	            ),
	            'choices' => array (
	                'short' => 'Short (300px)',
	                'medium' => 'Medium (450px)',
	                'tall' => 'Tall (600px)',
	            ),
	            'other_choice' => 0,
	            'save_other_choice' => 0,
	            'default_value' => 'medium',
	            'layout' => 'horizontal',
	        ),
	        array (
	            'key' => 'field_52fa6cdf02d67',
	            'label' => 'Backdrop Image',
	            'name' => 'backdrop_image',
	            'prefix' => '',
	            'type' => 'image',
	            'instructions' => '',
	            'required' => 1,
	            'conditional_logic' => array (
	                array (
	                    array (
	                        'field' => 'field_542180eed18eb',
	                        'operator' => '==',
	                        'value' => 'image',
	                    ),
	                ),
	            ),
	            'wrapper' => array (
	                'width' => '',
	                'class' => '',
	                'id' => '',
	            ),
	            'preview_size' => 'medium',
	            'library' => 'all',
	            'return_format' => 'array',
	        ),
	        array (
	            'key' => 'field_54db9b181722b',
	            'label' => 'Make Backdrop Image a Link?',
	            'name' => 'backdrop_image_has_url',
	            'prefix' => '',
	            'type' => 'true_false',
	            'instructions' => '',
	            'required' => 1,
	            'conditional_logic' => array (
	                array (
	                    array (
	                        'field' => 'field_542180eed18eb',
	                        'operator' => '==',
	                        'value' => 'image',
	                    ),
	                ),
	            ),
	            'wrapper' => array (
	                'width' => '',
	                'class' => '',
	                'id' => '',
	            ),
	            'message' => '',
	            'default_value' => 0,
	        ),
	        array (
	            'key' => 'field_54db9a52809e1',
	            'label' => 'Backdrop Image URL',
	            'name' => 'backdrop_image_url',
	            'prefix' => '',
	            'type' => 'url',
	            'instructions' => '',
	            'required' => 1,
	            'conditional_logic' => array (
	                array (
	                    array (
	                        'field' => 'field_542180eed18eb',
	                        'operator' => '==',
	                        'value' => 'image',
	                    ),
	                    array (
	                        'field' => 'field_54db9b181722b',
	                        'operator' => '==',
	                        'value' => '1',
	                    ),
	                ),
	            ),
	            'wrapper' => array (
	                'width' => '',
	                'class' => '',
	                'id' => '',
	            ),
	            'default_value' => '',
	            'placeholder' => 'http://yourURL.com',
	        ),
	        array (
	            'key' => 'field_542181bfd18f0',
	            'label' => 'Backdrop Slider Speed',
	            'name' => 'backdrop_slider_speed',
	            'prefix' => '',
	            'type' => 'number',
	            'instructions' => 'In milliseconds',
	            'required' => 1,
	            'conditional_logic' => array (
	                array (
	                    array (
	                        'field' => 'field_542180eed18eb',
	                        'operator' => '==',
	                        'value' => 'slider',
	                    ),
	                ),
	            ),
	            'wrapper' => array (
	                'width' => 50,
	                'class' => '',
	                'id' => '',
	            ),
	            'default_value' => 5000,
	            'placeholder' => '',
	            'prepend' => '',
	            'append' => '',
	            'min' => 0,
	            'max' => '',
	            'step' => 1,
	            'readonly' => 0,
	            'disabled' => 0,
	        ),
	        array (
	            'key' => 'field_5421a6fb12322',
	            'label' => 'Backdrop Slider Fade Speed',
	            'name' => 'backdrop_slider_fade',
	            'prefix' => '',
	            'type' => 'number',
	            'instructions' => 'In milliseconds',
	            'required' => 1,
	            'conditional_logic' => array (
	                array (
	                    array (
	                        'field' => 'field_542180eed18eb',
	                        'operator' => '==',
	                        'value' => 'slider',
	                    ),
	                ),
	            ),
	            'wrapper' => array (
	                'width' => 50,
	                'class' => '',
	                'id' => '',
	            ),
	            'default_value' => 750,
	            'placeholder' => '',
	            'prepend' => '',
	            'append' => '',
	            'min' => 0,
	            'max' => '',
	            'step' => 1,
	            'readonly' => 0,
	            'disabled' => 0,
	        ),
	        array (
	            'key' => 'field_5421811fd18ec',
	            'label' => 'Backdrop Slider',
	            'name' => 'backdrop_slider',
	            'prefix' => '',
	            'type' => 'repeater',
	            'instructions' => '',
	            'required' => 1,
	            'conditional_logic' => array (
	                array (
	                    array (
	                        'field' => 'field_542180eed18eb',
	                        'operator' => '==',
	                        'value' => 'slider',
	                    ),
	                ),
	            ),
	            'wrapper' => array (
	                'width' => 50,
	                'class' => '',
	                'id' => '',
	            ),
	            'min' => '',
	            'max' => '',
	            'layout' => 'table',
	            'button_label' => 'Add Slider',
	            'sub_fields' => array (
	                array (
	                    'key' => 'field_54218139d18ed',
	                    'label' => 'Image',
	                    'name' => 'image',
	                    'prefix' => '',
	                    'type' => 'image',
	                    'instructions' => '',
	                    'required' => 0,
	                    'conditional_logic' => 0,
	                    'wrapper' => array (
	                        'width' => '',
	                        'class' => '',
	                        'id' => '',
	                    ),
	                    'preview_size' => 'medium',
	                    'library' => 'all',
	                    'return_format' => 'array',
	                ),
	            ),
	        ),
	        array (
	            'key' => 'field_54dba1ebaa980',
	            'label' => 'Slide Links',
	            'name' => 'backdrop_slide_urls',
	            'prefix' => '',
	            'type' => 'repeater',
	            'instructions' => 'Be sure to include the same number of links as there are slides, or this functionality will break.',
	            'required' => 1,
	            'conditional_logic' => array (
	                array (
	                    array (
	                        'field' => 'field_542180eed18eb',
	                        'operator' => '==',
	                        'value' => 'slider',
	                    ),
	                    array (
	                        'field' => 'field_54dba1b1aa97f',
	                        'operator' => '==',
	                        'value' => '1',
	                    ),
	                ),
	            ),
	            'wrapper' => array (
	                'width' => 50,
	                'class' => '',
	                'id' => '',
	            ),
	            'min' => '',
	            'max' => '',
	            'layout' => 'row',
	            'button_label' => 'Add URL',
	            'sub_fields' => array (
	                array (
	                    'key' => 'field_54dba249aa983',
	                    'label' => 'Slide Url',
	                    'name' => 'slide_url',
	                    'prefix' => '',
	                    'type' => 'url',
	                    'instructions' => '',
	                    'required' => 0,
	                    'conditional_logic' => 0,
	                    'wrapper' => array (
	                        'width' => '',
	                        'class' => '',
	                        'id' => '',
	                    ),
	                    'default_value' => '',
	                    'placeholder' => '',
	                ),
	            ),
	        ),
	        array (
	            'key' => 'field_54dba1b1aa97f',
	            'label' => 'Should slides have links?',
	            'name' => 'backdrop_slider_has_links',
	            'prefix' => '',
	            'type' => 'true_false',
	            'instructions' => '',
	            'required' => 1,
	            'conditional_logic' => array (
	                array (
	                    array (
	                        'field' => 'field_542180eed18eb',
	                        'operator' => '==',
	                        'value' => 'slider',
	                    ),
	                ),
	            ),
	            'wrapper' => array (
	                'width' => 50,
	                'class' => '',
	                'id' => '',
	            ),
	            'message' => '',
	            'default_value' => 0,
	        ),
	    ),
	    'location' => array (
	        array (
	            array (
	                'param' => 'page_template',
	                'operator' => '==',
	                'value' => 'default',
	            ),
	        ),
	        array (
	            array (
	                'param' => 'page_template',
	                'operator' => '==',
	                'value' => 'page-expander.php',
	            ),
	        ),
	        array (
	            array (
	                'param' => 'taxonomy',
	                'operator' => '==',
	                'value' => 'product_cat',
	            ),
	        ),
	    ),
	    'menu_order' => 100,
	    'position' => 'acf_after_title',
	    'style' => 'default',
	    'label_placement' => 'top',
	    'instruction_placement' => 'label',
	    'hide_on_screen' => array (
	        0 => 'excerpt',
	        1 => 'custom_fields',
	        2 => 'discussion',
	        3 => 'comments',
	        4 => 'featured_image',
	        5 => 'categories',
	        6 => 'tags',
	        7 => 'send-trackbacks',
	    ),
	));

	endif;