<?php
	/*===========================================================================
	DEMETER: EXPANDER SETTINGS
	===========================================================================*/
	// Expander Settings
	
	if( function_exists('register_field_group') ):

	register_field_group(array (
		'key' => 'group_542313295c4cd',
		'title' => 'Expander Settings',
		'fields' => array (
			array (
				'key' => 'field_5421882910801',
				'label' => 'Display content on load or animate content open on load?',
				'name' => 'expander_onload',
				'prefix' => '',
				'type' => 'radio',
				'instructions' => 'User will still have the option to close or expand the content manually.',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => 50,
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					'closed' => 'Keep Closed on Load',
					'animate' => 'Animate on Load',
					'display' => 'Display on Load',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => 'closed',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_5421890010802',
				'label' => 'Time before slider expands on load',
				'name' => 'expander_animate_time',
				'prefix' => '',
				'type' => 'number',
				'instructions' => 'The amount of time taken before the expanding content on the homepage expands. Number is in milliseconds.',
				'required' => 1,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_5421882910801',
							'operator' => '==',
							'value' => 'animate',
						),
					),
				),
				'wrapper' => array (
					'width' => 50,
					'class' => '',
					'id' => '',
				),
				'default_value' => 5000,
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 0,
				'max' => '',
				'step' => 1,
				'readonly' => 0,
				'disabled' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-expander.php',
				),
			),
		),
		'menu_order' => 1,
		'position' => 'acf_after_title',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));

	endif;