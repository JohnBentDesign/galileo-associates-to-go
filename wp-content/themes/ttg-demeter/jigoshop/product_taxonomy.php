<?php
	/**
	 * Product taxonomy template
	 *
	 * DISCLAIMER
	 *
	 * Do not edit or add directly to this file if you wish to upgrade Jigoshop to newer
	 * versions in the future. If you wish to customise Jigoshop core for your needs,
	 * please use our GitHub repository to publish essential changes for consideration.
	 *
	 * @package             Jigoshop
	 * @category            Catalog
	 * @author              Jigoshop
	 * @copyright           Copyright © 2011-2014 Jigoshop.
	 * @license             GNU General Public License v3
	 */

	// VARIABLES //
	$term 	= get_term_by( 'slug', get_query_var($wp_query->query_vars['taxonomy']), $wp_query->query_vars['taxonomy']);
	$title 		= apply_filters('jigoshop_product_taxonomy_header', '<h1 class="page-title">' . wptexturize( $term->name ) . '</h1>');

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>

<?php
	// JIGOSHOP: BEFORE CONTENT //
	do_action('jigoshop_before_main_content');
?>

	<div class="shop-list primary">
		<div class="row">

			<?php
				// PRODUCT LISTING //
			?>
			<div class="medium-7 columns">

				<?php // Header //?>
				<header>
					<h1><?= $title; ?></h1>
					<div class="product-nav top">
						<?php get_template_part('parts/shared/pagination'); ?>
					</div>
				</header>

				<?php
					ob_start();
					jigoshop_get_template_part( 'loop', 'shop' );
					$products_list_html = ob_get_clean();
					echo apply_filters( 'jigoshop_products_list', $products_list_html );
				?>

				<footer>
					<div class="product-nav bottom">
						<?php get_template_part('parts/shared/pagination'); ?>
					</div>
				</footer>

			</div>

			<?php
				// SIDEBAR //
				get_template_part('parts/shared/sidebar');
			?>

		</div>
	</div>

	<div class="product-nav bottom columns clearfix">
		<?php get_template_part('parts/shared/pagination'); ?>
	</div>

<?php
	// JIGOSHOP: AFTER CONTENT //
	do_action('jigoshop_after_main_content');

	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>