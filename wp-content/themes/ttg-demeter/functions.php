<?php
	/*===========================================================================
	INCLUDE THE INCLUDES
	===========================================================================*/
	// Advanced Custom Fields
	include('includes/advanced-custom-fields/theme-styling.php');
	include('includes/advanced-custom-fields/page-expander.php');
	include('includes/advanced-custom-fields/page-details.php');



	/*===========================================================================
	ADD THEME SUPPORTS
	===========================================================================*/
	add_theme_support('post-thumbnails');
	add_theme_support('menus');



	/*===========================================================================
	CUSTOM THUMBNAIL SIZES
	===========================================================================*/
	add_image_size('TTG Featured Image', 1024, 500, true);
	add_image_size('TTG Gallery Normal Display', 637, 9999, false);
	add_image_size('TTG Medium Thumbnail', 350, 350, true);



	/*===========================================================================
	REGISTER MENUS
	===========================================================================*/
	if(!function_exists('ttg_menus')){
		function ttg_menus() {
			register_nav_menus(array(
				'menu-main' 		=> 'Main Menu',
				'menu-top' 			=> 'Top Menu',
				'menu-footer' 		=> 'Footer Menu'
			));
		}
		add_action('init', 'ttg_menus');
	}



	/*===========================================================================
	REGISTER SIDEBARS
	===========================================================================*/
	$sidebarGeneral = array(
		'name'          => 'General Sidebar',
		'id'            => 'sidebar-general',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	);
	register_sidebar($sidebarGeneral);

	$sidebarExpander = array(
		'name'          => 'Expander Sidebar',
		'id'            => 'sidebar-expander',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	);
	register_sidebar($sidebarExpander);

	$sidebarBlog = array(
		'name'          => 'Blog Sidebar',
		'id'            => 'sidebar-blog',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	);
	register_sidebar($sidebarBlog);


	function ttg_sidebar_jigoshop() {
		// Sometimes Jigoshop is de-activated - do nothing without it
		if(!class_exists('jigoshop')) {
			return;
		}
		else {
			// Add Shop Sidebar
			$sidebarShop = array(
				'name'          => 'Shop Sidebar',
				'id'            => 'sidebar-shop',
				'description'   => '',
				'class'         => '',
				'before_widget' => '<div class="widget">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>'
			);
			register_sidebar($sidebarShop);
		}
	}
	add_action('after_setup_theme', 'ttg_sidebar_jigoshop');



	/*===========================================================================
	ENQUEUE SCRIPTS AND STYLES
	===========================================================================*/
	// JAVASCRIPTS
	//---------------------------------------------------------------------------
	function ttg_scripts(){
		if(!is_admin()){
			// Plugins
			wp_enqueue_script('tinynav', get_template_directory_uri() . '/javascripts/tinynav.min.js', array('jquery'), '', true);
			wp_enqueue_script('backstretch', get_template_directory_uri() . '/javascripts/jquery.backstretch.min.js', array('jquery'), '', false);

			// Demeter Scripts
			wp_enqueue_script('demeter', get_template_directory_uri() . '/javascripts/demeter.js', array('jquery', 'foundation', 'tinynav'), '', true);
		}
	}
	add_action('wp_enqueue_scripts', 'ttg_scripts');

	// STYLESHEETS
	//---------------------------------------------------------------------------
	function ttg_styles(){
		if(!is_admin()){
			// Webfonts
			wp_enqueue_style('fonts-opensans', 'http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700|Open+Sans:400,700,300');

			// Demeter Stylesheets
			wp_enqueue_style('demeter', get_template_directory_uri() . '/stylesheets/css/style.css');
			wp_enqueue_style('responsive', get_template_directory_uri() . '/stylesheets/responsive.css');
		}
	}
	add_action('wp_enqueue_scripts', 'ttg_styles');

	// RECOMPILES THE CSS ONLY WHEN THE OPTIONS PAGE IS SAVED
	//---------------------------------------------------------------------------
	function ttg_compile_css( $post_id ) {
		$page = htmlspecialchars($_GET["page"]);
		if($page == 'acf-options-theme-styling'){
			// Compile our CSS
			include(get_template_directory() . '/stylesheets/php-sass-watcher.php');
		}
	}
	// run after ACF saves the $_POST['fields'] data
	add_action('acf/save_post', 'ttg_compile_css', 20);

