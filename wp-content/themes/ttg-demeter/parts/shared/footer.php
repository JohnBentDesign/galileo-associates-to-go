<?php
	/*===========================================================================
	FOOTER- MAIN
	===========================================================================*/
	// Contains the copyright and footer navigation
?>	
	<footer class="tertiary main row">
		
		<?php
			// Let's figure out if we have both the connect and share buttons in the footer
			$maxSocialize = ( (is_array(get_field('social_button_location', 'options')) && in_array('Header', get_field('social_button_location', 'options')) ) && ( get_field('share_button_location', 'options') && in_array('Header', get_field('share_button_location', 'options')) ) ) ? ' max-socialize' : '';
		?>

		<div class="medium-12 large-7 large-push-5 columns">
			<?php
				// SOCIAL- CONNECT
				//===================================
				pantheon_display_social_connect('footer');

				// SOCIAL- SHARE
				//===================================
				pantheon_display_social_share('footer', 'post', $post);
			?>

			<?php
				// FOOTER NAVIGATION
				//===================================
				$menuFooter = array(
					'theme_location'  => 'menu-footer',
					'container'       => 'nav',
					'container_class' => 'footer',
					'menu_class'      => 'menu',
					'depth'           => 1,
					'fallback_cb'		=> 0
				);
				wp_nav_menu($menuFooter);
			?>
		</div>

		<div class="medium-12 large-5 large-pull-7 columns<?= $maxSocialize; ?>">
			<div class="copyright">
				<p>&copy;<?php echo date('Y') . ' '; bloginfo('name'); ?>. Site Powered by <a href="http://techtherapytogo.com/" target="_blank">Technology Therapy To Go</a>.</p>
			</div>
		</div>

	</footer>