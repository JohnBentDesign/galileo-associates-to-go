<?php
	/*===========================================================================
	SIDEBAR: MAIN
	=============================================================================
	Right hand sidebar that appears on most pages.
	*/

	$hasExpander 	= is_page_template('page-expander.php');
	$postType 		= get_post_type();
?>
<aside class="sidebar medium-5 large-4 columns">
	<div class="content">

		<?php
			// SIDEBAR - EXPANDER
			//===================================
			if($hasExpander){
				if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Expander Sidebar') ): endif;
			}

			else {
				// SIDEBAR - GENERAL
				//===================================
				if(is_page()){
					if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('General Sidebar') ): endif;
				}

				// SIDEBAR - BLOG
				//===================================
				if( ($postType == 'post') || is_search() ){
					if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar') ): endif;
				}

				// SIDEBAR - SHOP
				//===================================
				if( ($postType == 'product') ){
					if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Shop Sidebar') ): endif;
				}
			}
		?>

	</div>
</aside><?php // /.sidebar ?>