			<?php
				// FOOTER // 
				get_template_part('parts/shared/footer');
			?>

		<?php 
			global $hasExpander;
			if($hasExpander):
		?>
			</div><?php // /.sticky ?>
		<?php endif; ?>

		<?php 
			// WORDPRESS FOOTER //
			wp_footer();

			// CUSTOM JAVASCRIPT //
			$customScript = get_field('advanced_js', 'options');
			if($customScript){
				echo $customScript;
			}
		?>

	</body>
</html>