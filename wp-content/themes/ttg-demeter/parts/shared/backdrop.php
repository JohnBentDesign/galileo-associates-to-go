<?php
	/*===========================================================================
	BACKDROP
	===========================================================================*/
	// Get Variables
	global $hasExpander;
	global $jigoshopShopID;

	// Get some IDs
	$isBlog 		= ((is_home() && get_option('page_for_posts')) || (is_single() && ($post->post_type == 'post')) || is_category() || is_day() || is_tag() || is_month() || is_year()) ? true : false;
	$blogID 		= get_option('page_for_posts');
	$postType 		= get_post_type();
	$isProductArch 	= ($postType == 'product') && is_archive();

	// Figure out where we're pulling our backdrop from
	if(is_post_type_archive('product' )) 						{ 	$useID = $jigoshopShopID; }
	elseif($isProductArch)										{
																	$term 			= get_query_var($wp_query->query_vars['taxonomy']);
																	$tax 			= $wp_query->query_vars['taxonomy'];
																	$termObj 		= get_term_by('slug', $term, $tax);
																	$useID 			= $termObj;
																}
	elseif($isBlog)												{ $useID = $blogID; }
	elseif($postType == 'product') 								{ $useID = $jigoshopShopID; }
	elseif( ($postType == 'post') || ($postType == 'page') )	{ $useID = $post->ID; }
	else 														{ $useID = ''; }
?>

	<?php
		// Figure out what the backdrop will be displaying
		$backdropType 	= get_field('backdrop_type', $useID);
		$backdropHeight = ($hasExpander) ? ' full-height' : get_field('backdrop_height', $useID);
	?>

   <?php
		// IMAGE //
		if( ($backdropType == 'image') && $useID ):
			$backdropObj       	= get_field('backdrop_image', $useID);
			$backdropAlt       	= $backdropObj['alt'];
			$backdropImage     	= pantheon_display_post_field_image($backdropObj, 'full', 'style', false);
			
			// Optional Link for Image
			$backdropHasLink 	= get_field('backdrop_image_has_url', $useID);
			$backdropLink      	= get_field('backdrop_image_url', $useID);
	?>

			<?php 
				// Check if image is set to have a link
				if($backdropHasLink):
			?>
					<a href="<?= $backdropLink; ?>">
						<div class="backdrop static <?= $postType . ' ' . $backdropHeight; ?>" <?= $backdropImage; ?>>
							<img src="<?= get_template_directory_uri(); ?>/images/spacer.gif" alt="<?= $backdropAlt; ?>" />
						</div>
					</a>
			<?php else: ?>
					<div class="backdrop static <?= $postType . ' ' . $backdropHeight; ?>" <?= $backdropImage; ?>>
						<img src="<?= get_template_directory_uri(); ?>/images/spacer.gif" alt="<?= $backdropAlt; ?>" />
					</div>
			<?php endif; ?>

	<?php
		elseif($isProductArch):
			if(($imageDisplay != 'none') || $isProductArch){
				if(!$isProductArch || is_post_type_archive('product')){
					$imageID 		= ($imageDisplay == 'default') ? 'options' : $useID;
					$imageObj 		= get_field('backdrop_image', $imageID);
				}
				else {
					$imageGet	= jigoshop_product_cat_image($termObj->term_id);
					$prodImage	= ($imageGet && !strpos($imageGet['image'], 'placeholder.png')) ? ' style="background-image: url(' . $imageGet['image'] . ')"' : '';
					$imageObj 	= '';
				}
			}

			if($imageObj || $prodImage):
	?>
				<div class="backdrop static short <?= $postType; ?>" <?= $prodImage; ?>>
					<img src="<?= get_template_directory_uri(); ?>/images/spacer.gif" alt="<?= $backdropAlt; ?>" />
				</div>
	<?php 	
			endif;
		endif;
	?>

	<?php
		// Backdrop Slider
		if( ($backdropType == 'slider') && $useID ):
			$backdropSpeed          = get_field('backdrop_slider_speed', $useID);
			$backdropFade           = get_field('backdrop_slider_fade', $useID);
			$backdropCenter         = get_field('backdrop_slider_center', $useID);
			$backdropSlider         = get_field('backdrop_slider', $useID);
			$backdropSliderHasLinks = get_field('backdrop_slider_has_links', $useID);
			$firstUrl = "";
			// Having a dumb moment where I can't figure out how to get the first item, so I'm running a loop and breakng after the first iteration.
			while( have_rows('backdrop_slide_urls') ): the_row();
				$firstUrl = get_sub_field('slide_url');
				break;
			endwhile;
	?>

	<?php if($backdropSliderHasLinks): ?>
		<div class="backdrop slider <?= $postType . ' ' . $backdropHeight; ?>">
			<a id="backstretch-url" href="<?= $firstUrl; ?>">
				<div class="backdrop-images">
					<img src="<?= get_template_directory_uri(); ?>/images/spacer.gif" alt="design element" class="spacer" />
				</div>
		   </a>
		</div>
	 <?php else: ?>
		<div class="backdrop slider <?= $postType . ' ' . $backdropHeight; ?>">
			<div class="backdrop-images">
				<img src="<?= get_template_directory_uri(); ?>/images/spacer.gif" alt="design element" class="spacer" />
			</div>
		</div>
	<?php endif; ?>

	<script>
		jQuery('.backdrop.slider .backdrop-images').backstretch([
		<?php
		   while(have_rows('backdrop_slider', $useID)): the_row();
		   $imageObj = get_sub_field('image');
		   $imageURL = $imageObj['url'];
		   echo '"' . $imageURL . '",';
		   endwhile;
		   ?>
		   ], {
			   fade: <?= $backdropFade; ?>,
			   duration: <?= $backdropSpeed; ?>
		});

		jQuery(document).ready(function() {
		   var sliderUrls = [
			<?php
				// build array of slide urls
				while(have_rows('backdrop_slide_urls', $useID)): the_row();
				$slideUrl = get_sub_field('slide_url');
				echo '"' . $slideUrl . '",';
				endwhile;
			?>
			];

			jQuery(window).on("backstretch.before", function (e, instance, index) {
				jQuery("#backstretch-url").prop("href", sliderUrls[index]);
			});
		});
	</script>
	<?php endif; ?>
