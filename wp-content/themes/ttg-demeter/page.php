<?php
	/*===========================================================================
	INDEX
	=============================================================================
	Default fallback style. Default page for blog viewing page
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');

	// Check if we have an expander
	global $hasExpander;

	// Get Expander for expander type pages
	if($hasExpander){
		$expander 	= ( get_field('expander_onload') ) ? ' data-expander-display="' . get_field('expander_onload') . '"' : '';
		$timer 		= ( get_field('expander_animate_time') && $expander ) ? ' data-expander-timer="' . get_field('expander_animate_time') . '"' : '';
		$fullPage 	= '';
	}
	else {
		$expander = $timer = '';
		$fullPage = 'full primary';
	}
?>
	<div class="container <?= $fullPage . $hasExpander; ?>" <?= $expander . $timer; ?>>

		<?php if($hasExpander): ?>
			<div class="row">
				<div class="medium-7 large-6 columns">
					<h1>
						<a href="#" class="control">
							<span class="arrow-up-large"><span class="ion-chevron-up"></span></span>
							<?php the_title(); ?>
						</a>
					</h1>
				</div>
				<div class="medium-5 large-4 columns">
					<?php pantheon_display_social_connect('page'); ?>
				</div>
			</div>
		<?php endif; ?>

		<div class="row">
			<div class="main medium-7 large-6 columns">
				<?php 
					if(have_posts()):
						while(have_posts()): the_post();
				?>
						<div class="content">
							<?php if(!$hasExpander): ?>
								<h1><?php the_title(); ?></h1>
							<?php endif; ?>

							<?php the_content(); ?>
						</div>
				<?php
						endwhile;
					endif;
				?>			
			</div>

			<?php
				// SIDEBAR //
				get_template_part('parts/shared/sidebar');
			?>
		</div>

	</div>

<?php
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>