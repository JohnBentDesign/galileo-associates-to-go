<?php
	/*===========================================================================
	Template Name: Homepage
	=============================================================================
	Template specifically for making it easier to target ACF display
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>

	<?php
	// SLIDER //
	if(have_rows('home_slider')):
		// Slider Overall Settings
		$slide 	= 	array(
			'overlay' 		=> get_field('home_slide_overlay'),
			'time'			=> get_field('home_slide_time'),
			'transition'	=> get_field('home_slide_time_animate'),
			'type'			=> get_field('home_slide_animate'),
			'arrows'		=> get_field('home_slide_arrows'),
			'nodes'			=> get_field('home_slide_nodes'),
			'florish' 		=> get_field('home_slide_florish'),
			'types'			=> get_field('home_slide_types'),
			'overlay-type' 	=> get_field('home_slide_overlay_type')
		);
	?>
		<div class="home-slideshow">
			<div class="home-slider" data-slider-time="<?= $slide['time']; ?>" data-slider-animate="<?= $slide['transition']; ?>" data-slider-fade="<?= $slide['type']; ?>" data-slider-arrows="<?= $slide['arrows']; ?>" data-slider-nodes="<?= $slide['nodes']; ?>">

				<?php
					// CUSTOM SLIDES //
					if(in_array('custom', $slide['types'])):
						while ( have_rows('home_slider') ) : the_row();
							// Slide Information
							$image 			= get_sub_field('image');
							$title 			= get_sub_field('title') ? '<h2>' . get_sub_field('title') . '</h2>' : '';
							$content 		= get_sub_field('content');
							$ctaDisplay 	= get_sub_field('cta_display');
							$ctaText 		= get_sub_field('cta_text');
							$ctaType 		= get_sub_field('cta_type');
							$ctaInternal 	= get_sub_field('cta_internal');
							$ctaExternal 	= get_sub_field('cta_external');
							$ctaOutput 		= ($ctaDisplay) ? '<a href="' . $ctaInternal . $ctaExternal . '" class="button">' . $ctaText . '</a>' : '';
							$overlay 		= (($slide['overlay'] != 'none') && ($slide['overlay-type'] == 'block')) ? ' overlay-' . $slide['overlay'] : '';
				?>
							<div class="slide" style="background-image: url(<?= $image['sizes']['large']; ?>)">
								<?php if($title || $content || $ctaOutput): ?>
									<div class="slide-content<?= $overlay; ?>">
										<?= $title . $content . $ctaOutput; ?>
									</div>
								<?php endif; ?>

								<?php if(($slide['overlay'] != 'none') && ($slide['overlay-type'] == 'fade')): ?>
									<div class="overlay overlay-grad-<?= $slide['overlay']; ?>"></div>
								<?php endif; ?>
							</div>
				<?php 
						endwhile;
					endif;
				?>

				<?php 
					// BLOG & PORTFOLIO SLIDES //
					homeSlideLoop('post', get_field('home_slide_count_blog'), $slide);
					homeSlideLoop('portfolio', get_field('home_slide_count_portfolio'), $slide);
				?>

				<?php if($slide['florish']): ?>
					<?php get_template_part('parts/page/home', 'curve'); ?>
				<?php endif; ?>

			</div>
		</div>
	<?php endif; ?>

	<div class="container">

		<?php
			if(have_posts()):
				while(have_posts()): the_post();

					$sections = get_field('section');

					if($sections):
						$i = 1;
						$rowing = 0;
						foreach($sections as $section):
							// Our columns need to be cleared at the right times... So let's row!
							$sectionWidth 	= $section['section_width'];
							$rowContainer 	= iris_rowing($sectionWidth, $rowing, $i);
							$rowing 		= $rowContainer['rowing'];

							echo $rowContainer['open'];

							// Build our section
							$build = new iris_section_build($section);

							// Output our column
							$build->section_construct($i);

							if($rowContainer['close']){
								echo $rowContainer['close'];
							}

							// Increment our section
							$i++;
						endforeach;
					endif;

					// var_dump($rowing);

				endwhile;
			endif;
		?>

	</div>

<?php
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>