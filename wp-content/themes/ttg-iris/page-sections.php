<?php
	/*===========================================================================
	Template Name: Sectional
	=============================================================================
	Template for displaying varying box sizes
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');

	if(post_password_required()):
		get_template_part('parts/shared/password');

	else:
?>

	<div class="container">

		<?php 
			if(have_posts()):
				while(have_posts()): the_post();

					$sections = get_field('section');

					if($sections):
						$i = 1;
						$rowing = 0;
						foreach($sections as $section):
							// Our columns need to be cleared at the right times... So let's row!
							$sectionWidth 	= $section['section_width'];
							$rowContainer 	= iris_rowing($sectionWidth, $rowing, $i);
							$rowing 		= $rowContainer['rowing'];

							echo $rowContainer['open'];

							// Build our section
							$build = new iris_section_build($section);

							// Output our column
							$build->section_construct($i);

							echo $rowContainer['close'];

							// Increment our section
							$i++;
						endforeach;
					endif;

					// var_dump($rowing);

				endwhile;
			endif;
		?>			

	</div>

<?php
	endif;

	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>