(function($){
	$(document).foundation();

	$(document).ready(function(){

		//===================================================
		// HOMEPAGE SLIDER
		//===================================================
		var $slider = $('.home-slider');

		// Show only if homepage slider is available
		if($slider.length > 0){
			// Variables
			var $slideFade 		= $slider.attr('data-slider-fade'),
            $slideTime 		= $slider.attr('data-slider-time'),
            $slideAnimate 	= $slider.attr('data-slider-animate'),
            $slideNodes 	= $slider.attr('data-slider-nodes'),
            $slideArrows 	= $slider.attr('data-slider-arrows');

            if($slideFade == 1) { $slideFade = true; }
            else { $slideFade = false; }

            if($slideNodes == 1) { $slideNodes = true; }
            else { $slideNodes = false; }

            if($slideArrows == 1) { $slideArrows = true; }
            else { $slideArrows = false; }

			// Initialize Slideshow
			$('.home-slider').slick({
				autoplay: true,
				autoplaySpeed: $slideTime,
				arrows: $slideArrows,
				dots: $slideNodes,
				fade: $slideFade,
				infinite: true,
				lazyLoad: 'ondemand',
				speed: $slideAnimate,
			});
		}


		//===================================================
		// RESPONSIVE NAVIGATION
		//===================================================
		// Responsive Navigation
		//---------------------------
		$('nav.main ul.menu').tinyNav({
			active: 'selected',
			header: 'Navigation',
			indent: '- ',
		});
		$('.tinynav').customSelect();
		$('.customSelectInner').html('Navigation');


		//===================================================
		// SECTIONS
		//===================================================
		// Hover Effect Text Color
        function hoverTextColor() {
            var blockHoverTextColors = '';
            $('.animate-me').each(function() {
                var hasHoverBgColor = $(this).data('hover-bg-color');
                var hasHoverTextColor = $(this).data('hover-text-color');
                if(hasHoverBgColor) {
                    blockHoverTextColors += '[data-hover-bg-color="' + hasHoverBgColor + '"].animate-me:before { background-color:' + hasHoverBgColor + ';}';
                }
                if(hasHoverTextColor) {
                    blockHoverTextColors += '[data-hover-text-color="' + hasHoverTextColor + '"]:hover > * > * > * > * { color:' + hasHoverTextColor + ';}';
                }
            });
            if(blockHoverTextColors.length) {
                $('head').append('<style>'+ blockHoverTextColors +'</style>');
            }
        }
        hoverTextColor();

        $('.section-block').hover(function(){
            $(this).toggleClass('is-hovered');
        });


        //===================================================
        // PORTFOLIO
        //===================================================
        var portfolioSlider   = $('#portfolio-slider');
        var portfolioCarousel = $('#portfolio-carousel');

        if(portfolioSlider.length) {
            portfolioSlider.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: true,
                dots: false,
                asNavFor: '#portfolio-carousel',
                adaptiveHeight: true
            });
            portfolioCarousel.slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: false,
                asNavFor: '#portfolio-slider',
                // centerMode: true,
                focusOnSelect: true
            });
        }

        // Toggle tag widget class
        $('.widget-tags .select').click(function(){
            $(this).parents('.selector').toggleClass('active');
        });

    });
})(jQuery);

