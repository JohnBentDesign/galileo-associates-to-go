<?php
	/*===========================================================================
	INDEX
	=============================================================================
	Default fallback style. Default page for blog viewing page
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');

	$isPortfolio = ($post->post_type == 'portfolio') ? ' portfolio' : '';
?>

	<div class="container primary<?= $isPortfolio; ?>">
		<div class="row">

			<?php
				// POST LOOP //
				if($post->post_type == 'gallery'){
					get_template_part('parts/posts/loop', 'gallery');
					get_template_part('parts/shared/pagination');
				}
				else {
            ?>
                    <?php if(!is_post_type_archive('portfolio') && !is_tag() && !is_category()): ?>
					<div class="small-12 columns">
						<h2 class="blog-title"><?= get_the_title(get_option('page_for_posts')); ?></h2>
					</div>
                    <?php endif; ?>

					<div class="main medium-8 columns">
						<?php get_template_part('parts/posts/loop'); ?>
						<?php get_template_part('parts/shared/pagination'); ?>
					</div>

					<?php
						// SIDEBAR //
						get_template_part('parts/shared/sidebar');
					?>

			<?php
				}
			?>

		</div>
	</div>

<?php
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>