<svg version="1.1" id="florish" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1400 39" style="position: absolute; bottom: -2px; left: -25%; width: 150%;" xml:space="preserve">
    <g>
        <path style="fill:<?= get_field('color_primary', 'options'); ?>" d="M0,0v39h1400V0c-167.8,19-419.1,31-700,31S167.8,19,0,0z"/>
    </g>
</svg>