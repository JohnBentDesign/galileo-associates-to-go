<?php
	/*===========================================================================
	HEADER- MAIN
	===========================================================================*/
	// Contains the logo and main navigation

	$headerTop 		= get_field('header_top', 'options');
	$headerBg 		= get_field('header_bgcolor', 'options');
?>
	<header class="main main-header row" data-equalizer>

		<?php // LOGO // ?>
		<div class="primary medium-3 large-2 columns" data-equalizer-watch>
			<a href="<?php echo get_bloginfo ( 'url' ); ?>" class="logo">
				<?php
					if(get_field('header_logo', 'options')){
						pantheon_display_post_field_image(get_field('header_logo', 'options'), 'medium', 'image');
					}
					else {
						bloginfo('title');
					}
				?>
			</a>
		</div>

		<div class="header-navigation medium-9 large-10 columns overlay-<?= $headerBg; ?>" data-equalizer-watch>

			<div class="<?= $headerBg; ?> upper">
				<div class="upper-inner">
					<?php
						// CONTACT DETAILS
						//===================================
						get_template_part('parts/shared/contact');
					?>

					<?php
						// SOCIAL- CONNECT
						//===================================
						if(is_array($headerTop) && in_array('social', $headerTop)){
							pantheon_display_social_connect('header');
						}
					?>

					<?php
						// CUSTOM CONTENT
						//===================================
						if(is_array($headerTop) && in_array('custom', $headerTop)){
							echo '<div class="custom">';
							echo wpautop(get_field('header_custom', 'options'));
							echo '</div>';
						}
					?>

					<?php
						// TOP NAVIGATION
						//===================================
						if(is_array($headerTop) && in_array('nav', $headerTop)){
							$menuTop = array(
								'theme_location' 	=> 'menu-top',
								'container'			=> 'nav',
								'container_class'	=> 'top-nav',
								'menu_class'		=> 'menu',
								'depth'				=> 1,
								'fallback_cb'		=> 0
							);
							wp_nav_menu($menuTop);
						}
					?>
				</div>
			</div>

			<?php
				// MAIN NAVIGATION
				//===================================
				echo '<div class="nav-container">';
				$menuMain = array(
					'theme_location' 	=> 'menu-main',
					'container'			=> 'nav',
					'container_class'	=> 'main',
					'menu_class'		=> 'menu',
					'depth'				=> 3,
					'fallback_cb'		=> 0
				);
				wp_nav_menu($menuMain);
				echo '</div>';
			?>
		</div>

	</header><?php // /header.main ?>
