<?php
	/*===========================================================================
	PASSWORD
	===========================================================================*/
	// Ah-ah-ah, you didn't say the magic word

	if(post_password_required()):
?>

		<div class="container full">
			<div class="row">

				<div class="main small-10 small-centered columns">

					<?php
						// Page Featured Image //
						$featured = get_field('page_featured');

						if($featured):
							echo '<div class="content-featured secondary">';
							pantheon_display_post_field_image(get_field('page_featured'), 'TTG Featured Image', 'image', true);
							echo '</div>';
						endif;
					?>
					
					<div class="inner primary">
						<?php echo get_the_password_form(); ?>
					</div>			

				</div>

			</div>
		</div>

<?php
	endif;
