<?php
	/*===========================================================================
	SIDEBAR: MAIN
	=============================================================================
	Right hand sidebar that appears on most pages.
	*/

	$hasExpander 	= is_page_template('page-expander.php');
	$postType 		= get_post_type();
?>
<aside class="sidebar medium-4 columns">

	<?php
		// SIDEBAR - PORTFOLIO
		//===================================
		if($postType == 'portfolio'){
			// // CATEGORIES
			// $taxonomy = 'category';
			// $tax_terms = get_terms($taxonomy);
			
			// if($tax_terms){
			// 	echo '<div class="widget widget-tags">';
			// 	echo '<h3 class="widget-title">Categories</h3>';
			// 	echo '<ul>';
			// 	foreach ($tax_terms as $tax_term) {
			// 		echo '<li>' . '<a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" title="' . sprintf( __( "View all posts in %s" ), $tax_term->name ) . '" ' . '>' . $tax_term->name.'</a></li>';
			// 	}
			// 	echo '</ul>';
			// 	echo '</div>';
			// }

			// TAGS
			$taxonomy = 'post_tag';
			$tax_terms = get_terms($taxonomy);
			
			if($tax_terms){
				echo '<div class="widget widget-tags">';
				echo '<h3 class="widget-title">Portfolio Tags</h3>';
				echo '<ul class="selector">';
				echo '<li class="selections"><p class="select">Select One <span class="ion-arrow-down-b ion-icons"></span></p><ul>';
				foreach ($tax_terms as $tax_term) {
					echo '<li>' . '<a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" title="' . sprintf( __( "View all posts in %s" ), $tax_term->name ) . '" ' . '>' . $tax_term->name.'</a></li>';
				}
				echo '</ul></li></ul>';
				echo '</div>';
			}
		}

		// SIDEBAR - GENERAL
		//===================================
		if(is_post_type_archive('post') || is_home() || (is_single() && ($postType != 'portfolio'))){
			if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar') ): endif;
		}
		// SIDEBAR - BLOG
		//===================================
		else {
			if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('General Sidebar') ): endif;
		}
	?>

</aside><?php // /.sidebar ?>