<?php
	/*===========================================================================
	FOOTER- MAIN
	===========================================================================*/
	// Contains the copyright and footer navigation
?>	
	<footer class="primary main">

		<?php
			// FOOTER BLOCKS
			//===================================
			// Let's check what we need to show
			$footerBlocks = get_field('footer_display', 'options');

			if(!empty($footerBlocks)):
				$columnSize = (count($footerBlocks) == 2) ? 'medium-6' : 'medium-12';
		?>
				<div class="row" data-equalizer>

					<?php 
						// LEFT SIDE //
						if(in_array('left', $footerBlocks)):
							// Get our variables to style the footer
							$leftBgColor = get_field('footer_left_bg_color', 'options');
							$leftBgImage = (get_field('footer_left_bg_image', 'options')) ? pantheon_display_post_field_image(get_field('footer_left_bg_image', 'options'), 'large', 'style', false) : '';
							$leftOverlay = (get_field('footer_left_bg_overlay', 'options') != 'none') ? ' overlay-' . get_field('footer_left_bg_overlay', 'options') : '';
							$leftPadding = get_field('footer_left_bg_pad', 'options') ? ' padded' : '';
					?>
							<div class="section-block sitemap columns <?= $columnSize; ?>" data-equalizer-watch>
								<div class="<?= $leftBgColor; ?>"<?= $leftBgImage; ?>>
									<div class="clearfix<?= $leftPadding; ?>">
										<div class="inner text-center<?= $leftOverlay; ?>">
											<h4>Site Map</h4>
											<?php
												// Footer Navigation
												$menuFooter = array(
													'theme_location'  	=> 'menu-footer',
													'container'       	=> 'nav',
													'container_class' 	=> 'footer',
													'menu_class'      	=> 'menu',
													'depth'           	=> 1,
													'fallback_cb'		=> 0
												);
												wp_nav_menu($menuFooter);
											?>
											<p class="copyright">&copy;<?php echo date('Y') . ' '; bloginfo('name'); ?>. Site Powered by <a href="http://techtherapytogo.com/" target="_blank">Technology Therapy To Go</a>.</p>
										</div>
									</div>
								</div>
							</div>
					<?php endif; ?>

					<?php
						// RIGHT SIDE // 
						if(in_array('right', $footerBlocks)):
							// Get our variables to style the footer
							$rightBgColor = get_field('footer_right_bg_color', 'options');
							$rightBgImage = (get_field('footer_right_bg_image', 'options')) ? pantheon_display_post_field_image(get_field('footer_right_bg_image', 'options'), 'large', 'style', false) : '';
							$rightOverlay = (get_field('footer_right_bg_overlay', 'options') != 'none') ? ' overlay-' . get_field('footer_right_bg_overlay', 'options') : '';
							$rightPadding = get_field('footer_right_bg_pad', 'options') ? ' padded' : '';
					?>
							<div class="section-block custom columns <?= $columnSize; ?>" data-equalizer-watch>
								<div class="<?= $rightBgColor; ?>"<?= $rightBgImage; ?>>
									<div class="clearfix<?= $rightPadding; ?>">
										<div class="inner text-center<?= $rightOverlay; ?>">
											<?= get_field('footer_right_content', 'options'); ?>
										</div>
									</div>
								</div>
							</div>
					<?php endif; ?>
				</div>
		<?php endif; ?>
		
		<div class="row lower">
			<div class="small-12 columns text-center information">
				<?php
					// Get Contact Information!
					$addressOne 		= get_field('general_address_one', 'options');
					$addressTwo 		= get_field('general_address_two', 'options');
					$addressCity 		= get_field('general_address_city', 'options');
					$addressState 		= get_field('general_address_state_abbr', 'options');
					$addressZip 		= get_field('general_address_zip', 'options');
					$phone 				= get_field('general_phone', 'options');
					$phoneFree			= get_field('general_phone_free', 'options');
					$fax 				= get_field('general_fax', 'options');
					$emailPrimary 		= get_field('general_email_primary', 'options');
					$emailSecondary 	= get_field('general_email_secondary', 'options');

					// Make an array with our details so we don't show empty containers
					$contactDetails = array($addressOne, $addressTwo, $addressCity, $addressState, $addressZip, $phone, $phoneFree, $fax, $emailPrimary, $emailSecondary);

					if(!empty($contactDetails)):
						echo '<div class="vcard contact-details">';
						// ADDRESS //
						if($addressOne || $addressCity || $addressState || $addressZip){
							echo '<div class="adr">';
							// Address: Company Name
							echo '<p class="fn org">' . get_bloginfo('title') . '</p>';
							// Address: Street
							if($addressOne) { echo '<p class="street-address">' . $addressOne . '</p>'; }
							if($addressTwo) { echo '<p class="extended-address">' . $addressTwo . '</p>'; }
							// Address: Locale
							if($addressCity || $addressState || $addressZip) {
								echo '<p class="locale">';
								if($addressCity) 									{ echo '<span class="locality">' . $addressCity . '</span>'; }
								if($addressCity && ($addressState || $addressZip)) 	{ echo ', '; }
								if($addressState) 									{ echo '<span class="region">' . $addressState . '</span> '; }
								if($addressZip) 									{ echo '<span class="postal-code">' . $addressZip . '</span>'; }
								echo '</p>';
							}
							echo '</div>';
						}
						echo '</div>';

						// NUMBERS //
						if($phone || $phoneFree || $fax){
							echo '<div class="numbers">';

							if($phone) 		{ echo '<p class="tel"><span class="prepend">Phone:</span>' . $phone . '</p>'; }
							if($phoneFree) 	{ echo '<p class="tel" type="work"><span class="prepend">Toll Free:</span>' . $phoneFree . '</p>'; }
							if($fax)		{ echo '<p class="tel" type="fax"><span class="prepend">Fax:</span>' . $fax . '</p>'; }

							echo '</div>';
						}

						// EMAILS //
						if($emailPrimary || $emailSecondary){
							echo '<div class="emails">';

							if($emailPrimary) 	{ echo '<a href="mailto:' . $emailPrimary . '">' . $emailPrimary . '</a>'; }
							if($emailSecondary) { echo '<a href="mailto:' . $emailSecondary . '">' . $emailSecondary . '</a>'; }

							echo '</div>';
						}
		
					endif;
				?>
			</div>
		</div>

		<div class="text-center">
			<?php if(empty($footerBlocks) || !in_array('left', $footerBlocks)): ?>
				<p class="copyright">&copy;<?php echo date('Y') . ' '; bloginfo('name'); ?>. Site Powered by <a href="http://techtherapytogo.com/" target="_blank">Technology Therapy To Go</a>.</p>
			<?php endif; ?>
			<a href="#" class="top">Back to Top</a>
		</div>

	</footer>