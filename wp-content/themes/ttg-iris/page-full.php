<?php
	/*===========================================================================
	Template Name: Full Width Page
	=============================================================================
	Style of page with no sidebar
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');

	if(post_password_required()):
		get_template_part('parts/shared/password');

	else:
?>
	<div class="container full">
		<div class="row">

			<div class="main small-10 small-centered columns">		
				<div class="inner">

					<?php 
						if(have_posts()):
							while(have_posts()): the_post();
					?>
							<div class="content">
								<?php 
									if(get_field('page_header_title') || !has_post_thumbnail()){
										echo '<h1>' . get_the_title() . '</h1>';
									}
								?>
								<?php the_content(); ?>
							</div>
					<?php
							endwhile;
						endif;
					?>	

				</div>			
			</div>

		</div>
	</div>

<?php
	endif;

	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>