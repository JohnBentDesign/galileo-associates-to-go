<?php
	/*==================================================================
	ROWING
	
	Row, row, row our columns, gently down the... whatever.
	=================================================================*/
	function iris_rowing($width, $rowTotal, $count){
		$container 	= array();

		// Check our section's width
		switch ($width) {
			case 'fourth': 			$width = 3; 	break;
			case 'third': 			$width = 4; 	break;
			case 'half': 			$width = 6; 	break;
			case 'two-third': 		$width = 8; 	break;
			case 'three-fourth': 	$width = 9; 	break;
			case 'full': 			$width = 12; 	break;
			default:				$width = 6; 	break;
		}

		// Defaults
		$container['open'] = $container['rowing'] = $container['close'] = '';

		// If we're going into this as the first position, we're going to open a row
		if($rowTotal == 0){
			$container['open'] = '<div class="row" data-equalizer>';
		}

		// If the columns add up to less than 12, then we aren't ready to close the row just yet
		if($rowTotal < 12){
			$rowTotal += $width;
			$container['rowing'] = $rowTotal;

			// If we went OVER twelve
			if($rowTotal > 12){
				$rowTotal = $width;
				$container['rowing'] = $rowTotal;
				$container['open'] = '</div><div class="row" data-equalizer>';
			}
		}

		// If it's a complete row (or close to it), we're going to close the row and reset the total
		if( $rowTotal == 12 ){
			$container['close'] = '</div>';
			$container['rowing'] = 0;
		}

		return $container;
	}