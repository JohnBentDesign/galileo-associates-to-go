<?php
	/*===========================================================================
	400
	=============================================================================
	Display for 404 pages
	*/

	// Get 404 page content
	$fourohfourPage = get_field('404_content', 'options');

	$title = ($fourohfourPage[0]) ? get_the_title($fourohfourPage[0]) : 'This page does not exist';
	$content = ($fourohfourPage[0]) ? apply_filters('the_content', get_post_field('post_content', $fourohfourPage[0])) : '<p>Sorry. It seems we can\'t find what you were looking for.</p>';

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>

	<div class="container full">
		<div class="row">
			<div class="main small-10 small-centered columns">

				<?php pantheon_display_post_featured_image($fourohfourPage[0], 'TTG Featured Image', false); ?>
				<h1><?= $title; ?></h1>
				<?= $content; ?>

			</div>
		</div>
	</div>

<?php get_template_part('parts/shared/footer', 'html'); ?>