
<?php
	/*===========================================================================
	INCLUDE THE INCLUDES
	===========================================================================*/
	// Advanced Custom Fields
	include('includes/advanced-custom-fields/theme-options.php');
	include('includes/advanced-custom-fields/homepage-slider.php');
	include('includes/advanced-custom-fields/page-settings.php');
	include('includes/advanced-custom-fields/sections.php');
	include('includes/advanced-custom-fields/portfolio.php');
	include('includes/advanced-custom-fields/show.php');

	include('includes/iris_class_section_build.php');
	include('includes/function_rowing.php');



	/*===========================================================================
	ADD THEME SUPPORTS
	===========================================================================*/
	add_theme_support('post-thumbnails');
	add_theme_support('menus');



	/*===========================================================================
	CUSTOM THUMBNAIL SIZES
	===========================================================================*/
	// add_image_size('TTG Featured Image', 1024, 500, true);
	add_image_size('TTG Gallery Normal Display', 637, 9999, false);
	add_image_size('TTG Medium Thumbnail', 350, 350, true);



	/*===========================================================================
	REGISTER MENUS
	===========================================================================*/
	if(!function_exists('ttg_menus')){
		function ttg_menus() {
			register_nav_menus(array(
				'menu-main' 		=> 'Main Menu',
				'menu-top' 			=> 'Top Menu',
				'menu-footer' 		=> 'Footer Menu'
             ));
		}
		add_action('init', 'ttg_menus');
	}



	/*===========================================================================
	REGISTER SIDEBARS
	===========================================================================*/
	$sidebarGeneral = array(
		'name'          => 'General Sidebar',
		'id'            => 'sidebar-general',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
       );
	register_sidebar($sidebarGeneral);
	$sidebarBlog = array(
		'name'          => 'Blog Sidebar',
		'id'            => 'sidebar-blog',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
       );
	register_sidebar($sidebarBlog);



	/*===========================================================================
	ENQUEUE SCRIPTS AND STYLES
	===========================================================================*/
	// JAVASCRIPTS
	//---------------------------------------------------------------------------
	function ttg_scripts(){
		if(!is_admin()){
			// Plugins
			if(is_page_template('page-home.php') || get_post_type() == 'portfolio'){
				wp_enqueue_script('slick', get_template_directory_uri() . '/javascripts/plugins/slick.min.js', array('jquery'), '', false);
			}
			wp_enqueue_script('customselect', get_template_directory_uri() . '/javascripts/plugins/customSelect.min.js', array('jquery'), '', true);
			wp_enqueue_script('tinynav', get_template_directory_uri() . '/javascripts/plugins/tinynav.min.js', array('jquery'), '', true);

			// Iris Scripts
			wp_enqueue_script('iris', get_template_directory_uri() . '/javascripts/iris.js', array('jquery', 'foundation', 'tinynav', 'customselect'), '', true);
		}
	}
	add_action('wp_enqueue_scripts', 'ttg_scripts');

	// STYLESHEETS
	//---------------------------------------------------------------------------
	function ttg_styles(){
		if(!is_admin()){
			// Webfonts
			wp_enqueue_style('fonts-opensans', 'https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700|Open+Sans:400,700,300');

			// Iris Stylesheets
			wp_enqueue_style('iris', get_template_directory_uri() . '/stylesheets/css/style.css');
		}
	}
	add_action('wp_enqueue_scripts', 'ttg_styles');

	function ttg_styles_admin() {
		if(is_admin()){
			wp_enqueue_style('custom-admin', get_template_directory_uri() . '/stylesheets/css/admin.css');
		}
	}
	add_action('admin_enqueue_scripts', 'ttg_styles_admin');

	// RECOMPILES THE CSS ONLY WHEN THE OPTIONS PAGE IS SAVED
	//---------------------------------------------------------------------------
	if(get_field('dev_compile', 'option')){
		include(get_template_directory() . '/stylesheets/php-sass-watcher.php');
	}
	else {
		function ttg_compile_css( $post_id ) {
			$page = htmlspecialchars($_GET["page"]);
			if($page == 'acf-options-theme-styling'){
				// Compile our CSS
				include(get_template_directory() . '/stylesheets/php-sass-watcher.php');
			}
		}
		// run after ACF saves the $_POST['fields'] data
		add_action('acf/save_post', 'ttg_compile_css', 20);
	}


	/*===========================================================================
	ADDITIONAL FUNCTIONS
	===========================================================================*/
	// REMOVE "PROTECTED" FROM PASSWORD PAGE'S TITLES
	//---------------------------------------------------------------------------
	add_filter('protected_title_format', 'blank');
	function blank($title) {
		return '%s';
	}


    // Color Helper
    //---------------------------------------------------------------------------
    function getThemeColor($color_nicename) {
        if ($color_nicename == 'primary') {
            $color = (get_field('color_primary', 'options')) ? get_field('color_primary', 'options') : '#ffffff';
        } else if($color_nicename == 'secondary') {
            $color = (get_field('color_secondary', 'options')) ? get_field('color_secondary', 'options')   : '#565656';
        } else if($color_nicename == 'tertiary') {
            $color = (get_field('color_tertiary', 'options')) ? get_field('color_tertiary', 'options')    : '#00aeef';
        } else if($color_nicename == 'quaternary') {
            $color = (get_field('color_quaternary', 'options')) ? get_field('color_quaternary', 'options')  : '#691c33';
        }
        return $color;
    }

    // Color Contrast Helper
    //---------------------------------------------------------------------------
    function getContrastingColor($hexcolor){
        $r = hexdec(substr($hexcolor,0,2));
        $g = hexdec(substr($hexcolor,2,2));
        $b = hexdec(substr($hexcolor,4,2));
        $yiq = (($r*299)+($g*587)+($b*114))/1000;
        return ($yiq >= 128) ? getThemeColor('secondary') : getThemeColor('primary');
    }

    //---------------------------------------------------------------------------
    function excerpt($limit) {
       $excerpt = explode(' ', get_the_excerpt(), $limit);
       if (count($excerpt)>=$limit) {
           array_pop($excerpt);
           $excerpt = implode(" ",$excerpt).'...';
       } else {
           $excerpt = implode(" ",$excerpt);
       }
       $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
       return $excerpt;
    }

    //---------------------------------------------------------------------------
    function content($limit) {
       $content = explode(' ', get_the_content(), $limit);
       if (count($content)>=$limit) {
           array_pop($content);
           $content = implode(" ",$content).'...';
       } else {
           $content = implode(" ",$content);
       }
       $content = preg_replace('/[.+]/','', $content);
       $content = apply_filters('the_content', $content);
       $content = str_replace(']]>', ']]&gt;', $content);
       return $content;
    }


    // Blog/Portfolio Slide Generator
    //---------------------------------------------------------------------------
	function homeSlideLoop($postType = null, $postCount = 3, $slides = null){
		if(in_array($postType, $slides['types'])):
			// Get our posts
			$thePosts 	= get_posts(array(
				'posts_per_page'    => $postCount,
				'post_type'			=> $postType,
				'meta_query' 		=> array(
					'relation'			=> 'OR',
					array( // new and edited posts
						'key' 			=> 'hide_from_home',
						'compare' 		=> '!=',
						'value' 		=> 1
					),
					array( // get old posts w/out custom field
						'key' 			=> 'hide_from_home',
						'value' 		=> '1',
						'compare' 		=> 'NOT EXISTS'
					)),
				));

			if(isset($thePosts) && $postCount != 0):
				foreach($thePosts as $post): setup_postdata($post);
					$image 		= (has_post_thumbnail($post->ID)) ? pantheon_display_post_featured_image($post->ID, 'large', null, 'url', false) : pantheon_display_post_field_image(get_field('default_home_slider_' . $postType, 'options'), 'large', 'url', false);
					$image 		= (get_field('home_img_alt', $post->ID)) ? pantheon_display_post_field_image(get_field('home_img_alt', $post->ID), 'large', 'url', false) : $image;
					$overlay 	= (($slides['overlay'] != 'none') && ($slides['overlay-type'] == 'block')) ? ' overlay-' . $slides['overlay'] : '';
					$native 	= (get_field('home_fill', $post->ID)) ? ' no-fill' : '';

					if($image):
?>
						<div class="slide<?= $native; ?>" style="background-image: url('<?= $image; ?>')">
							<div class="slide-content<?= $overlay; ?>">
								<h2><?= $post->post_title; ?></h2>
								<?php echo wpautop(wp_trim_words($post->post_content, 25, '... <br><br><a class="button" href="' . get_permalink($post->ID) . '">Read More &raquo;</a>')); ?>
							</div>

							<?php if(($slide['overlay'] != 'none') && ($slide['overlay-type'] == 'fade')): ?>
								<div class="overlay overlay-grad-<?= $slides['overlay']; ?>"></div>
							<?php endif; ?>
						</div>
<?php 
					endif;
				endforeach;
			wp_reset_postdata();
			endif;
		endif;
	}


    /*===========================================================================
    REGISTER PORTFOLIO CUSTOM POST TYPE
    ===========================================================================*/
    function iris_portfolio() {

        $labels = array(
            'name'                => _x( 'Portfolio', 'Post Type General Name', 'text_domain' ),
            'singular_name'       => _x( 'Portfolio Item', 'Post Type Singular Name', 'text_domain' ),
            'menu_name'           => __( 'Portfolio', 'text_domain' ),
            'name_admin_bar'      => __( 'Portfolio', 'text_domain' ),
            'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
            'all_items'           => __( 'All Items', 'text_domain' ),
            'add_new_item'        => __( 'Add New Item', 'text_domain' ),
            'add_new'             => __( 'Add New', 'text_domain' ),
            'new_item'            => __( 'New Item', 'text_domain' ),
            'edit_item'           => __( 'Edit Item', 'text_domain' ),
            'update_item'         => __( 'Update Item', 'text_domain' ),
            'view_item'           => __( 'View Item', 'text_domain' ),
            'search_items'        => __( 'Search Item', 'text_domain' ),
            'not_found'           => __( 'Not found', 'text_domain' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
            );
        $args = array(
            'label'               => __( 'Portfolio Item', 'text_domain' ),
            'description'         => __( 'Portfolio Pieces ', 'text_domain' ),
            'labels'              => $labels,
            'supports'            => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', ),
            'taxonomies'          => array('category', 'post_tag'),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'menu_position'       => 20,
            'menu_icon'           => 'dashicons-portfolio',
            'show_in_admin_bar'   => true,
            'show_in_nav_menus'   => true,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
            );
        register_post_type( 'portfolio', $args );
    }
    add_action( 'init', 'iris_portfolio', 0 );

    // Add post type to tags
    function wpa_cpt_tags( $query ) {
	    if ( ($query->is_tag() && $query->is_main_query()) || ($query->is_category() && $query->is_main_query()) ) {
	        $query->set( 'post_type', array( 'post', 'portfolio' ) );
	    }
	}
	add_action( 'pre_get_posts', 'wpa_cpt_tags' );