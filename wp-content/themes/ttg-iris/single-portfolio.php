<?php
    /*===========================================================================
    Single Portfolio Template (Currently a clone of page.php)
    =============================================================================
    Template for displaying varying box sizes
    */

    // HEADER //
    get_template_part('parts/shared/header', 'html');

    if(post_password_required()):
        get_template_part('parts/shared/password');

    else:
?>
    <div class="container">
        <div class="row">

            <div class="images medium-7 columns">
                <div class="portfolio-gallery-wrap">

                    <?php
                        $portfolioImages     = get_field('portfolio_gallery');
                        $portfolioImagesType = get_field('portfolio_gallery_style');

                        if($portfolioImages && $portfolioImagesType == 'slider'):
                    ?>
                            <div id="portfolio-slider" class="portfolio-slider">
                                <?php foreach( $portfolioImages as $image ): ?>
                                    <div class="portfolio-slider__slide">
                                        <img src="<?php echo $image['sizes']['large'] ?>">
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div id="portfolio-carousel" class="portfolio-carousel">
                                <?php foreach( $portfolioImages as $image ): ?>
                                    <div class="portfolio-carousel__slide">
                                        <img src="<?php echo $image['sizes']['thumbnail'] ?>">
                                    </div>
                                <?php endforeach; ?>
                            </div>
                    <?php 
                        elseif($portfolioImages && $portfolioImagesType == 'lightbox'):
                            $gallery_columns = get_field('portfolio_gallery_columns') ?: 3;
                            $image_ids       = get_field('portfolio_gallery', false, false);
                            $shortcode       = '[gallery columns="'. $gallery_columns .'" size="large" link="none" ids="' . implode(',', $image_ids) . '"]';
                            echo do_shortcode( $shortcode );
                        endif;
                    ?>

                </div>
            </div>
            
            <div class="main medium-5 columns">
                <?php
                    if(have_posts()):
                        while(have_posts()): the_post();
                ?>
                            <div class="content">
                                <?php if(get_field('page_header_title')): ?>
                                    <h1><?php the_title(); ?></h1>
                                <?php endif; ?>
                                <?php the_content(); ?>
                            </div>
                            <div class="meta">
                                <?php  
                                    pantheon_display_post_categories($post->ID);
                                    pantheon_display_post_tags($post->ID);
                                ?>
                            </div>
                <?php
                        endwhile;
                    endif;
                ?>
            </div>

        </div>
    </div>

<?php
    endif;

    // FOOTER //
    get_template_part('parts/shared/footer', 'html');
?>