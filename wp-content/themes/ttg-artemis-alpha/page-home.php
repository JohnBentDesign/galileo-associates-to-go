<?php
	/*===========================================================================
	Template Name: Homepage
	=============================================================================
	Homepage 
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>
	<div class="container primary">


		<?php 
			// CALL TO ACTION //
			get_template_part('parts/shared/cta');

			// CALLOUTS //
			if(have_rows('home_callouts')):

				echo '<div class="callouts row">';

				// Some Variables
				$calloutCount 	= 1;
				$calloutBG 		= get_field('home_callouts_bg');

				while(have_rows('home_callouts')): the_row();
					$bg 			= ($calloutBG) ? ' class="has-bg ' . get_sub_field('background_color') . '"' : '';
					$imageObj 		= get_sub_field('image');
					$imageOutput	= pantheon_display_post_field_image($imageObj, 'TTG Medium Thumbnail', 'image', false);
					$icon 			= get_sub_field('icon');
					$iconOutput 	= ($icon) ? '<span class="icon ' . $icon . '"></span>' : '';
					$header 		= (get_sub_field('header')) ? '<span class="title">' . get_sub_field('header') . '</span>' : '';
					$content 		= get_sub_field('content');
					$ctaLinkType 	= get_sub_field('cta_type');
					$ctaOutput 		= pantheon_display_post_cta(array(
							'echo' 		=> false,
							'repeater' 	=> 'sub-field',
							'class' 	=> 'cta-link',
							'content' 	=> $iconOutput . $imageOutput . $header
						)
					);

					if($header && $ctaLinkType){
						$headerOutput	= '<h4' . $bg . '>' . $ctaOutput . '</h4>';	
					}
					else if($header) {
						$headerOutput	=  '<h4' . $bg . '>' . $imageOutput . $iconOutput . $header . '</h4>';
					}
					else {
						$headerOutput 	= $imageOutput . $iconOutput;
					}
		?>
					<div class="callout callout-<?= $calloutCount; ?> medium-6 large-3 columns">

						<?= $headerOutput; ?>
						<?= $content; ?>
						<?php pantheon_display_post_cta(array('class' => 'cta-link-outside', 'repeater' => 'sub-field')); ?>

					</div>
		<?php
					$calloutCount++;
				endwhile;

				echo '</div>';

			endif;
		?>


	</div>

<?php
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>