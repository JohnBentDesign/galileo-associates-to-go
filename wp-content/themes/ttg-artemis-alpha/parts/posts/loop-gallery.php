<?php
	/*===========================================================================
	GALLERY LOOP
	=============================================================================
	Gallery post loop for index.php
	*/
	
	// Figure out where we are
	$listing = (is_home() || is_archive() || is_search()) ? true : false;
	if($listing){ $postOutput = ' excerpt'; }
	else if($listing == false){ $postOutput = ' full'; }

	// Check if our galleries need to be full width on non-listing pages
	$galleryFullWidth = get_field('gallery_width');
	echo '<div class="main columns">';

	// THE LOOP //
	if(have_posts()){

		// If it's the blog listing page, wrap our articles in a div
		if($listing){ echo '<div class="gallery post-list">'; }

		// Start the loop
		while(have_posts()): the_post();
			if($listing){
				$title 		= '<h2><a href="' . get_permalink() . '">' . get_the_title() . '</a></h2>';
				$content 	= (wpautop(get_the_excerpt())) ? '<div class="content">' . wpautop(get_the_excerpt()) . '</div>' : '';
				$imageSize 	= 'TTG Featured Image';
			}
			elseif($listing == false){
				$title 		= '';
				$content 	= (wpautop(get_the_content())) ? '<div class="content">' . wpautop(get_the_content()) . '</div>' : '';
				$imageSize 	= 'TTG Featured Image';
			}
?>
			<article class="post<?= $postOutput; ?> gallery">

				<?php // POST HEADER // ?>
				<?php if($listing): ?>
					<header>		
						<?= $title; ?>

						<div class="categorizing">
							<?php pantheon_display_post_categories($post->ID); ?>
						</div>
					</header>
				<?php endif; ?>

				<?php // POST CONTENT // ?>
				<div class="the-post clearfix">
					
					<?php
						// Display featured image next to the excerpt if blog listing page 
						if($listing){ pantheon_display_post_featured_image($post->ID, $imageSize, get_permalink()); }
					
						echo $content; 

						// GALLERY //
						if(is_single()){
							pantheon_display_gallery();
						}
					?>

				</div>

			</article><?php // /.post ?>	

<?php 
		endwhile;

		// Close the blog listing div
		if($listing){ echo '</div>'; }
		echo '</div>';
	}
	else{
		echo '<p>Sorry, but nothing matched your search criteria. Please try again with different keywords.</p>';
		get_template_part('searchform');
	}
?>