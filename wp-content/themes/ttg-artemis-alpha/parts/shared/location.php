<?php
	/*===========================================================================
	LOCATION
	===========================================================================*/
	// Location band at the bottom of the site

	// Check were we can even display the CTA band
	$locationDisplayCheck = get_field('location_display', 'options');
	if( !empty($locationDisplayCheck) ):

		// Make an array to check our condition
		$displayLocation = array();

		// Loop through and see if any of these conditions are true for this page
		foreach($locationDisplayCheck as $locationDisplay){
			switch($locationDisplay){
				case 'home': 				$location = is_front_page(); 								break;
				case 'blog': 				$location = is_home(); 										break;
				case 'page': 				$location = is_page(); 										break;
				case 'post': 				$location = is_single(); 									break;
				case 'gallery': 			$location = is_post_type_archive('gallery'); 				break;
				case 'gallery-single': 		$location = (get_post_type() == 'gallery') ? true : false; 	break;
				default: 					$location = false;											break;
			}

			// Push value to the array
			array_push($displayLocation, $location);
		}

		// If 'true' is found in the array, and this specific page is allowing the CTA, we're going to display the CTA on this page.
		if( in_array(true, $displayLocation) && (get_field('location_page_display') != 'no') ):

			// Location Details
			$locationTitle 			= get_field('location_title', 'options');
			$locationTitleOutput 	= ($locationTitle) ? '<h3 class="location-title">' . $locationTitle . '</h3>' : '';
			$locationContent 		= get_field('location_content', 'options');
			$locationAddress 		= get_field('location_address', 'options');

			// Map
			$locationMapType		= get_field('location_display_type', 'options');
			$locationMap 			= get_field('location_map', 'options');
			$locationImage 			= get_field('location_image', 'options');

			// Get column size
			$locationColumn 		= ($locationMapType && ($locationTitle || $locationContent || $locationAddress)) ? 'medium-8 ' : '';
			$detailsColumn			= ($locationMapType && ($locationTitle || $locationContent || $locationAddress)) ? 'medium-4 ' : '';

			if($locationMap || $locationTitle || $locationContent || $locationAddress):
?>
				<div class="location-band senary">
					<div class="row">

						<?php if( $locationMapType ): ?>
							<div class="<?= $locationColumn; ?>columns">
								<?php if($locationMapType == 'map'):?>
									<div class="location-map">
										<div class="marker" data-lat="<?php echo $locationMap['lat']; ?>" data-lng="<?php echo $locationMap['lng']; ?>"></div>
									</div>
								<?php 
									elseif($locationMapType == 'image'):
										pantheon_display_post_field_image($locationImage, 'TTG Featured Image');
									endif;
								?>
							</div>
						<?php endif; ?>

						<?php if($locationTitle || $locationContent || $locationAddress): ?>
							<div class="location-info <?= $detailsColumn; ?>columns">
								<?= $locationTitleOutput; ?>
								<?= $locationContent; ?>
								<?php 
									/*===========================================================================
									CONTACT DETAILS
									===========================================================================*/
									// Get all our information!
									$addressOne 		= get_field('general_address_one', 'options');
									$addressTwo 		= get_field('general_address_two', 'options');
									$addressCity 		= get_field('general_address_city', 'options');
									$addressState 		= get_field('general_address_state_abbr', 'options');
									$addressZip 		= get_field('general_address_zip', 'options');
									$phone 				= get_field('general_phone', 'options');
									$phoneFree			= get_field('general_phone_free', 'options');
									$fax 				= get_field('general_fax', 'options');
									$emailPrimary 		= get_field('general_email_primary', 'options');
									$emailSecondary 	= get_field('general_email_secondary', 'options');

									// Make an array with our details so we don't show empty containers
									global $contactDetails;
									$contactDetails = array($addressOne, $addressTwo, $addressCity, $addressStateLong, $addressStateAbbr, $addressZip, $phone, $phoneFree, $fax, $emailPrimary, $emailSecondary);

									if(!empty($contactDetails)):

										echo '<div class="vcard">';

												// ADDRESS //
												if($addressOne || $addressTwo || $addressCity || $addressState || $addressZip){
													echo '<div class="adr">';

													// Address: Company Name
													echo '<p class="fn org">' . get_bloginfo('title') . '</p>';
													// Address: Street
													if($addressOne) { echo '<p class="street-address">' . $addressOne . '</p>'; }
													if($addressTwo) { echo '<p class="extended-address">' . $addressTwo . '</p>'; }
													// Address: Locale
													if($addressCity || $addressState || $addressZip) {
														echo '<p class="locale">';
														if($addressCity) 									{ echo '<span class="locality">' . $addressCity . '</span>'; }
														if($addressCity && ($addressState || $addressZip)) 	{ echo ', '; }
														if($addressState) 									{ echo '<span class="region">' . $addressState . '</span> '; }
														if($addressZip) 									{ echo '<span class="postal-code">' . $addressZip . '</span>'; }
														echo '</p>';
													}

													echo '</div>';
												}

												// NUMBERS //
												if($phone || $phoneFree || $fax){
													echo '<div class="numbers">';

													if($phone) 		{ echo '<p class="tel">' . $phone . '</p>'; }
													if($phoneFree) 	{ echo '<p class="tel" type="work">' . $phoneFree . '</p>'; }
													if($fax)		{ echo '<p class="tel" type="fax">' . $fax . '</p>'; }

													echo '</div>';
												}

												// EMAILS //
												if($emailPrimary || $emailSecondary){
													echo '<div class="emails">';

													if($emailPrimary) 	{ echo '<a href="mailto:' . $emailPrimary . '">' . $emailPrimary . '</a>'; }
													if($emailSecondary) { echo '<a href="mailto:' . $emailSecondary . '">' . $emailSecondary . '</a>'; }

													echo '</div>';
												}
										
										echo '</div>';

									endif;
								?>
							</div>
						<?php endif; ?>

					</div>
				</div>

				<?php if( !empty($locationMap) ): ?>
					<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
					<script type="text/javascript">
					(function($) {
					 
					/*
					*  render_map
					*
					*  This function will render a Google Map onto the selected jQuery element
					*
					*  @type	function
					*  @date	8/11/2013
					*  @since	4.3.0
					*
					*  @param	$el (jQuery element)
					*  @return	n/a
					*/
					 
					function render_map( $el ) {
					 
						// var
						var $markers = $el.find('.marker');
					 
						// vars
						var args = {
							zoom		: 16,
							center		: new google.maps.LatLng(0, 0),
							mapTypeId	: google.maps.MapTypeId.ROADMAP
						};
					 
						// create map	        	
						var map = new google.maps.Map( $el[0], args);
					 
						// add a markers reference
						map.markers = [];
					 
						// add markers
						$markers.each(function(){
					 
					    	add_marker( $(this), map );
					 
						});
					 
						// center map
						center_map( map );
					 
					}
					 
					/*
					*  add_marker
					*
					*  This function will add a marker to the selected Google Map
					*
					*  @type	function
					*  @date	8/11/2013
					*  @since	4.3.0
					*
					*  @param	$marker (jQuery element)
					*  @param	map (Google Map object)
					*  @return	n/a
					*/
					 
					function add_marker( $marker, map ) {
					 
						// var
						var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
					 
						// create marker
						var marker = new google.maps.Marker({
							position	: latlng,
							map			: map
						});
					 
						// add to array
						map.markers.push( marker );
					 
						// if marker contains HTML, add it to an infoWindow
						if( $marker.html() )
						{
							// create info window
							var infowindow = new google.maps.InfoWindow({
								content		: $marker.html()
							});
					 
							// show info window when marker is clicked
							google.maps.event.addListener(marker, 'click', function() {
					 
								infowindow.open( map, marker );
					 
							});
						}
					 
					}
					 
					/*
					*  center_map
					*
					*  This function will center the map, showing all markers attached to this map
					*
					*  @type	function
					*  @date	8/11/2013
					*  @since	4.3.0
					*
					*  @param	map (Google Map object)
					*  @return	n/a
					*/
					 
					function center_map( map ) {
					 
						// vars
						var bounds = new google.maps.LatLngBounds();
					 
						// loop through all markers and create bounds
						$.each( map.markers, function( i, marker ){
					 
							var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
					 
							bounds.extend( latlng );
					 
						});
					 
						// only 1 marker?
						if( map.markers.length == 1 )
						{
							// set center of map
						    map.setCenter( bounds.getCenter() );
						    map.setZoom( 16 );
						}
						else
						{
							// fit to bounds
							map.fitBounds( bounds );
						}
					 
					}
					 
					/*
					*  document ready
					*
					*  This function will render each map when the document is ready (page has loaded)
					*
					*  @type	function
					*  @date	8/11/2013
					*  @since	5.0.0
					*
					*  @param	n/a
					*  @return	n/a
					*/
					 
					$(document).ready(function(){
					 
						$('.location-map').each(function(){
					 
							render_map( $(this) );
					 
						});
					 
					});
					 
					})(jQuery);
					</script>

<?php 
				endif;
			endif;
		endif;
	endif;