<?php
	/*===========================================================================
	HEADER- MAIN
	===========================================================================*/
	// Contains the logo and main navigation
?>
	<?php 
		// ANNOUNCEMENT
		//===================================
		$announcement = get_field('header_announcement', 'options');
		if($announcement):
	?>
			<div class="announcement">
				<div class="row">
					<div class="columns">
						<p><?= $announcement; ?></p>
					</div>
				</div>
			</div>
	<?php endif; ?>


	<header class="main primary">

		<?php 		
			// TOP NAVIGATION
			//===================================
		?>
		<div class="row">
			<div class="columns">
				<?php	
					$alignment = get_field('header_top_nav_align', 'options');
					wp_nav_menu(array(
							'theme_location' 	=> 'menu-top',
							'menu'				=> 'Header Top Menu',
							'container'			=> 'nav',
							'container_class'	=> 'menu-top',
							'menu_class'		=> 'menu text-' . $alignment,
							'depth'				=> 1
						)
					);
				?>
			</div>
		</div>
		

		<?php 		
			// LOGO
			//===================================
		?>
		<div class="row">
			<div class="columns">
				<?php if(is_front_page()) { echo '<h1>'; } ?>
					<a href="<?php bloginfo('home'); ?>" class="logo">
						<?php 
							if(get_field('header_logo', 'options')){
								$logoObj 	= get_field('header_logo', 'options');
								pantheon_display_post_field_image($logoObj, 'TTG Logo');
							}
							else {
								bloginfo('title');
							}
						?>
					</a>
				<?php if(is_front_page()) { echo '</h1>'; } ?>
			</div>
		</div>


		<?php 
			// LOCATION BAND & SEARCH
			//===================================

			// Contact Details
			//-----------------------------------
			// Make an array with our details
			$contactDetails = array(
				'addressOne' 		=> get_field('general_address_one', 'options'),
				'addressCity' 		=> get_field('general_address_city', 'options'),
				'addressStateAbbr' 	=> get_field('general_address_state_abbr', 'options'),
				'addressZip' 		=> get_field('general_address_zip', 'options'),
				'phone' 			=> get_field('general_phone', 'options'),
			);

			// Check if this ribbon should even appear
			$displayRibbon 	= '';
			$displayAddress = '';
			foreach($contactDetails as $key => $value){
				if( ($value != '') && ($displayRibbon == '') ) {
					$displayRibbon = true;
				}
				if( ($key != 'phone') && ($value != '') && ($displayAddress == '') ) {
					$displayAddress = true;
				}
			}

			// Check if we are displaying address
			$noAddress 	= (!$displayAddress) ? ' no-address' : '';
		?>
		<div class="info-band<?= $noAddress; ?> quaternary">
			<div class="row">

				<div class="search medium-7 medium-centered large-uncentered large-4 large-push-8 columns">
					<?php get_template_part('parts/shared/searchform'); ?>
				</div>

				<?php 
					// Contact Details
					//-----------------------------------
					// Make an array with our details
					$contactDetails = array(
						'addressOne' 		=> get_field('general_address_one', 'options'),
						'addressCity' 		=> get_field('general_address_city', 'options'),
						'addressStateAbbr' 	=> get_field('general_address_state_abbr', 'options'),
						'addressZip' 		=> get_field('general_address_zip', 'options'),
						'phone' 			=> get_field('general_phone', 'options'),
					);

					// Check if this ribbon should even appear
					$displayRibbon 	= '';
					$displayAddress = '';
					foreach($contactDetails as $key => $value){
						if( ($value != '') && ($displayRibbon == '') ) {
							$displayRibbon = true;
						}
						if( ($key != 'phone') && ($value != '') && ($displayAddress == '') ) {
							$displayAddress = true;
						}
					}

					if($displayRibbon):
						$noPhone 	= (!$contactDetails['phone']) ? ' no-phone' : '';
				?>
						<div class="ribbon large-8 large-pull-4 columns<?= $noPhone . $noAddress; ?>">
							<div class="row">

								<?php
									// Address Details
									if($displayAddress){
										if( !empty($contactDetails) ) { echo '<div class="location medium-9 columns"><p>'; }	
										echo $contactDetails['addressOne'];
										if( ($contactDetails['addressOne']) && $contactDetails['addressCity']) { echo ', '; }
										echo $contactDetails['addressCity'];
										if( ($contactDetails['addressOne'] || $contactDetails['addressCity']) && $contactDetails['addressStateAbbr']) { echo ', '; }
										echo $contactDetails['addressStateAbbr'];
										echo ' ' . $contactDetails['addressZip'];
										if( !empty($contactDetails) ) { echo '</p><span class="end-cap"></span></div>'; }
									}

									// Our Phone Number
									if($contactDetails['phone']) { 
										if($displayAddress == ''){ 
											echo '<div class="phone medium-6 columns tertiary lighter">';
											echo '<span class="front-cap"></span>';
											echo '<p>Get in Touch: ' . $contactDetails['phone'] . '</p>';
										}
										else {
											echo '<div class="phone medium-3 columns tertiary lighter">';
											echo '<p>' . $contactDetails['phone'] . '</p>';
										}
										echo '<span class="end-cap"></span></div>';
									}
								?>

							</div>
						</div>
				<?php endif; ?>

			</div>
			<?php if(!empty($displayRibbon)) { echo '<span class="half-band show-for-medium-up tertiary"></span>'; } ?>
		</div>


		<?php
			// MAIN NAVIGATION
			//===================================
		?>
		<div class="navigation">
			<div class="row">
				<div class="columns">
					<?php	
						wp_nav_menu(
							array(
								'theme_location' 	=> 'menu-main',
								'menu'				=> 'Main Menu',
								'container'			=> 'nav',
								'container_class'	=> 'main',
								'menu_class'		=> 'menu',
								'depth'				=> 3
							)
						);
					?>
				</div>
			</div>
		</div>


		<?php
			// BACKDROP
			//===================================
			get_template_part('parts/shared/backdrop');
		?>

	
		<?php
			// BREADCRUMBS
			//===================================
			if ( function_exists('yoast_breadcrumb') && !is_front_page() ) {
				yoast_breadcrumb('<div class="row"><div class="column"><nav class="breadcrumbs quaternary">','</nav></div></div>');
			}
		?>

	</header><?php // /header.main ?>
