<?php
	/*===========================================================================
	BACKDROP
	===========================================================================*/
	// Backdrop and Homepage Slider

	$isBlog = ((is_home() && get_option('page_for_posts')) || (is_single() && ($post->post_type == 'post')) || is_category() || is_day() || is_tag() || is_month() || is_year()) ? true : false; 
	$blogID = get_option('page_for_posts');

	if( (get_field('featured_image_display') == 'no') || is_front_page() ){
		$imageStyle = '';
	}
	elseif(is_home() || ($isBlog && !has_post_thumbnail($post->ID)) ){
		$imageStyle = pantheon_display_post_featured_image($blogID, 'TTG Featured Image', null, 'style', false);
	}
	elseif(has_post_thumbnail($post->ID)){
		$imageStyle = pantheon_display_post_featured_image($post->ID, 'TTG Featured Image', null, 'style', false);
	}
	elseif(get_field('featured_image_default', 'options')){
		$imageObj 	= get_field('featured_image_default', 'options');
		$imageStyle = pantheon_display_post_field_image($imageObj, 'TTG Featured Image', 'style', false);
	}
	else{
		$imageStyle = '';
	}
?>
	<div class="backdrop">
		<?php 
			// INTERIOR PAGE HEADER
			//===================================
			if( !is_front_page() ):
		?>
			<div class="backdrop-bg row"<?= $imageStyle; ?>>
				<div class="columns">
					<h1>
						<span class="title">
							<?php 
								if(is_search()){
									echo 'Search';
								}
								else if($isBlog)  {
									echo get_page($blogID)->post_title;
								}
								else if(is_post_type_archive()){
									echo 'Gallery';
								}
								else if(is_404()){
									echo 'Page Not Found';
								}
								else {
									the_title();
								}
							?>
						</span>
						<span class="band quinary"></span>
					</h1>
					<div class="overlay left"></div>
					<div class="overlay right"></div>
				</div>
			</div>
	
		<?php 
			// SLIDER HOMEPAGE
			//===================================
			elseif(is_front_page() && have_rows('home_slideshow') ):
				$slideSpeed 		= (get_field('home_slideshow_speed')) ? get_field('home_slideshow_speed') : 5000;
		?>			
				<div class="homepage-slider">
					<?php
						while(have_rows('home_slideshow')): the_row();
							$title 		= get_sub_field('slide_title') ? '<h2>' . get_sub_field('slide_title') . '</h2>' : '';
							$content 	= get_sub_field('slide_content');
							$ctaText 	= get_sub_field('slide_cta_text');
							$ctaLink 	= get_sub_field('slide_cta_link');
							$ctaOutput 	= ($ctaText && $ctaLink) ? '<a href="' . $ctaLink . '" class="button tiny">' . $ctaText . '</a>' : '';
							$imageObj 	= get_sub_field('slide_image');
							$imageStyle = pantheon_display_post_field_image($imageObj, 'TTG Featured Image', 'style', false);
					?>
							<div class="slide"<?= $imageStyle; ?>>
								<img src="<?= get_template_directory_uri(); ?>/images/spacer.gif" alt="design element" height="1" width="1" />
								<div class="slider-info">
									<div class="row">
										<div class="slider-text-container large-6 columns">
											<div class="slider-text">
												<?= $title . $content . $ctaOutput; ?>
											</div>
										</div>
									</div>
								</div>
								<div class="overlay"></div>
							</div>		
					<?php endwhile; ?>
				</div>

				<script>
					jQuery(document).ready(function(){
						jQuery('.homepage-slider').slick({
							autoplay: true,
							autoplaySpeed: <?= $slideSpeed; ?>,
							arrows: false,
							fade: true,
							draggable: false
						});
					});
				</script>

		<?php endif; ?>
	</div>