<?php
	/*===========================================================================
	CALL TO ACTION BAND
	===========================================================================*/
	// The call to action band that displays at the bottom of the site

	// Check were we can even display the CTA band
	$ctaDisplayCheck = get_field('cta_display', 'options');
	if( !empty($ctaDisplayCheck) ) {

		// Make an array to check our condition
		$displayCTA = array();

		// Loop through and see if any of these conditions are true for this page
		foreach($ctaDisplayCheck as $ctaDisplay){
			switch($ctaDisplay){
				case 'home': 				$cta = is_front_page(); 								break;
				case 'blog': 				$cta = is_home(); 										break;
				case 'page': 				$cta = is_page(); 										break;
				case 'post': 				$cta = is_single(); 									break;
				case 'gallery': 			$cta = is_post_type_archive('gallery'); 				break;
				case 'gallery-single': 		$cta = (get_post_type() == 'gallery') ? true : false; 	break;
				default: 					$cta = false;											break;
			}

			// Push value to the array
			array_push($displayCTA, $cta);
		}

		// If 'true' is found in the array, and this specific page is allowing the CTA, we're going to display the CTA on this page.
		if( in_array(true, $displayCTA) && (get_field('cta_page_display') != 'no') ):

			// Get our variables (depends on if the page needs the content overwritten)
			$ctaTitle 	= (get_field('cta_page_title')) ? get_field('cta_page_title') : get_field('cta_title', 'options');
			$ctaContent = (get_field('cta_page_content')) ? get_field('cta_page_content') : get_field('cta_content', 'options');
?>
			<div class="cta tertiary lighter">
				<div class="row">
	
					<?php if($ctaTitle): ?>
						<div class="cta-title medium-4 columns">
							<h3><?= $ctaTitle; ?></h3>
						</div>
					<?php endif; ?>

					<?php if($ctaContent): ?>
						<div class="cta-content medium-8 columns tertiary">
							<?= $ctaContent; ?>
						</div>
					<?php endif; ?>

				</div>

				<span class="half-band show-for-medium-up tertiary"></span>
			</div>

<?php
		endif;
	}
?>