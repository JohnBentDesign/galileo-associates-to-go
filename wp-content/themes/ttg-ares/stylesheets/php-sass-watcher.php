<?php
/**
 * Class SassWatcher
 *
 * This simple tool compiles all .scss files in folder A to .css files (with exactly the same name) into folder B.
 * To keep things as minimal as possible, this tool compiles every X seconds, regardless of changes within the files.
 * This seems weird, but makes sense as checking for changes in the files is more CPU-extensive than simply
 * re-compiling them. SassWatcher uses scssphp, the best SASS compiler in PHP available.
 *
 * SassWatcher is not a standalone compiler, it's just a little method that uses the excellent scssphp compiler written
 * by Leaf Corcoran (https://twitter.com/moonscript), which can be found here: http://leafo.net/scssphp/ and adds
 * automatic interval-compiling to it.
 *
 * The currently supported version of SCSS syntax is 3.2.12, which is the latest one.
 * To avoid confusion: SASS is the name of the language itself, and also the "name" of the "first" version of the
 * syntax (which was quite different than CSS). Then SASS's syntax was changed to "SCSS", which is more like CSS, but
 * with awesome additional possibilities and features.
 *
 * The compiler uses the SCSS syntax, which is recommended and mostly used. The old SASS syntax is not supported.
 *f
 * @see SASS Wikipedia: http://en.wikipedia.org/wiki/Sass_%28stylesheet_language%29
 * @see SASS Homepage: http://sass-lang.com/
 * @see scssphp, the used compiler (in PHP): http://leafo.net/scssphp/
 *
 * How to use this tool:
 *
 * 1. Edit $sass_watcher->watch( ... ); in the last line of this file and put your stuff in here, see the parameter
 *    list below.
 * 2. Make sure PHP can write into your CSS folder.
 * 3. Run the script:
 *    a) simple way, from browser, just enter the URL to scss-compiler.php: http://127.0.0.1/folder/php-sass-watcher.php
 *       The script will run forever, even if you close the browser window.
 *    b) PHPStorm users can run the script by right-clicking the file and selecting "Run php-sass-watcher.php".
 * 4. To stop the script, stop/restart your Apache/Nginx/etc. or press the red "stop process button in PHPStorm.
 *
 * The parameters:
 *
 *  1. relative path to your SCSS folder
 *  2. relative path to your CSS folder (make sure PHP has write-rights here)
 *  3. the compiling interval (in seconds)
 *  4. relative path to the scss.inc.php file, which is the main file of the SASS compiler used
 *     here. Download the script manually from http://leafo.net/scssphp/ or "require" it via Composer:
 *     "leafo/scssphp": "0.0.9"
 *  5. optional: how the .css output should look like. See http://leafo.net/scssphp/docs/#output_formatting for more.
 *
 * How the tool works:
 *
 * Every X seconds ALL files in the scss folder will be compiled to same-name .css files in the css folder.
 * The tool does not stop when a .scss file is broken, has syntax error or similar.
 * The tool does not compile when .scss file is broken, has syntax error or similar. It will only compile next time
 * when there's a valid scss file.
 */

/*==========================================================================
CUSTOM COLORS DEFINED BY USER
==========================================================================*/
$primaryColor       = (get_field('color_primary', 'options'))       ? get_field('color_primary', 'options')     : '#ffffff';
$secondaryColor     = (get_field('color_secondary', 'options'))     ? get_field('color_secondary', 'options')   : '#565656';
$tertiaryColor      = (get_field('color_tertiary', 'options'))      ? get_field('color_tertiary', 'options')    : '#00aeef';
$quaternaryColor    = (get_field('color_quaternary', 'options'))    ? get_field('color_quaternary', 'options')  : '#691c33';

$overlayColor       = (get_field('bg_color_main', 'options'))       ? get_field('bg_color_main', 'options')     : '#565656';
$overlayOpacity     = (get_field('bg_opacity_main', 'options'))     ? get_field('bg_opacity_main', 'options')   : '.5';

$user_colors = '
    // FUNCTIONS
    //-------------------------------------------------------------
    // More Accurate Color Contrastor (http://codepen.io/bluesaunders/pen/FCLaz)

    // Calculate brightness of a given color.
    @function brightness($color) {
        @return ((red($color) * .299) + (green($color) * .587) + (blue($color) * .114)) / 255 * 100%;
    }

    // Compares contrast of a given color to the light/dark arguments and returns whichever is most "contrasty"
    @function color-contrast($color, $dark: $dark-text-default, $light: $light-text-default) {
        @if $color == null {
            @return null;
        }
        @else {
            $color-brightness: brightness($color);
            $light-text-brightness: brightness($light);
            $dark-text-brightness: brightness($dark);
            @return if(abs($color-brightness - $light-text-brightness) > abs($color-brightness - $dark-text-brightness), $light, $dark);
        }
    }


    // VARIABLES
    //-------------------------------------------------------------
    // Raw
    $primary: ' . $primaryColor . ';
    $secondary: ' . $secondaryColor . ';
    $tertiary: ' . $tertiaryColor . ';
    $quaternary: ' . $quaternaryColor . ';


    // PRIMARY
    //-------------------------------------------------------------
    .primary { background-color: $primary; }
    .primary-darker { background-color: darken($primary, 10%); }
    .primary-lighter { background-color: lighten($primary, 10%); }
    .primary, .overlay-primary {
        color: lighten($secondary, 15%);
        h1, h1 a, h2, h3, h4, h5, h6, a {
            color: $tertiary;
        }
        p, ol, ul, li, blockquote, dl, dt, dd, table, tr, td, span, pre, label, input, textarea {
            color: lighten($secondary, 15%);
        }
        hr { background-color: $tertiary; }
        blockquote { background-color: darken($primary, 10%); }
        a.button, input[type=submit], div.button {
            background-color: $tertiary;
            color: $primary;
            &:hover {
                background-color: darken($tertiary, 10%);
                color: $primary;
            }
            a { color: $primary; }
        }
        #main-menu {
            li:hover, .current_page_item, .current_menu_ancestor { background-color: $primary; }
            .sub-menu {
                background-color: $primary;
                li:hover { background-color: darken($primary, 10%); }
            }

        }
    }

    // SECONDARY
    //-------------------------------------------------------------
    .secondary { background-color: $secondary; }
    .secondary-lighter { background-color: lighten($secondary, 10%); }
    .secondary, .overlay-secondary {
        h1, h1 a, h2, h3, h4, h5, h6, a {
            color: $primary;
        }
        p, ol, ul, li, blockquote, dl, dt, dd, table, tr, td, span, pre, label, input, textarea {
            color: $primary;
        }
        hr { background-color: $tertiary; }
        blockquote { background-color: darken($secondary, 10%); }
        a.button, input[type=submit], div.button {
            background-color: rgba($primary, .8);
            border-color: $primary;
            color: $secondary;
            &:hover {
                background-color: darken($primary, 10%);
                color: $primary;
                border-color: $tertiary;
            }
        }
        #main-menu {
            li:hover, .current_page_item, .current_menu_ancestor { background-color: $secondary; }
            .sub-menu {
                background-color: $secondary;
                li:hover { background-color: darken($secondary, 10%); }
            }
        }
    }

    // TERTIARY
    //-------------------------------------------------------------
    .tertiary { background-color: $tertiary; }
    .tertiary-lighter  { background-color: lighten($tertiary, 10%); }
    .tertiary, .overlay-tertiary {
        h1, h1 a, h2, h3, h4, h5, h6, a {
            color: $primary;
        }
        p, ol, ul, li, blockquote, dl, dt, dd, table, tr, td, span, pre, label, input, textarea {
            color: $primary;
        }
        hr { background-color: $tertiary; }
        blockquote { background-color: darken($tertiary, 10%); }
        a.button, input[type=submit], div.button {
            background-color: rgba($primary, .8);
            border-color: $primary;
            color: $tertiary;
            &:hover {
                background-color: darken($primary, 10%);
                color: $tertiary;
                border-color: $quaternary;
            }
        }
        .st-single-color a:before { color: $primary !important; }
        #main-menu {
            li:hover, .current_page_item, .current_menu_ancestor { background-color: $tertiary; }
            .sub-menu {
                background-color: $tertiary;
                li:hover { background-color: darken($tertiary, 10%); }
            }
        }
    }

    // QUARTERNARY
    //-------------------------------------------------------------
    .quaternary, .has-bg { background-color: $quaternary; }
    .quaternary-lighter { background-color: lighten($quaternary, 10%); }
    .quaternary, .overlay-quaternary, .has-bg {
        h1, h1 a, h2, h3, h4, h5, h6, a {
            color: $primary;
        }
        p, ol, ul, li, blockquote, dl, dt, dd, table, tr, td, span, pre, label, input, textarea {
            color: $primary;
        }
        hr { background-color: $tertiary; }
        blockquote { background-color: darken($quaternary, 10%); }
        a.button, input[type=submit], div.button {
            background-color: rgba($tertiary, .8);
            border-color: $tertiary;
            color: $primary;
            &:hover {
                background-color: darken($tertiary, 10%);
                color: $primary;
                border-color: $secondary;
            }
        }
        #main-menu {
            li:hover, .current_page_item, .current_menu_ancestor { background-color: $quaternary; }
            @include bp(1082px) {
                .sub-menu {
                    background-color: $quaternary;
                    li:hover { background-color: darken($quaternary, 10%); }
                }
            }
            @include bpmax(1081px) {
                .inline-list {
                    background-color: $quaternary;
                    a { border-bottom: 1px solid rgba(lighten($quaternary, 10%), .5); }
                    a:hover { background-color: darken($quaternary, 7%); }
                    .sub-menu { background-color: rgba(darken($quaternary,10%), .5);}
                }
            }
        }
    }


    body { background-color: $secondary; }

    .content {
        h1, h2, h3, h4, h5, h6 { color: $tertiary; }
        a, blockquote cite a { color: $quaternary; }
    }
    .slick-dots li button {
        background-color: $primary;
    }
    aside.sidebar { border-left: 1px solid darken($primary, 10%); }
    .social a:before { color: $primary !important; }
    .categorizing { border-top: 1px dotted darken($primary, 7%); }

.upper .columns { border-left: 1px solid rgba($primary, .5); }
.grid-item__container, .post.excerpt, .post.full header { border-top: 1px solid darken($primary, 10%); }
.grid-content__item { border-top: 1px solid darken($primary, 10%); }
.widget li:nth-child(odd) { background-color: darken($primary, 7%); }
.widget li:nth-child(even) { background-color: darken($primary, 4%); }

//overlays
.no-overlay {}
.overlay-primary { background-color: rgba(' . $primaryColor . ', .8);}
.overlay-secondary { background-color: rgba(' . $secondaryColor . ', .8);}
.overlay-tertiary { background-color: rgba(' . $tertiaryColor . ', .8);}
.overlay-quaternary { background-color: rgba(' . $quaternaryColor . ', .8);}

//Page Specific
.icon-area {
    background-color: darken(' . $primaryColor . ', 2.5%);
}

input {
    border-color: ' . $quaternaryColor . ';
}


//Darkened Subnavs
//
.cta-button {
    color: ' . $tertiaryColor . ';
    &:hover {
        color: lighten(' . $tertiaryColor . ', 10%);
    }
}

input[type="submit"] {
    cursor: pointer;
    @include bp($beta) {
        background-color: ' . $tertiaryColor . ';
        border-color: ' . $tertiaryColor . ';
    }
}

.shape:hover {
    background-color: lighten(' . $tertiaryColor . ', 10%);
}

.rectangle__title {
    color: ' . $primaryColor . ';
}

.main-nav {
    a {
        color: ' . $primaryColor . ';
    }
}

//Header Links
.header-content a {
    color: ' . $secondaryColor . ';
}

.page-title {
    background-color: ' . $primaryColor . ';
}

$slick-dot-color: ' . $primaryColor . ' !default;
$slick-dot-color-active: ' . $tertiaryColor . ' !default;

.the-grid__title {
    background-color: ' . $primaryColor . ';
}

.shape:hover {
    background-color: lighten(' . $tertiaryColor . ', 10%);
}

.rectangle__title {
    color: ' . $primaryColor . ';
}

.grid-content__title {
    background-color: ' . $primaryColor . ';
}

#searchsubmit {
    color: ' . $primaryColor . ';
}

#s {
    color: ' . $secondaryColor . ';
}


.overlay.overlay-primary {
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzFlNTc5OSIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUwJSIgc3RvcC1jb2xvcj0iIzI5ODlkOCIgc3RvcC1vcGFjaXR5PSIwLjUiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzdkYjllOCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
    background: -moz-linear-gradient(top, rgba(30,87,153,0) 0%, rgba($primary, .25) 77%,  rgba($primary, .75) 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(30,87,153,0)), color-stop(77%,rgba($primary, .75)), color-stop(100%, rgba($primary, .75)));
    background: -webkit-linear-gradient(top, rgba(30,87,153,0) 0%,rgba($primary, .25) 77%, rgba($primary, .75) 100%);
    background: -o-linear-gradient(top, rgba(30,87,153,0) 0%,rgba($primary, .25) 77%, rgba($primary, .75) 100%);
    background: -ms-linear-gradient(top, rgba(30,87,153,0) 0%,rgba($primary, .25) 77%, rgba($primary, .75) 100%);
    background: linear-gradient(to bottom, rgba(30,87,153,0) 0%,rgba($primary, .25) 77%, rgba($primary, .75) 100%);
}
.overlay.overlay-secondary {
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzFlNTc5OSIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUwJSIgc3RvcC1jb2xvcj0iIzI5ODlkOCIgc3RvcC1vcGFjaXR5PSIwLjUiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzdkYjllOCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
    background: -moz-linear-gradient(top, rgba(30,87,153,0) 0%, rgba($secondary, .25) 77%,  rgba($secondary, .75) 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(30,87,153,0)), color-stop(77%,rgba($secondary, .75)), color-stop(100%, rgba($secondary, .75)));
    background: -webkit-linear-gradient(top, rgba(30,87,153,0) 0%,rgba($secondary, .25) 77%, rgba($secondary, .75) 100%);
    background: -o-linear-gradient(top, rgba(30,87,153,0) 0%,rgba($secondary, .25) 77%, rgba($secondary, .75) 100%);
    background: -ms-linear-gradient(top, rgba(30,87,153,0) 0%,rgba($secondary, .25) 77%, rgba($secondary, .75) 100%);
    background: linear-gradient(to bottom, rgba(30,87,153,0) 0%,rgba($secondary, .25) 77%, rgba($secondary, .75) 100%);
}
.overlay.overlay-tertiary {
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzFlNTc5OSIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUwJSIgc3RvcC1jb2xvcj0iIzI5ODlkOCIgc3RvcC1vcGFjaXR5PSIwLjUiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzdkYjllOCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
    background: -moz-linear-gradient(top, rgba(30,87,153,0) 0%, rgba($tertiary, .25) 77%,  rgba($tertiary, .75) 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(30,87,153,0)), color-stop(77%,rgba($tertiary, .75)), color-stop(100%, rgba($tertiary, .75)));
    background: -webkit-linear-gradient(top, rgba(30,87,153,0) 0%,rgba($tertiary, .25) 77%, rgba($tertiary, .75) 100%);
    background: -o-linear-gradient(top, rgba(30,87,153,0) 0%,rgba($tertiary, .25) 77%, rgba($tertiary, .75) 100%);
    background: -ms-linear-gradient(top, rgba(30,87,153,0) 0%,rgba($tertiary, .25) 77%, rgba($tertiary, .75) 100%);
    background: linear-gradient(to bottom, rgba(30,87,153,0) 0%,rgba($tertiary, .25) 77%, rgba($tertiary, .75) 100%);
}
.overlay.overlay-quaternary {
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzFlNTc5OSIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUwJSIgc3RvcC1jb2xvcj0iIzI5ODlkOCIgc3RvcC1vcGFjaXR5PSIwLjUiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzdkYjllOCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
    background: -moz-linear-gradient(top, rgba(30,87,153,0) 0%, rgba($quaternary, .25) 77%,  rgba($quaternary, .75) 100%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(30,87,153,0)), color-stop(77%,rgba($quaternary, .75)), color-stop(100%, rgba($quaternary, .75)));
    background: -webkit-linear-gradient(top, rgba(30,87,153,0) 0%,rgba($quaternary, .25) 77%, rgba($quaternary, .75) 100%);
    background: -o-linear-gradient(top, rgba(30,87,153,0) 0%,rgba($quaternary, .25) 77%, rgba($quaternary, .75) 100%);
    background: -ms-linear-gradient(top, rgba(30,87,153,0) 0%,rgba($quaternary, .25) 77%, rgba($quaternary, .75) 100%);
    background: linear-gradient(to bottom, rgba(30,87,153,0) 0%,rgba($quaternary, .25) 77%, rgba($quaternary, .75) 100%);
}

';


$user_custom_font_style = pantheon_function_font_select(array(
    'header'    => 'header',
    'footer'    => 'footer',
    'sidebar'   => 'aside.sidebar',
    'widget'    => '.widget',
    'post'      => '.the-post',
    'page'      => '#content-area',
    'gallery'   => 'article.gallery',
    'product'   => 'li.product',
    )
);

$user_custom_text_style = pantheon_function_text_style(array(
    'header'    => 'header',
    'footer'    => 'footer',
    'sidebar'   => 'aside.sidebar',
    'widget'    => '.widget',
    'post'      => '.the-post',
    'page'      => '#content-area',
    'gallery'   => 'article.gallery',
    'product'   => 'li.product',
    )
);

$user_custom_styles = get_field('advanced_css', 'options');


/*==========================================================================*/

class SassWatcher
{
    /**
     * Watches a folder for .scss files, compiles them every X seconds
     * Re-compiling your .scss files every X seconds seems like "too much action" at first sight, but using a
     * "has-this-file-changed?"-check uses more CPU power than simply re-compiling them permanently :)
     * Beside that, we are only compiling .scss in development, for production we deploy .css, so we don't care.
     *
     * @param string $scss_folder source folder where you have your .scss files
     * @param string $css_folder destination folder where you want your .css files
     * @param int $interval interval in seconds
     * @param string $scssphp_script_path path where scss.inc.php (the scssphp script) is
     * @param string $format_style CSS output format, ee http://leafo.net/scssphp/docs/#output_formatting for more.
     */
    public function watch($scss_folder, $css_folder, $scssphp_script_path, $format_style = "scss_formatter_compressed", $user_defined_colors = null, $user_defined_custom_font = null, $user_defined_styles = null)
    {
        // load the compiler script (scssphp), more here: http://www.leafo.net/scssphp
        require $scssphp_script_path;
        $scss_compiler = new scssc();

        // set the path to your to-be-imported mixins. please note: custom paths are coming up on future releases!
        $scss_compiler->setImportPaths($scss_folder);
        // set css formatting (normal, nested or minimized), @see http://leafo.net/scssphp/docs/#output_formatting
        $scss_compiler->setFormatter($format_style);

        // get all .scss files from scss folder
        $filelist = glob($scss_folder . "*.scss");

        // step through all .scss files in that folder
        foreach ($filelist as $file_path) {
            // get path elements from that file
            $file_path_elements = pathinfo($file_path);

            // get file's name without extension
            $file_name = $file_path_elements['filename'];

            // get .scss's content, put it into $string_sass
            $string_sass = file_get_contents($scss_folder . $file_name . ".scss");

            // Add our user defined colors to the compiled code
            $string_sass .= $user_defined_colors;

            // Add our user font styles to the compiled code
            $string_sass .= $user_defined_custom_font;

            // Add our user defined styles to the compiled code (so we can save it)
            $string_sass .= $user_defined_styles;

            // try/catch block to prevent script stopping when scss compiler throws an error
            try {
                // compile this SASS code to CSS
                $string_css = $scss_compiler->compile($string_sass);
                // write CSS into file with the same filename, but .css extension
                file_put_contents($css_folder . $file_name . ".css", $string_css);
            } catch (Exception $e) {
                echo 'There was an error compiling your CSS.';
            }
        }
    }
}

$sass_watcher = new SassWatcher();
$sass_watcher->watch(get_template_directory() . '/stylesheets/sass/', get_template_directory() . '/stylesheets/css/', get_template_directory() . '/stylesheets/scss.inc.php', 'scss_formatter', $user_colors, $user_custom_font_style, $user_custom_styles);
