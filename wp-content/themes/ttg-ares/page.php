<?php
	/*===========================================================================
	PAGE
	============================================================================*/
?>

<?php get_template_part('parts/shared/header', 'html'); ?>

<!-- Main Content Area -->
<div class="main-wrap">
	<main>

	   <?php get_template_part('parts/shared/page-header'); ?>
	   <?php get_template_part('parts/shared/callout-band'); ?>

	   <div id="content-area" class="primary row single-col">
			<div class="main medium-8 columns">
				<?php if(have_posts()): while(have_posts()): the_post(); ?>
					<div class="content">
						<?php the_content(); ?>
					</div>
				<?php endwhile; endif; ?>
			</div>
			<?php get_template_part('parts/shared/sidebar'); ?>
		</div>

		<?php get_template_part('parts/shared/the-grid'); ?>
		<?php get_template_part('parts/shared/the-grid-content'); ?>

	</main>
</div>

<!-- Get the Footer -->
<?php get_template_part('parts/shared/footer', 'html'); ?>