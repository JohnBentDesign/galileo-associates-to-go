<?php
/*===========================================================================
Template Name: Single Column
=============================================================================
*/
?>

<?php get_template_part('parts/shared/header', 'html'); ?>

<!-- Main Content Area -->
<div class="main-wrap">
    <main>

        <?php get_template_part('parts/shared/page-header'); ?>
        <?php get_template_part('parts/shared/callout-band'); ?>

        <div id="content-area" class="content primary row single-col">
            <div class="columns">
                <?php
                    if(have_posts()):
                        while(have_posts()): the_post();
                            the_content();
                        endwhile;
                    endif;
                ?>
            </div>
        </div>

        <?php get_template_part('parts/shared/the-grid'); ?>
        <?php get_template_part('parts/shared/the-grid-content'); ?>

    </main>
</div>

<?php get_template_part('parts/shared/footer', 'html'); ?>