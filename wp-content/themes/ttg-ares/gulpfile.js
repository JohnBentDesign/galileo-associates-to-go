var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    minify      = require('gulp-minify-css'),
    prefix      = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    reload      = browserSync.reload;

gulp.task('styles', function() {
    gulp.src('stylesheets/sass/style.scss')
        .pipe(sass())
        .pipe(minify())
        .pipe(prefix({
            browsers: ['last 2 versions']
        }))
        .pipe(gulp.dest('stylesheets/css/'))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('php-reload', function() {
    reload();
});

gulp.task('watch', function() {
    gulp.watch('stylesheets/sass/**/*.scss', ['styles']);
    gulp.watch('**/*.php', ['php-reload']);
});

gulp.task('browser-sync', function() {
    browserSync({
        proxy: "localhost/",
        open: 'external'
    });
});

gulp.task('default', gulp.parallel('browser-sync', 'styles', 'watch'));