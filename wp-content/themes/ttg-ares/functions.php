<?php
	/*===========================================================================
	INCLUDE THE INCLUDES SO THAT THEY'RE INCLUDED
	===========================================================================*/
	// Advanced Custom Fields
	include('includes/advanced-custom-fields/ares-theme.php');
    include('includes/advanced-custom-fields/ares-page.php');
    include('includes/advanced-custom-fields/ares-home-slider.php');
    include('includes/advanced-custom-fields/ares-home-sidebar.php');

    // Backup JSON also included in 'includes/advanced-custom-fields/json/'


    /*===========================================================================
    ADD THEME SUPPORTS
    ===========================================================================*/
    add_theme_support('post-thumbnails');
    add_theme_support('menus');


    /*===========================================================================
    CUSTOM THUMBNAIL SIZES
    ===========================================================================*/
    add_image_size('TTG Featured Image', 1024, 500, true);
    add_image_size('TTG Gallery Normal Display', 637, 9999, true);
    add_image_size('TTG Medium Thumbnail', 350, 350, true);


    /*===========================================================================
    REGISTER MENUS
    ===========================================================================*/
    if(!function_exists('ttg_menus')){
        function ttg_menus() {
            register_nav_menus(array(
                'menu-main'         => 'Main Menu',
                'menu-top'          => 'Top Menu',
                'menu-footer'       => 'Footer Menu'
                ));
        }
        add_action('init', 'ttg_menus');
    }


    /*===========================================================================
    REGISTER SIDEBARS
    ===========================================================================*/
    $sidebarGeneral = array(
        'name'          => 'General Sidebar',
        'id'            => 'sidebar-general',
        'description'   => '',
        'class'         => '',
        'before_widget' => '<div class="widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>'
        );
    register_sidebar($sidebarGeneral);

    $sidebarBlog = array(
        'name'          => 'Blog Sidebar',
        'id'            => 'sidebar-blog',
        'description'   => '',
        'class'         => '',
        'before_widget' => '<div class="widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>'
        );
    register_sidebar($sidebarBlog);

    $homeBlog = array(
        'name'          => 'Home Sidebar',
        'id'            => 'sidebar-home',
        'description'   => '',
        'class'         => '',
        'before_widget' => '<div class="widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>'
        );
    register_sidebar($homeBlog);


    /*===========================================================================
    ENQUEUE SCRIPTS AND STYLES
    ===========================================================================*/
    // JAVASCRIPTS
    //---------------------------------------------------------------------------
    function ttg_scripts(){
        if(!is_admin()){
            // Vendor
            if(is_page_template('page-home.php')){
                wp_enqueue_script('slick', get_template_directory_uri() . '/js/vendor/slick.min.js', array('jquery'), '', false);
            }

            // Ares Scripts
            wp_enqueue_script('hephaestus', get_template_directory_uri() . '/js/ares.js', array('jquery', 'foundation'), '', true);
        }
    }
    add_action('wp_enqueue_scripts', 'ttg_scripts');

    // STYLESHEETS
    //---------------------------------------------------------------------------
    function ttg_styles(){
        if(!is_admin()){
            // Webfonts
            wp_enqueue_style('fonts-opensans', 'https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700|Open+Sans:400,700,300');

            // hephaestus Stylesheets
            wp_enqueue_style('ares', get_template_directory_uri() . '/stylesheets/css/style.css');
        }
    }
    add_action('wp_enqueue_scripts', 'ttg_styles');

    function ttg_styles_admin() {
        if(is_admin()){
            wp_enqueue_style('custom-admin', get_template_directory_uri() . '/stylesheets/css/admin.css');
        }
    }
    add_action('admin_enqueue_scripts', 'ttg_styles_admin');

    // RECOMPILES THE CSS ONLY WHEN THE OPTIONS PAGE IS SAVED
    //---------------------------------------------------------------------------
    if(get_field('dev_compile', 'option')){
        include(get_template_directory() . '/stylesheets/php-sass-watcher.php');
    }
    else {
        function ttg_compile_css( $post_id ) {
            $page = htmlspecialchars($_GET["page"]);
            if($page == 'acf-options-theme-styling'){
                // Compile our CSS
                include(get_template_directory() . '/stylesheets/php-sass-watcher.php');
            }
        }
        // run after ACF saves the $_POST['fields'] data
        add_action('acf/save_post', 'ttg_compile_css', 20);
    }


    // Add Touch ARIA Roles to Dropdowns (for IE 10+)
    add_filter('nav_menu_link_attributes', 'add_aria_haspopup_atts', 10, 3);
    function add_aria_haspopup_atts($atts, $item, $args)
    {
        if (in_array('menu-item-has-children', $item->classes)) {
        $atts['aria-haspopup'] = 'true';
        }
        return $atts;
    }