<?php
	/*===========================================================================
	SIDEBAR: MAIN
	=============================================================================
	Right hand sidebar that appears on most pages.
	*/

	$postType = get_post_type();
?>
<aside class="sidebar medium-4 columns">

	<?php
		// SIDEBAR - GENERAL
		//===================================
		if(is_front_page()){
			if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Home Sidebar') ): endif;
		}
		// SIDEBAR - GENERAL
		//===================================
		elseif(is_page()){
			if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('General Sidebar') ): endif;
		}
		// SIDEBAR - BLOG
		//===================================
		else {
			if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar') ): endif;
		}
	?>

</aside><?php // /.sidebar ?>