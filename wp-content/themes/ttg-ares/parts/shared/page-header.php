<?php 
	//-----------------------
	// PAGE HEADER
	//-----------------------
	// Get all our lovely header pieces
	$image        = pantheon_display_post_featured_image($post->ID, 'large', null, 'style', false);
	$overlayColor = get_field('overlay_color');
	$align        = get_field('content_placement');
	$content      = get_field('content');
	$pageTitle    = '<h1>' . get_the_title() . '</h1>';

	// Get title placements
	$default 	= get_field('page_title_place', 'options');
	$pageSet	= get_field('page_title_place');
	$placement 	= ($pageSet && ($pageSet != 'default')) ? $pageSet : $default;

	// Get band color
	$default 	= get_field('page_title_bg', 'options');
	$pageSet	= get_field('page_title_bg');
	$bgColor 	= ($pageSet && ($pageSet != 'default')) ? $pageSet : $default;

	// Build Title
	function pageTitle($text, $place, $color){
		if(strpos($place, 'band')){
			echo '<div class="' . $color . '">';
			echo '<div class="' . $color . '-lighter row text-center">';
			echo $text;
			echo '</div>';
			echo '</div>';
		}
		else {
			echo '<div class="row primary text-center">';
			echo $text;
			echo '</div>';
		}
	}
?>

<header class="page">

	<?php if(strpos($placement, 'above') !== false){ pageTitle($pageTitle, $placement, $bgColor); } ?>

	<?php if($image): ?>
		<div class="row">
			<div class="header-image"<?= $image; ?>>
				<?php if($content): ?>
					<div class="header-content overlay-<?= $overlayColor ?> header-position-<?= $align ?>">
						<?= $content ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

	<?php if(strpos($placement, 'below') !== false){ pageTitle($pageTitle, $placement, $bgColor); } ?>

</header>
