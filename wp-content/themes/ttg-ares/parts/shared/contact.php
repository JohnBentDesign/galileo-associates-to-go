<?php 
	/*===========================================================================
	CONTACT DETAILS
	===========================================================================*/
	// Get all our information!
	$addressOne 		= get_field('general_address_one', 'options');
	$addressTwo 		= get_field('general_address_two', 'options');
	$addressCity 		= get_field('general_address_city', 'options');
	$addressState 		= get_field('general_address_state_abbr', 'options');
	$addressZip 		= get_field('general_address_zip', 'options');
	$phone 				= get_field('general_phone', 'options');
	$emailPrimary 		= get_field('general_email_primary', 'options');

	// Make an array with our details so we don't show empty containers
	global $contactDetails;
	$contactDetails = array($addressOne, $addressTwo, $addressCity, $addressState, $addressZip, $phone, $emailPrimary);

	// Header Address/Phone
	$headerTop = get_field('header_top', 'options');
	$hasPhone 	= is_array($headerTop) && in_array('phone', $headerTop);
	$hasAddress = is_array($headerTop) && in_array('address', $headerTop);
	$hasEmail 	= is_array($headerTop) && in_array('email', $headerTop);

	if(!empty($contactDetails) && ($hasPhone || $hasAddress || $hasEmail) ):

		echo '<div class="vcard">';

				// ADDRESS //
				if($hasAddress && ($addressOne || $addressTwo || $addressCity || $addressState || $addressZip)){
					echo '<div class="adr">';

					// Address: Company Name
					echo '<p class="fn org">' . get_bloginfo('title') . '</p>';
					// Address: Street
					if($addressOne) { echo '<p class="street-address">' . $addressOne . '</p>'; }
					if($addressTwo) { echo '<p class="extended-address">' . $addressTwo . '</p>'; }
					// Address: Locale
					if($addressCity || $addressState || $addressZip) {
						echo '<p class="locale">';
						if($addressCity) 									{ echo '<span class="locality">' . $addressCity . '</span>'; }
						if($addressCity && ($addressState || $addressZip)) 	{ echo ', '; }
						if($addressState) 									{ echo '<span class="region">' . $addressState . '</span> '; }
						if($addressZip) 									{ echo '<span class="postal-code">' . $addressZip . '</span>'; }
						echo '</p>';
					}

					echo '</div>';
				}

				// NUMBERS //
				if($hasPhone && ($phone || $phoneFree)){
					echo '<div class="numbers">';
					if($phone) 		{ echo '<p class="tel"><a href="tel:' . $phone . '">' . $phone . '</a></p>'; }
					echo '</div>';
				}

				// EMAILS //
				if($hasEmail && ($emailPrimary || $emailSecondary)){
					echo '<div class="emails">';
					if($emailPrimary) 	{ echo '<a href="mailto:' . $emailPrimary . '">' . $emailPrimary . '</a>'; }
					echo '</div>';
				}
		
		echo '</div>';

	endif;


