<?php 
	//-----------------------
	// ICON GRID
	//-----------------------
	$hasGrid = get_field('icon_grid_area');

	if($hasGrid):
		if(have_rows('icon_items')) :
			$gridTitle 		= get_field('grid_title');
			$iconStyle 		= get_field('icon_style');
			$linkType 		= get_field('icon_grid_link_type');
			$gridColor		= get_field('grid_bgcolor');
			$gridWidth 		= get_field('icon_grid_width');
?>
			<div class="the-grid primary row">
				<div class="columns">
					<?php
						if($gridTitle):
							echo '<h3 class="text-center">' . $gridTitle . '</h3>';
						endif;
					?>
					<ul class="grid-item__container medium-block-grid-<?= $gridWidth; ?>">
						<?php 
							while (have_rows('icon_items')) : the_row();
								$image 		= get_sub_field('image');
								$iconType 	= get_sub_field('icon_type');

								// Set Url
								if ($linkType === 'internal') :
									$iconGridUrl  = get_sub_field('internal_url');
								elseif ($linkType === 'external') :
									$iconGridUrl  = get_sub_field('external_url');
								elseif ($linkType === 'jump') :
									$removeSpaces = str_replace(' ', '-', get_sub_field('text'));
									$iconGridUrl  = '#' . $removeSpaces;
								endif;
						?>
								<li class="grid-item__icon has-<?= $iconStyle; ?>">
									<a href="<?= $iconGridUrl ?>">
										<?php if($iconType == "image"): ?>
											<div class="<?= $iconStyle; ?> shape shape--image-bg <?= $gridColor; ?>" style="background-image: url('<?= $image ?>');"></div>
										<?php elseif($iconType == "custom-icon"): ?>
											<div class="<?= $iconStyle; ?> shape <?= $gridColor; ?>">
												<img class="custom-icon-img" src="<?php the_sub_field('custom_icon') ?>"></img>
											</div>
										<?php endif; ?>
									</a>
									<?php if ($iconStyle === 'rectangle'): ?>
										<a class="grid-item__label <?= $gridColor; ?>" href="<?= $iconGridUrl ?>"><h3 class="rectangle__title"><?php the_sub_field('text'); ?></h3></a>
									<?php else : ?>
										<a class="grid-item__label" href="<?= $iconGridUrl ?>"><h3><?php the_sub_field('text'); ?></h3></a>
									<?php endif; ?>
									<?php
										if($linkType != 'jump'):
											the_sub_field('content_content');
										endif;
									?>
								</li>
						<?php endwhile; ?>
					</ul>
				</div>
			</div>
<?php 
		endif;
	endif;
?>

