<?php 
	// The Footer!
	$footerBG 		= get_field('footer_color', 'options');
	$footerBtmBG 	= get_field('footer_btm_color', 'options');
?>
<footer class="main">

	<?php if(have_rows('footer_columns', 'options')): ?>
		<div class="<?= $footerBG; ?> upper">
			<div class="<?= $footerBG; ?>-lighter row" data-equalizer>

				<?php
					$count = count(get_field('footer_columns', 'options'));
					switch ($count) {
						case 2: 	$columnSize = 'medium-6'; break;
						case 3: 	$columnSize = 'medium-4'; break;
						default: 	$columnSize = 'medium-12'; break;
					}

				    while(have_rows('footer_columns', 'options')): the_row();
				    	$title = $content = '';

				    	echo '<div class="columns ' . $columnSize . '" data-equalizer-watch>';

						if( get_row_layout() == 'custom_content' ){
							$title 		= get_sub_field('title') ? '<h4>' . get_sub_field('title') . '</h4>' : '';
							$content 	= get_sub_field('content');
							echo $title;
							echo $content;
						}
						if( get_row_layout() == 'social_buttons' ){
							$title 		= get_sub_field('title') ? '<h4>' . get_sub_field('title') . '</h4>' : '';
							$social 	= get_sub_field('social_kind');
							echo $title;
							if($social == 'connect') { pantheon_display_social_connect('footer'); }
							if($social == 'share') { pantheon_display_social_share('footer'); }
						}
						if( get_row_layout() == 'latest_post' ){
							$title 		= get_sub_field('title') ? '<h4>' . get_sub_field('title') . '</h4>' : '';
							$postinfo 	= get_sub_field('post_information');
							$number 	= get_sub_field('post_count');
							$args 		= array( 
											'post_type' 		=> 'post',
											'post_status' 		=> 'publish',
											'posts_per_page' 	=> $number,
											'order' 			=> 'DESC',
											'orderby' 			=> 'date',
										);
							$the_query = new WP_Query( $args );

							echo $title;

							if ( $the_query->have_posts() ) :
								echo '<ul class="post-list">';
								while ( $the_query->have_posts() ) : $the_query->the_post();
									$excerpt 	= (in_array('excerpt', $postinfo)) ? '<div class="excerpt">'. wpautop(wp_trim_words(get_the_content(), 15, '... <a href="' . get_permalink() . '">Read More &raquo;</a></div>')) : '';
									$date 		= (in_array('date', $postinfo)) ? '<date>' . get_the_date() . '</date>' : '';

									echo '<li>';
									echo '<a href="' . get_permalink() . '" class="title">' . get_the_title() . '</a>';
									echo $date;
									echo $excerpt;
									echo '</li>';

								endwhile;
								echo '</ul>';
							endif;
							wp_reset_postdata();
						}

						echo '</div>';

					endwhile;
				?>

			</div>
		</div>
	<?php endif; ?>

	<div class="<?= $footerBtmBG; ?>">
		<div class="row">
			<div class="large-centered text-center footer-inner">
				<div class="column small-12 medium-8 small-centered">
					<?php
						//Footer Menu
						$menuFooter = array(
							'theme_location'  	=> 'menu-footer',
							'container'       	=> 'nav',
							'container_class' 	=> 'inline-menu',
							'menu_class'      	=> 'menu inline-list',
							'depth'           	=> 1,
							'fallback_cb'		=> 0
						);
						wp_nav_menu($menuFooter);
					?>
				</div>
				<p class="copyright">&copy;<?php echo date('Y') . ' '; bloginfo('name'); ?>. All Rights Reserved. Powered by <a href="http://technologytherapy.com/" target="_blank">Technology Therapy To Go.</a></p>
			</div>
		</div>
	</div>

</footer>