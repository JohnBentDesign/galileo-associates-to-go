<?php
$hasGlobalCallout = get_field('global_callout', 'options');
$globalCalloutTitle = get_field('global_callout_title', 'options');


// Superdope Callout
$hasCallout     = get_field('has_callout');
$calloutType    = get_field('callout_type');
$calloutTitle   = get_field('callout_heading');
$calloutCopy    = get_field('callout_copy');
$linkText       = get_field('link_text');
$calloutBG      = get_field('callout_bgcolor');
if (get_field('callout_style')) : $button = 'button' ; endif;


//Set Internal vs. External Links
if ($calloutType == "int") :
    $calloutUrl  = get_field('callout_internal_url');
elseif ($calloutType == "ext") :
    $calloutUrl  = get_field('callout_external_url');
endif;
?>

<!-- Callout Band -->
<?php if($hasCallout) : ?>

    <section class="callout <?= $calloutBG; ?> text-center">
        <div class="row <?= $calloutBG; ?>-lighter">
            <?php if($calloutTitle) : ?>
                <h2><?= $calloutTitle ?></h2>
            <?php endif ; ?>
            <?= $calloutCopy ?>
            <a class="<?= $button ?>" href="<?= $calloutUrl ?>"><?= $linkText ?></a>
        </div>
    </section>

<?php endif; ?>