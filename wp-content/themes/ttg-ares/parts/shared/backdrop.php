<?php
	/*===========================================================================
	BACKDROP
	===========================================================================*/
	$isBlog 		= ((is_home() && get_option('page_for_posts')) || (is_single() && ($post->post_type == 'post')) || is_category() || is_day() || is_tag() || is_month() || is_year()) ? true : false; 
	$blogID 		= get_option('page_for_posts');
	$postType 		= get_post_type();

	// Figure out where we're pulling our backdrop from
	if($isBlog) 						{ $useID = $blogID; }
	elseif( ($postType == 'post') || ($postType == 'page') )	{ $useID = $post->ID; }
	else 								{ $useID = ''; }

	// Figure out what we're showing
	$hasFeatured 	= has_post_thumbnail( $useID );
	$hasTitle 		= get_field('page_header_title', $useID);
	$overlayColor 	= get_field('page_header_overlay', $useID);
?>

	<?php 
		// IMAGE //
		if( !is_page_template('page-home.php') && $hasFeatured && $useID ):
			$backdropImage 	= pantheon_display_post_featured_image($useID, 'large', null, 'style', false);	
	?>
			<div class="backdrop static <?= $postType; ?>" <?= $backdropImage; ?>>
				<?php if(!$hasTitle): ?>
					<h1 class="<?= $overlayColor; ?>"><?php the_title(); ?></h1>
				<?php endif; ?>
			</div>
	<?php endif; ?>