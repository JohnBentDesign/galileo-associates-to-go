<?php 
	//-----------------------
	// ICON GRID - CONTENT
	//-----------------------
	$hasGrid = get_field('icon_grid_area');

	if($hasGrid):
		if(have_rows('icon_items')) :
			$iconStyle      = get_field('icon_style');
			$linkType       = get_field('icon_grid_link_type');
			$gridColor      = get_field('grid_bgcolor');
			$gridWidth      = get_field('icon_grid_width');
			
			if ($linkType === "jump") :
?>
				<section class="grid-content primary row">
					<?php 
						while (have_rows('icon_items')) : the_row();
							$content 		= get_sub_field('content_content');
							$title   		= get_sub_field('text');
							$replaceSpaces 	= str_replace(' ', '-', get_sub_field('text'));
							$contentUrl  	= $replaceSpaces;
							$iconType 		= get_sub_field('icon_type');
							$image 			= get_sub_field("image");
							if (get_sub_field("override_image")) : $image = get_sub_field("the_override_image") ; endif ;
					?>

							<article id="<?= $contentUrl ?>" class="grid-content__item row">
								<aside class="grid-content__item__sidebar medium-3 columns icon-area__icon">
									<?php if($iconType == "image"): ?>
										<div class="grid-content__img" style="background-image: url('<?= $image ?>');"></div>
									<?php elseif($iconType == "custom-icon"): ?>
										<div class="shape tertiary">
											<img class="custom-icon-img" src="<?php the_sub_field('custom_icon') ?>"></img>
										</div>
									<?php endif; ?>
								</aside>

								<div class="bleurgh medium-9 columns">
									<h3 class="grid-content__title"><?= $title ?></h3>
									<?= $content ?>
								</div>
							</article>
					<?php endwhile; ?>
				</section>
<?php 
			endif;
		endif;
	endif;
?>