<?php
	// GLOBAL VARIABLES //
global $hasExpander;
$hasExpander = (is_page_template('page-home.php') || is_page_template('page-expander.php')) ? ' expander' : false;
$image        = get_field('image');

?>
<!doctype html>
<html>
<head>
  <?php // META TAGS // ?>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <?php // BLOG TITLE // ?>
  <title><?php wp_title(''); ?></title>

  <?php // FAVICON // ?>
  <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

  <?php // OG Tag // ?>
    <?php if(isset($image)): ?>
        <meta property="og:image" content="<?php echo $image; ?>" />
    <?php endif; ?>

  <?php // HEADER JAVASCRIPT // ?>
  <?php echo get_field('advanced_js_header', 'options'); ?>
  <?php
			// WORDPRESS HEADER //
  wp_head();
  ?>
</head>

<body <?php body_class(); ?>>
<div class="primary-darker">
  <?php
			// HEADER //
  get_template_part('parts/shared/header');
  ?>