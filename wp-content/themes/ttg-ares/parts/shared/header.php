<?php
//Topbar Config
$hasTopbar     = get_field('has_topbar', 'options');
$topbarColor   = get_field('topbar_color', 'options');
$topbarOptions = get_field('topbar_contains', 'options');
$hasPhone      = is_array($topbarOptions) && in_array('phone', $topbarOptions);
$hasNav        = is_array($topbarOptions) && in_array('topnav', $topbarOptions);
$hasCustom     = is_array($topbarOptions) && in_array('custom', $topbarOptions);
$topbarCustom  = get_field('topbar_custom', 'options');
$phoneNumber   = get_field('general_phone', 'options');
$navType 	   = get_field('nav_type', 'options');
$navColor 		= get_field('nav_color', 'options');

//Logo Config
$logo      = get_field('header_logo', 'options');
$logoAlign = get_field('logo_align', 'options');

// Main Nav Config
$navAlign = get_field('nav_align', 'options');

?>

<header class="main">

	<div class="row">
		<?php if($hasTopbar): ?>
			<div class="topbar <?= $topbarColor ?> columns large-12">
				<?php if($hasPhone): ?>
					<span class="phone"><a href="tel://<?= $phoneNumber ?>"><?= $phoneNumber ?></a></span>
				<?php endif; ?>

				<?php if($hasCustom): ?>
					<span class="custom"><?= $topbarCustom; ?></span>
				<?php endif; ?>

				<?php if($hasNav): ?>
					<?php $menuTop = array(
						'theme_location'    => 'menu-top',
						'container'         => 'nav',
						'container_class'   => 'topbar-nav',
						'menu_class'        => '',
						'depth'             => 0,
						'fallback_cb'       => 0
						);
					wp_nav_menu($menuTop);
					?>
				<?php endif; ?>
				
				<!-- Use the social icon func provided by Pantheon -->
				<?php pantheon_display_social_connect('header'); ?>
			</div>
		<?php endif; ?>
	</div>

	<div class="row">
		<div class="primary text-<?= $logoAlign; ?>">
			<div class="logo">
				<a href="<?php echo get_bloginfo ( 'url' ); ?>">
					<?php
						if(get_field('header_logo', 'options')){
							pantheon_display_post_field_image($logo, 'full', 'image');
						}
						else {
							bloginfo('title');
						}
					?>
				</a>
			</div>
		</div>
	</div>

	<div class="<?= $navColor;  ?>">
		<div class="<?= $navColor;  ?>-lighter row main-nav text-<?= $navAlign; ?>">
			<a href="#" id="menu-pull" class="menu-pull"><i class="navicon ion-navicon-round"></i>Navigation</a>
			<?php //Main Menu
				$menuMain = array(
				'theme_location' 	=> 'menu-main',
				'container'			=> 'nav',
				'container_class'	=> 'inline-menu ' . $navType,
				'container_id'      => 'main-menu',
				'menu_class'		=> 'inline-list',
				'depth'				=> 3,
				'fallback_cb'		=> 0
				);
				wp_nav_menu($menuMain);
			?>
		</div>
	</div>

</header>
