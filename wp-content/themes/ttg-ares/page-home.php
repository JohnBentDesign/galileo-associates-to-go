<?php
/*===========================================================================
Template Name: Homepage
=============================================================================
Template specifically for making it easier to target ACF display           */
?>

<?php get_template_part('parts/shared/header', 'html'); ?>

<!-- Homepage Slider -->
<?php if(have_rows('home_slider')):
    // Slider Config
    $contentSide        = get_field('home_slide_content_side');
    $overlayColor       = get_field('home_slide_overlay');
    $sliderTime         = get_field('home_slide_time');
    $sliderTimeAnimate  = get_field('home_slide_time_animate');
    $slideAnimate       = get_field('home_slide_animate');
    $slideType          = ($slideAnimate == 'fade') ? 1 : 0;
    ?>

    <div id="home-slider" class="home-slideshow quaternary side-<?= $contentSide; ?>">
        <div class="home-slider" data-slider-time="<?= $sliderTime; ?>" data-slider-animate="<?= $sliderTimeAnimate; ?>" data-slider-fade="<?= $slideType; ?>" data-slider-overlay="<?= $overlayColor; ?>-overlay">

            <?php 
                while ( have_rows('home_slider') ) : the_row();
                    // Slide Config
                    $image          = get_sub_field('slide_image');
                    $title          = get_sub_field('title');
                    $content        = get_sub_field('content');
                    $ctaDisplay     = get_sub_field('cta_display');
                    $ctaText        = get_sub_field('cta_text');
                    $ctaType        = get_sub_field('cta_type');
                    $ctaInternal    = get_sub_field('cta_internal');
                    $ctaExternal    = get_sub_field('cta_external');
                    $ctaOutput      = ($ctaDisplay) ? '<a href="' . $ctaInternal . $ctaExternal . '" class="cta-button">' . $ctaText . '</a>' : '';
                    $overlay        = ($overlayColor != 'no') ? 'overlay-' . $overlayColor : '';
                    $noverlay       = ($overlayColor == 'no') ? 'noverlay' : '';
            ?>
                    <div class="slide" style="background-image: url(<?= $image['sizes']['TTG Featured Image']; ?>)">
                        <div class="slide-content row">
                            <div class="slide-inner">
                                <h2><?= $title; ?></h2>
                                <?= wpautop($content); ?>
                                <?= $ctaOutput; ?>
                            </div>
                        </div>
                        <div class="overlay <?= $noverlay; ?> <?= $overlay; ?>"></div>
                    </div>
            <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>

<!-- Main Content Area -->
<div class="main-wrap">
    <main>

        <?php get_template_part('parts/shared/callout-band'); ?>

        <div class="row primary">
            <?php
                $sidebar    = get_field('display_sidebar');
                $size       = ($sidebar) ? 'medium-8' : 'medium-12';
            ?>
            <div id="content-area" class="content <?= $size; ?> columns">
                <?php
                if(have_posts()):
                    while(have_posts()): the_post();
                the_content();
                endwhile;
                endif;
                ?>
            </div>
            <?php if($sidebar){ get_template_part('parts/shared/sidebar'); } ?>
        </div><?php // /.content ?>

        <?php get_template_part('parts/shared/the-grid'); ?>
        <?php get_template_part('parts/shared/the-grid-content'); ?>

    </main>
</div>

<!-- Get the Footer -->
<?php get_template_part('parts/shared/footer', 'html'); ?>