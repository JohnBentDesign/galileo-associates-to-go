#Ares Theme

###Global Modules
- Topbar:
    + Contains:
        * Phone Number
        * Custom Content
        * Navigation
        * Social Icons
    + Config:
        * Alignment (Left, Right, Center)
        * Color (Theme Colors)
- Logo Area
    + Contains:
        * Logo Image
    + Config:
        * Alignment (Left, Right, Center)
- Primary Navigation
    + Contains:
        * Main Navigation
    + Config:
        * Alignment (Left, Right, Center)
        * Color (Theme Colors)
- Callout Band
    + Contains:
        * Image
        * Title
        * Copy
        * CTA
    + Config:
        * Default Content (Latest Blog Excerpt, etc.)
        * Image, Title, Copy, CTA Alignment


###Homepage Modules
- Slider
    + Contains:
        * Background Image
        * Title
        * Subtitle
    + Config:
        * Background Placement/Crop
        * Title Alignment (Left, Right, Center)
- Icon Area
    + Contains:
        * Title
        * Icons
            - Title
            - Icon
    + Config:
        * Title Alignment (Left, Right, Center)
        * Role (Jump to Id, internal links, external links)
        * Color (Theme Colors)
        * Icon Shape (Circle, Square, Rectangle (no img), etc.)
        * Icon Type (Icon, Image, )