<?php
/*===========================================================================
INDEX -  Default fallback style. Default page for blog viewing page
===========================================================================*/
?>

<?php get_template_part('parts/shared/header', 'html'); ?>

<!-- Main Content Area -->
<div class="main-wrap">
    <main>

        <!-- Top Border Above Content -->
        <div class="secondary-border row"></div>

        <!-- Icon Grid (Top Section of Jump List) -->
        <?php get_template_part('parts/shared/the-grid'); ?>

        <!-- Callout Band -->
        <?php get_template_part('parts/shared/callout-band'); ?>

        <!-- Icon Content (The stuff the Icon Grid links to) -->
        <?php get_template_part('parts/shared/the-grid-content'); ?>


        <!-- Two Column Layout -->
        <div id="content-area" class="primary row single-col">


            <div class="main medium-8 columns">

                <?php
                $featured = get_field('page_featured');
                if($featured):
                    echo '<div class="content-featured">';
                pantheon_display_post_field_image(get_field('page_featured'), 'TTG Featured Image', 'image', true);
                echo '</div>';
                endif;
                ?>

                <div class="inner primary">

                    <div class="content">
                        <!-- <h2 class="blog-title"><?= get_the_title(get_option('page_for_posts')); ?></h2> -->
                        <?php get_template_part('parts/posts/loop'); ?>
                        <?php get_template_part('parts/shared/pagination'); ?>
                    </div>

                </div>
            </div>

            <?php get_template_part('parts/shared/sidebar'); ?>

        </div>
    </main>
</div>

<!-- Get the Footer -->
<?php get_template_part('parts/shared/footer', 'html'); ?>