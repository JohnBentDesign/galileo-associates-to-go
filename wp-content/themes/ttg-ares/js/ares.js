jQuery(document).foundation();

(function($){
    $(document).ready(function(){

        //Toggle Menu
        jQuery("#menu-pull").click(function () {
            jQuery("#main-menu").toggleClass("active");
        });

        //===================================================
        // HOMEPAGE SLIDER
        //===================================================
        $slider = $('.home-slider');

        // Show only if homepage slider is available
        if($slider.length > 0){
            // Variables
            var $slideFade      = $slider.attr('data-slider-fade'),
            $slideTime      = $slider.attr('data-slider-time'),
            $slideAnimate   = $slider.attr('data-slider-animate');

            if($slideFade == 1) { $slideFade = true; }
            else { $slideFade = false; }

            // Initialize Slideshow
            $('.home-slider').slick({
                autoplay: true,
                autoplaySpeed: $slideTime,
                arrows: false,
                dots: true,
                fade: $slideFade,
                infinite: true,
                lazyLoad: 'ondemand',
                speed: $slideAnimate,
                // onInit : function(){
                //     slideChange();
                // },
                // onAfterChange : function(index){
                //     slideChange();
                // }
            });
        }

        // // slideChange Function
        // function slideChange(){
        //     var $activeSlide    = $('.slick-active'),
        //     $activeTitle    = $activeSlide.attr('data-slider-title'),
        //     $activeContent  = $activeSlide.attr('data-slider-content'),
        //     $activeCTA      = $activeSlide.attr('data-slider-cta'),
        //     $slideContent   = $('.slide-content');

        //     $slideContent.fadeOut();

        //     // If content is present, we'll switch the content with it
        //     if($activeTitle || $activeContent || $activeCTA){             
        //         // Change dat content
        //         $slideContent.find('.slide-inner').html('<h2>' + $activeTitle + '</h2>' + $activeContent + $activeCTA);

        //         $slideContent.fadeIn();
        //     }
        //     // ELSE, we'll remove the box
        //     else {
        //         $slideContent.fadeOut();
        //     }
        // }
    });
})(jQuery);