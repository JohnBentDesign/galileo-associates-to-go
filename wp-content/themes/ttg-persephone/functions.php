<?php
	/*===========================================================================
	INCLUDE THE INCLUDES
	===========================================================================*/
	// Advanced Custom Fields
	include('includes/advanced-custom-fields/theme-styling.php');
	include('includes/advanced-custom-fields/page-home.php');
	include('includes/advanced-custom-fields/page-details.php');
	include('includes/advanced-custom-fields/page-backdrop.php');
	include('includes/advanced-custom-fields/shop-category.php');
	include('includes/advanced-custom-fields/shop-category-backdrop.php');
	include('includes/advanced-custom-fields/tax-image.php');

	

	/*===========================================================================
	ADD THEME SUPPORTS
	===========================================================================*/
	add_theme_support('post-thumbnails');
	add_theme_support('menus');



	/*===========================================================================
	CUSTOM THUMBNAIL SIZES
	===========================================================================*/
	add_image_size('TTG Featured Image', 1024, 500, true);
	add_image_size('TTG Gallery Normal Display', 637, 9999, false);
	add_image_size('TTG Homepage Thumbnail', 400, 300, true);
	add_image_size('TTG Post Featured Image', 770, 350, true);
	add_image_size('TTG Medium Thumbnail', 350, 350, true);



	/*===========================================================================
	REGISTER MENUS
	===========================================================================*/
	if(!function_exists('ttg_menus')){
		function ttg_menus() {
			register_nav_menus(array(
                'menu-main'         => 'Main Menu',
                'menu-footer' 		=> 'Footer Menu'
                ));
		}
		add_action('init', 'ttg_menus');
	}



	/*===========================================================================
	REGISTER SIDEBARS
	===========================================================================*/
	$sidebarBlog = array(
		'name'          => 'Blog Sidebar',
		'id'            => 'sidebar-blog',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>'
       );
	register_sidebar($sidebarBlog);

	$sidebarGeneral = array(
		'name'          => 'General Sidebar',
		'id'            => 'sidebar-general',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>'
       );
	register_sidebar($sidebarGeneral);

	$sidebarShop = array(
		'name'          => 'Shop Sidebar',
		'id'            => 'sidebar-shop',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>'
       );
	register_sidebar($sidebarShop);



	/*===========================================================================
	ENQUEUE SCRIPTS AND STYLES
	===========================================================================*/
	// JAVASCRIPTS
	//---------------------------------------------------------------------------
	function ttg_scripts(){
		if(!is_admin()){
			// Plugins
			wp_enqueue_script('tinynav', get_template_directory_uri() . '/javascripts/plugins/tinynav.min.js', array('jquery'), '', true);
			wp_enqueue_script('customselect', get_template_directory_uri() . '/javascripts/plugins/customSelect.min.js', array('jquery'), '', true);
			wp_enqueue_script('slick', get_template_directory_uri() . '/javascripts/plugins/slick.min.js', array('jquery'), '', true);
            wp_enqueue_script('hoverintent', get_template_directory_uri() . '/javascripts/plugins/hoverintent.min.js', array('jquery'), '', true);

			// Persephone Scripts
            wp_enqueue_script('persephone', get_template_directory_uri() . '/javascripts/persephone.js', array('jquery', 'foundation', 'tinynav', 'customselect', 'slick', 'hoverintent'), '', true);
        }
    }
    add_action('wp_enqueue_scripts', 'ttg_scripts');

	// STYLESHEETS
	//---------------------------------------------------------------------------
    function ttg_styles(){
      if(!is_admin()){
			// Web Fonts
         wp_enqueue_style('google-fonts', 'http://fonts.googleapis.com/css?family=Arvo:400,400italic|Alegreya+Sans:300,400,700,400italic,700italic', '', '', true);

			// Persephone Styles
         wp_enqueue_style('persephone', get_template_directory_uri() . '/stylesheets/css/style.css');
     }
 }
 add_action('wp_enqueue_scripts', 'ttg_styles');

	// RECOMPILES THE CSS ONLY WHEN THE OPTIONS PAGE IS SAVED
	//---------------------------------------------------------------------------
	if(get_field('dev_compile', 'option')){
		include(get_template_directory() . '/stylesheets/php-sass-watcher.php');
	}
	else {
		function ttg_compile_css( $post_id ) {
			$page = htmlspecialchars($_GET["page"]);
			if($page == 'acf-options-theme-styling'){
				// Compile our CSS
				include(get_template_directory() . '/stylesheets/php-sass-watcher.php');
			}
		}
		// run after ACF saves the $_POST['fields'] data
		add_action('acf/save_post', 'ttg_compile_css', 20);
	}



	/*===========================================================================
	FIX SEARCH WIDGET TO USE SEARCHFORM.PHP
	===========================================================================*/
	function ttg_filter_searchform($args) {
		// Let us know if it's a search form in a widget area
		global $blogSearch;
		$postType 	= get_post_type();
		$blogSearch = ($postType == 'post') ? true : false;

		extract($args);
		echo $before_widget;
		include (TEMPLATEPATH . '/parts/shared/searchform.php');
		echo $after_widget;
	}
	$widgetOptions = array('classname' => 'widget_search', 'description' => __( "A search form for your blog"));
	wp_register_sidebar_widget('search',__('Search'),'ttg_filter_searchform',$widgetOptions);



	/*===========================================================================
	SEARCH FORM VARIATIONS
	===========================================================================*/
	if(isset($_GET['search-type'])) {
		$type = $_GET['search-type'];
		global $searchType;

		if($type == 'blog') {
			$searchType = 'blog';
		} elseif($type == 'no-blog') {
			$searchType = 'no-blog';
		}
	}

    /*===========================================================================
    Custom Menu
    ===========================================================================*/

    //Add Main Menu Options Page if Dropline Menu is selected in theme options
    if(get_field('enable_dropline', 'options')) {
        acf_add_options_sub_page('Main Menu');            
    }