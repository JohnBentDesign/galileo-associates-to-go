<?php
	/*===========================================================================
	PERSEPHONE: TAX FEATURED IMAGE
	===========================================================================*/
	
	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_persephone-tax-featured-images',
			'title' => '[Persephone] Tax: Featured Images',
			'fields' => array (
				array (
					'key' => 'field_53629fdea3869',
					'label' => 'Featured Image',
					'name' => 'featured_image',
					'type' => 'image',
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'ef_taxonomy',
						'operator' => '==',
						'value' => 'category',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
				array (
					array (
						'param' => 'ef_taxonomy',
						'operator' => '==',
						'value' => 'gallery-categories',
						'order_no' => 0,
						'group_no' => 1,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}

