<?php
	/*===========================================================================
	PERSEPHONE: SHOP CATEGORY
	===========================================================================*/
		
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_acf_persephone-shop-categories',
	'title' => 'Shop Call Out Band',
	'fields' => array (
		array (
			'key' => 'field_535974c1412eb',
			'label' => 'Call to Action Band to Display',
			'name' => 'shop_cta_band',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'default' => 'Default Band (set in Options -> Theme Styling)',
				'custom' => 'Custom',
				'none' => 'None',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => 'default',
			'layout' => 'horizontal',
		),
		array (
			'key' => 'field_535974fb412ec',
			'label' => 'Title',
			'name' => 'shop_band_title',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_535974c1412eb',
						'operator' => '==',
						'value' => 'custom',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_53597522412ed',
			'label' => 'Content',
			'name' => 'shop_band_content',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_535974c1412eb',
						'operator' => '==',
						'value' => 'custom',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'toolbar' => 'full',
			'media_upload' => 1,
			'tabs' => 'all',
		),
		array (
			'key' => 'field_53597537412ee',
			'label' => 'Display call to action?',
			'name' => 'shop_band_cta_display',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_535974c1412eb',
						'operator' => '==',
						'value' => 'custom',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
		),
		array (
			'key' => 'field_5359754b412ef',
			'label' => 'Call to Action Text',
			'name' => 'shop_band_cta_text',
			'type' => 'text',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_535974c1412eb',
						'operator' => '==',
						'value' => 'custom',
					),
					array (
						'field' => 'field_53597537412ee',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_53597564412f1',
			'label' => 'Call to Action Link Type',
			'name' => 'shop_band_cta_type',
			'type' => 'radio',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_535974c1412eb',
						'operator' => '==',
						'value' => 'custom',
					),
					array (
						'field' => 'field_53597537412ee',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'internal' => 'Internal Page',
				'external' => 'External Page',
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'horizontal',
		),
		array (
			'key' => 'field_5359757a412f2',
			'label' => 'Internal Link',
			'name' => 'shop_band_cta_internal',
			'type' => 'relationship',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_535974c1412eb',
						'operator' => '==',
						'value' => 'custom',
					),
					array (
						'field' => 'field_53597564412f1',
						'operator' => '==',
						'value' => 'internal',
					),
					array (
						'field' => 'field_53597537412ee',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'id',
			'post_type' => array (
				0 => 'post',
				1 => 'page',
				2 => 'product',
				3 => 'gallery',
			),
			'taxonomy' => array (
			),
			'filters' => array (
				0 => 'search',
				1 => 'post_type',
			),
			'max' => 1,
			'min' => 0,
			'elements' => array (
				0 => 'featured_image',
				1 => 'post_type',
				2 => 'post_title',
			),
		),
		array (
			'key' => 'field_535975a5412f3',
			'label' => 'External Link',
			'name' => 'shop_band_cta_external',
			'type' => 'text',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_535974c1412eb',
						'operator' => '==',
						'value' => 'custom',
					),
					array (
						'field' => 'field_53597564412f1',
						'operator' => '==',
						'value' => 'external',
					),
					array (
						'field' => 'field_53597537412ee',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'taxonomy',
				'operator' => '==',
				'value' => 'product_cat',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;