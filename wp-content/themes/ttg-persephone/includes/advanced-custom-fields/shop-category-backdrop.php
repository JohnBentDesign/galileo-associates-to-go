<?php
	/*===========================================================================
	PERSEPHONE: SHOP CATEGORY BACKDROP
	===========================================================================*/
		
	if( function_exists('register_field_group') ):

	register_field_group(array (
		'key' => 'group_5450fdf310fc2',
		'title' => '[Persephone] Shop: Backdrop (Taxonomy)',
		'fields' => array (
			array (
				'key' => 'field_5450fdf31aa02',
				'label' => '',
				'name' => '',
				'prefix' => '',
				'type' => 'message',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'message' => '<h2 style="background-color: #0074a2; padding: 5px 10px; color: #ffffff">BACKDROP</h2>',
			),
			array (
				'key' => 'field_5450fdf31aa52',
				'label' => 'Overlay',
				'name' => '',
				'prefix' => '',
				'type' => 'tab',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
			),
			array (
				'key' => 'field_5450fdf31aa65',
				'label' => 'Overlay Display',
				'name' => 'backdrop_overlay',
				'prefix' => '',
				'type' => 'radio',
				'instructions' => 'Use the default overlay color set under Options -> Theme Styling -> Backdrop, set your own here, or choose to show no overlay',
				'required' => 0,
				'conditional_logic' => 0,
				'choices' => array (
					'default' => 'Default',
					'custom' => 'Custom',
					'none' => 'No Overlay',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => 'default',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_5450fdf31aa80',
				'label' => 'Overlay Color',
				'name' => 'backdrop_overlay_color',
				'prefix' => '',
				'type' => 'select',
				'instructions' => 'Uses the colors set under Options -> Theme Styling -> Colors',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_5450fdf31aa65',
							'operator' => '==',
							'value' => 'custom',
						),
					),
				),
				'choices' => array (
					'overlay-primary' => 'Primary',
					'overlay-secondary' => 'Secondary',
					'overlay-tertiary' => 'Tertiary',
					'overlay-quaternary' => 'Quaternary',
				),
				'default_value' => array (
					'overlay-tertiary' => 'overlay-tertiary',
				),
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'ajax' => 0,
				'placeholder' => '',
				'disabled' => 0,
				'readonly' => 0,
			),
			array (
				'key' => 'field_5450fdf31aa89',
				'label' => 'Content',
				'name' => '',
				'prefix' => '',
				'type' => 'tab',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
			),
			array (
				'key' => 'field_5450fdf31aa93',
				'label' => 'Hide Page Title?',
				'name' => 'backdrop_title',
				'prefix' => '',
				'type' => 'true_false',
				'instructions' => 'If title is hidden, you will need to manually add an H1 title to the content body of the page.',
				'required' => 0,
				'conditional_logic' => 0,
				'message' => '',
				'default_value' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'taxonomy',
					'operator' => '==',
					'value' => 'product_cat',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'acf_after_title',
		'style' => 'seamless',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));

	endif;

