<?php
	/*===========================================================================
	PERSEPHONE: PAGE DETAILS
	===========================================================================*/
		
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_acf_persephone-page-details',
	'title' => 'Page Gallery',
	'fields' => array (
		array (
			'key' => 'field_535011b2f0ccb',
			'label' => 'Attach Gallery',
			'name' => 'page_gallery',
			'type' => 'relationship',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'id',
			'post_type' => array (
				0 => 'gallery',
			),
			'taxonomy' => array (
			),
			'filters' => array (
				0 => 'search',
				1 => 'post_type',
			),
			'max' => 1,
			'min' => 0,
			'elements' => array (
				0 => 'featured_image',
				1 => 'post_type',
				2 => 'post_title',
			),
		),
		array (
			'key' => 'field_535039ed6d6f3',
			'label' => 'Gallery Title',
			'name' => 'page_gallery_title',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
			array (
				'param' => 'page_template',
				'operator' => '!=',
				'value' => 'page-home.php',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;