jQuery(document).foundation();

jQuery(document).ready(function($) {

    // Fade In/Out Main Menu
    //---------------------------
    function aphroditeMenuEnter() {
        if(!jQuery(this).is(':animated')){
            jQuery(this).find('.sub-menu').slideDown();
        }
    }
    function aphroditeMenuExit() {
        jQuery(this).find('.sub-menu:visible').stop().slideUp();
    }
    jQuery('.dropdown').hoverIntent(aphroditeMenuEnter, aphroditeMenuExit);

	//============================
	// Homepage Slider
	//============================
	jQuery('.home-slider').slick({
		autoplay: true,
		autoplaySpeed: 7000,
		arrows: false,
		dots: true,
		fade: true,
		infinite: true,
		speed: 1000,
		onInit: function(){
			jQuery('.home-slider .slide:first-child').addClass('fade-in');
		},
		onAfterChange: function(){
			jQuery('.home-slider .slide').removeClass('fade-in').removeClass('.fade-out');
			jQuery('.home-slider .slick-active').addClass('fade-in');
		}
	});

	//============================
	// Reveal Product Details
	//============================
	var hash = window.location.hash;
	jQuery('.product .reveal-modal[data-slug="' + hash + '"]').foundation('reveal', 'open');
        
	//============================
	// TinyNav + CustomSelect
	//============================
	jQuery('nav.main ul.menu').tinyNav({
		active: 'selected',
		header: 'Navigation',
		indent: '- ',
	});
	jQuery('.tinynav').customSelect();
	jQuery('.customSelectInner').html('<span class="title">Navigation</span> <span class="ion-navicon-round icon"></span>');
});
