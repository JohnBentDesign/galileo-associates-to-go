<?php
	/*===========================================================================
	GALLERY LOOP
	=============================================================================
	Gallery post loop for index.php
	*/
	
	// Figure out where we are
	$listing = (is_home() || is_archive() || is_search()) ? true : false;
	if($listing){ $postOutput = ' excerpt'; }
	else if($listing == false){ $postOutput = ' full'; }

	// Check if our galleries need to be full width on non-listing pages
	$galleryFullWidth = get_field('gallery_width');
	$galleryContainerClass = ($galleryFullWidth) ? '' : ' large-12 large-centered';
	echo '<div class="main columns' . $galleryContainerClass . '">';

	// THE LOOP //
	if(have_posts()){

		// If it's the blog listing page, wrap our articles in a div
		if($listing){ echo '<div class="gallery post-list">'; }

		// Start the loop
		while(have_posts()): the_post();
			if($listing){
				$title 		= '<h2><a href="' . get_permalink() . '">' . get_the_title() . '</a></h2>';
				$content 	= wpautop(get_the_excerpt());
				$imageSize 	= 'TTG Featured Image';
			}
			elseif($listing == false){
				$title 		= '<h1>' . get_the_title() . '</h1>';
				$content 	= wpautop(get_the_content());
				$imageSize 	= 'TTG Featured Image';
			}
?>
			<article class="post<?= $postOutput; ?> gallery">

				<?php // POST HEADER // ?>
				<header>
					<?php if($listing): ?>
						<div class="divider">
							<span class="diamond"></span><span class="diamond"></span><span class="diamond"></span>
						</div>
					<?php endif; ?>
					
					<?= $title; ?>

					<div class="categorizing">
						<?php
							// CATEGORIES
							//===================================
							pantheon_display_post_categories($post->ID);

							// SOCIAL- SHARE
							//===================================
							pantheon_display_social_share('gallery', 'post', $post);
						?>
					</div>
					
				</header>

				<?php // POST CONTENT // ?>
				<div class="the-post clearfix">
					
					<?php
						// Display featured image next to the excerpt if blog listing page 
						if($listing){ pantheon_display_post_featured_image($post->ID, $imageSize, get_permalink()); }
					?>
					
					<div class="content">
						<?= $content; ?>
					</div>

					<?php 
						// GALLERY //
						if(is_single()){
							pantheon_display_gallery();
						}
					?>

				</div>

			</article><?php // /.post ?>	

<?php 
		endwhile;

		// Close the blog listing div
		if(!is_single()){ 
			echo '</div>';
			get_template_part('parts/shared/pagination');
		}
		echo '</div>';
	}
	else{
		echo '<p>Sorry, but nothing matched your search criteria. Please try again with different keywords.</p>';
		get_template_part('searchform');
	}
?>