<!doctype html>
<html>
	<head>
		<?php // META TAGS // ?>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<?php // BLOG TITLE // ?>
		<title><?php wp_title(''); ?></title>
	
		<?php 
			// WORDPRESS HEADER //
			wp_head();
		?>
	</head>

	<body <?php body_class(); ?>>
		<?php
			// HEADER // 
			get_template_part('parts/shared/header');
		?>
