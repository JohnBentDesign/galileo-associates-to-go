<?php
	/*===========================================================================
	FOOTER- MAIN
	===========================================================================*/
	// Contains the copyright and footer navigation

	// Figure out what needs to be shown
	$column1 			= get_field('footer_column_1', 'options');
	$column2 			= get_field('footer_column_2', 'options');
	$column3 			= get_field('footer_column_3', 'options');
	$column4 			= get_field('footer_column_4', 'options');

	$columnWidth1		= ($column1 != 'none') ? 2 : 0;
	$columnWidth2		= ($column2 != 'none') ? 5 : 0;
	$columnWidth3		= ($column3 != 'none') ? 3 : 0;
	$columnWidth4		= ($column4 != 'none') ? 2 : 0;

	$columnTotal 		= $columnWidth1 + $columnWidth2 + $columnWidth3 + $columnWidth4;
	if($columnTotal != 12) {
		$columnDiff = 12 - $columnTotal;
		$columnWidth2 += $columnDiff;
	}

	// Figure out how many columns we have
	$columnArray 		= array($column1, $column2, $column3, $column4);
	$columnCount		= 0;
	foreach($columnArray as $column){
		if($column != 'none'){
			$columnCount++;
		}
	}

	// Figure out social button/shop status
	$checkConnectLoc 	= get_field('social_button_location', 'options');
	$displayConnect		= ( !empty($checkConnectLoc) && in_array('footer', $checkConnectLoc) ) ? true : false;
	$displayShop 		= (get_field('shop_enable', 'options')) ? true : false;
?>	
	<footer class="main">

		<?php
			//===================================
			// UPPER FOOTER
			//===================================
			// Only show it if we actually have any content to display
			if($columnCount > 0):
		?>
				<div class="upper bg-tertiary">
					<div class="row" data-equalizer>

						<?php 
							// COLUMN 1 (contact)
							//===================================				
							if($column1 != 'none'):
						?>
								<div class="contact large-<?= $columnWidth1; ?> columns" data-equalizer-watch>
									<?php 
										echo '<h3>' . get_field('footer_column_1_title', 'options') . '</h3>';
										
										// Contact Details
										if($column1 == 'contact'){
											get_template_part('parts/shared/contact');
										}
										// Custom
										elseif($column1 == 'custom'){
											the_field('footer_column_1_custom', 'options');
										}
									?>
								</div>
						<?php endif; ?>

						<?php
							// COLUMN 2 (navigation)
							//===================================
							if($column2 != 'none'):
						?>
								<div class="page-nav large-<?= $columnWidth2; ?> columns" data-equalizer-watch>
									<?php
										echo '<h3>' . get_field('footer_column_2_title', 'options') . '</h3>';
										
										// Navigation
										if($column2 == 'nav'){
											$menuFooter = array(
												'theme_location'  => 'menu-footer',
												'menu'            => 'Footer Menu',
												'container'       => 'nav',
												'container_class' => 'footer',
												'menu_class'      => 'menu',
												'depth'           => 1,
											);
											wp_nav_menu($menuFooter);
										}
										// Custom
										elseif($column2 == 'custom'){
											the_field('footer_column_2_custom', 'options');
										}
									?>
								</div>
						<?php endif; ?>

						<?php
							// COLUMN 3 (form)
							//===================================
							if($column3 != 'none'):
						?>
								<div class="form large-<?= $columnWidth3; ?> columns" data-equalizer-watch> 
									<?php										
										// Form
										if($column3 == 'form'){
											// Display Form from Shortcode
											echo do_shortcode( get_field('footer_form_shortcode', 'options') );
										}
										// Custom
										elseif($column3 == 'custom'){
											echo '<h3>' . get_field('footer_column_3_title', 'options') . '</h3>';
											the_field('footer_column_3_custom', 'options');
										}
									?>
								</div>
						<?php endif; ?>

						<?php
							// COLUMN 4 (connect)
							//===================================
							if($column4 == 'social' && $displayConnect):
						?>
								<div class="connect large-<?= $columnWidth4; ?> columns" data-equalizer-watch>
									<?php 
										echo '<h3>' . get_field('footer_column_4_title', 'options') . '</h3>';
										pantheon_display_social_connect('footer');
									?>
								</div>
						<?php 
							// COLUMN 4 (custom)
							//===================================
							elseif($column4 == 'custom'):
						?>
								<div class="connect large-<?= $columnWidth4; ?> columns" data-equalizer-watch>
									<?php 
										echo '<h3>' . get_field('footer_column_4_title', 'options') . '</h3>';
										the_field('footer_column_4_custom', 'options');
									?>
								</div>
						<?php endif; ?>

					</div>
				</div>
		<?php endif; ?>

		<?php
			//===================================
			// LOWER FOOTER
			//===================================
			// Check if Jigoshop is active
			$columnSizeCopy = ($displayShop) ? ' large-6' : '';
		?>
		<div class="lower bg-quaternary">
			<div class="row">

				<?php
					$payments = get_field('shop_gateway', 'options');

					// ACCEPTED CREDIT CARDS // 
					if($displayShop && $payments):
				?>
						<div class="payments large-6 columns">
							<span class="icon ion-card"></span>
							<span><?= $payments; ?></span>
						</div>
				<?php endif; ?>

				<div class="copyright columns<?= $columnSizeCopy; ?>">
					<p>&copy; <?php echo date('Y') . ' '; bloginfo('name'); ?>. Site powered by <a href="http://techtherapytogo.com/" target="_blank">Technology Therapy To Go</a>.</p>
				</div>

			</div>
		</div>

	</footer>