<?php
/*===========================================================================
HEADER- MAIN
===========================================================================*/
// Contains the logo and main navigation

// Figure out what needs to be shown
$upperLeft 			= get_field('header_upper_left', 'options');
$upperRight 		= get_field('header_upper_right', 'options');
$lowerLeft 			= get_field('header_lower_left', 'options');
$lowerRight 		= get_field('header_lower_right', 'options');

// Figure out what needs to be shown
$checkConnectLoc 	= get_field('social_button_location', 'options');
$displayConnect		= ( !empty($checkConnectLoc) && in_array('header', $checkConnectLoc) ) ? true : false;
$displayShop 		= (get_field('shop_enable', 'options')) ? true : false;
?>
<header class="main">

    <?php 
//===================================
// UPPER HEADER
//===================================
// Only show it if we actually have any content to display
    if(($upperLeft != 'none') || ($upperRight != 'none') || ($displayConnect && $displayShop)):
        $sizeUpperLeft 	= (($upperLeft != 'none') && ($upperRight != 'none')) ? 'large-7' : 'large-12';
        $sizeUpperRight = (($upperLeft != 'none') && ($upperRight != 'none')) ? 'large-5' : 'large-12';
    ?>
    <div class="upper bg-tertiary-light">
        <div class="row">

            <?php
// UPPER LEFT (address)
//===================================
            if($upperLeft != 'none'):
                ?>
            <div class="<?= $sizeUpperLeft; ?> columns">
                <?php 								
// Contact Details
                if($upperLeft == 'address'){
                    get_template_part('parts/shared/contact');
                }
// Custom
                elseif($upperLeft == 'custom'){
                    echo get_field('header_upper_left_custom', 'options');
                }
                ?>
            </div>
        <?php endif; ?>

        <?php
// UPPER RIGHT (hours & connect)
//===================================
        if(($upperRight != 'none') || ($displayConnect && $displayShop)):
            ?>			
        <div class="connect-hours <?= $sizeUpperRight; ?> columns">
            <?php 		
// Hours
            if($upperRight == 'hours'){
                $hours = get_field('hours', 'options');
                if($hours):
                    while(have_rows('hours', 'options')): the_row();
                echo '<p><span class="day">' . get_sub_field('day') . '</span> ' . get_sub_field('time') . '</p>';
                endwhile;
                endif;
            }
// Custom
            elseif($upperRight == 'custom'){
                echo '<p><span>' . get_field('header_upper_right_custom', 'options') . '</span></p>';
            }
// Social Connect (only if shop is enabled)
            if($displayConnect && $displayShop){
                pantheon_display_social_connect('header');
            }
            ?>
        </div>
    <?php endif; ?>

</div>
</div>
<?php endif; ?>

<?php 
//===================================
// LOWER HEADER
//===================================
?>
<div class="lower bg-primary">
    <div class="row">

        <?php 
// LOGO //
        $columnSizeLogo = ($lowerLeft == 'none') ? ' large-offset-4' : ' large-push-4';
        $columnEndLogo	= (!$displayShop && ($lowerRight == 'none')) ? ' end' : '';
        ?>
        <div class="logo large-4 columns<?= $columnSizeLogo . $columnEndLogo; ?>">
            <a href="<?php echo get_bloginfo ( 'url' ); ?>" class="logo">
                <?php 
                $logoObj = get_field('logo_image', 'options');

// Output a Logo Image (if we have one)
                if($logoObj){
                    pantheon_display_post_field_image($logoObj, 'full');
                }
// Else, we'll just put out the blog title
                else {
                    bloginfo('title');
                }
                ?>
            </a>
        </div>

        <?php 
// LOWER LEFT (search) //
        if($lowerLeft != 'none'):
            ?>
        <div class="large-4 large-pull-4 columns">
            <?php 
            if($lowerLeft == 'search'){
                get_template_part('parts/shared/searchform');
            }
            elseif($lowerLeft == 'custom'){
                the_field('header_lower_left_custom', 'options');
            }
            ?>
        </div>
    <?php endif; ?>

    <?php 
// LOWER RIGHT (social/shop) //
    if($lowerRight != 'none'):
        ?>
    <?php 
// Custom Content
    if($lowerRight == 'custom'):
        ?>
    <div class="custom large-4 columns">
        <?php the_field('header_lower_right_custom', 'options'); ?>
    </div>

    <?php
// Cart Stats
    elseif($displayShop):
        ?>
    <div class="cart-stats large-4 columns">
        <span class="icon ion-ios7-cart"></span>
        <?php 
// Cart Contents
        $cart_contents = jigoshop_cart::$cart_contents;
        if(!empty($cart_contents)){        
            echo '&nbsp;', '<b>'.jigoshop_cart::$cart_contents_count.'</b>', '&nbsp;', 'item(s)';
        }
        else {
            echo '&nbsp;', '<b>', '0', '</b>', '&nbsp;', 'item(s)';
        }
// Cart Total & Link
        $cart_contents = jigoshop_cart::$cart_contents;
        if(!empty($cart_contents)){
            echo '&nbsp;|&nbsp;' . jigoshop_cart::get_cart_total(). '&nbsp;', '&nbsp;', '|', '&nbsp;', '&nbsp;', '<a href="' . get_permalink(jigoshop_get_page_id('cart')) . '">View Cart</a>';
        }
        else {
            echo '&nbsp;', '$0.00', '&nbsp;', '&nbsp;', '|', '&nbsp;', '&nbsp;', '<a href="' . get_permalink(jigoshop_get_page_id('cart')) . '">View Cart</a>';
        }
        ?>
    </div>
    <?php 
// Connect
    elseif($displayConnect):
        ?>
    <div class="connect large-4 columns">
        <div>
            <span>Connect:</span> 
            <?= pantheon_display_social_connect('header'); ?>                               
        </div>
    </div>
<?php endif; ?>
<?php endif; ?>

</div>
</div>

<?php 
//===================================
// MAIN NAVIGATION
//===================================
?>
<div class="navigation bg-quaternary">
    <div class="row">


        <?php if(get_field('enable_dropline', 'options')) {
            // WORDPRESS MENU //
            get_template_part('parts/shared/header-dropline');
        } else { ?>
        <div class="columns">
        <?php  
            $menuMain = array(
            'theme_location'    => 'menu-main',
            'menu'              => 'Main Menu',
            'container'         => 'nav',
            'container_class'   => 'main',
            'menu_class'        => 'menu',
            'depth'             => 3,
            'fallback_cb'       => 0
            );
            wp_nav_menu($menuMain);
        } ?>
    </div>


</div>
</div>

<?php 
//===================================
// BACKDROP
//===================================
if(!is_front_page()){
    get_template_part('parts/shared/backdrop');
}

//===================================
// BREADCRUMBS
//===================================
if(function_exists('yoast_breadcrumb') && !is_front_page()){
    yoast_breadcrumb('<div class="row"><div class="column"><nav class="breadcrumbs">','</nav></div></div>');
}
?>

</header>

