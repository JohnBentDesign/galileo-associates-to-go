<?php
	/*===========================================================================
	SEARCH FORM
	=============================================================================
	Search form to be used across the site
	*/

	// Figure out what our search will output
	global $blogSearch;
	$postType = get_post_type();
	$searchType = ( ($postType == 'post') && $blogSearch ) ? 'blog' : 'no-blog';
?>
	<form method="get" class="searchform clearfix" action="<?php bloginfo('home'); ?>/">
		<input type="text" name="s" id="s" value="Search" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;"/>
		<input type="hidden" name="search-type" value="<?= $searchType; ?>" />
		<input type="submit" id="searchsubmit" value="GO" class="btn icon ion-ios-search-strong" />
	</form>