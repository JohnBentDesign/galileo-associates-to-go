<?php
	/*===========================================================================
	SIDEBAR: MAIN
	=============================================================================
	Right hand sidebar that appears on most pages.
	*/
	if( (get_post_type() != 'gallery') && !is_post_type_archive('gallery') ):
?>
		<div class="sidebar medium-4 columns">

			<?php 
				if(is_page()){
					if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('General Sidebar')){}
				}
				else if(get_post_type() == 'product'){
					if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Shop Sidebar')){}	
				}
				else {
					if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar')){}
				}
			?>

		</div><?php // /.sidebar ?>
<?php endif; ?>