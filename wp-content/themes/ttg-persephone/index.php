<?php
	/*===========================================================================
	INDEX
	=============================================================================
	Default fallback style. Default page for blog viewing page
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>
	<div class="container">
		<div class="row">

			<?php 
				// What did the user search for (if search page)?
				if(is_search()){
					echo '<h1 class="free">Search Results for "' . get_search_query() . '"</h1>';
				}
				// Or what category are we currently viewing?
				else if(is_category()){
					echo '<h1 class="free">Category for "' . single_cat_title('', false) . '"</h1>';	
				}
				else if(is_tag()){
					echo '<h1 class="free">Term for "' . single_cat_title('', false) . '"</h1>';	
				}
				else if(is_day()){
					echo '<h1 class="free">Daily archives for ' . get_the_date() . '</h1>';
				}
				else if(is_month()){ 
					echo '<h1 class="free">Monthly archives for ' . get_the_date('F, Y') . '</h1>';
				}
				else if(is_year()){
					echo '<h1 class="free">Yearly archives for ' . get_the_date('Y') . '</h1>';
				}

				// POST LOOP //
				if($post->post_type == 'gallery'){
					get_template_part('parts/posts/loop', 'gallery');	
					get_template_part('parts/shared/pagination');
				}
				else {
					echo '<div class="main medium-8 columns">';
					get_template_part('parts/posts/loop');
					get_template_part('parts/shared/pagination');
					echo '</div>';
				}

				// SIDEBAR //
				get_template_part('parts/shared/sidebar');
			?>

		</div>
	</div>

<?php
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>