<?php
/*===========================================================================
Template Name: Homepage
=============================================================================
Page template for Homepage. Same as page.php, just makes it easier to determine
when to show slide custom fields on editor page.
*/

// VARIABLES //
$sections 	= get_field('home_display');
$varArray 	= (gettype($sections) == 'array') ? true : false;
$hasCTA 	= ( ($varArray && in_array('cta', $sections)) || ($sections == 'cta') ) ? true : false;
$hasSlider 	= ( ($varArray && in_array('slider', $sections)) || ($sections == 'slider') ) ? true : false;
$hasSmall 	= ( ($varArray && in_array('featured-small', $sections)) || ($sections == 'featured-small') ) ? true : false;
$hasLarge 	= ( ($varArray && in_array('featured-large', $sections)) || ($sections == 'featured-large') ) ? true : false;


// HEADER //
get_template_part('parts/shared/header', 'html');
?>
<div class="container">

    <?php 
//---------------------------------
// SLIDESHOW
//---------------------------------
    if($hasSlider && have_rows('home_slider') ):
        echo '<div class="home-slider">';

    while ( have_rows('home_slider') ) : the_row();
    $slideImage 	= pantheon_display_post_field_image(get_sub_field('image'), 'large', 'style', false);
    $slideTitle 	= (get_sub_field('title')) ? '<h2>' . get_sub_field('title') . '</h2>' : '';
    $slideContent 	= get_sub_field('content');
    $slideCTA 		= (get_sub_field('cta_display')) ? pantheon_display_post_cta(array('echo' => false, 'repeater' => 'sub-field', 'class' => 'button')) : '';
    $slideAlign     = 'block-' . get_sub_field('alignment');
    ?>	
    <div class="slide" <?= $slideImage; ?>>
        <div class="table">
            <div class="table-row">
                <div class="table-cell">
                    <div class="slide-details text-center columns <?= $slideAlign; ?>">
                        <?= $slideTitle . $slideContent . $slideCTA; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    endwhile;

    echo '</div>';
    endif;

//---------------------------------
// CALL OUT
//---------------------------------
    $ctaBand = get_field('home_cta_band');

    if($hasCTA && $ctaBand):
        ?>
    <div class="cta-band bg-tertiary">
        <div class="row">
            <div class="columns">
                <?= $ctaBand; ?>
            </div>
        </div>
    </div>
    <?php 
    endif; 

//---------------------------------
// FEATURED POSTS: SMALL
//---------------------------------
    if($hasSmall && have_rows('home_featured_small')):
        ?>
    <div class="home-featured row" data-equalizer>
        <?php
        while(have_rows('home_featured_small')): the_row();
// GET VARIABLES //
        $hasSmallLinkType 	= get_sub_field('featured_type');
        $hasSmallBtnText 	= get_sub_field('featured_btn');
        $hasSmallOvertitle	= get_sub_field('featured_title');
        $hasSmallExplain 	= (get_sub_field('featured_explain')) ? '<p>' . get_sub_field('featured_explain') . '</p>' : '';
        $hasSmallOverimage	= pantheon_display_post_field_image(get_sub_field('featured_image'), 'TTG Homepage Thumbnail', 'image', false);

// Our variables depending on what type of info we're displaying
        if($hasSmallLinkType == 'page'){
            $hasSmallField 		= get_sub_field('featured_post');
            $hasSmallLink 		= get_permalink($hasSmallField[0]);
            $hasSmallTitle 		= (!$hasSmallOvertitle) ? get_the_title($hasSmallField[0]) : $hasSmallOvertitle;
            $hasSmallImage 		= (has_post_thumbnail($hasSmallField[0])) ? pantheon_display_post_featured_image($hasSmallField[0], 'TTG Homepage Thumbnail', false, 'image', false) : '';
            $hasSmallImage 		= ($hasSmallOverimage) ? $hasSmallOverimage : $hasSmallImage;
        }
        else if( ($hasSmallLinkType == 'category') || ($hasSmallLinkType == 'product') || ($hasSmallLinkType == 'gallery') ){
            $hasSmallFieldName 	= 'featured_' . $hasSmallLinkType;
            $hasSmallField 		= get_sub_field($hasSmallFieldName);
            $hasSmallLink 		= get_term_link($hasSmallField);
            $hasSmallTitle 		= (!$hasSmallOvertitle) ? $hasSmallField->name : $hasSmallOvertitle;
            if($hasSmallLinkType == 'product'){
                $hasSmallImageRawURL	= jigoshop_product_cat_image($hasSmallField->term_id);
                global $wpdb;
                $hasSmallImageRawObj	= $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $hasSmallImageRawURL)); 
                $hasSmallImageID 		= $hasSmallImageRawObj[0];
                $hasSmallImageObj 		= wp_get_attachment_image_src($hasSmallImageID, 'TTG Homepage Thumbnail');
                $hasSmallImage 			= '<img src="' . $hasSmallImageObj[0] . '" width="' . $hasSmallImageObj[1] . '" height="' . $hasSmallImageObj[2] . '" alt="' . get_post_meta($hasSmallImageID, '_wp_attachment_image_alt', true) . '" />';
                $hasSmallImage 			= ($hasSmallOverimage) ? $hasSmallOverimage : $hasSmallImage;
            }
            else {
                $hasSmallImageObj 		= get_field('featured_image', $hasSmallField);
                $hasSmallImage 			= pantheon_display_post_field_image($hasSmallImageObj, 'TTG Homepage Thumbnail', 'image', false);
                $hasSmallImage 			= ($hasSmallOverimage) ? $hasSmallOverimage : $hasSmallImage;
            }
        }
        else if($hasSmallLinkType == 'external'){
            $hasSmallTitle 			= get_sub_field('featured_title');
            $hasSmallLink 			= get_sub_field('featured_external');
            $hasSmallImage 			= pantheon_display_post_field_image(get_sub_field('featured_image'), 'TTG Homepage Thumbnail', 'image', false);
        }
        else {
            $hasSmallField = $hasSmallLink = $hasSmallTitle = $hasSmallOvertitle = $hasSmallImage = '';
        }
        ?>

        <div class="featured columns large-4" data-equalizer-watch>
            <a href="<?= $hasSmallLink; ?>" class="img-zoom"<?php if($hasSmallLinkType == 'external'){ echo ' target="_blank"'; } ?>>
                <div class="image">
                    <?= $hasSmallImage; ?>
                </div>
                <div class="details">
                    <?php
                    echo '<h4>' . $hasSmallTitle . '</h4>';
                    echo $hasSmallExplain;
                    echo '<span class="button tiny">' . $hasSmallBtnText . '</span>';
                    ?>
                </div>
            </a>
        </div>

    <?php endwhile; ?>
</div>
<?php 
endif;

//---------------------------------
// FEATURED POSTS: LARGE
//---------------------------------
if($hasLarge && have_rows('home_featured_large')):
    ?>
<div class="home-featured featured-large row" data-equalizer>
    <?php
    while(have_rows('home_featured_large')): the_row();
// GET VARIABLES //
    $hasLargeLinkType 	= get_sub_field('featured_type');
    $hasLargeBtnText 	= get_sub_field('featured_btn');
    $hasLargeOvertitle	= get_sub_field('featured_title');
    $hasLargeExplain 	= (get_sub_field('featured_explain')) ? '<p>' . get_sub_field('featured_explain') . '</p>' : '';
    $hasLargeOverimage 	= pantheon_display_post_field_image(get_sub_field('featured_image'), 'TTG Homepage Thumbnail', 'image', false);

// Our variables depending on what type of info we're displaying
    if($hasLargeLinkType == 'page'){
        $hasLargeField 		= get_sub_field('featured_post');
        $hasLargeLink 		= get_permalink($hasLargeField[0]);
        $hasLargeTitle 		= (!$hasLargeOvertitle) ? get_the_title($hasLargeField[0]) : $hasLargeOvertitle;
        $hasLargeImage 		= (has_post_thumbnail($hasLargeField[0])) ? pantheon_display_post_featured_image($hasLargeField[0], 'TTG Homepage Thumbnail', false, 'image', false) : '';
        $hasLargeImage 		= ($hasLargeOverimage) ? $hasLargeOverimage : $hasLargeImage;
    }
    else if( ($hasLargeLinkType == 'category') || ($hasLargeLinkType == 'product') || ($hasLargeLinkType == 'gallery') ){
        $hasLargeFieldName 	= 'featured_' . $hasLargeLinkType;
        $hasLargeField 		= get_sub_field($hasLargeFieldName);
        $hasLargeLink 		= get_term_link($hasLargeField);
        $hasLargeTitle 		= (!$hasLargeOvertitle) ? $hasLargeField->name : $hasLargeOvertitle;
        if($hasLargeLinkType == 'product'){
            $hasLargeImageRawURL	= jigoshop_product_cat_image($hasLargeField->term_id);
            global $wpdb;
            $hasLargeImageRawObj	= $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $hasLargeImageRawURL)); 
            $hasLargeImageID 		= $hasLargeImageRawObj[0];
            $hasLargeImageObj 		= wp_get_attachment_image_src($hasLargeImageID, 'TTG Homepage Thumbnail');
            $hasLargeImage 			= '<img src="' . $hasLargeImageObj[0] . '" width="' . $hasLargeImageObj[1] . '" height="' . $hasLargeImageObj[2] . '" alt="' . get_post_meta($hasLargeImageID, '_wp_attachment_image_alt', true) . '" />';
            $hasLargeImage 			= ($hasLargeOverimage) ? $hasLargeOverimage : $hasLargeImage;
        }
        else {
            $hasLargeImageObj 		= get_field('featured_image', $hasLargeField);
            $hasLargeImage 			= pantheon_display_post_field_image($hasLargeImageObj, 'TTG Homepage Thumbnail', 'image', false);
            $hasLargeImage 			= ($hasLargeOverimage) ? $hasLargeOverimage : $hasLargeImage;
        }
    }
    else if($hasLargeLinkType == 'external'){
        $hasLargeTitle 			= get_sub_field('featured_title');
        $hasLargeLink 			= get_sub_field('featured_external');
        $hasLargeImage 			= pantheon_display_post_field_image(get_sub_field('featured_image'), 'TTG Homepage Thumbnail', 'image', false);
    }
    else {
        $hasLargeField = $hasLargeLink = $hasLargeTitle = $hasLargeOvertitle = $hasLargeImage = '';
    }
    ?>

    <div class="featured columns large-6" data-equalizer-watch>
        <a href="<?= $hasLargeLink; ?>" class="img-zoom"<?php if($hasLargeLinkType == 'external'){ echo ' target="_blank"'; } ?>>
            <div class="image">
                <?= $hasLargeImage; ?>
            </div>
            <div class="details">
                <?php
                echo '<h4>' . $hasLargeTitle . '</h4>';
                echo $hasLargeExplain;
                echo '<span class="button tiny">' . $hasLargeBtnText . '</span>';
                ?>
            </div>
        </a>
    </div>

<?php endwhile; ?>
</div>
<?php endif; ?>

</div>

<?php
// FOOTER //
get_template_part('parts/shared/footer', 'html');
?>