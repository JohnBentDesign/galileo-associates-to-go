<?php
	/*===========================================================================
	INDEX
	=============================================================================
	Default fallback style. Default page for blog viewing page
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>
	<div class="container">
		
		<div class="row">
			<?php 
				//---------------------------------
				// POST CONTENT
				//---------------------------------
			?>
			<div class="main medium-8 columns">
				<?php 
					if(have_posts()):
						while(have_posts()): the_post();
							
							the_content();

						endwhile;
					endif;
				?>			
			</div>

			<?php
				//---------------------------------
				// SIDEBAR
				//---------------------------------
				get_template_part('parts/shared/sidebar');
			?>
		</div>

		<?php 
			//---------------------------------
			// GALLERY
			//---------------------------------
			$gallery 		= get_field('page_gallery');
			$galleryID 		= $gallery[0];
			$galleryTitle 	= (get_field('page_gallery_title')) ? '<h2>' . get_field('page_gallery_title') . '</h2>' : '';

			if($galleryID):
		?>
				<div class="row">
					<div class="columns">
						<div class="page-gallery on-page">

								<?php
									// TITLE //
									echo $galleryTitle;

									// DISPLAY GALLERY //
									pantheon_display_gallery($galleryID, 'normal', 'thumbnail');
								?>

						</div>
					</div>
				</div>
		<?php
			endif;
		?>

	</div>

<?php
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>