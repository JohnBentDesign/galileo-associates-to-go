<?php
/**
 * Class SassWatcher
 *
 * This simple tool compiles all .scss files in folder A to .css files (with exactly the same name) into folder B.
 * To keep things as minimal as possible, this tool compiles every X seconds, regardless of changes within the files.
 * This seems weird, but makes sense as checking for changes in the files is more CPU-extensive than simply
 * re-compiling them. SassWatcher uses scssphp, the best SASS compiler in PHP available.
 *
 * SassWatcher is not a standalone compiler, it's just a little method that uses the excellent scssphp compiler written
 * by Leaf Corcoran (https://twitter.com/moonscript), which can be found here: http://leafo.net/scssphp/ and adds
 * automatic interval-compiling to it.
 *
 * The currently supported version of SCSS syntax is 3.2.12, which is the latest one.
 * To avoid confusion: SASS is the name of the language itself, and also the "name" of the "first" version of the
 * syntax (which was quite different than CSS). Then SASS's syntax was changed to "SCSS", which is more like CSS, but
 * with awesome additional possibilities and features.
 *
 * The compiler uses the SCSS syntax, which is recommended and mostly used. The old SASS syntax is not supported.
 *
 * @see SASS Wikipedia: http://en.wikipedia.org/wiki/Sass_%28stylesheet_language%29
 * @see SASS Homepage: http://sass-lang.com/
 * @see scssphp, the used compiler (in PHP): http://leafo.net/scssphp/
 *
 * How to use this tool:
 *
 * 1. Edit $sass_watcher->watch( ... ); in the last line of this file and put your stuff in here, see the parameter
 *    list below.
 * 2. Make sure PHP can write into your CSS folder.
 * 3. Run the script:
 *    a) simple way, from browser, just enter the URL to scss-compiler.php: http://127.0.0.1/folder/php-sass-watcher.php
 *       The script will run forever, even if you close the browser window.
 *    b) PHPStorm users can run the script by right-clicking the file and selecting "Run php-sass-watcher.php".
 * 4. To stop the script, stop/restart your Apache/Nginx/etc. or press the red "stop process button in PHPStorm.
 *
 * The parameters:
 *
 *  1. relative path to your SCSS folder
 *  2. relative path to your CSS folder (make sure PHP has write-rights here)
 *  3. the compiling interval (in seconds)
 *  4. relative path to the scss.inc.php file, which is the main file of the SASS compiler used
 *     here. Download the script manually from http://leafo.net/scssphp/ or "require" it via Composer:
 *     "leafo/scssphp": "0.0.9"
 *  5. optional: how the .css output should look like. See http://leafo.net/scssphp/docs/#output_formatting for more.
 *
 * How the tool works:
 *
 * Every X seconds ALL files in the scss folder will be compiled to same-name .css files in the css folder.
 * The tool does not stop when a .scss file is broken, has syntax error or similar.
 * The tool does not compile when .scss file is broken, has syntax error or similar. It will only compile next time
 * when there's a valid scss file.
 */

// We need to be able to read our database and functions in Wordpress, so we'll find where wp-load.php is and load it
function find_wp_config_path() {
	$dir = dirname(__FILE__);
	do {
		if( file_exists($dir."/wp-config.php") ) {
			return $dir;
		}
	} while( $dir = realpath("$dir/..") );
	return null;
}
require_once( find_wp_config_path()  . '/wp-load.php' );

// In order to make this truely incredible, we need to check our colors so the theme can smartly change the color values
function check_color($hex, $modifier, $parentSame = null){
	//break up the color in its RGB components
	$hex 	= str_replace('#', '', $hex);
	$r 		= hexdec(substr($hex,0,2));
	$g 		= hexdec(substr($hex,2,2));
	$b 		= hexdec(substr($hex,4,2));

	// We're going to ignore some color values if they are too bright. It's making it difficult for our grays
	$r 		= ($r > 130) ? $r : 0;
	$g 		= ($g > 130) ? $g : 0;
	$b 		= ($b > 130) ? $b : 0;

	// Add it all together, weighting green because it can be tricky, and now we have our RGB value
	$rgb 	= $r + ($g * 1.8) + $b;

	if($parentSame){
		// If we're altering the color of something, where its parent container is the same color,
		// we need to override our logic and just make it darker or lighter
		if(($rgb > 0) && ($rgb > 0)) {
			return 'darken';
		}
		else if(($rgb == 0)) {
			return 'lighten';
		}
	}
	else if( ($modifier == 'lighten') && ( ($rgb > 0) && ($rgb <= 550) ) ){
		// Light Color being told to get lighter; Stop it
		return 'darken';
	}
	else if( ($modifier == 'darken') && ( ($rgb == 0) || ($rgb > 550) ) ){
		// Dark color being told to get darker; Stop it
		return 'lighten';
	}
	else {
		// This porridge is just right
		return $modifier;
	}
}



/*==========================================================================
CUSTOM COLORS DEFINED BY USER
==========================================================================*/
$primaryColor       = (get_field('color_primary', 'options'))       ? get_field('color_primary', 'options')     : '#ffffff';
$secondaryColor     = (get_field('color_secondary', 'options'))     ? get_field('color_secondary', 'options')   : '#565656';
$tertiaryColor      = (get_field('color_tertiary', 'options'))      ? get_field('color_tertiary', 'options')    : '#00aeef';
$quaternaryColor    = (get_field('color_quaternary', 'options'))    ? get_field('color_quaternary', 'options')  : '#691c33';

$user_colors = '
	@import "includes";

	// FUNCTIONS
	//-------------------------------------------------------------
	// More Accurate Color Contrastor (http://codepen.io/bluesaunders/pen/FCLaz)

	// Calculate brightness of a given color.
	@function brightness($color) {
		@return ((red($color) * .299) + (green($color) * .587) + (blue($color) * .114)) / 255 * 100%;
	}

	// Compares contrast of a given color to the light/dark arguments and returns whichever is most "contrasty"
	@function color-contrast($color, $dark: $dark-text-default, $light: $light-text-default) {
		@if $color == null {
		    @return null;
		}
		@else {
			$color-brightness: brightness($color);  
			$light-text-brightness: brightness($light);
			$dark-text-brightness: brightness($dark);
			@return if(abs($color-brightness - $light-text-brightness) > abs($color-brightness - $dark-text-brightness), $light, $dark);  
		}
	}


	// VARIABLES
	//-------------------------------------------------------------
	// Raw
	$primary: ' . $primaryColor . ';
	$secondary: ' . $secondaryColor . ';
	$tertiary: ' . $tertiaryColor . ';
	$quaternary: ' . $quaternaryColor . ';
	

	// Primary //
	.bg-primary,
	.backdrop h1 + p:before,
	.reveal-modal .details h3:after,
	.cta-band h3:after, .cta-band .button,
	.slider h2 + p:before,
	.slick-dots li button {
		background-color: ' . $primaryColor . ';
	}
	.reveal-modal .form {
		background-color: rgba(' . $primaryColor . ', .75);
	}
	header.main {
		p, .adr, .numbers {
			border-color: ' . $primaryColor . ';
		}
	}
	footer.reveal {
		border-color: rgba(' . $primaryColor . ', .5);
	}
	.reveal-modal .details, .cta-band, .widget.has-bg {
		h1, h2, h3, h4, h5, h6, p, span, a, ul, ol, li {
			color: ' . $primaryColor . ';
		}
	}
	.backdrop h1, .backdrop p,
	.post time span, .continue, .continue:hover,
	.reveal-modal .form p.alt, .reveal-modal .social a.symbol, .reveal-modal .controls a,
	nav.main ul ul li:hover a,
	.pagination .current,
	#breadcrumb a, #breadcrumb, .navigation a, .navigation a span,
	.home-featured a:hover p, .home-featured a:hover h4,
	.home .slide-details h2, .home .slide-details p, .home .slide-details a {
		color: ' . $primaryColor . ';
	}
	.widget.has-bg {
		h4:after {
			background-color: ' . $quaternaryColor . ';
		}
		a.button {
			background-color: ' . $primaryColor . ';
			color: ' . $tertiaryColor . ';
			&:hover {
				background-color: ' . check_color($quaternaryColor, 'darken', true) . '(' . $quaternaryColor . ', 20%);	
				color: ' . $primaryColor . ';
			}
		}
	}
	.overlay-primary {
		background-color: rgba(' . $primaryColor . ', .50);
		h1,p { 
			color: color-contrast(' . $primaryColor . ', darken(' . $primaryColor . ', 100%), lighten(' . $primaryColor . ', 100%));
			text-shadow: 0 0 10px ' . $primaryColor . ';
		}
	}
	.sidebar .button a, .widget .social-connect a:before, nav.main span {
		color: $primary;
	}
	

	// Secondary //
	.bg-secondary {
		background-color: ' . $secondaryColor . ';
	}
	.the-post, footer.post {
		border-color: ' . check_color($secondaryColor, 'lighten', false) . '(' . $secondaryColor . ', 50%);
	}
	p, ul, ol, dt, dd, span, table, td, tr, body, table tr td, table tr th,
	.continue:hover {
		color: ' . $secondaryColor . ';
	}
	.categorizing p,
	.product-thumb .details .price,
	header.main .cart-stats {
		color: ' . check_color($secondaryColor, 'lighten', false) . '(' . $secondaryColor . ', 20%);
	}
	.sidebar .widget .label.secondary {
		background-color: ' . check_color($secondaryColor, 'lighten', false) . '(' . $secondaryColor . ', 20%);
		color: ' . check_color($secondaryColor, 'lighten', false) . '(' . $secondaryColor . ', 70%);
	}
	.overlay-secondary {
		background-color: rgba(' . $secondaryColor . ', .50);
		h1,p { 
			color: color-contrast(' . $secondaryColor . ', darken(' . $secondaryColor . ', 100%), lighten(' . $secondaryColor . ', 100%));
			text-shadow: 0 0 10px ' . $secondaryColor . ';
		}
	}


	// Tertiary //
	.bg-tertiary,
	.post time, .continue,
	nav.main ul > li.current_page_item > a:after,
	.button,
	.reveal-modal .form .alt,
	.pagination .current,
	.ilightbox-toolbar.dark a, .ilightbox-holder.dark,
	.home-featured a:hover,
	.widget.has-bg {
		background-color: ' . $tertiaryColor . ';
	}
	.reveal-modal-bg,
	.ilightbox-overlay.dark {
		background-color: rgba(' . $tertiaryColor . ', .95);	
	}
	nav.main ul ul {
		background-color: rgba(' . $tertiaryColor . ', .75);
	}
	.overlay-tertiary {
		background-color: rgba(' . $tertiaryColor . ', .50);
		h1, p { 
			color: color-contrast(' . $tertiaryColor . ', darken(' . $tertiaryColor . ', 100%), lighten(' . $tertiaryColor . ', 100%));
			text-shadow: 0 0 10px ' . $tertiaryColor . ';
		}
	}
	.gallery-overlay { 
		background-color: rgba(' . $tertiaryColor . ', .8);
		h4 { color: darken(' . $primaryColor . ', 10%); }
		p { color: ' . $primaryColor . '; }
	}
	.reveal-modal .form {
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, span, label {
			color: ' . $tertiaryColor . ';
		}
	}
	a, nav.main > ul > li > a:hover,
	.product-thumb .details strong,
	.cta-band .button,
	.home-featured a:hover span {
		color: ' . $tertiaryColor . ';
	}
	a:hover {
		color: ' . check_color($tertiaryColor, 'darken', true) . '(' . $tertiaryColor . ', 10%);	
	}
        // .lower .connect {
        //         span, .social-connect a {
        //                 color: rgba(' . $tertiaryColor . ', .60);
        //                 &:hover {color: '. $tertiaryColor .'}
        //         }
        // }
	header.main .upper, footer.main .lower {
		p, span, a {
			color: $tertiary;
		}
	}
	.bg-tertiary-light {
		background-color: lighten($tertiary, 75%);
	}
	header.main .upper {
		border-bottom: 1px solid lighten($tertiary, 50%);
	}
	footer.main h3, footer.main p, footer.main a, footer.main span, footer.main li, footer.main .lower p, footer.main .lower a {
		color: $primary;
	}
	input, textarea {
		border-color: $tertiary;
		color: $tertiary;
	}
	

	// Quaternary //
	.bg-quaternary,
	.continue:hover,
	.home-featured a:hover span {
		background-color: ' . $quaternaryColor . ';
	}
	.button:hover,
	.button:active,
	nav.main ul ul li:hover {
		background-color: ' . check_color($quaternaryColor, 'darken', true) . '(' . $quaternaryColor . ', 20%);	
	}
	.page-gallery,
	.sidebar li,
	.navigation,
	div.main {
		border-color: ' . $quaternaryColor . ';
	}
	nav.main ul ul a {
		color: ' . $quaternaryColor . ';
	}
	nav.main > ul > li > a {
		color: ' . check_color($quaternaryColor, 'darken', true) . '(' . $quaternaryColor . ', 50%);	
	}
	h1, h2, h3, h4, h5, h6 {
		color: ' . check_color($quaternaryColor, 'darken', true) . '(' . $quaternaryColor . ', 20%);	
	}
	.button-alt, .jigoshop-cart .quantity input.plus, .jigoshop-cart .quantity input.minus {
		background-color: ' . $quaternaryColor . ';
		color: ' . $primaryColor . ';
		&:hover {
			background-color: ' . check_color($quaternaryColor, 'darken', true) . '(' . $quaternaryColor . ', 10%);	
			color: ' . $primaryColor . ';
		}
	}
	.overlay-quaternary {
		background-color: rgba(' . $quaternaryColor . ', .50);
		h1, p { 
			color: color-contrast(' . $quaternaryColor . ', darken(' . $quaternaryColor . ', 100%), lighten(' . $quaternaryColor . ', 100%));
			text-shadow: 0 0 10px ' . $quaternaryColor . ';
		}
	}
	nav.main ul > li:hover {
		background-color: lighten($quaternary, 10%);
		a {
			color: $primary;
		}
	}

	// Gallery //
	.gallery-list .tabs dd.active a { background-color: ' . $tertiaryColor . '; }
	.gallery-list .tabs dd a { background-color: ' . $secondaryColor . '; }

	// Shop //
	#breadcrumb, .product-nav.bottom, .product-single .product-details .panel, .product-single .product-details .active {
		background-color: ' . check_color($quaternaryColor, 'darken', true) . '(' . $quaternaryColor . ', 20%);	
	}
	.product-single {
		.tabs li { background-color: ' . $quaternaryColor . '; }	
		.related.products { border-color: ' . $quaternaryColor . '; }
		.panel {
			h1, h2, h3, h4, h5, h6, p, span, a, ul, ol, li, input, textarea {
				color: ' . check_color($quaternaryColor, 'darken', true) . '(' . $quaternaryColor . ', 70%);	
			}
			input, textarea {
				background-color: rgba(' . $primaryColor . ', .75);
			}
		}
	}
	.jigoshop {
		form.cart button, .button, input[type=submit] {
			background-color: ' . $tertiaryColor . ';
			color: ' . check_color($tertiaryColor, 'lighten', true) . '(' . $tertiaryColor . ', 60%);
			&:hover {
				background-color: darken(' . $tertiaryColor . ', 5%);
			}	
		}
		.button-alt {
			background-color: ' . $quaternaryColor . ';
			color: ' . check_color($quaternaryColor, 'darken', true) . '(' . $quaternaryColor . ', 60%);	
		}
		.quantity input.qty {
			background-color: rgba(' . $quaternaryColor . ', .5);
			color: ' . check_color($quaternaryColor, 'darken', true) . '(' . $quaternaryColor . ', 60%);	
		}
		.bg-tertiary {
			h3 {
				color: ' . check_color($tertiaryColor, 'lighten', true) . '(' . $tertiaryColor . ', 60%);	
			}
			p, span, label {
				color: ' . check_color($tertiaryColor, 'lighten', true) . '(' . $tertiaryColor . ', 100%);	
			}
			input {
				color: ' . check_color($tertiaryColor, 'darken', true) . '(' . $tertiaryColor . ', 100%);	
			}
			input[type=submit] {
				background-color: ' . $quaternaryColor . ';
				color: ' . check_color($quaternaryColor, 'darken', true) . '(' . $quaternaryColor . ', 60%);	
			}
			.alt {
				color: ' . $tertiaryColor . ';
				background-color: ' . $primaryColor . ';
			}
		}
	}

	// Social Icons //
	.social-share, .social-connect {
		&.st-single-color {
			.bg-primary &, .container &, .product-summary & {
				a:before { color: ' . $tertiaryColor . '; }
				a:hover:before { color: darken(' . $tertiaryColor . ', 10%); }
			}
			.bg-secondary &, .bg-tertiary &, .bg-quaternary & {
				a:before { color: ' . $primaryColor . '; }
				a:hover:before { color: darken(' . $primaryColor . ', 10%); }
			}
		}
	}

';

$user_custom_styles = get_field('advanced_css', 'options');

/*==========================================================================*/

class SassWatcher
{
	/**
	 * Watches a folder for .scss files, compiles them every X seconds
	 * Re-compiling your .scss files every X seconds seems like "too much action" at first sight, but using a
	 * "has-this-file-changed?"-check uses more CPU power than simply re-compiling them permanently :)
	 * Beside that, we are only compiling .scss in development, for production we deploy .css, so we don't care.
	 *
	 * @param string $scss_folder source folder where you have your .scss files
	 * @param string $css_folder destination folder where you want your .css files
	 * @param int $interval interval in seconds
	 * @param string $scssphp_script_path path where scss.inc.php (the scssphp script) is
	 * @param string $format_style CSS output format, ee http://leafo.net/scssphp/docs/#output_formatting for more.
	 */
	public function watch($scss_folder, $css_folder, $scssphp_script_path, $format_style = "scss_formatter", $user_defined_colors = null, $user_defined_styles = null)
	{
		// load the compiler script (scssphp), more here: http://www.leafo.net/scssphp
		require $scssphp_script_path;
		$scss_compiler = new scssc();

		// set the path to your to-be-imported mixins. please note: custom paths are coming up on future releases!
		$scss_compiler->setImportPaths($scss_folder);
		// set css formatting (normal, nested or minimized), @see http://leafo.net/scssphp/docs/#output_formatting
		$scss_compiler->setFormatter($format_style);

		// get all .scss files from scss folder
		$filelist = glob($scss_folder . "*.scss");

		// step through all .scss files in that folder
		foreach ($filelist as $file_path) {
			// get path elements from that file
			$file_path_elements = pathinfo($file_path);

			// get file's name without extension
			$file_name = $file_path_elements['filename'];

			// get .scss's content, put it into $string_sass
			$string_sass = file_get_contents($scss_folder . $file_name . ".scss");

			// Add our user defined colors to the compiled code
			$string_sass .= $user_defined_colors;

			// Add our user defined styles to the compiled code (so we can save it)
			$string_sass .= $user_defined_styles;

			// try/catch block to prevent script stopping when scss compiler throws an error
			try {
				// compile this SASS code to CSS
				$string_css = $scss_compiler->compile($string_sass);
				// write CSS into file with the same filename, but .css extension
				file_put_contents($css_folder . $file_name . ".css", $string_css);
			} catch (Exception $e) {
				// here we could put the exception message, but who cares ...
				echo 'Gary was here; Ash is a loser!';
			}
		}
	}
}

$sass_watcher = new SassWatcher();
$sass_watcher->watch(get_template_directory() . '/stylesheets/sass/', get_template_directory() . '/stylesheets/css/', get_template_directory() . '/stylesheets/scss.inc.php', 'scss_formatter', $user_colors, $user_custom_styles);
