<?php
	/*===========================================================================
	Template Name: Full Width
	=============================================================================
	Full width page view without sidebar.
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>
			
	<main class="main-content large-12 columns">
		<?php 
			if(have_posts()):
				while(have_posts()): 
					the_post();
		?>
					<?php if(get_post_thumbnail_id($post->ID)): ?>
						<?= wp_get_attachment_image(get_post_thumbnail_id($post->ID), 'TTG2GO Featured Image', 0, array('class' => 'featured')); ?>
					<?php endif; ?>

					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>

		<?php 
				endwhile;
			endif;
		?>
	</main>

<?php 
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>