jQuery(document).foundation();

jQuery(document).ready(function(){
	//============================
	// Reveal Product Details
	//============================
	// var hash = window.location.hash;
	// jQuery('.product .reveal-modal[data-slug="' + hash + '"]').foundation('reveal', 'open');


	//============================
	// Homepage Slider
	//============================
	jQuery('.home-slider').slick({
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: false,
		dots: true,
		infinite: true,
		fade: true
	});



	//============================
	// Tinynav/Custom Select
	//============================
	jQuery('nav.main ul.menu').tinyNav({
		header: 'Navigation'
	});
	jQuery('.categories-nav .cat-nav').tinyNav({
		header: 'Categories'
	});
	jQuery('select.tinynav').customSelect();



	//============================
	// Hide Category Sidebar Sub Nav
	//============================
	jQuery('.categories-nav .cat-nav ul').hide();
	
	// Show children of an active li parent
	jQuery('.cat-nav .current-cat > ul').show();

	// Show children when a child is active
	jQuery('.cat-nav .current-cat').parents('ul').show().parents('li').addClass('current-cat-parent');



	//============================
	// Pseudeo Sticky Category Sidebar
	//============================
	// http://css-tricks.com/scrollfollow-sidebar/
	// Get window height and width for sidebar
	var windowWidth		= jQuery(window).innerWidth();

	jQuery(window).resize(function(){
		windowWidth		= jQuery(window).innerWidth();
	});


	// Call the function on load and on resize
	stickySidebar();
	jQuery(window).resize(function() {
		stickySidebar();
	});

	// Write the function
	function stickySidebar() {
		if(windowWidth > 1024 && jQuery('.sticky').length) {
			var jQuerysidebar   = jQuery('.sticky'), 
				jQuerywindow    = jQuery(window),
				offset     = jQuerysidebar.offset(),
				topPadding = 15;

			jQuerywindow.scroll(function() {
				if (jQuerywindow.scrollTop() > offset.top) {
					jQuerysidebar.stop().animate({
						marginTop: jQuerywindow.scrollTop() - offset.top + topPadding
					});
				} else {
					jQuerysidebar.stop().animate({
						marginTop: 0
					});
				}
			});
		}
		else {
			jQuery(window).unbind('scroll');
			jQuery('.sticky').removeAttr('style');
		}
	}

});