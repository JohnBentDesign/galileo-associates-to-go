<?php
	/*===========================================================================
	HECATE: STYLING OPTIONS
	===========================================================================*/

	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_hecate-options-theme-styling',
			'title' => '[Hecate] Options: Theme Styling',
			'fields' => array (
				array (
					'key' => 'field_5390be377226b',
					'label' => '',
					'name' => '',
					'type' => 'message',
					'message' => '<h2 style="background-color: #0074a2; padding: 5px 10px; color: #ffffff">GENERAL SETTINGS</h2>',
				),
				array (
					'key' => 'field_536a6d2bf4882',
					'label' => 'Colors',
					'name' => '',
					'type' => 'tab',
				),
				array (
					'key' => 'field_536a6d9bf4883',
					'label' => 'Primary Color',
					'name' => 'color_primary',
					'type' => 'color_picker',
					'default_value' => '#ffffff',
				),
				array (
					'key' => 'field_536a6dbbfe4ad',
					'label' => 'Secondary Color',
					'name' => 'color_secondary',
					'type' => 'color_picker',
					'default_value' => '#565656',
				),
				array (
					'key' => 'field_536a6dc6fe4ae',
					'label' => 'Tertiary Color',
					'name' => 'color_tertiary',
					'type' => 'color_picker',
					'default_value' => '#00aeef',
				),
				array (
					'key' => 'field_536a6dcffe4af',
					'label' => 'Quaternary Color',
					'name' => 'color_quaternary',
					'type' => 'color_picker',
					'default_value' => '#691c33',
				),
				array (
					'key' => 'field_536a6de0fe4b0',
					'label' => 'Header',
					'name' => '',
					'type' => 'tab',
				),
				array (
					'key' => 'field_536a6cd5f4881',
					'label' => 'Logo',
					'name' => 'header_logo',
					'type' => 'image',
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_53ab2822f86c9',
					'label' => 'Section Divider Type',
					'name' => 'header_divider',
					'type' => 'radio',
					'instructions' => 'This changes the divider type that will separate the pieces in the section above the main navigation',
					'choices' => array (
						'none' => 'None',
						'line' => 'Line',
						'bullet' => 'Bullet',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => 'none',
					'layout' => 'horizontal',
				),
				array (
					'key' => 'field_538deef7c3116',
					'label' => 'Display Components',
					'name' => 'header_components',
					'type' => 'checkbox',
					'instructions' => 'Social Icon settings can be found in the "Social Media" section below.',
					'choices' => array (
						'tagline' => 'Tagline',
						'social' => 'Social Icons',
						'nav' => 'Secondary Nav',
						'nav-shop' => 'Shop Nav',
						'search' => 'Search Bar',
					),
					'default_value' => '',
					'layout' => 'horizontal',
				),
				array (
					'key' => 'field_53ab288ef86ca',
					'label' => 'Search Display Type',
					'name' => 'header_search',
					'type' => 'radio',
					'instructions' => 'This will change how the search form is displayed in the section above the main navigation',
					'conditional_logic' => array (
						'status' => 1,
						'rules' => array (
							array (
								'field' => 'field_538deef7c3116',
								'operator' => '==',
								'value' => 'search',
							),
						),
						'allorany' => 'all',
					),
					'choices' => array (
						'underline' => 'Underlined Search Form',
						'filled' => 'Search Form with Background Color',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => 'underline',
					'layout' => 'horizontal',
				),
				array (
					'key' => 'field_537f82dabbf1a',
					'label' => 'Tagline',
					'name' => 'header_tagline',
					'type' => 'text',
					'instructions' => 'This text will appear under the site\'s logo/name. It doesn\'t have to be a tagline--it can also display phone numbers, hours, etc.',
					'conditional_logic' => array (
						'status' => 1,
						'rules' => array (
							array (
								'field' => 'field_538deef7c3116',
								'operator' => '==',
								'value' => 'tagline',
							),
						),
						'allorany' => 'all',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_536a6ea0a8a46',
					'label' => 'Footer',
					'name' => '',
					'type' => 'tab',
				),
				array (
					'key' => 'field_538de83f00a43',
					'label' => 'Footer Content',
					'name' => 'footer_content',
					'type' => 'wysiwyg',
					'instructions' => 'This is additional content that will be added in a band above the main footer. You can put text, images, links, etc. in this section.',
					'default_value' => '',
					'toolbar' => 'full',
					'media_upload' => 'yes',
				),
				array (
					'key' => 'field_53b41a1d8abf4',
					'label' => 'Shop',
					'name' => '',
					'type' => 'tab',
				),
				array (
					'key' => 'field_53b41a278abf5',
					'label' => 'Turn off sticky category navigation?',
					'name' => 'shop_sticky',
					'type' => 'true_false',
					'instructions' => 'Shops that have a lot of categories (6+) may run into performance issues with having the category navigation follow the user on scroll. Check this to remove this option.',
					'message' => '',
					'default_value' => 0,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'acf-options-theme-styling',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}








