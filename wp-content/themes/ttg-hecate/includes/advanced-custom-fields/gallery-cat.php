<?php
	/*===========================================================================
	HECATE: GALLERY CATEGORY
	===========================================================================*/

	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_hecate-gallery-categories',
			'title' => '[Hecate] Gallery: Categories',
			'fields' => array (
				array (
					'key' => 'field_5391d91abfeb7',
					'label' => 'Featured Image',
					'name' => 'gallery_cat_image',
					'type' => 'image',
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
				array (
					'key' => 'field_5390c7aa4d311',
					'label' => 'Category Order Position',
					'name' => 'gallery_cat_order',
					'type' => 'number',
					'instructions' => 'Assign an order number to this category. When it\'s displayed in a list, it will follow this order in ASCENDING order. Default value is 0, which falls back onto category name in alphabetical order.',
					'default_value' => 0,
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => 0,
					'max' => '',
					'step' => 1,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'ef_taxonomy',
						'operator' => '==',
						'value' => 'gallery-categories',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}






