<?php
/**
 * Class SassWatcher
 *
 * This simple tool compiles all .scss files in folder A to .css files (with exactly the same name) into folder B.
 * To keep things as minimal as possible, this tool compiles every X seconds, regardless of changes within the files.
 * This seems weird, but makes sense as checking for changes in the files is more CPU-extensive than simply
 * re-compiling them. SassWatcher uses scssphp, the best SASS compiler in PHP available.
 *
 * SassWatcher is not a standalone compiler, it's just a little method that uses the excellent scssphp compiler written
 * by Leaf Corcoran (https://twitter.com/moonscript), which can be found here: http://leafo.net/scssphp/ and adds
 * automatic interval-compiling to it.
 *
 * The currently supported version of SCSS syntax is 3.2.12, which is the latest one.
 * To avoid confusion: SASS is the name of the language itself, and also the "name" of the "first" version of the
 * syntax (which was quite different than CSS). Then SASS's syntax was changed to "SCSS", which is more like CSS, but
 * with awesome additional possibilities and features.
 *
 * The compiler uses the SCSS syntax, which is recommended and mostly used. The old SASS syntax is not supported.
 *
 * @see SASS Wikipedia: http://en.wikipedia.org/wiki/Sass_%28stylesheet_language%29
 * @see SASS Homepage: http://sass-lang.com/
 * @see scssphp, the used compiler (in PHP): http://leafo.net/scssphp/
 *
 * How to use this tool:
 *
 * 1. Edit $sass_watcher->watch( ... ); in the last line of this file and put your stuff in here, see the parameter
 *    list below.
 * 2. Make sure PHP can write into your CSS folder.
 * 3. Run the script:
 *    a) simple way, from browser, just enter the URL to scss-compiler.php: http://127.0.0.1/folder/php-sass-watcher.php
 *       The script will run forever, even if you close the browser window.
 *    b) PHPStorm users can run the script by right-clicking the file and selecting "Run php-sass-watcher.php".
 * 4. To stop the script, stop/restart your Apache/Nginx/etc. or press the red "stop process button in PHPStorm.
 *
 * The parameters:
 *
 *  1. relative path to your SCSS folder
 *  2. relative path to your CSS folder (make sure PHP has write-rights here)
 *  3. the compiling interval (in seconds)
 *  4. relative path to the scss.inc.php file, which is the main file of the SASS compiler used
 *     here. Download the script manually from http://leafo.net/scssphp/ or "require" it via Composer:
 *     "leafo/scssphp": "0.0.9"
 *  5. optional: how the .css output should look like. See http://leafo.net/scssphp/docs/#output_formatting for more.
 *
 * How the tool works:
 *
 * Every X seconds ALL files in the scss folder will be compiled to same-name .css files in the css folder.
 * The tool does not stop when a .scss file is broken, has syntax error or similar.
 * The tool does not compile when .scss file is broken, has syntax error or similar. It will only compile next time
 * when there's a valid scss file.
 */

/*==========================================================================
CUSTOM COLORS DEFINED BY USER
==========================================================================*/
$primaryColor       = (get_field('color_primary', 'options'))       ? get_field('color_primary', 'options')     : '#ffffff';
$secondaryColor     = (get_field('color_secondary', 'options'))     ? get_field('color_secondary', 'options')   : '#565656';
$tertiaryColor      = (get_field('color_tertiary', 'options'))      ? get_field('color_tertiary', 'options')    : '#00aeef';
$quaternaryColor    = (get_field('color_quaternary', 'options'))    ? get_field('color_quaternary', 'options')  : '#691c33';

$user_colors = '
	@import "includes";

	// Variables //
	$primaryColorContrast70: color-contrast(' . $primaryColor . ', darken(' . $primaryColor . ', 70%), lighten(' . $primaryColor . ', 70%));
	$secondaryColorContrast70: color-contrast(' . $secondaryColor . ', darken(' . $secondaryColor . ', 70%), lighten(' . $secondaryColor . ', 70%));
	$tertiaryColorContrast70: color-contrast(' . $tertiaryColor . ', darken(' . $tertiaryColor . ', 70%), lighten(' . $tertiaryColor . ', 70%));
	$quaternaryColorContrast70: color-contrast(' . $quaternaryColor . ', darken(' . $quaternaryColor . ', 70%), lighten(' . $quaternaryColor . ', 70%));

	$primaryColorContrast15: color-contrast(' . $primaryColor . ', darken(' . $primaryColor . ', 15%), lighten(' . $primaryColor . ', 15%));
	$secondaryColorContrast15: color-contrast(' . $secondaryColor . ', darken(' . $secondaryColor . ', 15%), lighten(' . $secondaryColor . ', 15%));
	$tertiaryColorContrast15: color-contrast(' . $tertiaryColor . ', darken(' . $tertiaryColor . ', 15%), lighten(' . $tertiaryColor . ', 15%));
	$quaternaryColorContrast15: color-contrast(' . $quaternaryColor . ', darken(' . $quaternaryColor . ', 15%), lighten(' . $quaternaryColor . ', 15%));

	$primaryColorContrast5: color-contrast(' . $primaryColor . ', darken(' . $primaryColor . ', 5%), lighten(' . $primaryColor . ', 5%));
	$secondaryColorContrast5: color-contrast(' . $secondaryColor . ', darken(' . $secondaryColor . ', 5%), lighten(' . $secondaryColor . ', 5%));
	$tertiaryColorContrast5: color-contrast(' . $tertiaryColor . ', darken(' . $tertiaryColor . ', 5%), lighten(' . $tertiaryColor . ', 5%));
	$quaternaryColorContrast5: color-contrast(' . $quaternaryColor . ', darken(' . $quaternaryColor . ', 5%), lighten(' . $quaternaryColor . ', 5%));

	// Primary //
	.primary {
		background-color: ' . $primaryColor . ';
		h1, h2, h3, h4, h5, p, span, blockquote, blockquote p, dl, dt, dd, table, table th, table th tr, table td, ul, ol, li, body & {
			color: ' . $secondaryColor . ';
		}
		.st-single-color.social a:before {
			color: ' . $tertiaryColor . ' !important;
		}
		.st-single-color.social a:hover:before {
			color: $tertiaryColorContrast15 !important;
		}
	}
	.primary.adjusted {
		background-color: $primaryColorContrast5;
	}
	.button.primary {
		color: ' . $tertiaryColor . ';
		&:hover {
			color: ' . $primaryColor . ';
			background-color: ' . $tertiaryColor . ';
		}
		.feature & {
			color: ' . $tertiaryColor . ';
			background-color: ' . $primaryColor . ';
		}
	}
	.rule {
		background-color: $primaryColorContrast15;
	}
	.home .orbit-container .orbit-bullets-container .orbit-bullets li {
		border-color: ' . $primaryColor .';
		background-color: ' . $primaryColor .';
	}
	.footer-add, .the-post, footer.post .social, 
	.cat-nav, .gallery .content, .gallery .content .categories, .gallery .content .social-share,
	aside.sidebar .widget, aside.sidebar,
	.contact .social-connect:nth-child(2), .contact .social-share:nth-child(2),
	.product-summary .product_meta, .product-summary form, .product-summary .social {
		border-color: $primaryColorContrast15;
	}
	.home-slider {
		h2, p, span {
			color: ' . $primaryColor . ';
		}
	}
	span.onsale {
		color: ' . $primaryColor . ';
	}
	.home .contact .contact-icon {
		background-color: ' . $tertiaryColor . ';
		color: ' . $primaryColor . ';
	}
	.line-up {
		border-color: ' . $primaryColor . ';
		color: ' . $primaryColor . ';
	}
	.breadcrumb {
		background-color: $primaryColorContrast15;
		a {
			color: $primaryColorContrast70;
		}
	}
	nav.main .menu > li:hover > a,
	time .year, time .month,
	.cat-nav .current-cat > a, .cat-nav a:hover,
	.product-details .form .alt, .product-details .alt,
	nav.shop:before {
		color: ' . $primaryColor . ';
	}
	.overlay .product-details {
		h1, h2, h3, h4, h5, p, span, blockquote, blockquote p, dl, dt, dd, table, table th, table th tr, table td, ul, ol, li {
			color: ' . $primaryColor . ';
		}
		.form {
			h1, h2, h3, h4, h5, p, span, blockquote, blockquote p, dl, dt, dd, table, table th, table th tr, table td, ul, ol, li {
				color: ' . $tertiaryColor . ';
			}
		}
	}
	.product-details .form {
		background-color: rgba(' . $primaryColor . ', .7);
	}
	.product-thumb .button {
		background-color: ' . $primaryColor . ';
		color: ' . $tertiaryColor . ';
		&:hover {
			background-color: ' . $tertiaryColor . ';
			color: ' . $primaryColor . ';
		}
	}
	aside.sidebar .searchform {
		background-color: $primaryColorContrast15;
		input[type="submit"] {
			color: ' . $primaryColor . ';
			&:hover {
				color: ' . $tertiaryColor . ';
			}
		}
	}
	aside.sidebar .gform_wrapper {
		input[type=text], input[type=url], input[type=email], input[type=tel], input[type=number], input[type=password], textarea.textarea {
			background-color: $primaryColorContrast15;
			color: ' . $secondaryColor . ';
		}
	}
	aside.sidebar .widget.has-bg .button {
		background-color: ' . $primaryColor . ';
		color: ' . $tertiaryColor . ';
		&:hover {
			background-color: ' . $quaternaryColor . ';
			color: ' . $primaryColor . ';
		}
	}
	.overlay-primary {
		background-color: rgba(' . $primaryColor . ', .7);
		h2, h3, h4, h5, p, span {
			color: ' . $secondaryColor . ';
		}
		.featured {
			background-color: ' . $tertiaryColor . ';
			color: ' . $primaryColor . ';
		}
	}
	.border-primary {
		border-color: ' . $primaryColor . ';
	}
	.intro.tertiary {
		h1, h2, h3, h4, h5, p, span, blockquote, blockquote p, dl, dt, dd, table, table th, table th tr, table td, ul, ol, li, body & {
			color: ' . $primaryColor . ';
		}
	}
	.current-cat, .current-cat-parent {
		background-color: rgba($primaryColorContrast15, .5);
	}
	.current-cat-parent ul {
		background-color: rgba($primaryColorContrast70, .10);
	}
	.product-details .panel {
		background-color: rgba(' . $primaryColor . ', .5);
	}


	// Secondary //
	.cat-nav a, .counter,
	aside.sidebar ul a {
		color: ' . $secondaryColor . ';
	}
	.flex-control-paging li a, .flex-control-paging li a.flex-active, .gallery .tabs dd a, h1:after  {
		background-color: ' . $secondaryColor . ';
	}
	.overlay-secondary {
		background-color: rgba(' . $secondaryColor . ', .7);
		h2, h3, h4, h5, p, span {
			color: ' . $primaryColor . ';
		}	
		.featured {
			background-color: ' . $primaryColor . ';
			color: ' . $secondaryColor . ';
		}
	}
	.border-secondary {
		border-color: ' . $secondaryColor . ';
	}


	// Tertiary //
	.tertiary,
	time, .continue,
	.cat-nav .current-cat > a, .cat-nav a:hover,
	.gallery .tabs dd.active a,
	.product-details .alt,
	.widget.has-bg {
		background-color: ' . $tertiaryColor . ';
	}
	header.main, footer.main {
		h1, h2, h3, h4, h5, p, span, blockquote, blockquote p, dl, dt, dd, table, table th, table th tr, table td, ul, ol, li, a {
			color: ' . $primaryColor . ';
		}
		a:hover {
			color: $primaryColorContrast15;
		}
	}
	.cta-box, .widget.has-bg {
		h1, h2, h3, h4, h5, a {
			color: ' . $primaryColor . ';
		}
		p, span, blockquote, blockquote p, dl, dt, dd, table, table th, table th tr, table td, ul, ol, li {
			color: $primaryColorContrast15;
		}
	}
	.overlay-tertiary, .gallery-overlay {
		background-color: rgba(' . $tertiaryColor . ', .7);
		h2, h3, h4, h5, p, span {
			color: ' . $primaryColor . ';
		}	
		.featured {
			background-color: ' . $primaryColor . ';
			color: ' . $tertiaryColor . ';
		}
	}
	.button, .continue {
		color: ' . $primaryColor . ';
		background-color: ' . $quaternaryColor . ';
		&:hover {
			background-color: ' . $tertiaryColor . ';
			color: ' . $primaryColor . ';
		}
	}
	.counter .current {
		color: ' . $tertiaryColor . ';
	}
	a, .flex-direction-nav a {
		color: ' . $tertiaryColor . ';
		&:hover {
			color: $tertiaryColorContrast5;
		}
	}
	.home .slick-dots .slick-active button {
		background-color: ' . $tertiaryColor . ';
	}
	.home .slick-dots button {
		background-color: ' . $primaryColor . ';
		border-color: ' . $primaryColor . ';
	}
	footer.main {
		border-color: $tertiaryColorContrast5;
	}
	.home .contact {
		p, span, a {
			color: ' . $tertiaryColor . ';
		}
		.interested:hover, a:hover {
			color: $tertiaryColorContrast15;
		}
	}
	.reveal-modal-bg, div.pp_overlay {
		background-color: rgba(' . $tertiaryColor . ', .9);
	}
	.has-children.active, .has-children ul a:hover, .has-children.active .active a {
		background-color: rgba(' . $tertiaryColor . ', .40);
	}
	aside.sidebar li:hover {
		background-color: ' . $tertiaryColor . ';
		color: ' . $primaryColor . ';
		a {
			color: ' . $primaryColor . ';
		}
	}
	aside.sidebar .gform_wrapper li:hover,
	div.pp_default a:before {
		color: ' . $tertiaryColor . ';
	}
	.border-tertiary {
		border-color: ' . $tertiaryColor . ';
	}



	// Quaternary //
	.quaternary {
		background-color: ' . $quaternaryColor . ';
		h1, h2, h3, h4, h5, p, span, blockquote, blockquote p, dl, dt, dd, table, table th, table th tr, table td, ul, ol, li, a {
			color: ' . $primaryColor . ';
		}
	}
	nav.main .menu > li > a:after,
	.product-details h3:after {
		background-color: ' . $quaternaryColor . ';
	}
	header.main .upper {
		a, input, span {
			color: ' . $quaternaryColor . ';
		}
		.searchform {
			border-color: ' . $quaternaryColor . ';
		}
	}
	nav.main ul ul {
		background-color: rgba(' . $quaternaryColor . ', .95);
		li {
			border-color: rgba(' . $tertiaryColor .', .25);
		}
		a {
			color: ' . $tertiaryColor .';
			&:hover { 
				color: $tertiaryColorContrast15;
				background-color: rgba(' . $tertiaryColor .', .25);
			}
		}
	}
	time .date, .price, .product-thumb span.price,
	aside.sidebar .gform_wrapper .gfield_required, aside.sidebar .gform_wrapper div.validation_error {
		color: ' . $quaternaryColor . ';
	}
	.product-details footer.reveal {
		span, a, .st-single-color.social a:before {
			color: ' . $quaternaryColor . ' !important;
		}
		.st-single-color.social a:hover:before {
			color: $quaternaryColorContrast15 !important;
		}
	}
	footer.reveal {
		border-color: ' . $quaternaryColor . ';
	}
	.button.view-details, .button-alt, header.main .customSelect {
		background-color: ' . $quaternaryColor . ';
		color: ' . $primaryColor . ';
		&:hover {
			background-color: ' . $tertiaryColor . ';
			color: ' . $primaryColor . ';
		}
	}
	.overlay-quaternary {
		background-color: rgba(' . $quaternaryColor . ', .7);
		h2, h3, h4, h5, p, span {
			color: ' . $primaryColor . ';
		}
		.featured {
			background-color: ' . $primaryColor . ';
			color: ' . $quaternaryColor . ';
		}
	}
	.border-quaternary {
		border-color: ' . $quaternaryColor . ';
	}
	header.main .filled .searchform {
		background-color: ' . $quaternaryColor . ';
		input {
			color: ' . $primaryColor . ';
		}
	}

	// GALLERY //
	.ilightbox-overlay.dark {
		background-color: ' . $secondaryColor . ' !important;
	}

	// SHOP //
	div.jigoshop_message {
		background-color: $quaternaryColorContrast5;
		border-color: $quaternaryColorContrast15;
		span {
			color: ' . $primaryColor . ';
		}
	}
	.quantity input.plus:hover, .quantity input.minus:hover {
		background-color: ' . $quaternaryColor . ';
		color: ' . $primaryColor . ';
	}
	.jigoshop .add-cart .button {
		background-color: ' . $quaternaryColor . ';
		color: ' . $primaryColor . ';
	}

	// Gallery //
	.gallery-list .tabs dd.active a { background-color: ' . $tertiaryColor . '; }
	.gallery-list .tabs dd a { background-color: ' . $secondaryColor . '; }

	// Shop //
	.product-single .product-details .panel, .product-single .product-details .active {
		background-color: ' . $tertiaryColor . ';	
	}
	.product-single {
		.tabs li { background-color: ' . $quaternaryColor . '; }	
		.related.products { border-color: ' . $quaternaryColor . '; }
		.panel {
			h1, h2, h3, h4, h5, h6, p, span, a, ul, ol, li, input, textarea {
				color: ' . $primaryColor . ';	
			}
			input, textarea {
				background-color: rgba(' . $primaryColor . ', .75);
			}
		}
	}
	.jigoshop {
		form.cart button, .button, input[type=submit] {
			background-color: ' . $tertiaryColor . ';
			color: ' . $primaryColor . ';
			&:hover {
				background-color: darken(' . $tertiaryColor . ', 5%);
			}	
		}
		.button-alt {
			background-color: ' . $quaternaryColor . ';
			color: ' . $primaryColor . ';	
		}
		.quantity input.qty {
			background-color: rgba(' . $quaternaryColor . ', .5);
			color: ' . $primaryColor . ';	
		}
		.tertiary {
			h3, p, span, label, input {
				color: ' . $primaryColor . ';	
			}
			input[type=submit] {
				background-color: ' . $quaternaryColor . ';
				color: ' . $tertiaryColor . ';
			}
			.alt {
				color: ' . $tertiaryColor . ';
				background-color: ' . $primaryColor . ';
			}
		}
	}
	.jigoshop-products {
		.product-details {
			h3, p {
				color: ' . $primaryColor . ';
			}
			.button {
				color: ' . $tertiaryColor . ';
				background-color: ' . $quaternaryColor . ';
				&:hover {
					color: ' . $primaryColor . ';
					background-color: ' . $tertiaryColor . ';
				}
			}
		}
	}

	// Blog //
	#commentform input[type=submit] {
		background-color: ' . $tertiaryColor . ';
		color: ' . $primaryColor . ';
		&:hover {
			background-color: ' . $quaternaryColor . ';
			color: ' . $primaryColor . ';
		}
	}

	// Sidebar //
	.widget {
		&.has-bg {
			.social a:before {
				color: ' . $primaryColor . ' !important;
			}
			.button {
				background-color: ' . $primaryColor . ';
				a { color: ' . $tertiaryColor . '; }
			}
			.button:hover {
				background-color: ' . $quaternaryColor . ';
				a { color: ' . $primaryColor . '; }
			}
		}
		.button {
			background-color: ' . $quaternaryColor . ';
			a { color: ' . $primaryColor . '; }
		}
		.button:hover {
			background-color: ' . $tertiaryColor . ';
			a { color: ' . $primaryColor . '; }
		}
	}

	// Social Icons //
	.social-share, .social-connect {
		&.st-single-color {
			.primary &, .container &, .product-summary &, .widget & {
				a:before { color: ' . $tertiaryColor . '; }
				a:hover:before { color: darken(' . $tertiaryColor . ', 10%); }
			}
			.secondary &, .tertiary &, .quaternary &, .widget & {
				a:before { color: ' . $primaryColor . '; }
				a:hover:before { color: darken(' . $primaryColor . ', 10%); }
			}
		}
	}

	// MEDIA QUERIES //
	@media only screen and (max-width: 64em) {
		aside.sidebar, .single-gallery .content {
			background-color: $primaryColorContrast15;
			.searchform,
			.gform_wrapper input[type=text], .gform_wrapper input[type=url], .gform_wrapper input[type=email], .gform_wrapper input[type=tel], .gform_wrapper input[type=number], .gform_wrapper input[type=password], .gform_wrapper textarea.textarea {
				background-color: $primaryColorContrast5;
				input[type="submit"] {
					color: ' . $secondaryColor . ';
				}
			}
			.widget, .categories, .social {
				border-color: $primaryColorContrast5;
			}
		}
		.categories-nav .customSelect {
			background-color: $primaryColorContrast15;
		}
	}
';

$user_custom_font_style = pantheon_function_font_select(array(
		'header' 	=> 'header.main',
		'footer' 	=> 'footer.main',
		'sidebar' 	=> 'aside.sidebar',
		'widget' 	=> '.widget',
		'post' 		=> '.the-post',
		'page' 		=> '.main-content',
		'gallery' 	=> 'article.gallery',
		'product' 	=> 'li.product',
	)
);

$user_custom_text_style = pantheon_function_text_style(array(
		'header' 	=> 'header.main',
		'footer' 	=> 'footer.main',
		'sidebar' 	=> 'aside.sidebar',
		'widget' 	=> '.widget',
		'post' 		=> '.the-post',
		'page' 		=> '.main-content',
		'gallery' 	=> 'article.gallery',
		'product' 	=> 'li.product',
	)
);

$user_custom_styles = get_field('advanced_css', 'options');


/*==========================================================================*/

class SassWatcher
{
	/**
	 * Watches a folder for .scss files, compiles them every X seconds
	 * Re-compiling your .scss files every X seconds seems like "too much action" at first sight, but using a
	 * "has-this-file-changed?"-check uses more CPU power than simply re-compiling them permanently :)
	 * Beside that, we are only compiling .scss in development, for production we deploy .css, so we don't care.
	 *
	 * @param string $scss_folder source folder where you have your .scss files
	 * @param string $css_folder destination folder where you want your .css files
	 * @param int $interval interval in seconds
	 * @param string $scssphp_script_path path where scss.inc.php (the scssphp script) is
	 * @param string $format_style CSS output format, ee http://leafo.net/scssphp/docs/#output_formatting for more.
	 */
	public function watch($scss_folder, $css_folder, $scssphp_script_path, $format_style = "scss_formatter", $user_defined_colors = null, $user_defined_custom_font = null, $user_defined_custom_text = null, $user_defined_styles = null)
	{
		// load the compiler script (scssphp), more here: http://www.leafo.net/scssphp
		require $scssphp_script_path;
		$scss_compiler = new scssc();

		// set the path to your to-be-imported mixins. please note: custom paths are coming up on future releases!
		$scss_compiler->setImportPaths($scss_folder);
		// set css formatting (normal, nested or minimized), @see http://leafo.net/scssphp/docs/#output_formatting
		$scss_compiler->setFormatter($format_style);

		// get all .scss files from scss folder
		$filelist = glob($scss_folder . "*.scss");

		// step through all .scss files in that folder
		foreach ($filelist as $file_path) {
			// get path elements from that file
			$file_path_elements = pathinfo($file_path);

			// get file's name without extension
			$file_name = $file_path_elements['filename'];

			// get .scss's content, put it into $string_sass
			$string_sass = file_get_contents($scss_folder . $file_name . ".scss");

			// Add our user defined colors to the compiled code
			$string_sass .= $user_defined_colors;

			// Add our user font styles to the compiled code
			$string_sass .= $user_defined_custom_font;

			// Add our user text styles to the compiled code
			$string_sass .= $user_defined_custom_text;

			// Add our user defined styles to the compiled code (so we can save it)
			$string_sass .= $user_defined_styles;

			// try/catch block to prevent script stopping when scss compiler throws an error
			try {
				// compile this SASS code to CSS
				$string_css = $scss_compiler->compile($string_sass);
				// write CSS into file with the same filename, but .css extension
				file_put_contents($css_folder . $file_name . ".css", $string_css);
			} catch (Exception $e) {
				echo 'Gary was here; Ash is a loser!';
			}
		}
	}
}

$sass_watcher = new SassWatcher();
$sass_watcher->watch(get_template_directory() . '/stylesheets/sass/', get_template_directory() . '/stylesheets/css/', get_template_directory() . '/stylesheets/scss.inc.php', 'scss_formatter', $user_colors, $user_custom_font_style, $user_custom_text_style, $user_custom_styles);
