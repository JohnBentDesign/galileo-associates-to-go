<?php
	/*===========================================================================
	INDEX
	=============================================================================
	Generic view for post listings. Also includes search, category, and archive
	listing view
	*/

	// Header and sidebar
	get_template_part('parts/shared/header', 'html');

	// POST LOOP //

	// GALLERY POST/LISTING
	//--------------------------------------
	if($post->post_type == 'gallery'):
		get_template_part('parts/posts/loop', 'gallery');	
		get_template_part('parts/shared/pagination');


	// BLOG POST/LISTING
	//--------------------------------------
	else:
		echo '<div class="main-content large-9 columns">';
		get_template_part('parts/posts/loop');
		get_template_part('parts/shared/pagination');
		echo '</div>';
		get_template_part('parts/shared/sidebar');
	endif;

	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>