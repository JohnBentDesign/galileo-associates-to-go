<?php
	/*===========================================================================
	PAGE
	=============================================================================
	View for pages
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>
		
	<main class="main-content the-page large-9 columns">
		<?php 
			if(have_posts()):
				while(have_posts()): 
					the_post();

					// Featured Image
					pantheon_display_post_featured_image($post->ID, 'TTG Homepage Featured - 1 Column', false);

					// Title
					echo '<h1>' . get_the_title() . '</h1>';

					// Content
					the_content();

				endwhile;
			endif;
		?>
	</main>

	<?php
		// SIDEBAR //
		get_template_part('parts/shared/sidebar'); 
	?>

<?php 
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>