<?php
	/**
	* Product template
	*
	* DISCLAIMER
	*
	* Do not edit or add directly to this file if you wish to upgrade Jigoshop to newer
	* versions in the future. If you wish to customise Jigoshop core for your needs,
	* please use our GitHub repository to publish essential changes for consideration.
	*
	* @package             Jigoshop
	* @category            Catalog
	* @author              Jigoshop
	* @copyright           Copyright © 2011-2014 Jigoshop.
	* @license             GNU General Public License v3
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');


	// VARIABLES //
	global $isSingle;
	$isSingle 				= is_single();
	$activeShop 			= get_field('shop_enable', 'options');
	$activeShopCat 			= get_field('shop_enable_cat', 'options');
	$activeShopCatSelect 	= get_field('shop_enable_cat_select', 'options');
	
	//---------------------------------
	// CATEGORY LISTING
	//---------------------------------
	get_template_part('parts/shared/sidebar', 'categories');
?>
	
	<div class="large-10 columns">
		<?php 
			// PRODUCT LOOP //
			if(have_posts()):
				while(have_posts()):
					the_post();
					global $_product;
					$_product = new jigoshop_product( $post->ID );

					// PRODUCT PURCHASE ACTIVATE
					//---------------------------------
					// Figure out Shop Activation
					$cartShop = false;
					$postCats = get_the_terms($post->ID, 'product_cat');
					$hasPrice = ($_product->get_price_html() == 'Call for Price') ? false : true;

					// If the shop is just active without category selection, just activate it!
					if($activeShop && !$activeShopCat && $hasPrice){
						$cartShop = true;
					}
					// If we have an active shop AND active category selection, we'll loop through and see if any of its categories activate it for sales
					elseif($activeShop && $activeShopCat && $hasPrice){
						foreach ($postCats as $id => $category) {
							if(in_array($id, $activeShopCatSelect)){
								$cartShop = true;
							}
						}
					}
					// Else, we'll just make it impossible to buy
					else {
						$cartShop = false;
					}

					//---------------------------------
					// PRODUCT
					//---------------------------------
					// Form
					$formDisplay 	= get_field('product_form_type');
					$formObj 		= ( ($formDisplay == 'default') || ($formDisplay == NULL) ) ? get_field('product_form', 'options') : get_field('product_form_choose');
					$formAlt 		= get_field('product_alt', 'options');
					// Description
					$showTabs 		= ( $_product->has_attributes() || $_product->has_dimensions() || $_product->has_weight() ) ? true : false;
					$showDesc 		= (get_the_content()) ? true : false;
		?>


					<?php
						// PRODUCT SUMMARY
						//---------------------------------
					?>
					<div class="product-summary">
						<div class="row">
							<div class="large-6 columns">
								<?php do_action('jigoshop_before_single_product_summary'); ?>
							</div>
							<div class="large-6 columns">
								<?php
									// Price
									jigoshop_template_single_price( $post, $_product );
									// Title
									echo '<h1>' . get_the_title() . '</h1>';
									// Meta (sku, categories, tags)
									jigoshop_template_single_meta( $post, $_product );
									// Social Share
									pantheon_display_social_share('product', 'post', $post);
									// Description
									if($showDesc):
										echo '<div class="description">';
										the_content();
										echo '</div>';
									endif;
									// Add to Cart
									if($cartShop){
										jigoshop_template_single_add_to_cart( $post, $_product );
									}
									
									//---------------------------------
									// INACTIVE: INTEREST FORM
									//---------------------------------
									if(($formDisplay != 'none') && (!$cartShop)): 
										echo '<div class="interest-form tertiary">';

										// Display Form
										if($formDisplay != 'none'){
											echo do_shortcode( $formObj );
										}

										if(($formDisplay != 'none') && $formAlt){
											echo '<p class="alt-break">or:</p>';
										}

										// Display Alt Contact Details
										if($formAlt){
											echo '<p class="alt">' . $formAlt . '</p>';
										}			
										echo '</div>';
									endif;
								?>
							</div>
						</div>
					</div>

					<?php
						// PRODUCT DETAILS
						//---------------------------------
						if($showTabs):
					?>
					<div class="product-details">
						<div class="row">
							<div class="large-12 columns">
								<?php jigoshop_output_product_data_tabs(); ?>
							</div>
						</div>
					</div>
					<?php endif; ?>

					<?php
						$crossIDS 	= get_post_meta($post->ID, 'crosssell_ids', true);
						$hasRelated = get_field('shop_related', 'options');
						$jigoshop_options 	= Jigoshop_Base::get_options();
						$catalogColumn		= $jigoshop_options->get_option('jigoshop_catalog_columns');

						if(!empty($crossIDS) && $hasRelated){
							// CROSS SELL
							//---------------------------------
							pantheon_display_post_shop_xsell($catalogColumn);								
						}
						elseif($hasRelated) {
							// RELATED PRODUCTS
							//---------------------------------
							jigoshop_related_products($catalogColumn);
						}
					?>

		<?php 
				endwhile; 
			endif;
		?>
	</div>

<?php
	// JIGOSHOP: AFTER CONTENT //
	do_action('jigoshop_after_main_content');

	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>
