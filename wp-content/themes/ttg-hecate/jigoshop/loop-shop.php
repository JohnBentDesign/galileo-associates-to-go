<?php
/**
 * Loop shop template
 *
 * DISCLAIMER
 *
 * Do not edit or add directly to this file if you wish to upgrade Jigoshop to newer
 * versions in the future. If you wish to customise Jigoshop core for your needs,
 * please use our GitHub repository to publish essential changes for consideration.
 *
 * @package             Jigoshop
 * @category            Catalog
 * @author              Jigoshop
 * @copyright           Copyright © 2011-2014 Jigoshop.
 * @license             GNU General Public License v3
 */

	// VARIABLES //
	global $columns, $per_page, $isSingle;
	$activeShop 			= get_field('shop_enable', 'options');
	$activeShopCat 			= get_field('shop_enable_cat', 'options');
	$activeShopCatSelect 	= get_field('shop_enable_cat_select', 'options');

	// PULL INFO FROM JIGOSHOP //
	$jigoshop_options 	= Jigoshop_Base::get_options();
	$catalogColumn		= $jigoshop_options->get_option('jigoshop_catalog_columns');
	$catalogCount 		= $jigoshop_options->get_option('jigoshop_catalog_per_page');

	// SHARE LINK //
	$catalogURL			= @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
	$catalogURL			.= ( $_SERVER["SERVER_PORT"] !== 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
	$catalogURL			.= $_SERVER["REQUEST_URI"];

	// JIGOSHOP: BEFORE LOOP ACTION //
	do_action('jigoshop_before_shop_loop');

	// LOOP VARIABLES //
	$loop = 0;
	if(!isset($columns) || !$columns){
		switch (apply_filters( 'loop_shop_columns', 3 )) {
			case 1: $columnSize = 12; break;
			case 2: $columnSize = 6; break;
			case 3: $columnSize = 4; break;
			case 4: $columnSize = 3; break;
			case 5: $columnSize = 3; break;
			case 6: $columnSize = 2; break;
			case 7: $columnSize = 2; break;
			case 8: $columnSize = 2; break;
			case 9: $columnSize = 2; break;
			case 10: $columnSize = 2; break;
			default: $columnSize = 4; break;
		}
	}
	else {
		$columns;
	}
	$columns = (!isset($columns) || !$columns) ? apply_filters( 'loop_shop_columns', 3 ) : $columns;

	// SINGLE PAGE //
	if($isSingle){
		$columnSize = 3;
	}

	//only start output buffering if there are products to list
	if(have_posts()){
		ob_start();
	}

	// PRODUCT LOOP //
	if(have_posts()):
		while(have_posts()):
			the_post();
			$_product = new jigoshop_product( $post->ID );
			$loop++;

			// PRODUCT PURCHASE ACTIVATE
			//---------------------------------
			// Figure out 
			$cartShop = false;
			$postCats = get_the_terms($post->ID, 'product_cat');

			// If the shop is just active without category selection, just activate it!
			if($activeShop && !$activeShopCat){
				$cartShop = true;
			}
			// If we have an active shop AND active category selection, we'll loop through and see if any of its categories activate it for sales
			elseif($activeShop && $activeShopCat){
				foreach ($postCats as $id => $category) {
					if(in_array($id, $activeShopCatSelect)){
						$cartShop = true;
					}
				}
			}
			// Else, we'll just make it impossible to buy
			else {
				$cartShop = false;
			}

			//---------------------------------
			// PRODUCT
			//---------------------------------
			// Display
			$displayType 	= get_field('product_display', 'options');
			// Slug
			$slug 			= $post->post_name;
			// Product
			$linkAttr 		= 'href="' . get_permalink($post->ID) . '"';
			// Get Fields
			$fields 		= get_post_custom();
			// Image
			$imageThumb 	= pantheon_display_post_featured_image($post->ID, 'TTG Medium Thumbnail', false, 'image', false); 
			$imageFull 		= pantheon_display_post_featured_image($post->ID, 'large', false, 'image', false); 
			// Form
			$formDisplay 	= get_field('product_form_type');
			$formObj 		= ( ($formDisplay == 'default') || ($formDisplay == NULL) ) ? get_field('product_form', 'options') : get_field('product_form_choose');
			$formAlt 		= get_field('product_alt', 'options');
?>
			<li class="product columns small-12 medium-6 large-<?= $columnSize; ?><?php if ($loop%$columns==0) echo ' last'; if (($loop-1)%$columns==0) echo ' first'; ?>">

				<?php 
					// PRODUCT THUMB
					//---------------------------------
				?>
				<div class="feature product-thumb">
					<span class="overlay overlay-tertiary">
						<span class="table">
							<span class="table-row">
								<span class="table-cell">
									<span class="details">
										<?php 
											// Details //
											echo '<h4>' . get_the_title() . '</h4>';
											if(!$isSingle):
												echo '<span class="price-point">Price: ';
												do_action('jigoshop_after_shop_loop_item_title', $post, $_product);
												echo '</span>';
											endif;
										?>
									</span>
									<span class="buttons">
										<?php // Links // ?>
										<a <?= $linkAttr; ?> class="button view-details">View Details</a>
										<?php 											
											if($cartShop && !$isSingle){
												echo '<span class="add-cart">';
												do_action('jigoshop_after_shop_loop_item', $post, $_product);
												echo '</span>';
											}
										?>
									</span>
								</span>
							</span>
						</span>
					</span>
					<?php if($imageThumb) { echo $imageThumb; } ?>
				</div>
		
			</li>			
<?php
			if($loop == $per_page) break;

		endwhile;
	endif;

	//---------------------------------
	// DISPLAY PRODUCTS
	//---------------------------------
	if($loop == 0):
		$content = '<p class="info">'.__('No products found which match your selection.', 'jigoshop').'</p>';
	else:
		$found_posts = ob_get_clean();
		$content = '<ul class="product-list row">' . $found_posts . '</ul>';
	endif;

	echo apply_filters( 'jigoshop_loop_shop_content', $content );
	do_action( 'jigoshop_after_shop_loop' );

?>
</div>
