<?php
	/**
	* Archive template
	*
	* DISCLAIMER
	*
	* Do not edit or add directly to this file if you wish to upgrade Jigoshop to newer
	* versions in the future. If you wish to customise Jigoshop core for your needs,
	* please use our GitHub repository to publish essential changes for consideration.
	*
	* @package             Jigoshop
	* @category            Catalog
	* @author              Jigoshop
	* @copyright           Copyright © 2011-2014 Jigoshop.
	* @license             GNU General Public License v3
	*/

	// VARIABLES //
	$shop_page_id 	= jigoshop_get_page_id('shop');
	$shop_page 		= get_post($shop_page_id);

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>

<?php
	// JIGOSHOP: BEFORE CONTENT //
	// do_action('jigoshop_before_main_content');
?>

	<?php
		//---------------------------------
		// CATEGORY LISTING
		//---------------------------------
		get_template_part('parts/shared/sidebar', 'categories');
	?>

	<div class="large-10 columns">

		<?php
			//---------------------------------
			// PRODUCT LISTING
			//---------------------------------
			ob_start();
			jigoshop_get_template_part( 'loop', 'shop' );
			$products_list_html = ob_get_clean();
			echo apply_filters( 'jigoshop_products_list', $products_list_html );
		?>

	<div class="product-nav bottom columns clearfix">
		<?php get_template_part('parts/shared/pagination'); ?>
	</div>

	</div>

<?php
	// JIGOSHOP: AFTER CONTENT //
	do_action('jigoshop_after_main_content');

	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>
