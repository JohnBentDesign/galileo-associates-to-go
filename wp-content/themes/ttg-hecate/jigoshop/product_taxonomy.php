<?php
	/**
	 * Product taxonomy template
	 *
	 * DISCLAIMER
	 *
	 * Do not edit or add directly to this file if you wish to upgrade Jigoshop to newer
	 * versions in the future. If you wish to customise Jigoshop core for your needs,
	 * please use our GitHub repository to publish essential changes for consideration.
	 *
	 * @package             Jigoshop
	 * @category            Catalog
	 * @author              Jigoshop
	 * @copyright           Copyright © 2011-2014 Jigoshop.
	 * @license             GNU General Public License v3
	 */

	// VARIABLES //
	$term 	= get_term_by( 'slug', get_query_var($wp_query->query_vars['taxonomy']), $wp_query->query_vars['taxonomy']);

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>

<?php
	// JIGOSHOP: BEFORE CONTENT //
	// do_action('jigoshop_before_main_content');
?>

	<?php
		//---------------------------------
		// CATEGORY LISTING
		//---------------------------------
		get_template_part('parts/shared/sidebar', 'categories');
	?>

	<div class="large-10 columns">

		<?php
			//---------------------------------
			// INTRODUCTION
			//---------------------------------
			$termID 		= get_queried_object()->term_id;
			$termTitle		= get_queried_object()->name;
			$termImgArray 	= jigoshop_product_cat_image($term->term_id);
			$termImage 		= (!strpos($termImgArray['image'], 'placeholder.png')) ? wp_get_attachment_image($termImgArray['thumb_id'], 'TTG Homepage Featured - 1 Column') : '';
			$termDesc 	= term_description($termID, 'product_cat');

			if($termImage || $termDesc):
		?>
				<div class="row">
					<div class="large-12 columns">
						<div class="intro tertiary">
							<?= $termImage; ?>
							<div class="details">
								<h1><?= $termTitle; ?></h1>
								<?= wpautop(wp_trim_words($termDesc, 100, '...')); ?>
							</div>
						</div>
					</div>
				</div>
		<?php endif; ?>

		<?php
			//---------------------------------
			// PRODUCT LISTING
			//---------------------------------
			ob_start();
			jigoshop_get_template_part( 'loop', 'shop' );
			$products_list_html = ob_get_clean();
			echo apply_filters( 'jigoshop_products_list', $products_list_html );
		?>

		<div class="product-nav bottom columns clearfix">
			<?php get_template_part('parts/shared/pagination'); ?>
		</div>

	</div>


<?php
	// JIGOSHOP: AFTER CONTENT //
	do_action('jigoshop_after_main_content');

	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>