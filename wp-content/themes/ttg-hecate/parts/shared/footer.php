<?php
	//-------------------------
	// ADDITIONAL FOOTER
	//-------------------------
	$additionalFooter = get_field('footer_content', 'options');

	if($additionalFooter):
?>
	<div class="footer-add primary">
		<div class="row">
			<div class="large-12 columns">
				<?= $additionalFooter; ?>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php
	//-------------------------
	// FOOTER MAIN
	//-------------------------
?>
	<footer class="main tertiary">
		<div class="row">
			
			<div class="medium-9 columns">
				<?php 
					//-------------------------
					// FOOTER NAVIGATION
					//-------------------------
					$menuMain = array(
						'theme_location' 	=> 'menu-footer',
						'menu'				=> 'Footer Menu',
						'container'			=> 'nav',
						'container_class' 	=> 'footer',
						'menu_class'		=> 'menu',
						'depth'				=> 3,
						'fallback_cb'		=> false
					);
					wp_nav_menu($menuMain);

					//-------------------------
					// SOCIAL CONNECT
					//-------------------------
					pantheon_display_social_connect('footer');
				?>
			</div>

			<div class="medium-3 columns">
				<?php
					//-------------------------
					// SMALL PRINT
					//-------------------------
				?>
				<div class="small-print">
					<p class="copyright">&copy; <?= date('Y') . ' ' . get_bloginfo('name'); ?>. All rights reserved.</p>
					<p><a href="http://technologytherapy.com" target="_blank">Powered by Technology Therapy</a></p>
				</div>
			</div>

		</div>
	</footer>