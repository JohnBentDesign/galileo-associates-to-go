<?php 
	/*===========================================================================
	SIDEBAR: CATEGORIES
	=============================================================================
	Displays all the categories under the gallery custom post type and displays
	them as a navigation on the left hand side of the site
	*/

	// Get the post type of the current post
	$postType 	= $post->post_type;
	$postTax 	= ($postType == 'gallery') ? 'gallery-categories' : 'product_cat';

	// Check if it's a sticky nav
	$sticky 	= (!get_field('shop_sticky', 'options')) ? ' sticky' : '';

	echo '<aside class="categories-nav large-2 columns">';

	$args = array(
		'show_option_all'		=> false,
		'orderby'				=> 'slug',
		'order'					=> 'ASC',
		'style'					=> 'list',
		'show_count'			=> 0,
		'hide_empty'			=> 1,
		'use_desc_for_title'	=> 0,
		'hierarchical'			=> 1,
		'number'				=> null,
		'echo'					=> 1,
		'depth'					=> 0,
		'title_li'				=> '',
		'taxonomy'				=> $postTax,
		'walker'				=> null
	);
	
	echo '<ul class="cat-nav' . $sticky . '">';
	wp_list_categories($args);
	echo '</ul>';
	
	echo '</aside>';
?>