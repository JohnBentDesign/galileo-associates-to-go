<?php
	/*===========================================================================
	SIDEBAR: MAIN
	=============================================================================
	Right hand sidebar that appears on most pages.
	*/
?>
<aside class="sidebar large-3 columns">
	<?php
		//---------------------------------
		// SHOP
		//---------------------------------
		if(in_array('jigoshop', get_body_class())) {
			if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Shop Sidebar')){}
		}
		//---------------------------------
		// PAGE
		//---------------------------------
		elseif(is_page()){
			if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('General Sidebar')){}
		}
		//---------------------------------
		// POST
		//---------------------------------
		else {
			if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar')){}
		}
	?>
</aside>