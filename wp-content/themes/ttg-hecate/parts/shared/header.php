<?php
	/*===========================================================================
	HEADER
	===========================================================================*/
	$headerComponents 	= get_field('header_components', 'options');
	$headerDivider 		= get_field('header_divider', 'options');
	$headerSearch 		= get_field('header_search', 'options');
?>

<header class="main tertiary">
	<div class="row">

		<?php
			//-------------------------
			// LOGO
			//-------------------------
		?>
		<div class="logo large-3 columns">
			<a href="<?php echo get_bloginfo ( 'url' ); ?>">
				<?php 
					if(get_field('header_logo', 'options')){
						pantheon_display_post_field_image(get_field('header_logo', 'options'), 'full', 'image');
					}
					else {
						bloginfo('title');
					}
				?>
			</a>
		</div>

		<div class="large-9 columns">

			<?php
				//-------------------------
				// UPPER HEADER
				//-------------------------
				if(!empty($headerComponents)):
			?>
					<div class="row">
						<div class="upper large-12 columns <?= $headerDivider; ?>">

							<?php
								// Tagline
								//-------------------------
								if(in_array('tagline', $headerComponents)):
									echo '<div class="line-up"><p class="tagline">' . get_field('header_tagline', 'options') . '</p></div>';
								endif;								
							?>

							<?php
								// Social Icons
								//-------------------------
								if(in_array('social', $headerComponents)):
							?>
									<div class="line-up">
										<?php pantheon_display_social_connect('header'); ?>
									</div>
							<?php endif; ?>

							<?php 
								// Secondary Navigation
								//-------------------------
								if(in_array('nav', $headerComponents)):
									$menuSec = array(
										'theme_location' 	=> 'menu-sec',
										'menu'				=> 'Secondary Menu',
										'container'			=> 'nav',
										'container_class' 	=> 'secondary line-up',
										'menu_class'		=> 'menu',
										'depth'				=> 1,
										'fallback_cb'		=> false
									);
									wp_nav_menu($menuSec);
								endif;

								// Shop Navigation
								//-------------------------
								if(in_array('nav-shop', $headerComponents) && get_field('shop_enable', 'options')):
									$menuShop = array(
										'theme_location' 	=> 'menu-shop',
										'menu'				=> 'Shop Menu',
										'container'			=> 'nav',
										'container_class' 	=> 'shop line-up',
										'menu_class'		=> 'menu',
										'depth'				=> 1,
										'fallback_cb'		=> false
									);
									wp_nav_menu($menuShop);
								endif;
							?>

							<?php 
								// Search Bar
								//-------------------------
								if(in_array('search', $headerComponents)):
							?>
									<div class="line-up <?= $headerSearch; ?>">
										<?php get_template_part('searchform'); ?>
									</div>
							<?php endif; ?>
						</div>
					</div>
			<?php endif; ?>

			<?php 
				//-------------------------
				// MAIN NAVIGATION
				//-------------------------
				$menuMain = array(
					'theme_location' 	=> 'menu-main',
					'menu'				=> 'Main Menu',
					'container'			=> 'nav',
					'container_class' 	=> 'main',
					'menu_class'		=> 'menu',
					'depth'				=> 3,
					'fallback_cb'		=> false
				);
				wp_nav_menu($menuMain);
			?>

		</div>
	
	</div>
</header>

<?php 
	// BREADCRUMBS //
	if( (($post->post_parent) || is_archive() || is_single()) && function_exists('yoast_breadcrumb')){
		yoast_breadcrumb('<div class="breadcrumb"><div class="row"><div class="large-12 columns">','</div></div></div>');
	}
?>


