<!doctype html>
<html>
	<head>
		<?php // META TAGS // ?>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<?php // BLOG TITLE // ?>
		<title><?php wp_title(''); ?></title>

		<?php // FAVICON // ?>
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
	
		<?php 
			// WORDPRESS HEADER //
			wp_head();
		?>
	</head>

	<body <?php body_class('tertiary'); ?>>
		<?php
			// HEADER // 
			get_template_part('parts/shared/header');
		?>
		
		<?php
			// Opening tags for wrapper
			if(!is_front_page()):
		?>
			<div class="container primary adjusted">
				<div class="row">
		<?php endif; ?>