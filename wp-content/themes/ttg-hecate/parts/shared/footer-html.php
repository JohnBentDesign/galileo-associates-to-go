		<?php
			// Closing tags for wrapper
			if(!is_front_page()):
		?>
				</div><?php // /.row ?>
			</div><?php // /.container ?>
		<?php endif; ?>
		
		<?php 
			// FOOTER //
			get_template_part('parts/shared/footer');
		
			// WORDPRESS FOOTER //
			wp_footer();

			// CUSTOM JAVASCRIPT //
			$customScript = get_field('advanced_js', 'options');
			if($customScript){
				echo $customScript;
			}
		?>

	</body>
</html>