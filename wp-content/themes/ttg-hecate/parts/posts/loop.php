<?php
	/*===========================================================================
	POST LOOP
	=============================================================================
	Blog post loop for single.php and index.php
	*/
	
	// Figure out where we are
	$listing = (is_home() || is_archive() || is_search()) ? true : false;
	if($listing){ $postOutput = ' excerpt'; }
	else if($listing == false){ $postOutput = ' full'; }

	// THE LOOP
	//===================================
	if(have_posts()){

		// If it's the blog listing page, wrap our articles in a div
		if($listing){ echo '<div class="post-list">'; }

		// Start the loop
		while(have_posts()): the_post();
			// Get Our Variables
			$postDate 	= '<time datetime="' . get_the_date('Y-m-d') . '"><span class="date">' . get_the_date('j') . '</span><span class="month">' . get_the_date('M') . '</span><span class="year">\'' . get_the_date('y') . '</span></time>';
			$title 		= ($listing) ? '<h2><a href="' . get_permalink() . '">' . get_the_title() . '</a></h2>' : '<h1>' . get_the_title() . '</h1>';
			$link 		= ($listing) ? get_permalink() : false;
			$featured 	= (has_post_thumbnail()) ? '<div class="img-zoom">' . pantheon_display_post_featured_image($post->ID, 'TTG Featured Image', $link, 'image', false) . '</div>' : '';
?>
			<article class="post<?= $postOutput; ?>">
				<div class="the-post">

					<?php 
						//---------------------------------
						// POST HEADER
						//---------------------------------
					?>
					<header>
						<?php
							if($listing){
								echo $postDate;
							}
							else {
								echo $featured;
							}

							// Title of Post
							echo $title;

							// Categorizing
							if(has_category() || has_tag()){
								$categorizingSize = (has_category() && has_tag()) ? ' medium-6' : '';

								echo '<div class="categorizing"><div class="row">';

								// Post Categories
								if(has_category()){
									echo '<div class="columns' . $categorizingSize . '">';
									pantheon_display_post_categories($post->ID);
									echo '</div>';
								}

								// Post Tags
								if(has_tag()){
									echo '<div class="columns' . $categorizingSize . '">';
									pantheon_display_post_tags($post->ID);
									echo '</div>';
								}

								echo '</div></div>';

							}

							if($listing){
								echo $featured;
							}
						?>
					</header>

					<?php 
						//---------------------------------
						// POST CONTENT
						//---------------------------------
					?>
					<div class="content">
						<?php
							if($listing){
								the_excerpt();
							}
							else {
								echo $postDate;
								the_content();
							}
						?>
					</div>

					<?php 
						//---------------------------------
						// POST FOOTER
						//---------------------------------
						if(!$listing):
					?>
							<footer class="post">
									<?php
										// SHARE BUTTONS //
										pantheon_display_social_share('blog', 'post', $post);
									?>
							</footer>
					<?php endif; ?>
					
				</div>
			</article><?php // /.post ?>	

			<?php
				//---------------------------------
				// COMMENTS - LIST
				//---------------------------------
				$commentCount = get_comments_number();

				if(!$listing && ($commentCount > 0)):
			?>
					<div class="comment-intro">
						<h3>Comments</h3>
						<h4><?= $commentCount; ?> Response<?php if($commentCount > 1){ echo 's'; } ?> to <?= $post->post_title; ?></h4>
					</div>

					<ol class="comment-list">
						<?php
							//Gather comments for a specific page/post 
							$comments = get_comments(array(
								'post_id'	=> $post->ID,
								'status'	=> 'approve' //Change this to the type of comments to be displayed
							));

							//Display the list of comments
							wp_list_comments(array(
								'per_page' 			=> 10,
								'reverse_top_level' => false,
								'max_depth' 		=> 2
							), $comments);
						?>
					</ol>
			<?php endif; ?>

			<?php
				//---------------------------------
				// COMMENTS - FORM
				//---------------------------------
				if(!$listing):
			?>
					<div class="comments">
						<?php comment_form(); ?>
					</div>
			<?php endif; ?>

<?php			
		endwhile;

		// Close the blog listing div
		if($listing){ echo '</div>'; }
	}
	else{
		echo '<p>Sorry, but nothing matched your search criteria. Please try again with different keywords.</p>';
		get_template_part('searchform');
	}
?>