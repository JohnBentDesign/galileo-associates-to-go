<?php
	/*===========================================================================
	GALLERY LOOP
	=============================================================================
	Gallery post loop for index.php
	*/

	// Archive or Single? (I'll take the cake, please)
	//-----------------------------------------------	
	$listing 	= (is_archive() || is_search()) ? true : false;
	$postType 	= get_post_type();
	$postTax 	= ($postType == 'gallery') ? 'gallery-categories' : 'product_cat';


	//----------------------------------
	// CATEGORY SIDEBAR
	//----------------------------------
	get_template_part('parts/shared/sidebar', 'categories');


	//=========================================================
	// LISTINGS LOOP
	//=========================================================
	if($listing):
		$galleryArgs = array( 
			'post_type' 		=> 'gallery',
			'posts_per_page' 	=> 9,
			'order' 			=> 'DESC',
			'orderby' 			=> 'date',
			'paged' 			=> get_query_var('paged'),
		);
		$galleryQuery = new WP_Query($galleryArgs);

		if($galleryQuery->have_posts()):
?>

			<?php
				// Listing Container
				//-----------------------------------------------
				// Get some of the term's information
				$termID = $termTitle = $termImage = $termDesc = '';

				if(!is_post_type_archive('gallery')){
					$termID 	= get_queried_object()->term_id;
					$termTitle	= get_queried_object()->name;
					$termImage 	= pantheon_display_post_field_image(get_field('gallery_cat_image', 'gallery-categories_' . $termID), 'TTG Homepage Featured - 1 Column', 'image', false);
					$termDesc 	= term_description($termID, 'gallery-categories');
				}
			?>
				<div class="gallery-list large-10 columns">

					<?php
						// Intro Box
						//-----------------
						if($termImage || $termDesc):
					?>
							<div class="intro tertiary">
								<?= $termImage; ?>
								<div class="details">
									<h1><?= $termTitle; ?></h1>
									<?= wpautop(wp_trim_words($termDesc, 100, '...')); ?>
								</div>
							</div>
					<?php endif; ?>

					<div class="row">
						<?php
							// Start the Loop!
							//-----------------------------------------------
							while($galleryQuery->have_posts()): $galleryQuery->the_post();
								// Variables
								//-----------------
								$title 		= '<h2>' . get_the_title() . '</h2>';
								$content 	= wpautop(wp_trim_words(get_the_excerpt(), 20, '...'));
						?>
							<article class="gallery excerpt medium-6 large-4 columns">

								<?php
									// LISTING
									//===================================================
									if($listing):
								?>							
										<a href="<?= get_permalink(); ?>" class="feature">
											<span class="overlay overlay-tertiary">
												<span class="table">
													<span class="table-row">
														<span class="table-cell">
															<span class="details">
																<?= $title . $content; ?>
																<span class="button primary">View</span>
															</span>
														</span>
													</span>
												</span>
											</span>
											<?= pantheon_display_post_featured_image($post->ID, 'TTG Medium Thumbnail', false); ?>
										</a>
								<?php endif; ?>

							</article>
						<?php endwhile; wp_reset_postdata(); ?>
					</div>
				</div>

<?php
		endif;
	endif;

	//=========================================================
	// SINGLE LOOP
	//=========================================================
	if(!$listing && have_posts()):
		while(have_posts()): the_post();

			// Variables
			//-----------------
			$title 		= '<h1>' . get_the_title() . '</h1>';
			$content 	= wpautop(get_the_content());
?>
			<article class="gallery full large-10 columns">
				<div class="row">
					<div class="large-8 columns">
						<?php pantheon_display_gallery(); ?>
					</div>
					<div class="content large-4 columns">
						<?php 
							// Variables
							//-----------------
							echo $title;
							the_content();

							// Categories
							//-----------------
							pantheon_display_post_categories($post->ID);

							// Social Share
							//-----------------
							pantheon_display_social_share('gallery', 'post', $post);
						?>
					</div>
				</div>
			</article>
<?php
		endwhile; 	
	endif;
?>