<?php
	/*===========================================================================
	INCLUDE THE INCLUDES
	===========================================================================*/
	// Advanced Custom Fields
	include(get_template_directory() . '/includes/advanced-custom-fields/theme-styling.php');
	include(get_template_directory() . '/includes/advanced-custom-fields/page-home.php');
	include(get_template_directory() . '/includes/advanced-custom-fields/gallery-cat.php');



	/*===========================================================================
	ADD THEME SUPPORTS
	===========================================================================*/
	add_theme_support('post-thumbnails');
	add_theme_support('menus');



	/*===========================================================================
	CUSTOM THUMBNAIL SIZES
	===========================================================================*/
	add_image_size('TTG Logo', 480, 200, false);
	add_image_size('TTG Featured Image', 1024, 500, true);
	add_image_size('TTG Gallery Normal Display', 637, 9999, false);
	add_image_size('TTG Medium Thumbnail', 350, 350, true);
	add_image_size('TTG Homepage Featured - 2 Column', 500, 350, true);
	add_image_size('TTG Homepage Featured - 1 Column', 1000, 350, true);



	/*===========================================================================
	REGISTER MENUS
	===========================================================================*/
	if(!function_exists('ttg_menus')){
		function ttg_menus() {
			register_nav_menus(array(
				'menu-main' 		=> 'Main Menu',
				'menu-sec' 			=> 'Secondary Menu',
				'menu-shop' 		=> 'Shop Menu',
				'menu-footer'		=> 'Footer Menu'
			));
		}
		add_action('init', 'ttg_menus');
	}



	/*===========================================================================
	REGISTER SIDEBARS
	===========================================================================*/
	$sidebarBlog = array(
		'name'          => 'Blog Sidebar',
		'id'            => 'sidebar-blog',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>'
	);
	register_sidebar($sidebarBlog);

	$sidebarGeneral = array(
		'name'          => 'General Sidebar',
		'id'            => 'sidebar-general',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>'
	);
	register_sidebar($sidebarGeneral);
	
	// Shop Sidebar //
	if(class_exists('jigoshop')){
		$sidebarShop = array(
			'name'          => 'Shop Sidebar',
			'id'            => 'sidebar-shop',
			'description'   => '',
			'class'         => '',
			'before_widget' => '<div class="widget">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widgettitle">',
			'after_title'   => '</h4>'
		);
		register_sidebar($sidebarShop);
	}



	/*===========================================================================
	ENQUEUE SCRIPTS AND STYLES
	===========================================================================*/
	function ttg_scripts(){
		if(!is_admin()){
			// Plugins
			wp_enqueue_script('customSelect', get_template_directory_uri() . '/javascripts/plugins/customSelect.min.js', array('jquery'), '', true);
			wp_enqueue_script('slick', get_template_directory_uri() . '/javascripts/plugins/slick.min.js', array('jquery'), '', true);
			wp_enqueue_script('tinynav', get_template_directory_uri() . '/javascripts/plugins/tinynav.min.js', array('jquery'), '', true);
			// Hecate Scripts
			wp_enqueue_script('hecate', get_template_directory_uri() . '/javascripts/hecate.js', array('jquery', 'foundation', 'customSelect', 'slick', 'tinynav'), '', true);
		}
	}
	add_action('wp_enqueue_scripts', 'ttg_scripts');


	// STYLESHEETS
	//---------------------------------------------------------------------------
	function ttg_styles(){
		if(!is_admin()){
			// Register Stylesheets
			wp_enqueue_style('hecate', get_template_directory_uri() . '/stylesheets/css/style.css');
		}
	}
	add_action('wp_enqueue_scripts', 'ttg_styles');

	// RECOMPILES THE CSS ONLY WHEN THE OPTIONS PAGE IS SAVED
	//---------------------------------------------------------------------------
	function ttg_compile_css( $post_id ) {
		$page = htmlspecialchars($_GET["page"]);
		if($page == 'acf-options-theme-styling'){
			// Compile our CSS
			include(get_template_directory() . '/stylesheets/php-sass-watcher.php');
		}
	}
	// run after ACF saves the $_POST['fields'] data
	add_action('acf/save_post', 'ttg_compile_css', 20);


