<?php
	/*===========================================================================
	Template Name: Homepage
	=============================================================================
	Page template for Homepage. Same as page.php, just makes it easier to determine
	when to show slide custom fields on editor page.
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');

	// Variables
	$displaySections 	= get_field('home_sections');
	$isArray 			= is_array($displaySections);
?>

	<div class="main container primary adjusted">


		<?php
			//-------------------------
			// SLIDESHOW
			//-------------------------
			if($isArray && in_array('slideshow', $displaySections)):
		?>
			<div class="slideshow section">
				<div class="row">
					<div class="large-12 columns">
						
						<div class="home-slider">
							<?php
								// Loop through slides
								while(have_rows('home_slider')): the_row();
			 
			 						$imageLG	= pantheon_display_post_field_image(get_sub_field('image'), 'TTG Featured Image', 'url', false);
			 						$imageSM	= pantheon_display_post_field_image(get_sub_field('image'), 'TTG Medium Thumbnail', 'url', false);
			 						$title 		= (get_sub_field('title')) ? '<h2>' . get_sub_field('title') . '</h2>' : '';
			 						$content 	= get_sub_field('content');
			 						$link 		= get_sub_field('link');
			 						$linkOutput = ($link) ? '<a href="' . get_permalink($link[0]) . '">' . get_sub_field('link_text') . '</a>' : '';
			 				?>
									<div class="slide">
										<img class="slide-img" data-interchange="[<?= $imageSM; ?>, (only screen and (min-width: 1px))], [<?= $imageLG; ?>, (only screen and (min-width: 641px))]">
										<?php if($title || $content || $linkOutput): ?>
											<div class="slide-caption">
												<?= $title . $content . $linkOutput; ?>
											</div>
										<?php endif; ?>
									</div>
							<?php endwhile; ?>
						</div>

					</div>
				</div>
			</div>
		<?php endif; ?>


		<?php
			//-------------------------
			// MAIN CONTENT
			//-------------------------
			$hasThumb 			= has_post_thumbnail($post->ID);
			$columnSize 		= ($hasThumb) ? 'medium-8 large-9' : 'large-12';
			$columnSize 		= ($hasThumb && (get_field('home_main_side') == 'right')) ? $columnSize . ' large-pull-3' : $columnSize;
			$columnSizeImage 	= (get_field('home_main_side') == 'right') ? ' large-push-9' : '';
		?>
		<div class="main-content section">
			<main class="row">

				<div class="large-12 columns">
					<span class="rule"></span>
				</div>
			
				<?php 
					// Featured Image //
					if($hasThumb):
						echo '<div class="medium-4 large-3' . $columnSizeImage . ' columns">';
						pantheon_display_post_featured_image($post->ID, 'TTG Homepage Featured - 4/3 Column', false);
						echo '</div>';
					endif;
				?>

				<div class="<?= $columnSize; ?> columns">
					<h1><?php the_title(); ?></h1>
					<?php echo wpautop($post->post_content); ?>
					<?php pantheon_display_post_cta(); ?>
				</div>

			</main>
		</div>


		<?php
			//-------------------------
			// FEATURED POSTS
			//-------------------------
			$featuredPosts 		= get_field('home_featured');
			$featuredCount 		= count($featuredPosts);
			$featuredOverlay	= get_field('home_featured_overlay');
			$featuredBorder 	= (get_field('home_featured_border')) ? get_field('home_featured_border_color') : '';

			// Get some styling info based on how many features we have
			switch ($featuredPosts) {
				case ($featuredCount == 1): 		$thumbSize = 'TTG Homepage Featured - 1 Column'; $columnSize = 'large-12'; break;
				case ($featuredCount % 3 == 0): 	$thumbSize = 'TTG Medium Thumbnail'; $columnSize = 'medium-4'; break;
				case ($featuredCount % 4 == 0): 	$thumbSize = 'TTG Medium Thumbnail'; $columnSize = 'medium-6 large-3'; break;
				case ($featuredCount % 2 == 0): 	$thumbSize = 'TTG Homepage Featured - 2 Column'; $columnSize = 'medium-6'; break;
				case ($featuredCount % 2 == 1): 	$thumbSize = 'TTG Homepage Featured - 2 Column'; $columnSize = 'medium-6'; break;
			}

			// Display our features
			if($isArray && in_array('featured', $displaySections)):
		?>
				<div class="featured-posts section">
					<div class="row">

						<div class="large-12 columns">
							<span class="rule"></span>
						</div>

						<div class="features">
							<?php
								// Loop through features
								while(have_rows('home_featured')): the_row();
									// Get Featured Posts information to output
									$heading 		= (get_sub_field('heading')) ? '<span class="featured-title">' . get_sub_field('heading') . '</span>' : '';
									$content 		= (get_sub_field('content')) ? get_sub_field('content') : '';
									$details 		= ($heading || $content) ? '<span class="details">' . $heading . strip_tags($content, '<p><a>') . '</span>' : '';
									$image 			= pantheon_display_post_field_image(get_sub_field('image'), $thumbSize, 'image', false);
									$linkType 		= get_sub_field('link_type'); 
									switch ($linkType) {
										case 'page': 		
											$linkID = get_sub_field('link_post');
											$link 	= get_permalink($linkID[0]);  
											break;
										case 'gallery': 	$link = get_term_link(get_sub_field('link_cat_gallery'));  break;
										case 'product': 	$link = get_term_link(get_sub_field('link_cat_product'));  break;
										case 'post': 		$link = get_term_link(get_sub_field('link_cat_post'));  break;
									}
									$buttonText 	= get_sub_field('link_text');
									$buttonOutput 	= '<span class="featured button">' . $buttonText . '</span>';
							?>
									<div class="<?= $columnSize; ?> columns">
										<a href="<?= $link; ?>" class="feature <?= $featuredBorder; ?>">
											<span class="overlay <?= $featuredOverlay; ?>">
												<span class="table">
													<span class="table-row">
														<span class="table-cell">
															<?= $details . $buttonOutput; ?>
														</span>
													</span>
												</span>
											</span>
											<?= $image; ?>
										</a>
									</div>
							<?php endwhile; ?>
						</div>

					</div>
				</div>
		<?php endif; ?>


		<?php
			//-------------------------
			// CALL TO ACTION
			//-------------------------
			// Display our call outs
			if($isArray && (in_array('cta', $displaySections) || in_array('contact', $displaySections) || in_array('share', $displaySections)) ):
				// Call to Action Variables
				$ctaSide 		= get_field('home_cta_side');
				$ctaColumns 	= (in_array('contact', $displaySections) || in_array('share', $displaySections)) ? 'large-9' : 'large-12';
				$ctaPush 		= ($ctaSide == 'right') ? ' large-push-3' : '';
				$ctaHeading 	= (get_field('home_cta_heading')) ? '<h3>' . get_field('home_cta_heading') . '</h3>' : '';
				$ctaContent 	= get_field('home_cta_content');
				$ctaLinkType 	= get_field('home_cta_link_type'); 
				switch ($ctaLinkType) {
					case 'page': 		
						$ctaLinkID 	= get_field('home_cta_link_post');
						$ctaLink 	= get_permalink($ctaLinkID[0]);  
						break;
					case 'gallery': 	$ctaLink = get_term_link(get_field('home_cta_link_cat_gallery'));  break;
					case 'product': 	$ctaLink = get_term_link(get_field('home_cta_link_cat_product'));  break;
					case 'post': 		$ctaLink = get_term_link(get_field('home_cta_link_cat_post'));  break;
				}
				$ctaOutput 		= ($ctaLink) ? '<a href="' . $ctaLink . '" class="cta-go">' . get_field('home_cta_link_text') . '</a>' : ''; 
				$ctaImage 		= pantheon_display_post_field_image(get_field('home_cta_image'), 'TTG Medium Thumbnail', 'image', false);
				$ctaImageSide	= get_field('home_cta_image_side');
				$ctaContentSide = ($ctaImageSide == 'right') ? 'left' : 'right';
				$ctaDouble 		= ($ctaImage) ? ' has-image' : '';

				// Contact Variables
				$contactColumns = (in_array('cta', $displaySections)) ? 'large-3' : 'large-12';
				$contactPull 	= ($ctaSide == 'right') ? ' large-pull-9' : '';
				$contactLinkCO 	= (get_field('home_contact_co')) ? '<span class="link-co">' . get_field('home_contact_co') . '</span>' : '';
				$contactLinkFO 	= (get_field('home_contact_fo')) ? '<span class="link-fo">' . get_field('home_contact_fo') . '</span>' : '';
				$contactLinkID 	= get_field('home_contact_link');
				$contactLink 	= get_permalink($contactLinkID[0]);
				$contactIcon 	= (get_field('home_contact_icon')) ? '<span class="contact-icon ion-email"></span>' : '';
				$contactOutput 	= ($contactLink && in_array('contact', $displaySections)) ? '<a href="' . $contactLink . '" class="interested clearfix">' . $contactIcon . $contactLinkCO . $contactLinkFO . '</a>' : '';
				$contactConnect = (get_field('home_connect')) ? 'widget' : false;
		?>
				<div class="call-outs section">
					<div class="row">

						<div class="large-12 columns">
							<span class="rule"></span>
						</div>

						<div class="cta-contact">
							<?php if(in_array('cta', $displaySections)): ?>
								<div class="<?= $ctaColumns . $ctaPush; ?> columns">
									<div class="cta-box tertiary clearfix<?= $ctaDouble; ?>">
										<div class="call <?= $ctaContentSide; ?>">
											<?= $ctaHeading . $ctaContent . $ctaOutput; ?>
										</div>
										<?php if($ctaImage):?>
											<div class="image <?= $ctaImageSide; ?>">
												<?= $ctaImage; ?>
											</div>
										<?php endif; ?>
									</div>
								</div>
							<?php endif; ?>

							<?php if(in_array('contact', $displaySections) || in_array('share', $displaySections)): ?>
								<div class="<?= $contactColumns . $contactPull; ?> columns">
									<div class="contact">
										<?php
											echo $contactOutput;
											pantheon_display_social_connect($contactConnect); 
											pantheon_display_social_share(get_field('home_share'), 'post', $post);
										?>
									</div>
								</div>
							<?php endif; ?>
						</div>

					</div>
				</div>
		<?php endif; ?>

	</div>

<?php
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>