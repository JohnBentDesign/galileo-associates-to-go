<?php
	/*===========================================================================
	PAGE
	=============================================================================
	Used for all general page displays
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');

	$posts = get_field('page_sections');
	
	if($posts):
		foreach($posts as $post):
			setup_postdata($post);
    
    		get_template_part('parts/page/slides');

        endforeach;
    	wp_reset_postdata();
	endif;

	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>