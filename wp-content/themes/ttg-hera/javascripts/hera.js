jQuery(document).foundation();

jQuery(document).ready(function(){
	
	//============================
	// Smooth Scroll
	//============================
	smoothScroll.init();

	//============================
	// Slicknav
	//============================
	jQuery('nav.main ul.menu').slicknav({
		label: '',
		closeOnClick: true
	});
	jQuery('.slicknav_nav li').each(function(){
		jQuery(this).removeAttr('data-magellan-arrival');
	});

	//============================
	// Print
	//============================
 	jQuery('.print-me').click(function(){
 		jQuery('.print-me').parents('.popped').printThis({
			debug: true,
			importCSS: true,
			printContainer: false,
			pageTitle: '',
			removeInline: true,
			printDelay: 333,
			header: null,
			formValues: true
		});
 	});

});
