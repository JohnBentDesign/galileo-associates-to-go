<?php
/*===========================================================================
HEADER
===========================================================================*/
// Figure out how our header will be laid out by finding the position of the logo
$logoDisplay 		= get_field('header_position', 'options');
$connectLocation 	= get_field('social_button_location', 'options');

//Grab the content for the callout areas
$leftCalloutContent = get_field('left_callout_content', 'options');
$rightCalloutContent = get_field('right_callout_content', 'options');


// Get Logo and 1st Callout Positions
switch($logoDisplay){
    case 'above-center'	: $logoSize = 'logo-centered large-6 large-push-3';
                          $calloutPosition = 'large-3 large-pull-6'; break;

    case 'above-left' 	: $logoSize = 'logo-left large-6';
                          $calloutPosition = 'large-3'; break;
}

// Get Social Column Size
$socialSize = 'large-3';

// Magellan for Homepage
$magellan = (is_front_page()) ? 'data-magellan-expedition' : '';
?>

<header id="top" class="main primary header-<?= $logoDisplay; ?>" data-equalizer>
    <div class="row">

        <?php
        //-------------------------
        // LOGO
        //-------------------------
        ?>
        <div class="logo <?= $logoSize ?> columns" data-equalizer-watch>
            <h1>
                <a href="<?php bloginfo('home'); ?>">
                    <?php
                    if(get_field('header_logo', 'options')){
                        pantheon_display_post_field_image(get_field('header_logo', 'options'), 'large', 'image');
                    }
                    else {
                        bloginfo('title');
                    }
                    ?>
                </a>
            </h1>

            <?php
            //-------------------------
            // TAGLINE
            //-------------------------
            $tagline = get_field('header_tagline', 'options');
            if($tagline && ($logoDisplay != 'next')){
                echo '<p class="tagline">' . $tagline . '</p>';
            }
            ?>

        </div>

        <?php
        //-------------------------
        // Left Callout Area
        //-------------------------
        ?>
        <div class="callout <?= $calloutPosition ?> columns">
            <?= $leftCalloutContent ?>
        </div>

        <?php
        //-------------------------
        // Right Callout Area
        //-------------------------
        ?>
        <div class="callout large-3 columns">
            <?= $rightCalloutContent ?>
            <?php
            //-------------------------
            // SOCIAL
            //-------------------------
            if( ((gettype($connectLocation) == 'array') && in_array('header', $connectLocation)) || ($connectLocation == 'header') ):
                ?>
            <div class="social">
                <?php pantheon_display_social_connect('header'); ?>
            </div>
            <?php endif; ?>
        </div>

    <?php
    //-------------------------
    // NAVIGATION
    //-------------------------
    echo '<nav class="main small-12 columns" data-equalizer-watch ' . $magellan . '>';

    // Main Navigation
    //-------------------------
    $menuMain = array(
        'theme_location' 	=> 'menu-main',
        'menu'				=> 'Main Menu',
        'container'			=> false,
        'menu_class'		=> 'menu',
        'depth'				=> 0,
        'walker' 			=> new hera_menu_walker()
        );
    wp_nav_menu($menuMain);

    echo '</nav>';
    ?>

    <span class="band"></span>

</div>
</header>


