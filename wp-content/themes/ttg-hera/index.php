<?php
	/*===========================================================================
	PAGE
	=============================================================================
	Used for all general page displays
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');

	// THE LOOP
	if(have_posts()): 
		while(have_posts()): 
			the_post();

			get_template_part('parts/page/slides');

		endwhile;
	endif;

	// FORM //
	get_template_part('parts/shared/form');

	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>