<?php
	// Staff Function
	//---------------------------
	if(!function_exists('hera_display_post_staff')){
		function hera_display_post_staff($postObj, $popperBG){
			// Variables
			$staffNameRaw 	= get_the_title($postObj->ID);
			$staffSlug 		= sanitize_title($staffNameRaw);
			$staffName 		= '<h4>' . $staffNameRaw . '</h4>';
			$staffImage 	= pantheon_display_post_featured_image($postObj->ID, 'TTG Staff Image', false, 'image', false);
			$staffTitle 	= (get_field('staff_title')) ? '<em>' . get_field('staff_title') . '</em>' : '';
			$staffEmail 	= (get_field('staff_email')) ? '<a href="mailto:' . get_field('staff_email') . '" class="button" type="email">Contact</a>' : '';
			$staffNumber 	= (get_field('staff_number')) ? '<span><span class="icon ion-ios7-telephone"></span><a href="tel:' . get_field('staff_number') . '" type="work">' . get_field('staff_number') . '</a></span>' : '';
			$staffExtension = (get_field('staff_extension')) ? '<span><span class="icon ion-close-round"></span>' . get_field('staff_extension') . '</span>' : '';
			$staffCell 		= (get_field('staff_cell')) ? '<span><span class="icon ion-ios7-telephone-outline"></span><a href="tel:' . get_field('staff_cell') . '" class="cell">' . get_field('staff_cell') . '</a></span>' : '';
			$staffFacebook 	= (get_field('staff_facebook')) ? '<a href="' . get_field('staff_facebook') . '" class="symbol">&#xe027;</a>' : '';
			$staffTwitter 	= (get_field('staff_twitter')) ? '<a href="' . get_field('staff_twitter') . '" class="symbol">&#xe086;</a>' : '';
			$staffLinkedin 	= (get_field('staff_linkedin')) ? '<a href="' . get_field('staff_linkedin') . '" class="symbol">&#xe052;</a>' : '';
			$staffGoogle 	= (get_field('staff_google')) ? '<a href="' . get_field('staff_google') . '" class="symbol">&#xe039;</a>' : '';
?>
			<li class="staffer medium-4 large-2 columns">
				<a href="#" data-reveal-id="popped-<?= $staffSlug; ?>">
					<?= $staffImage; ?>
					<div class="staff-info">
						<?= $staffName; ?>
						<span class="opener ion-plus"></span>
					</div>
				</a>

				<div id="popped-<?= $staffSlug; ?>" class="staff-popped popped medium reveal-modal <?= $popperBG; ?>" data-reveal>
					<div class="row">
						<div class="staff-image medium-4 columns">
							<?= $staffImage; ?>
						</div>
						<div class="staff-content medium-8 columns">
							<header>
								<?= $staffName . $staffTitle; ?>
							</header>

							<?php the_content(); ?>

							<?php if($staffEmail || $staffNumber || $staffExtension || $staffCell || $staffFacebook || $staffTwitter || $staffLinkedin || $staffGoogle): ?>
								<div class="contact-details">
									<?php if($staffNumber || $staffExtension || $staffCell): ?>
										<div class="numbers">
											<?= $staffNumber . $staffExtension . $staffCell; ?>
										</div>
									<?php endif; ?>
									<?php if($staffEmail || $staffFacebook || $staffTwitter || $staffLinkedin || $staffGoogle): ?>
										<div class="contact">
											<?= $staffEmail . $staffFacebook . $staffTwitter . $staffLinkedin . $staffGoogle; ?>
										</div>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
					<a class="burst close-reveal-modal">&#215;</a>
				</div>
			</li>
<?php		
		}
	}