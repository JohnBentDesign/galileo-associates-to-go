<?php
	/*===========================================================================
	HERA: PAGE SECTIONS
	===========================================================================*/
	
	if( function_exists('register_field_group') ):

	register_field_group(array (
		'key' => 'group_542318fadf2dd',
		'title' => 'Section Select',
		'fields' => array (
			array (
				'key' => 'field_536cfbdf9cadf',
				'label' => 'Select Sections to Display',
				'name' => 'page_sections',
				'prefix' => '',
				'type' => 'relationship',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'object',
				'post_type' => array (
					0 => 'section',
				),
				'taxonomy' => array (
				),
				'filters' => array (
					0 => 'search',
					1 => 'post_type',
				),
				'max' => 12,
				'elements' => array (
					0 => 'featured_image',
					1 => 'post_type',
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-home.php',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => array (
			0 => 'the_content',
			1 => 'excerpt',
			2 => 'custom_fields',
			3 => 'discussion',
			4 => 'comments',
			5 => 'categories',
			6 => 'tags',
			7 => 'send-trackbacks',
		),
	));

	endif;










