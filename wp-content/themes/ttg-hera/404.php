<?php
	/*===========================================================================
	404
	=============================================================================
	Template automatically shown when a 404 error appears
	*/

	// Get 404 page content
	$fourohfourPage = get_field('404_content', 'options');

	$title = ($fourohfourPage[0]) ? get_the_title($fourohfourPage[0]) : 'This page does not exist';
	$content = ($fourohfourPage[0]) ? apply_filters('the_content', get_post_field('post_content', $fourohfourPage[0])) : 'Sorry. It seems we can\'t find what you were looking for.';

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>

	<article class="main row">
		<div class="large-8 large-centered columns">
			<?php pantheon_display_post_featured_image($fourohfourPage[0], 'TTG Featured Image', false); ?>
			<h1><?= $title; ?></h1>
			<?= $content; ?>
		</div>
	</article>

<?php 
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>