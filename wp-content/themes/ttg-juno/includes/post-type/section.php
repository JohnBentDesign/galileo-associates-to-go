<?php
	/*===========================================================================
	HERA: SECTION POST TYPE
	===========================================================================*/
	
	add_action( 'init', 'register_cpt_section' );

	function register_cpt_section() {

		$labels = array( 
			'name' => _x( 'Sections', 'section' ),
			'singular_name' => _x( 'Section', 'section' ),
			'add_new' => _x( 'Add New', 'section' ),
			'add_new_item' => _x( 'Add New Section', 'section' ),
			'edit_item' => _x( 'Edit Section', 'section' ),
			'new_item' => _x( 'New Section', 'section' ),
			'view_item' => _x( 'View Section', 'section' ),
			'search_items' => _x( 'Search Sections', 'section' ),
			'not_found' => _x( 'No sections found', 'section' ),
			'not_found_in_trash' => _x( 'No sections found in Trash', 'section' ),
			'parent_item_colon' => _x( 'Parent Section:', 'section' ),
			'menu_name' => _x( 'Sections', 'section' ),
		);

		$args = array( 
			'labels' => $labels,
			'hierarchical' => false,
			'supports' => array( 'title', 'revisions', 'editor', 'thumbnail' ),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 20,
			'show_in_nav_menus' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'has_archive' => false,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post'
		);

	    register_post_type('section', $args);
	}




