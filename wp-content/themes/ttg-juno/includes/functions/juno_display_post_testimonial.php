<?php
	// Testimonial Function
	//---------------------------
	if(!function_exists('juno_display_post_testimonial')){
		function juno_display_post_testimonial($display, $object, $columnSize = NULL){
			if($display){
				$testContent	= get_field('testimonial_content', $object[0]) ? '<div class="quote">' . get_field('testimonial_content', $object[0]) . '</div>' : '';
				$testAuthor		= get_field('testimonial_author', $object[0]);
				$testLocation	= get_field('testimonial_location', $object[0]);
?>
				<div class="testimonial-row row">
					<aside class="featured-testimonial <?= $columnSize; ?> columns">
						<blockquote>
							<?php
								echo $testContent;

								if($testAuthor || $testLocation){
									echo '<cite class="credit">';
									echo $testAuthor . ' ' .  $testLocation;
									echo '</cite>';
								}
							?>
						</blockquote>
					</aside>
				</div>
<?php
				
			}
		}
	}