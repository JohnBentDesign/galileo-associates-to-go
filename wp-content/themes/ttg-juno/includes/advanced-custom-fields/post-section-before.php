<?php
	/*===========================================================================
	JUNO: POST SECTION (BEFORE CONTENT)
	===========================================================================*/
	
	if( function_exists('register_field_group') ):

	register_field_group(array (
		'key' => 'group_acf_juno-post-section-before-content',
		'title' => 'Before Content',
		'fields' => array (
			array (
				'key' => 'field_5374d82b9a37f',
				'label' => 'Sub Title',
				'name' => 'slide_sub_title',
				'prefix' => '',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'section',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'acf_after_title',
		'style' => 'seamless',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));

	endif;