<?php
	/*===========================================================================
	400
	=============================================================================
	Display for 404 pages
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>

	<article class="main row">
		<div class="large-8 large-centered columns">
		
			<h1>Page Not Found</h1>
			<h3>Sorry, we couldn't find what you were looking for.</h2>
			<p>Please try going back to the <a href="<?= get_bloginfo('siteurl'); ?>">homepage</a> or using the menu in the footer.</p>

		</div>
	</article>
	
<?php get_template_part('parts/shared/footer', 'html'); ?>