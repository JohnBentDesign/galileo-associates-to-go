<?php
/**
 * Class SassWatcher
 *
 * This simple tool compiles all .scss files in folder A to .css files (with exactly the same name) into folder B.
 * To keep things as minimal as possible, this tool compiles every X seconds, regardless of changes within the files.
 * This seems weird, but makes sense as checking for changes in the files is more CPU-extensive than simply
 * re-compiling them. SassWatcher uses scssphp, the best SASS compiler in PHP available.
 *
 * SassWatcher is not a standalone compiler, it's just a little method that uses the excellent scssphp compiler written
 * by Leaf Corcoran (https://twitter.com/moonscript), which can be found here: http://leafo.net/scssphp/ and adds
 * automatic interval-compiling to it.
 *
 * The currently supported version of SCSS syntax is 3.2.12, which is the latest one.
 * To avoid confusion: SASS is the name of the language itself, and also the "name" of the "first" version of the
 * syntax (which was quite different than CSS). Then SASS's syntax was changed to "SCSS", which is more like CSS, but
 * with awesome additional possibilities and features.
 *
 * The compiler uses the SCSS syntax, which is recommended and mostly used. The old SASS syntax is not supported.
 *
 * @see SASS Wikipedia: http://en.wikipedia.org/wiki/Sass_%28stylesheet_language%29
 * @see SASS Homepage: http://sass-lang.com/
 * @see scssphp, the used compiler (in PHP): http://leafo.net/scssphp/
 *
 * How to use this tool:
 *
 * 1. Edit $sass_watcher->watch( ... ); in the last line of this file and put your stuff in here, see the parameter
 *    list below.
 * 2. Make sure PHP can write into your CSS folder.
 * 3. Run the script:
 *    a) simple way, from browser, just enter the URL to scss-compiler.php: http://127.0.0.1/folder/php-sass-watcher.php
 *       The script will run forever, even if you close the browser window.
 *    b) PHPStorm users can run the script by right-clicking the file and selecting "Run php-sass-watcher.php".
 * 4. To stop the script, stop/restart your Apache/Nginx/etc. or press the red "stop process button in PHPStorm.
 *
 * The parameters:
 *
 *  1. relative path to your SCSS folder
 *  2. relative path to your CSS folder (make sure PHP has write-rights here)
 *  3. the compiling interval (in seconds)
 *  4. relative path to the scss.inc.php file, which is the main file of the SASS compiler used
 *     here. Download the script manually from http://leafo.net/scssphp/ or "require" it via Composer:
 *     "leafo/scssphp": "0.0.9"
 *  5. optional: how the .css output should look like. See http://leafo.net/scssphp/docs/#output_formatting for more.
 *
 * How the tool works:
 *
 * Every X seconds ALL files in the scss folder will be compiled to same-name .css files in the css folder.
 * The tool does not stop when a .scss file is broken, has syntax error or similar.
 * The tool does not compile when .scss file is broken, has syntax error or similar. It will only compile next time
 * when there's a valid scss file.
 */

/*==========================================================================
CUSTOM COLORS DEFINED BY USER
==========================================================================*/
$primaryColor       = (get_field('color_primary', 'options'))       ? get_field('color_primary', 'options')     : '#ffffff';
$secondaryColor     = (get_field('color_secondary', 'options'))     ? get_field('color_secondary', 'options')   : '#565656';
$tertiaryColor      = (get_field('color_tertiary', 'options'))      ? get_field('color_tertiary', 'options')    : '#00aeef';

$user_colors = '
	@import "includes";

	// Variables //
	$primaryColorContrast70: color-contrast(' . $primaryColor . ', darken(' . $primaryColor . ', 70%), lighten(' . $primaryColor . ', 70%));
	$secondaryColorContrast70: color-contrast(' . $secondaryColor . ', darken(' . $secondaryColor . ', 70%), lighten(' . $secondaryColor . ', 70%));
	$tertiaryColorContrast70: color-contrast(' . $tertiaryColor . ', darken(' . $tertiaryColor . ', 70%), lighten(' . $tertiaryColor . ', 70%));

	$primaryColorContrast15: color-contrast(' . $primaryColor . ', darken(' . $primaryColor . ', 15%), lighten(' . $primaryColor . ', 15%));
	$secondaryColorContrast15: color-contrast(' . $secondaryColor . ', darken(' . $secondaryColor . ', 15%), lighten(' . $secondaryColor . ', 15%));
	$tertiaryColorContrast15: color-contrast(' . $tertiaryColor . ', darken(' . $tertiaryColor . ', 15%), lighten(' . $tertiaryColor . ', 15%));


	// Primary //
	.primary {
		background-color: ' . $primaryColor . ';
	}
	.primary, .page-slide.has-primary-overlay, .side.has-primary-overlay,  .slide-details.has-primary-overlay, .page-slide[class*=\'has-\'] .heading.primary {
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, cite, a, span, dialog .close-reveal-modal, .reveal-modal .close-reveal-modal, .numbers a {
			color: ' . $secondaryColor . ';
		}
		.button, .contact .symbol {
			color: ' . $primaryColor . ';
			background-color: ' . $tertiaryColor . ';
			&:hover {
				background-color: ' . $primaryColor . ';
				color: ' . $tertiaryColor . ';
			}
		}
		.staffer {
			.opener {
				color: ' . $primaryColor . ';
				background-color: ' . $tertiaryColor . ';
			}
			&:hover .opener {
				background-color: ' . $primaryColor . ';
				color: ' . $tertiaryColor . ';
			}
		}
		blockquote, .circle-arrow {
			border-color: ' . $tertiaryColor . ';
		}
		.featured-testimonial blockquote {
			background-color: rgba(' . $secondaryColor . ', .85);
			p, ul, ol, cite {
				color: ' . $primaryColor . ';
			}
		}
		a, &.staff-popped.reveal-modal header em {
			color: ' . $tertiaryColor . ';
		}
		.circle-arrow, .back-top {
			color: ' . $tertiaryColor . ';
			border-color: ' . $tertiaryColor . ';
			&:hover {
				background-color: ' . $tertiaryColor . ';
				color: ' . $primaryColor . ';
			}
		}
		.division-title h3 {
			background-color: rgba(' . $secondaryColor . ', .25);
		}
		nav.main li {
			&.active a, &:hover a:hover	{
				color: ' . $primaryColor . ';
			}
		}
		.page-slide.gallery .tabs dd a {
			background-color: ' . $tertiaryColor . ';
		}
		.gform_wrapper {
			.top_label {
				input.small, input.medium, input.large, textarea.textarea {
					background-color: ' . $secondaryColor . ';
					color: ' . $primaryColor . ';
				}
				label.gfield_label {
					span.gfield_required {
						color: ' . $tertiaryColor . ';		
					}
				}
			}
			.gform_footer {
				input.button, input[type=submit] {
					color: ' . $tertiaryColor . ';
					background-color: ' . $primaryColor . ';
					&:hover {
						background-color: ' . $tertiaryColor . ';
						color: ' . $primaryColor . ';
					}
				}
			}
		}
		.gallery-overlay {
			background-color: rgba(' . $tertiaryColor . ', .85);
			h4, p {
				color: ' . $primaryColor . ';
			}
		}
	}
	.primary-overlay {
		background-color: rgba(' . $primaryColor . ', .85);
	}
	.has-primary-overlay {
		.slick-dots li, .slick-dots li.active  {
			background-color: ' . $tertiaryColor . ';
		}
	}
	.popped.primary .burst {
		background-color: ' . $tertiaryColor . ';
		color: darken(' . $tertiaryColor . ', 15%);
		&:hover {
			color: ' . $primaryColor . ';
		}
	}
	nav.main li {
		border-color: rgba(' . $primaryColor . ', .5);
		a {
			color: ' . $primaryColor . ';
		}
	}


	// Secondary //
	.secondary {
		background-color: ' . $secondaryColor . ';
	}
	.secondary, .page-slide.has-secondary-overlay, .side.has-secondary-overlay,  .slide-details.has-secondary-overlay, .page-slide[class*=\'has-\'] .heading.secondary {
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, cite, a, span, dialog .close-reveal-modal, .reveal-modal .close-reveal-modal, .numbers a {
			color: ' . $primaryColor . ';
		}
		.button, .contact .symbol {
			color: ' . $primaryColor . ';
			background-color: ' . $tertiaryColor . ';
			&:hover {
				background-color: ' . $primaryColor . ';
				color: ' . $tertiaryColor . ';
			}
		}
		.staffer {
			.opener {
				color: ' . $primaryColor . ';
				background-color: ' . $tertiaryColor . ';
			}
			&:hover .opener {
				background-color: ' . $primaryColor . ';
				color: ' . $tertiaryColor . ';
			}
		}
		blockquote, .circle-arrow {
			border-color: ' . $primaryColor . ';
		}
		.featured-testimonial blockquote {
			background-color: rgba(' . $tertiaryColor . ', .85);
			p, ul, ol, cite {
				color: ' . $primaryColor . ';
			}
		}
		a, &.staff-popped.reveal-modal header em {
			color: ' . $tertiaryColor . ';
		}
		.circle-arrow, .back-top {
			color: ' . $tertiaryColor . ';
			border-color: ' . $tertiaryColor . ';
			&:hover {
				background-color: ' . $tertiaryColor . ';
				color: ' . $primaryColor . ';
			}
		}
		.division-title h3 {
			background-color: rgba(' . $primaryColor . ', .25);
		}
		.page-slide.gallery .tabs dd a {
			background-color: ' . $tertiaryColor . ';
		}
		.gform_wrapper {
			.top_label {
				input.small, input.medium, input.large, textarea.textarea {
					background-color: ' . $tertiaryColor . ';
					color: ' . $primaryColor . ';
				}
				label.gfield_label {
					span.gfield_required {
						color: ' . $tertiaryColor . ';		
					}
				}
			}
			.gform_footer {
				input.button, input[type=submit] {
					color: ' . $tertiaryColor . ';
					background-color: ' . $primaryColor . ';
					&:hover {
						background-color: ' . $tertiaryColor . ';
						color: ' . $primaryColor . ';
					}
				}
			}
		}
		.gallery-overlay {
			background-color: rgba(' . $tertiaryColor . ', .85);
			h4, p {
				color: ' . $primaryColor . ';
			}
		}
	}
	.secondary-overlay {
		background-color: rgba(' . $secondaryColor . ', .85);
	}
	.has-secondary-overlay {
		.slick-dots li, .slick-dots li.active  {
			background-color: ' . $primaryColor . ';
		}
	}
	.popped.secondary .burst {
		background-color: ' . $tertiaryColor . ';
		color: darken(' . $tertiaryColor . ', 15%);
		&:hover {
			color: ' . $secondaryColor . ';
		}
	}
	.header-next {
		.social {
			background-color: darken(' . $secondaryColor . ', 10%);
			a.symbol, .prepend {
				color: ' . $primaryColor . ';
			}
		}
		.prepend {
			background-color: darken(' . $secondaryColor . ', 5%);
		}
	}


	// Tertiary //
	.tertiary {
		background-color: ' . $tertiaryColor . ';
	}
	.tertiary, .page-slide.has-tertiary-overlay, .side.has-tertiary-overlay,  .slide-details.has-tertiary-overlay, .page-slide[class*=\'has-\'] .heading.tertiary {
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, cite, a, span, dialog .close-reveal-modal, .reveal-modal .close-reveal-modal, .numbers a {
			color: ' . $primaryColor . ';
		}
		.button, .contact .symbol {
			color: ' . $secondaryColor . ';
			background-color: ' . $primaryColor . ';
			&:hover {
				background-color: ' . $secondaryColor . ';
				color: ' . $primaryColor . ';
			}
		}
		.staffer {
			.opener {
				color: ' . $secondaryColor . ';
				background-color: ' . $primaryColor . ';
			}
			&:hover .opener {
				background-color: ' . $secondaryColor . ';
				color: ' . $primaryColor . ';
			}
		}
		blockquote, .circle-arrow {
			border-color: ' . $primaryColor . ';
		}
		.featured-testimonial blockquote {
			background-color: rgba(' . $secondaryColor . ', .85);
			color: ' . $primaryColor . ';
			p, ul, ol, cite {
				color: ' . $primaryColor . ';
			}
		}
		a, &.staff-popped.reveal-modal header em {
			color: ' . $secondaryColor . ';
		}
		.circle-arrow, .back-top {
			color: ' . $secondaryColor . ';
			border-color: ' . $secondaryColor . ';
			&:hover {
				background-color: ' . $secondaryColor . ';
				color: ' . $primaryColor . ';
			}
		}
		.opener {
			background-color: ' . $secondaryColor . ';
		}
		.division-title h3 {
			background-color: rgba(' . $primaryColor . ', .25);
		}
		.page-slide.gallery .tabs dd a {
			background-color: ' . $primaryColor . ';
		}
		.gform_wrapper {
			.top_label {
				input.small, input.medium, input.large, textarea.textarea {
					background-color: ' . $primaryColor . ';
					color: ' . $tertiaryColor . ';
				}
				label.gfield_label {
					span.gfield_required {
						color: ' . $secondaryColor . ';		
					}
				}
			}
			.gform_footer {
				input.button, input[type=submit] {
					color: ' . $primaryColor . ';
					background-color: ' . $secondaryColor . ';
					&:hover {
						background-color: ' . $primaryColor . ';
						color: ' . $secondaryColor . ';
					}
				}
			}
		}
		.gallery-overlay {
			background-color: rgba(' . $primaryColor . ', .85);
			h4, p {
				color: ' . $secondaryColor . ';
			}
		}
	}
	.tertiary-overlay {
		background-color: rgba(' . $tertiaryColor . ', .85);
	}
	.has-secondary-overlay {
		.slick-dots li, .slick-dots li.active  {
			background-color: ' . $primaryColor . ';
		}
	}
	.popped.tertiary .burst {
		background-color: ' . $primaryColor . '; 
		color: darken(' . $primaryColor . ', 15%);
		&:hover {
			color: ' . $tertiaryColor . ';
		}
	}
	nav.main li:hover, nav.main li.active {
		background-color: ' . $tertiaryColor . ';
	}

	// Column Heading Background Color
	.primary .heading[class*=\'has-\'], .heading.primary {
		background-color: $primaryColorContrast15;
		&:before {
			border-bottom-color: $primaryColorContrast15 !important;
		}
	}
	.secondary .heading[class*=\'has-\'], .heading.secondary {
		background-color: $secondaryColorContrast15;
		&:before {
			border-bottom-color: $secondaryColorContrast15 !important;
		}
	}
	.tertiary .heading[class*=\'has-\'], .heading.tertiary {
		background-color: $tertiaryColorContrast15;
		&:before {
			border-bottom-color: $tertiaryColorContrast15 !important;
		}
	}	
	.overlay.primary-overlay, .primary .heading[class*=\'has-\'] {
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, cite, nav.main li.active a, nav.main li a:hover, a, span, dialog .close-reveal-modal, .reveal-modal .close-reveal-modal {
			color: $primaryColorContrast70;
		}	
	}
	.overlay.secondary-overlay, .secondary .heading[class*=\'has-\'] {
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, cite, nav.main li.active a, nav.main li a:hover, a, span, dialog .close-reveal-modal, .reveal-modal .close-reveal-modal {
			color: $secondaryColorContrast70;
		}	
	}
	.overlay.tertiary-overlay, .tertiary .heading[class*=\'has-\'] {
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, cite, nav.main li.active a, nav.main li a:hover, a, span, dialog .close-reveal-modal, .reveal-modal .close-reveal-modal {
			color: $tertiaryColorContrast70;
		}	
	}

	// SlickNav
	.slicknav_menu {
		background-color: rgba(' . $secondaryColor . ', .9);
		border-color: ' . $tertiaryColor . ';
	}
	.slicknav_nav, .slicknav_menu  .slicknav_menutxtl, .slicknav_nav .slicknav_item:hover, .slicknav_nav a {
		color: ' . $primaryColor . ';
	}
	.slicknav_nav a:hover {
		background-color: ' . $tertiaryColor . ';
		color: ' . $primaryColor . ';
	}
	.slicknav_btn {
		color: ' . $secondaryColor . ';
		background-color: ' . $tertiaryColor . ';
	}
	.slicknav_nav {
		background-color: $secondaryColorContrast5;
	}

';

$user_custom_font_style = pantheon_function_font_select(array(
		'header' 	=> 'header.main',
		'footer' 	=> 'footer.main',
		'sidebar' 	=> 'aside.sidebar',
		'widget' 	=> '.widget',
		'post' 		=> '.page-slide',
		'page' 		=> '.page-slide',
		'gallery' 	=> '.page-slide.gallery',
		'product' 	=> 'li.product',
	)
);

$user_custom_text_style = pantheon_function_text_style(array(
		'header' 	=> 'header.main',
		'footer' 	=> 'footer.main',
		'sidebar' 	=> 'aside.sidebar',
		'widget' 	=> '.widget',
		'post' 		=> '.page-slide',
		'page' 		=> '.page-slide',
		'gallery' 	=> '.page-slide.gallery',
		'product' 	=> 'li.product',
	)
);

$user_custom_styles = get_field('advanced_css', 'options');

/*==========================================================================*/

class SassWatcher
{
	/**
	 * Watches a folder for .scss files, compiles them every X seconds
	 * Re-compiling your .scss files every X seconds seems like "too much action" at first sight, but using a
	 * "has-this-file-changed?"-check uses more CPU power than simply re-compiling them permanently :)
	 * Beside that, we are only compiling .scss in development, for production we deploy .css, so we don't care.
	 *
	 * @param string $scss_folder source folder where you have your .scss files
	 * @param string $css_folder destination folder where you want your .css files
	 * @param int $interval interval in seconds
	 * @param string $scssphp_script_path path where scss.inc.php (the scssphp script) is
	 * @param string $format_style CSS output format, ee http://leafo.net/scssphp/docs/#output_formatting for more.
	 */
	public function watch($scss_folder, $css_folder, $scssphp_script_path, $format_style = "scss_formatter", $user_defined_colors = null, $user_defined_custom_font = null, $user_defined_styles = null)
	{
		// load the compiler script (scssphp), more here: http://www.leafo.net/scssphp
		require $scssphp_script_path;
		$scss_compiler = new scssc();

		// set the path to your to-be-imported mixins. please note: custom paths are coming up on future releases!
		$scss_compiler->setImportPaths($scss_folder);
		// set css formatting (normal, nested or minimized), @see http://leafo.net/scssphp/docs/#output_formatting
		$scss_compiler->setFormatter($format_style);

		// get all .scss files from scss folder
		$filelist = glob($scss_folder . "*.scss");

		// step through all .scss files in that folder
		foreach ($filelist as $file_path) {
			// get path elements from that file
			$file_path_elements = pathinfo($file_path);

			// get file's name without extension
			$file_name = $file_path_elements['filename'];

			// get .scss's content, put it into $string_sass
			$string_sass = file_get_contents($scss_folder . $file_name . ".scss");

			// Add our user defined colors to the compiled code
			$string_sass .= $user_defined_colors;

			// Add our user font styles to the compiled code
			$string_sass .= $user_defined_custom_font;

			// Add our user defined styles to the compiled code (so we can save it)
			$string_sass .= $user_defined_styles;

			// try/catch block to prevent script stopping when scss compiler throws an error
			try {
				// compile this SASS code to CSS
				$string_css = $scss_compiler->compile($string_sass);
				// write CSS into file with the same filename, but .css extension
				file_put_contents($css_folder . $file_name . ".css", $string_css);
			} catch (Exception $e) {
				echo 'There was an error compiling your CSS.';
			}
		}
	}
}

$sass_watcher = new SassWatcher();
$sass_watcher->watch(get_template_directory() . '/stylesheets/sass/', get_template_directory() . '/stylesheets/css/', get_template_directory() . '/stylesheets/scss.inc.php', 'scss_formatter', $user_colors, $user_custom_font_style, $user_custom_styles);
