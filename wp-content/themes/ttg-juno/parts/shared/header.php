<?php
	/*===========================================================================
	HEADER
	===========================================================================*/
	// Figure out how our header will be laid out by finding the position of the logo
	$logoDisplay 		= get_field('header_position', 'options');
	$connectLocation 	= get_field('social_button_location', 'options');

	// Get Logo Column Size
	switch($logoDisplay){
		case 'above-center'	: $logoSize = 'logo-centered large-8 large-offset-2'; break;
		case 'above-left' 	: $logoSize = 'logo-left large-10'; break;
		default 			: $logoSize = 'logo-next large-2'; break;		
	}
	// Get Nav Column Size
	if( ($logoDisplay == 'next') && (((gettype($connectLocation) == 'array') && in_array('header', $connectLocation)) || ($connectLocation == 'header')) ) {
		$navSize = 'large-9 large-pull-1';
	}
	elseif( (((gettype($connectLocation) == 'array') && in_array('header', $connectLocation)) || ($connectLocation == 'header')) && ($logoDisplay == 'next') ) {
		$navSize = 'large-9';
	}
	elseif($logoDisplay == 'next'){
		$navSize = 'large-10';
	}
	else {
		$navSize = 'large-12';
	}
	// Get Social Column Size
	if($logoDisplay == 'next'){
		$socialSize = 'large-push-9 large-1';
		$socialWatch = ' data-equalizer-watch';
	}
	else {
		$socialSize = 'large-2';
	}
?>

<header id="top" class="main primary header-<?= $logoDisplay; ?>" data-magellan-expedition="fixed" data-equalizer>
	<div class="row">

		<?php
			//-------------------------
			// LOGO
			//-------------------------
		?>
		<div class="logo <?= $logoSize ?> columns" data-equalizer-watch>
			<a href="<?php echo get_bloginfo ( 'url' ); ?>">
				<span class="table">
					<span class="table-row">
						<span class="table-cell">
							<?php 
								if(get_field('header_logo', 'options')){
									pantheon_display_post_field_image(get_field('header_logo', 'options'), 'TTG Logo', 'image');
								}
								else {
									bloginfo('title');
								}
							?>
						</span>
					</span>
				</span>
			</a>
		</div>

		<?php
			//-------------------------
			// SOCIAL
			//-------------------------
			if( ((gettype($connectLocation) == 'array') && in_array('header', $connectLocation)) || ($connectLocation == 'header') ):
		?>
			<div class="social <?=$socialSize; ?> columns" <?= $socialWatch; ?>>
				<?php pantheon_display_social_connect('header'); ?>
			</div>
		<?php endif; ?>

		<?php 
			//-------------------------
			// NAVIGATION
			//-------------------------
			echo '<nav class="main secondary ' . $navSize . ' columns" data-equalizer-watch>';

				// Main Navigation
				//-------------------------
				$menuMain = array(
					'theme_location' 	=> 'menu-main',
					'menu'				=> 'Main Menu',
					'container'			=> false,
					'menu_class'		=> 'menu',
					'depth'				=> 0,
					'walker' 			=> new juno_menu_walker()
				);
				wp_nav_menu($menuMain);

			echo '</nav>';
		?>
	
	</div>
</header>


