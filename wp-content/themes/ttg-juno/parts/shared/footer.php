<footer class="main secondary">
	<div class="row">
		<div class="columns">
	
			<?php
				//-------------------------
				// BACK TO TOP
				//-------------------------
				$backToTop = (get_field('footer_top_btn', 'options')) ? get_field('footer_top_btn', 'options') : 'button';
				if($backToTop == 'button'){
					echo '<a data-scroll href="#top" class="back-top tiny button">Back to Top</a>';
				}
				else {
					echo '<a data-scroll href="#top" class="back-top circle-arrow ion-arrow-up-a"></a>';
				}
			?>

			<?php
				//-------------------------
				// SMALL PRINT
				//-------------------------
			?>
			<div class="small-print">
				<p class="copyright">&copy; <?= date('Y') . ' ' . get_bloginfo('name'); ?>. All rights reserved.</p>
				<p><a href="http://technologytherapy.com" target="_blank">Powered by Technology Therapy</a></p>
			</div>

		</div>
	</div>
</footer>