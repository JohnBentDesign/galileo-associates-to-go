		<?php 
			// FOOTER //
			get_template_part('parts/shared/footer');
		
			// WORDPRESS FOOTER //
			wp_footer();

			// CUSTOM CSS //
			$customScript = get_field('advanced_js', 'options');
			if($customScript){
				echo $customScript;
			}
		?>
		
	</body>
</html>