<?php
	/*===========================================================================
	SLIDES
	=============================================================================
	Used for normal and interactive slide display
	*/

	// Get slide count so we know whether or not to display the next button
	$slides 	= get_field('slide_type');

	// Get General Slide Info
	//-----------------------------------------------
	// Title
	$titleRaw 		= get_the_title();
	$title 			= '<h2>' . $titleRaw . '</h2>';
	$slug 			= sanitize_title($titleRaw);

	// Content
	$subtitle 		= (get_field('slide_sub_title', $post->ID)) ? '<h3>' . get_field('slide_sub_title') . '</h3>' : '';
	$header 		= '<header>' . $title . $subtitle . '</header>';
	$content 		= apply_filters( 'the_content', get_the_content() );
	$content 		= str_replace( ']]>', ']]&gt;', $content );
	$content 		= ($content) ? '<div class="content">' . $content . '</div>' : '';

	// Featured Image
	$featuredURL 	= pantheon_display_post_featured_image($post->ID, 'TTG Featured Image', null, 'url', false);
	$featuredOutput	= pantheon_display_post_featured_image($post->ID, 'TTG Featured Image', null, 'image', false);

	// Call to Action
	$ctaDisplay 	= get_field('slide_cta_display', $post->ID);
	$ctaOutput 		= ($ctaDisplay) ? pantheon_display_post_cta(array('prefix' => 'slide_', 'echo' => false)) : '';

	// Style Changes
	$styleChange	= get_field('slide_bg_change', $post->ID);
	$slideBorder 	= (get_field('slide_border', $post->ID)) ? ' has-border' : '';
	
	// Background Changes
	$bgColor 		= ($styleChange && get_field('slide_bg_color', $post->ID)) ? get_field('slide_bg_color', $post->ID) : 'primary';
	$bgImgObj 		= get_field('slide_bg_image', $post->ID);
	$bgImgStyleAdd	= ($styleChange && $bgImgObj) ? pantheon_display_post_field_image($bgImgObj, 'TTG Slide Background', 'style-add', false) : false;
	$bgImgOutput	= ($styleChange && $bgImgObj) ? pantheon_display_post_field_image($bgImgObj, 'TTG Slide Background', 'style', false) : false;	
	$bgOverlay 		= ($styleChange && $bgImgObj) ? get_field('slide_bg_overlay', $post->ID) : 'secondary-overlay';
	$hasBGOverlay 	= ($styleChange && $bgImgObj) ? ' has-' . get_field('slide_bg_overlay', $post->ID) : '';
	$bgFixed 		= ($styleChange && $bgImgObj && get_field('slide_bg_fixed')) ? ' has-fixed' : '';

	// Share
	$hasShare 		= get_field('slide_share');

	// Testimonial
	$testDisplay 	= get_field('slide_testimonial_display');
	$testObj		= get_field('slide_testimonial');

	// Jump Slide
	$jumpDisplay 	= get_field('slide_jump_display');
	$jumpBGColor	= (get_field('slide_jump_bg')) ? get_field('slide_jump_bg') : 'secondary';
	$jumpLink 		= (get_field('slide_jump_type') == 'icon') ?  '<a data-scroll href="#" class="next"><span class="ion-arrow-down-a circle-arrow"></span></a>' : '<a href="#" class="next button">' . get_field('slide_jump_text') . '</a>';

	// Classes
	$containerClass = $bgColor . ' ' . $slideBorder;
	$slideTypeClass = $hasBGOverlay . $bgFixed;


	//======================================================================================================
	// SLIDE OUTPUT
	//======================================================================================================
	while(has_sub_field('slide_type')):
?>

		<div id="<?= $slug; ?>" class="slide-container <?= $containerClass; ?>" data-magellan-destination="<?= $slug; ?>">
			<?php
				// Share Buttons
				//-----------------
				if($hasShare){
					pantheon_display_social_share('blog', 'post', $post, $content, $featuredURL, get_site_url() . '#' . $slug);
				}
			?>


			<?php
				// SLIDESHOW SECTION
				//===================================================
				if(get_row_layout() == 'slide_slideshow'):
					$bgOverlay = (get_field('slide_bg_overlay', $post->ID)) ? get_field('slide_bg_overlay', $post->ID) : 'secondary-overlay';
			?>
					<div class="page-slide slideshow<?= $slideTypeClass ?> has-<?= $bgOverlay; ?>">

						<?php
							//----------------------------------
							// SLIDESHOW
							//----------------------------------
							if(have_rows('slide_slideshow')):

								echo '<div class="slider">';

								while(have_rows('slide_slideshow') ) : the_row();
									// Get ALL the slideshow's info!
									//-----------------------------------------------
									$title 			= get_sub_field('slideshow_title') ? '<h2>' . get_sub_field('slideshow_title') . '</h2>' : '';
									$content 		= get_sub_field('slideshow_content');
									$image 			= pantheon_display_post_field_image(get_sub_field('slideshow_image'), 'TTG Slide Background', 'style', false);
									$ctaDisplay 	= get_sub_field('slideshow_cta_display', $post->ID);
									$ctaTextType 	= get_sub_field('slideshow_cta_text_type');
									$ctaText 		= ($ctaTextType == 'text') ? get_sub_field('slideshow_cta_text') : '<span class="ion-arrow-right-a circle-arrow"></span>';
									$ctaClass 		= ($ctaTextType == 'text') ? ' class="button"' : '';
									$ctaLinkType	= get_sub_field('slideshow_cta_type');
									$ctaInternal 	= get_sub_field('slideshow_cta_internal');
									$ctaExternal 	= get_sub_field('slideshow_cta_external');
									$ctaSlide 		= get_sub_field('slideshow_cta_slide');
									switch ($ctaLinkType) {
										case 'internal': $ctaLink = '<a href="' . get_permalink($ctaInternal[0]) . '"' . $ctaClass . '>' . $ctaText . '</a>'; break;
										case 'external': $ctaLink = '<a href="' . $ctaExternal . '"' . $ctaClass . ' target="_blank">' . $ctaText . '</a>'; break;
										case 'slide': $ctaLink = '<a data-scroll href="#' . sanitize_title(get_the_title($ctaSlide[0])) . '"' . $ctaClass . '>' . $ctaText . '</a>'; break;
									}
						?>

									<div <?= $image; ?> class="slides">
										<div class="slide-details large-6 columns has-<?= $bgOverlay; ?>">
											<?= $title . $content . $ctaLink; ?>
										</div>
										<div class="overlay <?= $bgOverlay; ?>"></div>
									</div>
						<?php
								endwhile;

								echo '</div>';

							endif;
						?>

					</div>
			<?php endif; ?>


			<?php
				// SIMPLE SECTION
				//===================================================
				if(get_row_layout() == 'slide_simple'):
					// Get ALL the slide's info!
					//-----------------------------------------------
					$bgFocus 		= get_sub_field('slide_bg_focus');
					$bgTrans 		= get_sub_field('slide_bg_transparent');
					$hasFocus 		= ($bgImgObj && $bgFocus) ? ' has-focus' : '';
					$hasBGOverlay 	= ($bgImgObj && !$bgFocus) ? $hasBGOverlay : '';
					$noContent 		= ($hasFocus) ? get_sub_field('slide_content_none') : false;
					$hasNoContent 	= ($noContent) ? ' hasNoContent' : '';
					$columnSize 	= ($bgImgObj && $bgFocus) ? 'large-9 large-centered' : 'large-12';
			?>
					<div class="page-slide simple<?= $slideTypeClass . $hasFocus . $hasNoContent; ?>"<?= $bgImgOutput; ?>>
						
						<?php 
							//----------------------------------
							// OVERLAY
							//----------------------------------
							if($bgImgObj && $styleChange && !$bgFocus) { 
								echo '<div class="overlay ' . $bgOverlay . '"></div>';
							}
						
							//----------------------------------
							// CONTENT
							//----------------------------------
							// Hide if set to image focus and to hide content
							if(!$noContent):
						?>
								<div class="row">	
									<div class="<?= $columnSize; ?> columns">
										<div class="slide-details <?php if($bgFocus && !$bgTrans){ echo str_replace('-overlay', '', $bgOverlay); } elseif($bgFocus){ echo ' ' . $bgOverlay; } ?>">
											
											<article>
												<?php
													// Content
													//-----------------
													echo $featuredOutput . $header . $content;

													// Pop Up Content
													//-----------------
													if(have_rows('slide_pop')):

														echo '<div class="popples">';
		

														while(have_rows('slide_pop')): the_row();
															$popTitle 		= get_sub_field('pop_title');
															$popSlug 		= sanitize_title($popTitle);
															$popExcerpt 	= get_sub_field('pop_excerpt');
															$popContent 	= wpautop(get_sub_field('pop_content'));
															$popBgColor 	= (get_sub_field('pop_bg_change')) ? get_sub_field('pop_bg_color') : 'tertiary';		
															$popPrint		= (get_sub_field('pop_print')) ? '<a class="print-me ion-ios7-printer">Print</a>' : '';
												?>
															<div class="popper">
																<h3><a href="#" data-reveal-id="<?= $slug; ?>-popped-<?= $popSlug; ?>" class="pop-link"><?= $popTitle; ?> <span class="ion-chevron-right"></span></a></h3>
																<p><?= $popExcerpt; ?></p>
															</div>

															<div id="<?= $slug; ?>-popped-<?= $popSlug; ?>" class="popped medium <?= $popBgColor; ?> reveal-modal" data-reveal>
																<h3 class="popped-title"><?= $popTitle; ?></h3>
																<?= $popContent; ?>
																<a class="burst close-reveal-modal">&#215;</a>
																<?= $popPrint; ?>
															</div>
												<?php

														endwhile;

														echo '</div>';

													endif;

													// Call to Action
													//-----------------
													echo $ctaOutput;
												?>
											</article>

											<?php
												// Testimonial
												//-----------------
												juno_display_post_testimonial($testDisplay, $testObj, 'large-8 large-centered');
											?>

										</div>
									</div>
								</div>
						<?php endif; ?>

					</div>

					<?php
						if($noContent):
							// Testimonial
							//-----------------
							juno_display_post_testimonial($testDisplay, $testObj, 'outside large-8 large-centered');
						endif;
					?>
			<?php endif; ?>


			<?php
				// SPLIT SECTION
				//===================================================
				if(get_row_layout() == 'slide_split'):
					// Get ALL the slide's info!
					//-----------------------------------------------	
					// Content
					$contentSide	= get_sub_field('slide_side');

					// Style Changes (Image)
					$bgColorOver	= (get_sub_field('slide_bg_color_image')) ? ' background-color: ' . get_sub_field('slide_bg_color_image') . '; ' : '';
					$bgImgFix		= (get_sub_field('slide_bg_fixed')) ? ' has-fixed' : '';
					$bgImgOutput	= ($bgImgStyleAdd || $bgColorOver) ? ' style="' . $bgImgStyleAdd . $bgColorOver . '"' : '';	

					// Classes
					$splitContentBG = ($contentSide == 'right') ? 'bg for-content right-side ' . $bgColor : 'bg for-content left-side ' . $bgColor;
					$splitImageBG	= ($contentSide == 'right') ? 'bg left-side ' . $bgImgFix : 'bg right-side ' . $bgImgFix;
					$splitContent 	= ($contentSide == 'right') ? 'right-side large-5 large-offset-6 columns ' . $bgColor : 'left-side large-5 columns ' . $bgColor;
			?>
					<div class="page-slide split">
					
						<?php 
							//----------------------------------
							// SPLIT BACKGROUND
							//----------------------------------
						?>
						<div class="<?= $splitImageBG; ?>"<?= $bgImgOutput; ?>></div>
						<div class="<?= $splitContentBG; ?>"></div>
							
						<?php 
							//----------------------------------
							// SPLIT CONTENT
							//----------------------------------
						?>
						<div class="row">							
							<div class="<?= $splitContent; ?>">
									
								<article class="slide-details">
									<?php
										// Content
										//-----------------
										echo $featuredOutput . $header . $content;

										// Pop Up Content
										//-----------------
										if(have_rows('slide_pop')):

											echo '<div class="popples">';
											$popCount = 0;

											while(have_rows('slide_pop')): the_row();
												$popTitle 		= get_sub_field('pop_title');
												$popSlug 		= sanitize_title($popTitle);
												$popExcerpt 	= get_sub_field('pop_excerpt');
												$popContent 	= wpautop(get_sub_field('pop_content'));
												$popBgColor 	= (get_sub_field('pop_bg_change')) ? get_sub_field('pop_bg_color') : 'tertiary';		
												$popPrint		= (get_sub_field('pop_print')) ? '<a class="print-me ion-ios7-printer">Print</a>' : '';
									?>
												<div class="popper">
													<h3><a href="#" data-reveal-id="<?= $slug; ?>-popped-<?= $popSlug; ?>" class="pop-link"><?= $popTitle; ?> <span class="ion-chevron-right"></span></a></h3>
													<p><?= $popExcerpt; ?></p>
												</div>

												<div id="<?= $slug; ?>-popped-<?= $popSlug; ?>" class="popped medium <?= $popBgColor; ?> reveal-modal" data-reveal>
													<h3 class="popped-title"><?= $popTitle; ?></h3>
													<?= $popContent; ?>
													<a class="burst close-reveal-modal">&#215;</a>
													<?= $popPrint; ?>
												</div>
									<?php

											endwhile;
											echo '</div>';
										endif;

										// Call to Action
										//-----------------
										echo $ctaOutput;
									?>
									</article>

									<?php
										// Testimonial
										//-----------------
										juno_display_post_testimonial($testDisplay, $testObj);
									?>

								</div>
							</div>

						</div>
			<?php endif; ?>


			<?php
				// COLUMN SECTION
				//===================================================
				if(get_row_layout() == 'slide_columns'):
					// Get ALL the slide's info!
					//-----------------------------------------------			
					// Columns
					$columns 		= get_sub_field('slide_column');
					$columnCount 	= get_sub_field('slide_column_count');
					$columnType 	= get_sub_field('slide_column_type');
					$columnOverlay 	= $bgColor . '-overlay';
			?>
					<div class="page-slide column-content<?= $slideTypeClass; ?>"<?= $bgImgOutput; ?>>

						<?php 
							//----------------------------------
							// OVERLAY
							//----------------------------------
							if($bgImgObj && $styleChange) { 
								echo '<div class="overlay ' . $bgOverlay . '"></div>';
							}
						?>

						<div class="row">
							<div class="large-5 columns">
								<article class="slide-details">
									<?php
										// Content
										//-----------------
										echo $featuredOutput . $header . $content . $ctaOutput;
									?>
								</article>
							</div>
						</div>

						<?php
							// Columns
							//-----------------
							if($columns):
						?>
								<ul class="row">
									<?php
										while(has_sub_field('slide_column')):
											// Content
											$title 			= get_sub_field('slide_column_title') ? '<h3>' . get_sub_field('slide_column_title') . '</h3>' : '';
											$content 		= get_sub_field('slide_column_content');
											
											// Image
											$imageObj 		= get_sub_field('slide_column_image');
											$imageOutput	= pantheon_display_post_field_image($imageObj, 'TTG Medium Thumbnail', 'image', false);
											
											// Column
											$ctaOutput 		= pantheon_display_post_cta(array('prefix' => 'slide_', 'echo' => false, 'repeater' => 'sub-field'));

											// Display
											$columnDisplay 	= ($columnType == 'normal') ? 'normal ' : 'compact ';

											// Normal
											//-----------------
											if($columnType == 'normal'){
									?>
												<li class="<?= $columnDisplay . $columnCount; ?> columns">
													<?= $imageOutput . $title . $content . $ctaOutput; ?>
												</li>

									<?php
											}

											// Compact
											//-----------------
											if( ($columnType == 'compact') && $imageObj ){
									?>
												<li class="<?= $columnDisplay . $columnCount; ?> columns">

													<div class="inner">
														<?= $imageOutput; ?>
														<div class="overlay <?= $columnOverlay; ?>">
														
															<div class="table">
																<div class="table-row">
																	<div class="table-cell">
																		<?= $title . $content; ?>
																	</div>
																</div>
															</div>

														</div>
													</div>

													<div class="heading <?= $bgColor; ?>">
														<?= $title . $ctaOutput; ?>
													</div>

												</li>

									<?php
											}
										endwhile;
									?>
								</ul>
						<?php endif; ?>

						<?php
							// Testimonial
							//-----------------
							juno_display_post_testimonial($testDisplay, $testObj, 'large-12');
						?>

					</div>
			<?php endif; ?>


			<?php
				// COMPLEX SECTION
				//===================================================
				if(get_row_layout() == 'slide_complex'):
					// Main Content
					//-----------------------
					// Content
					$contentSide		= get_sub_field('slide_side');
					$contentSideMain 	= ($contentSide != 'left') ? ' large-push-7' : '';
					$contentSideSec 	= ($contentSide != 'left') ? ' large-pull-5' : '';

					// Secondary Content
					//-----------------------
					// Get Extra Content
					$contentTypeSec		= get_sub_field('slide_extra_content_type');
					
					// Content
					$titleRawSec 		= get_sub_field('slide_title');
					$titleSec 			= ($titleRawSec) ? '<h2>' . $titleRawSec . '</h2>' : '';
					$subtitleSec 		= (get_sub_field('slide_subtitle', $post->ID)) ? '<h3>' . get_sub_field('slide_subtitle') . '</h3>' : '';
					$headerSec 			= ($titleSec || $subtitleSec) ? '<header>' . $titleSec . $subtitleSec . '</header>' : '';
					$contentSec 		= (get_sub_field('slide_content')) ? '<div class="content">' . wpautop(get_sub_field('slide_content')) . '</div>' : '';

					// Featured Image
					$featuredObjSec 	= get_sub_field('slide_featured');
					$featuredOutputSec	= pantheon_display_post_field_image($featuredObjSec, 'TTG Featured Image', 'image', false);

					// Call to Action
					$ctaDisplaySec 		= get_sub_field('slide_cta_display');
					$ctaOutputSec 		= ($ctaDisplaySec) ? pantheon_display_post_cta(array('prefix' => 'slide_', 'echo' => false, 'repeater' => 'sub-field')) : '';

					// Style Changes
					$styleChangeSec		= get_sub_field('slide_bg_change');
	
					// Background Changes
					$bgColorSec 		= ($styleChangeSec && get_sub_field('slide_bg_color')) ? get_sub_field('slide_bg_color') : 'primary';
					$bgImgObjSec 		= get_sub_field('slide_bg_image');
					$bgOverlaySec 		= (($contentTypeSec == 'content') && ($styleChangeSec && $bgImgObjSec)) ? get_sub_field('slide_bg_overlay') : 'secondary-overlay';
					$hasBGOverlaySec 	= (($contentTypeSec == 'content') && ($styleChangeSec && $bgImgObjSec)) ? ' has-' . get_sub_field('slide_bg_overlay') : '';
					$bgFixedSec 		= ((($contentTypeSec == 'image') || ($styleChangeSec && $bgImgObjSec)) && get_sub_field('slide_bg_fixed')) ? ' has-fixed' : '';
					$bgImgOutputSec		= ((($contentTypeSec == 'image') || ($styleChangeSec && $bgImgObjSec)) && ($bgFixedSec)) ? pantheon_display_post_field_image($bgImgObjSec, 'TTG Slide Background', 'style-add', false) : false;	
					$bgImgOutputSecImg	= ((($contentTypeSec == 'image') || ($styleChangeSec && $bgImgObjSec))) ? pantheon_display_post_field_image($bgImgObjSec, 'TTG Slide Background', 'image', false) : false;	
					$bgOverrideSec 		= get_sub_field('slide_bg_color_image');

					// Classes
					$containerClassSec 	= $bgColorSec;
					$slideTypeClassSec 	= $hasBGOverlaySec . $bgFixedSec;
					$imgStyleSec 		= ($bgImgOutputSec) ? ' style="' . $bgImgOutputSec . '"' : '';
					$imgStyleSec 		= ($bgOverrideSec && !$bgFixedSec) ? ' style="background-color: ' . $bgOverrideSec . '"' : $imgStyleSec;
			?>
					<div class="page-slide complex <?= $containerClass; ?>">

						<?php
							//----------------------------------
							// SIDES
							//----------------------------------
						?>
						<div class="row" data-equalizer>

							<?php
								//----------------------------------
								// SIDE- MAIN
								//----------------------------------
							?>
							<div class="side side-main large-5 columns <?= $slideTypeClass . $contentSideMain; ?>"<?= $bgImgOutput; ?> data-equalizer-watch>
								<?php 
									//----------------------------------
									// OVERLAY
									//----------------------------------
									if($bgImgObj && $styleChange) { 
										echo '<div class="overlay ' . $bgOverlay . '"></div>';
									}
								?>
								<article class="slide-details">
									<?php
										// Content
										//-----------------
										echo $featuredOutput . $header . $content;

										// Pop Up Content
										//-----------------
										if(have_rows('slide_pop')):

											echo '<div class="popples">';
											$popCount = 0;

											while(have_rows('slide_pop')): the_row();
												$popTitle 		= get_sub_field('pop_title');
												$popSlug 		= sanitize_title($popTitle);
												$popExcerpt 	= get_sub_field('pop_excerpt');
												$popContent 	= wpautop(get_sub_field('pop_content'));
												$popBgColor 	= (get_sub_field('pop_bg_change')) ? get_sub_field('pop_bg_color') : 'tertiary';		
												$popPrint		= (get_sub_field('pop_print')) ? '<a class="print-me ion-ios7-printer">Print</a>' : '';
									?>
												<div class="popper">
													<h3><a href="#" data-reveal-id="<?= $slug; ?>-popped-<?= $popSlug; ?>" class="pop-link"><?= $popTitle; ?> <span class="ion-chevron-right"></span></a></h3>
													<p><?= $popExcerpt; ?></p>
												</div>

												<div id="<?= $slug; ?>-popped-<?= $popSlug; ?>" class="popped medium <?= $popBgColor; ?> reveal-modal" data-reveal>
													<h3 class="popped-title"><?= $popTitle; ?></h3>
													<?= $popContent; ?>
													<a class="burst close-reveal-modal">&#215;</a>
													<?= $popPrint; ?>
												</div>
									<?php

											endwhile;
											echo '</div>';
										endif;

										// Call to Action
										//-----------------
										echo $ctaOutput;
									?>
								</article>

								<?php
									// Testimonial
									//-----------------
									juno_display_post_testimonial($testDisplay, $testObj);
								?>
							</div>


							<?php
								//----------------------------------
								// SIDE- SECONDARY
								//----------------------------------
							?>
							<div class="side side-secondary large-7 columns <?= $slideTypeClassSec . ' ' . $containerClassSec . $contentSideSec; ?>"<?= $imgStyleSec; ?> data-equalizer-watch>
								<?php 
									//----------------------------------
									// OVERLAY
									//----------------------------------
									if($bgImgObjSec && ($contentTypeSec == 'content') && $styleChangeSec) { 
										echo '<div class="overlay ' . $bgOverlaySec . '"></div>';
									}
								?>

								<?php if($contentTypeSec == 'content'): ?>
									<article class="slide-details">
										<?php
											// Content
											//-----------------
											echo $featuredOutputSec . $headerSec . $contentSec;

											// Pop Up Content
											//-----------------
											if(have_rows('slide_pop_extra_sec')):

												echo '<div class="popples">';


												while(have_rows('slide_pop_extra_sec')): the_row();
													$popTitle 		= get_sub_field('pop_title');
													$popSlug 		= sanitize_title($popTitle);
													$popExcerpt 	= get_sub_field('pop_excerpt');
													$popContent 	= wpautop(get_sub_field('pop_content'));
													$popBgColor 	= (get_sub_field('pop_bg_change')) ? get_sub_field('pop_bg_color') : 'tertiary';		
													$popPrint		= (get_sub_field('pop_print')) ? '<a class="print-me ion-ios7-printer">Print</a>' : '';
										?>
													<div class="popper">
														<h3><a href="#" data-reveal-id="<?= $slug; ?>-popped-<?= $popSlug; ?>" class="pop-link"><?= $popTitle; ?> <span class="ion-chevron-right"></span></a></h3>
														<p><?= $popExcerpt; ?></p>
													</div>

													<div id="<?= $slug; ?>-popped-<?= $popSlug; ?>" class="popped medium <?= $popBgColor; ?> reveal-modal" data-reveal>
														<h3 class="popped-title"><?= $popTitle; ?></h3>
														<?= $popContent; ?>
														<a class="burst close-reveal-modal">&#215;</a>
														<?= $popPrint; ?>
													</div>
										<?php

												endwhile;
												echo '</div>';
											endif;

											// Call to Action
											//-----------------
											echo $ctaOutputSec;
										?>
									</article>
								<?php elseif($bgImgObjSec && !$bgFixedSec): ?>
									<div class="table">
										<div class="table-row">
											<div class="table-cell">
												<?= $bgImgOutputSecImg; ?>
											</div>
										</div>
									</div>
								<?php endif; ?>
							</div>
						</div>


					</div>
			<?php endif; ?>


			<?php			
				// STAFF SECTION
				//===================================================
				if(get_row_layout() == 'slide_staff'):
					// Get ALL the slide's info!
					//-----------------------------------------------
					$staffType 		= get_sub_field('slide_staff_type');
					$staffChoice 	= get_sub_field('slide_staff_individual');
					$staffDivision 	= get_sub_field('slide_staff_division');
					$staffBGColor	= (get_sub_field('slide_staff_pop_change')) ? get_sub_field('slide_staff_pop_bgcolor') : 'secondary';
					
					// Resort our divisions by slug
					$sortArray 		= array(); 
					foreach($staffDivision as $division){ 
						foreach($division as $key=>$value){ 
							if(!isset($sortArray[$key])){ 
								$sortArray[$key] = array(); 
							} 
							$sortArray[$key][] = $value; 
						} 
					} 
					$orderby = 'slug';
					array_multisort($sortArray[$orderby],SORT_ASC,$staffDivision); 
			?>
					<div class="page-slide staff<?= $slideTypeClass; ?>"<?= $bgImgOutput; ?>>

						<?php 
							//----------------------------------
							// OVERLAY
							//----------------------------------
							if($bgImgObj && $styleChange) { 
								echo '<div class="overlay ' . $bgOverlay . '"></div>';
							}
						?>

						<div class="row">
							<div class="large-5 columns">
								<article class="slide-details">
									<?php
										// Content
										//-----------------
										echo $featuredOutput . $header . $content . $ctaOutput;
									?>
								</article>
							</div>
						</div>

						<?php 
							// STAFF CHOICE
							//-----------------
							if( $staffChoice && ($staffType == 'choose') ):
						?>
								<ul class="staff-list choose row">
									<?php
										// Loop Staff
										foreach( $staffChoice as $post):
											setup_postdata($post);

											// Function to display staff block
											juno_display_post_staff($post, $staffBGColor);

										endforeach;
									?>
								</ul>
						<?php 
								wp_reset_postdata();
							endif;
						?>

						<?php 
							// STAFF DIVISION
							//-----------------
							if( $staffDivision && ($staffType == 'division') ):
								foreach($staffDivision as $term):
									// Get Our Staffers
									$args = array( 
										'post_type' => 'staff',
										'numberposts' => -1,
										'tax_query' => array(
											array(
												'taxonomy' => 'divisions',
												'field' => 'id',
												'terms' => $term->term_id,
												'include_children' => false
											)
										),
										'orderby' => 'name',
										'order' => 'ASC'
									); 
									$the_query = new WP_Query( $args );
									 
									// The Loop
									if($the_query->have_posts()):
										echo '<ul class="staff-list division row">';
										echo '<li class="division-title columns"><h3>' . $term->name . '</h3></li>';
											while ( $the_query->have_posts() ) : $the_query->the_post();

												// Function to display staff block
												juno_display_post_staff($post, $staffBGColor);

											endwhile;
										echo '</ul>';
									endif;

									// Reset Post Data
									wp_reset_postdata();
								endforeach;
							endif;
						?>

						<?php
							// Testimonial
							//-----------------
							juno_display_post_testimonial($testDisplay, $testObj);
						?>

					</div>
			<?php endif; ?>


			<?php			
				// GALLERY SECTION
				//===================================================
				if(get_row_layout() == 'slide_gallery'):
					// Get ALL the slide's info!
					//-----------------------------------------------
					$galleryDisplay 	= get_sub_field('slide_gallery_obj');
			?>
					<div class="page-slide gallery<?= $slideTypeClass; ?>"<?= $bgImgOutput; ?>>

						<?php 
							//----------------------------------
							// OVERLAY
							//----------------------------------
							if($bgImgObj && $styleChange) { 
								echo '<div class="overlay ' . $bgOverlay . '"></div>';
							}
						?>

						<div class="row">
							<div class="large-5 columns">
								<article class="slide-details">
									<?php
										// Content
										//-----------------
										echo $featuredOutput . $header . $content . $ctaOutput;
									?>
								</article>
							</div>
						</div>

						<?php pantheon_display_gallery($galleryDisplay[0]); ?>

					</div>
			<?php endif; ?>
			

			<?php
				// TESTIMONIAL SECTION
				//===================================================
				if(get_row_layout() == 'slide_test'):
					// Get ALL the slide's info!
					//-----------------------------------------------
					// Testimonials
					$test 			= get_sub_field('slide_test');
					$testColumns	= get_sub_field('slide_test_columns');

					// Featured Image
					$featuredPos 	= get_sub_field('slide_featured_position');
					$featuredMid 	= (($featuredPos == 'middle') && $featuredOutput) ? true : false;
					$featuredSize 	= ($featuredMid) ? 'TTG Testimonial Image' : 'TTG Featured Image';
					$featuredOutput	= pantheon_display_post_featured_image($post->ID, $featuredSize, false, 'image', false);
		?>
						
					<div class="page-slide testimonials<?= $slideTypeClass; ?>"<?= $bgImgOutput; ?>>

						<?php 
							//----------------------------------
							// OVERLAY
							//----------------------------------
							if($bgImgObj && $styleChange) { 
								echo '<div class="overlay ' . $bgOverlay . '"></div>';
							}
						?>

						<div class="row">
							<div class="large-5 columns">
								<article class="slide-details">
									<?php
										// Content
										//-----------------
										if($featuredPos == 'above') { echo $featuredOutput; }
										echo $header . $content . $ctaOutput;
									?>
								</article>
							</div>
						</div>

						<div class="all-tests row">
							<?php 
								// Testimonials
								//-----------------
								if($test):
									// Variables
									$count 		= count($test);
									$i 			= 0;
									$featuredMid 	= (($featuredPos == 'middle') && $featuredOutput) ? true : false;
									$leftSide = $rightSide = $allSide = array();

									// Get Appropriate Column Size
									if($featuredMid){
										$columnSize = '';
									}
									else {
										switch ($testColumns) {
											case 'one': 	$columnSize = 'large-7 large-centered '; break;
											case 'two': 	$columnSize = 'large-6 '; break;
											case 'three': 	$columnSize = 'large-4 '; break;
											default: $columnSize = 'large-6 '; break;
										}
									}
									
									// Loop Through Testimonials
									foreach($test as $theTest):
										$i++;

										// Array Push to split up into columns if we have a middle image
										if( $featuredMid){
											if($i <= round($count/2) ){
												array_push($leftSide, $theTest);
											}
											else {
												array_push($rightSide, $theTest);
											}
										}
										// Or we'll just push it to an array that will output the testimonials in normal order
										else {
											array_push($allSide, $theTest);
										}

									endforeach;

									// Function for Loop
									if(!function_exists('juno_display_post_testimonial_slide')){
										function juno_display_post_testimonial_slide($postID, $columnSize = null){
											// Testimonial Variables
											$testContent	= get_field('testimonial_content', $postID) ? '<div class="quote">' . get_field('testimonial_content', $postID) . '</div>' : '';
											$testAuthor		= get_field('testimonial_author', $postID);
											$testLocation	= get_field('testimonial_location', $postID);

											if($columnSize) { echo '<div class="' . $columnSize . ' columns">'; }
											echo '<blockquote class="testimonial">';
											
											echo $testContent;
											if($testAuthor || $testLocation){
												echo '<cite class="credit">';
												echo $testAuthor . ' ' .  $testLocation;
												echo '</cite>';
											}

											echo '</blockquote>';
											if($columnSize) { echo '</div>'; }
										}
									}

									// Output Testimonials
									if($featuredMid){
										echo '<div class="large-4 columns">';
										foreach($leftSide as $theTest){
											juno_display_post_testimonial_slide($theTest->ID);
										}
										echo '</div><div class="large-4 columns">' . $featuredOutput . '</div><div class="large-4 columns">';
										foreach($rightSide as $theTest){
											juno_display_post_testimonial_slide($theTest->ID);
										}
										echo '</div>';
									}
									else {
										foreach($allSide as $theTest){
											juno_display_post_testimonial_slide($theTest->ID, $columnSize);
										}
									}

									wp_reset_postdata();
								endif;
							?>
						</div>

						<?
							// Testimonial
							//-----------------
							juno_display_post_testimonial($testDisplay, $testObj, 'large-12');
						?>

					</div>
			<?php endif; ?>


			<?php
				// CONTACT SECTION
				//===================================================
				if(get_row_layout() == 'slide_contact'):
					// Get ALL the slide's info!
					//-----------------------------------------------
					// Content
					$contentSide	= get_sub_field('slide_side');

					// Form
					$form 			= get_sub_field('slide_form');

					// Address Band
					$address 		= get_sub_field('slide_address');
					$addressBG		= get_sub_field('slide_address_bg_color');

					// Map
					$mapDisplay 	= get_sub_field('slide_map');
					$mapImage 		= ($mapDisplay == 'image') ? pantheon_display_post_field_image(get_sub_field('slide_map_image'), 'TTG Slide Background', 'style', false) : '';
					$mapGoogle 		= ($mapDisplay == 'google') ? get_sub_field('slide_map_google') : '';
			?>
					<div class="page-slide contact<?= $slideTypeClass; ?>"<?= $bgImgOutput; ?>>
						<?php 
							//----------------------------------
							// OVERLAY
							//----------------------------------
							if($bgImgObj) { 
								echo '<div class="overlay ' . $bgOverlay . '"></div>';
							}
						?>

						<div class="row">	
							<div class="columns">

								<?php
									// CONTENT STARTER
									//----------------------------------
									echo $featuredOutput;
								
									// CONTENT
									//----------------------------------
									$contentColumn 	= ($contentSide == 'right') ? 'large-push-6' : '';
									$formColumn 	= ($contentSide == 'right') ? 'large-pull-6' : '';
									$contentColumn 	= (!$form) ? 'large-9 large-centered' : $contentColumn;
								?>
								<div class="contact-details large-5 columns <?= $contentColumn; ?>">
									<article>
										<?php
											// Content
											//----------------- 
											echo $header . $content . $ctaOutput;
										?>
									</article>
								</div>

								<?php 
									// FORM
									//----------------------------------
									if($form):
								?>
									<div class="contact-form large-7 columns <?= $formColumn; ?>">
										<?php
					   						// Form
											//-----------------  
											echo do_shortcode($form);
										?>
									</div>
								<?php endif; ?>

								<?
									// Testimonial
									//-----------------
									juno_display_post_testimonial($testDisplay, $testObj);
								?>

							</div>
						</div>
					</div>

					<?php
						// Address Band
						//-----------------
						if($address):
					?>
						<aside class="address <?= $addressBG; ?>">
							<div class="row">
								<div class="columns">
									<h4><?= $address; ?></h4>
								</div>
							</div>
						</aside>
					<?php endif; ?>

					<?php
						// Map
						//-----------------
						if($mapDisplay != 'none'):
					?>
						<aside class="map"<?= $mapImage; ?>>
							<?= $mapGoogle; ?>
						</aside>
					<?php endif; ?>


			<?php endif; ?>
			
			</div>


			<?php
				// SLIDE JUMPER
				//===================================================
				if($jumpDisplay):
			?>
				<div class="jumper <?= $jumpBGColor; ?>">
					<?= $jumpLink; ?>
				</div>
			<?php endif; ?>

	<?php endwhile; ?>