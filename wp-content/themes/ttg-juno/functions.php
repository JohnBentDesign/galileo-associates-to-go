<?php
	/*===========================================================================
	INCLUDE THE INCLUDES
	===========================================================================*/
	// Custom Post Type
	include('includes/post-type/section.php');

	// Advanced Custom Fields
	include('includes/advanced-custom-fields/theme-styling.php');
	include('includes/advanced-custom-fields/post-section.php');
	include('includes/advanced-custom-fields/post-section-before.php');
	include('includes/advanced-custom-fields/post-section-styling.php');
	include('includes/advanced-custom-fields/page-section.php');

	// Custom Functions
	include('includes/functions/juno_display_post_testimonial.php');
	include('includes/functions/juno_display_post_staff.php');



	/*===========================================================================
	ADD THEME SUPPORTS
	===========================================================================*/
	add_theme_support('post-thumbnails');
	add_theme_support('menus');



	/*===========================================================================
	CUSTOM THUMBNAIL SIZES
	===========================================================================*/
	add_image_size('TTG Logo', 480, 80, false);
	add_image_size('TTG Featured Image', 1024, 500, true);
	add_image_size('TTG Gallery Normal Display', 637, 9999, false);
	add_image_size('TTG Medium Thumbnail', 350, 350, true);
	add_image_size('TTG Slide Background', 1600, 1000, false);
	add_image_size('TTG Testimonial Image', 375, 9999, false);
	add_image_size('TTG Staff Image', 250, 400, true);



	/*===========================================================================
	REGISTER MENUS
	===========================================================================*/
	if(!function_exists('ttg_menus')){
		function ttg_menus() {
			register_nav_menus(array(
				'menu-main' 		=> 'Main Menu'
			));
		}
		add_action('init', 'ttg_menus');
	}



	/*===========================================================================
	ENQUEUE SCRIPTS AND STYLES
	===========================================================================*/
	function ttg_scripts(){
		if(!is_admin()){
			// Juno Scripts
			wp_enqueue_script('slicknav', get_template_directory_uri() . '/javascripts/plugins/jquery.slicknav.min.js', array('jquery'), '', true);
			wp_enqueue_script('printthis', get_template_directory_uri() . '/javascripts/plugins/printThis.js', array('jquery'), '', true);
			wp_enqueue_script('smoothscroll', get_template_directory_uri() . '/javascripts/plugins/smooth-scroll.js', array('jquery'), '', true);
			wp_enqueue_script('slick', get_template_directory_uri() . '/javascripts/plugins/slick.min.js', array('jquery'), '', true);

			// Juno Scripts
			wp_enqueue_script('juno', get_template_directory_uri() . '/javascripts/juno.js', array('jquery', 'foundation', 'slicknav', 'printthis', 'smoothscroll'), '', true);
		}
	}
	add_action('wp_enqueue_scripts', 'ttg_scripts');


	// STYLESHEETS
	//---------------------------------------------------------------------------
	function ttg_styles(){
		if(!is_admin()){
			// Register Stylesheets
			wp_enqueue_style('juno', get_template_directory_uri() . '/stylesheets/css/style.css');
			wp_enqueue_style('responsive', get_template_directory_uri() . '/stylesheets/css/responsive.css');
		}
	}
	add_action('wp_enqueue_scripts', 'ttg_styles');

	function ttg_styles_admin() {
		if(is_admin()){
			wp_enqueue_style('custom-admin', get_template_directory_uri() . '/stylesheets/css/admin.css');
		}
	}
	add_action('admin_enqueue_scripts', 'ttg_styles_admin');

	// RECOMPILES THE CSS ONLY WHEN THE OPTIONS PAGE IS SAVED
	//---------------------------------------------------------------------------
	function ttg_compile_css( $post_id ) {
		$page = htmlspecialchars($_GET["page"]);
		if($page == 'acf-options-theme-styling'){
			// Compile our CSS
			include(get_template_directory() . '/stylesheets/php-sass-watcher.php');
		}
	}
	// run after ACF saves the $_POST['fields'] data
	add_action('acf/save_post', 'ttg_compile_css', 20);



	/*===========================================================================
	CUSTOM MENU WALKER FOR OUR SLIDE TARGETING
	===========================================================================*/
	class juno_menu_walker extends Walker_Nav_Menu {
		function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0){
			global $wp_query;

			// todo: $prepend and $append were not defined - setting them to blank to avoid errors for now.
			$prepend = $append = "";

			$section = get_post_type(url_to_postid($item->url));
			$sectionTitle = ($section == 'section') ? sanitize_title(get_the_title(url_to_postid($item->url))) : '';
			$liData 	= ($sectionTitle) ? ' data-magellan-arrival="' . $sectionTitle . '"' : '';

			$indent 		= ( $depth ) ? str_repeat( "\t", $depth ) : '';
			
			$class_names 	= $value = '';
			$classes 		= empty( $item->classes ) ? array() : (array) $item->classes;
			$class_names 	= join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
			$class_names 	= ' class="'. esc_attr( $class_names ) . '"';

			$output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names . $liData . '>';

			$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
			if(get_post_type(url_to_postid($item->url)) == 'section'){
				$beforehash = (!is_front_page()) ? get_site_url() . '/' : '';
				$attributes .= ! empty( $item->url )        ? ' href="' . $beforehash . '#'   . $sectionTitle .'"' : '';
			}
			else {
				$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
			}

			$item_output = $args->before;
			$item_output .= '<a data-scroll'. $attributes .'>';
			$item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
			$item_output .= '</a>';
			$item_output .= $args->after;

			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
	}


