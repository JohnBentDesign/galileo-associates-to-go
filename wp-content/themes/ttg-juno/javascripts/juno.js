jQuery(document).foundation();

//========================================================
// DOCUMENT READY
//========================================================
jQuery(document).ready(function(){

	//============================
	// Slideshow
	//============================
	jQuery('.slideshow .slider').slick({
		autoplay: true,
		autoplaySpeed: 7000,
		arrows: false,
		dots: true,
		fade: true,
		// onAfterChange: function(){
		// 	if(jQuery('.slider .slides:first-child').hasClass('slick-active')){
		// 		setTimeout(function(){
		// 			jQuery('.slider .slides:first-child').addClass('active-first');
		// 		}, 500);	
		// 	}
		// 	else {
		// 		jQuery('.slider .slides:first-child').removeClass('active-first');
		// 	}
		// },
		// onInit: function(){
		// 	setTimeout(function(){
		// 		jQuery('.slider .slides:first-child').addClass('active-first');
		// 	}, 500);
		// }
	});

	//============================
	// Smooth Scroll
	//============================
	jQuery('.jumper').each(function(){
		var nextSection = jQuery(this).next('.slide-container').attr('id');
		jQuery(this).find('a').attr('href', '#' + nextSection);
	});

	smoothScroll.init();

	jQuery('.menu-item a[href*=#]:not([href=#]), a[href*=#].next:not([href=#]), a[href*=#].back-top:not([href=#]), .slide-details a[href*=#]:not([href=#])').click(function() {

		var target = jQuery(this.hash);
		target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
		if (target.length) {
			jQuery('html,body').animate({
				scrollTop: target.offset().top
			}, 1000);
			return false;
		}
	});


	//============================
	// Slicknav
	//============================
	jQuery('nav.main ul.menu').slicknav({
		label: '',
		closeOnClick: true
	});

	//============================
	// Print
	//============================
 	jQuery('.print-me').click(function(){
 		jQuery('.print-me').parents('.popped').printThis({
			debug: true,
			importCSS: true,
			printContainer: false,
			pageTitle: '',
			removeInline: true,
			printDelay: 333,
			header: null,
			formValues: true
		});
 	});

});
