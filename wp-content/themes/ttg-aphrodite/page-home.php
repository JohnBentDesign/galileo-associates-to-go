<?php
	/*===========================================================================
	Template Name: Homepage
	=============================================================================
	Page template for Homepage. Same as page.php, just makes it easier to determine
	when to show slide custom fields on editor page.
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
	
	// SECTION- REPEATS //
	get_template_part('parts/page/section', 'repeat');

	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>