<?php
	/*===========================================================================
	PAGE
	=============================================================================
	Default style used for all pages
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
	
	// SECTION- REPEATS //
	get_template_part('parts/page/section', 'repeat');

	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>