<?php 
	/*===========================================================================
	PAGE CATEGORIES
	===========================================================================*/

	add_action( 'init', 'register_taxonomy_categories' );

	function register_taxonomy_categories() {

		$labels = array( 
			'name' => _x( 'Categories', 'categories' ),
			'singular_name' => _x( 'Category', 'categories' ),
			'search_items' => _x( 'Search Categories', 'categories' ),
			'popular_items' => _x( 'Popular Categories', 'categories' ),
			'all_items' => _x( 'All Categories', 'categories' ),
			'parent_item' => _x( 'Parent Category', 'categories' ),
			'parent_item_colon' => _x( 'Parent Category:', 'categories' ),
			'edit_item' => _x( 'Edit Category', 'categories' ),
			'update_item' => _x( 'Update Category', 'categories' ),
			'add_new_item' => _x( 'Add New Category', 'categories' ),
			'new_item_name' => _x( 'New Category', 'categories' ),
			'separate_items_with_commas' => _x( 'Separate categories with commas', 'categories' ),
			'add_or_remove_items' => _x( 'Add or remove categories', 'categories' ),
			'choose_from_most_used' => _x( 'Choose from the most used categories', 'categories' ),
			'menu_name' => _x( 'Categories', 'categories' ),
		);

		$args = array( 
			'labels' => $labels,
			'public' => true,
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'show_tagcloud' => true,
			'show_admin_column' => false,
			'hierarchical' => true,
			'rewrite' => false,
			'query_var' => true
		);

		register_taxonomy( 'categories', array('page'), $args );
	}