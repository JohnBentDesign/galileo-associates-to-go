<?php
	/*===========================================================================
	APHRODITE: SHOP BACKDROP TAXONOMY
	===========================================================================*/
	
	if( function_exists('register_field_group') ):

	register_field_group(array (
		'key' => 'group_5464fe93270a2',
		'title' => '[Aphrodite] Shop: Backdrop (Taxonomy)',
		'fields' => array (
			array (
				'key' => 'field_5464fea57d23c',
				'label' => '',
				'name' => '',
				'prefix' => '',
				'type' => 'message',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '<h2 style="background-color: #0074a2; padding: 5px 10px; color: #ffffff">BACKDROP</h2>',
			),
			array (
				'key' => 'field_5464feb37d23e',
				'label' => 'Overlay Display',
				'name' => 'backdrop_overlay',
				'prefix' => '',
				'type' => 'true_false',
				'instructions' => 'Check if overlay should be displayed',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 1,
			),
			array (
				'key' => 'field_5464fecf7d23f',
				'label' => 'Content',
				'name' => 'backdrop_title',
				'prefix' => '',
				'type' => 'true_false',
				'instructions' => 'If title is hidden, you will need to manually add an H1 title to the content body of the page.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'taxonomy',
					'operator' => '==',
					'value' => 'product_cat',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'seamless',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));

	endif;