<?php
	/*===========================================================================
	APHRODITE: PAGE EXCERPT
	===========================================================================*/
	
	if( function_exists('register_field_group') ):

	register_field_group(array (
		'key' => 'group_5423169d8e599',
		'title' => '[Aphrodite] Page: Page Excerpts',
		'fields' => array (
			array (
				'key' => 'field_534edea946af5',
				'label' => '',
				'name' => '',
				'prefix' => '',
				'type' => 'message',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '<h2 style="background-color: #0074a2; padding: 5px 10px; color: #ffffff">EXCERPT DETAILS</h2>',
			),
			array (
				'key' => 'field_534eded746af6',
				'label' => 'Excerpt Content',
				'name' => 'excerpt_content',
				'prefix' => '',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 1,
				'tabs' => 'all',
			),
			array (
				'key' => 'field_534edee946af7',
				'label' => 'Excerpt Link',
				'name' => 'excerpt_link',
				'prefix' => '',
				'type' => 'text',
				'instructions' => 'If this excerpt is selected to be placed somewhere, it will link to this full page.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => 'View More',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'gallery',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'seamless',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => array (
		),
	));

	endif;




