<?php
	/*===========================================================================
	APHRODITE: MAIN MENU
	===========================================================================*/
	
	if( function_exists('register_field_group') ):

	register_field_group(array (
		'key' => 'group_54231699a5f34',
		'title' => '[Aphrodite] Menu: Custom Menu',
		'fields' => array (
			array (
				'key' => 'field_53061b01b798c',
				'label' => 'Menu Links',
				'name' => 'menu_links',
				'prefix' => '',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'min' => '',
				'max' => '',
				'layout' => 'row',
				'button_label' => 'Add Menu Link',
				'sub_fields' => array (
					array (
						'key' => 'field_53061b86b798d',
						'label' => 'Main Menu Link',
						'name' => 'menu_link',
						'prefix' => '',
						'type' => 'page_link',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'post_type' => '',
						'taxonomy' => '',
						'allow_null' => 0,
						'multiple' => 0,
					),
					array (
						'key' => 'field_530ce1fbb8bd6',
						'label' => 'Main Menu Link Text',
						'name' => 'menu_text',
						'prefix' => '',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_53061d53b7990',
						'label' => 'Does this menu link have sub-links?',
						'name' => 'dropdown_display',
						'prefix' => '',
						'type' => 'true_false',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => '',
						'default_value' => 0,
					),
					array (
						'key' => 'field_53061bc2b798e',
						'label' => 'Dropdown Links',
						'name' => 'dropdown_links',
						'prefix' => '',
						'type' => 'repeater',
						'instructions' => 'Add dropdown links here. Some notes:
	<ul>
	<li>
	Menu Link
	<ol>
	<li>Select a page, post, gallery, product, media, etc. from a list of existing links found on this website</li>
	<li>If a "Menu Title" is provided, it will overwrite the selected link\'s default title</li>
	<li><strong>If this field is filled in, it will take priority over a custom link, and the custom link will be ignored</strong></li>
	</ol>
	</li>
	<li>
	Custom Menu Link
	<ol>
	<li>Copy and paste the FULL URL (that means it includes the http://) the field</li>
	<li>A title MUST be filled in</li>
	<li>If your custom link isn\'t working, make sure that the "Menu Link" is not filled in</li>
	</ol>
	</li>
	</ul>',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_53061d53b7990',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'min' => '',
						'max' => '',
						'layout' => 'table',
						'button_label' => 'Add Link',
						'sub_fields' => array (
							array (
								'key' => 'field_544aaf95442c3',
								'label' => 'Menu Link',
								'name' => 'link',
								'prefix' => '',
								'type' => 'page_link',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => 30,
									'class' => '',
									'id' => '',
								),
								'post_type' => '',
								'taxonomy' => '',
								'allow_null' => 1,
								'multiple' => 0,
							),
							array (
								'key' => 'field_5458007bb394c',
								'label' => 'Custom Menu Link',
								'name' => 'custom_link',
								'prefix' => '',
								'type' => 'text',
								'instructions' => 'Copy and paste FULL URLs here',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => 30,
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'readonly' => 0,
								'disabled' => 0,
							),
							array (
								'key' => 'field_544ab00a33752',
								'label' => 'Menu Title',
								'name' => 'title',
								'prefix' => '',
								'type' => 'text',
								'instructions' => 'If not filled in, it will use the default title of the selected link. Custom Menu Links REQUIRE this.',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => 30,
									'class' => '',
									'id' => '',
								),
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'maxlength' => '',
								'readonly' => 0,
								'disabled' => 0,
							),
						),
					),
					array (
						'key' => 'field_53061c1cb798f',
						'label' => 'Dropdown Image',
						'name' => 'dropdown_image',
						'prefix' => '',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_53061d53b7990',
									'operator' => '==',
									'value' => '1',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'preview_size' => 'TTG Medium Thumbnail',
						'library' => 'all',
						'return_format' => 'array',
					),
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-main-menu',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'seamless',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));

	endif;