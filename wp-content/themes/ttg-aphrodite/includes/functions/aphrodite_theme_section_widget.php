<?php
	/*===========================================================================
	SECTION- Widget Block
	===========================================================================*/
	if(!function_exists('aphrodite_section_widget')){
		function aphrodite_section_widget($array, $order, $counter = null, $vary = null){
			// Simplify our variables
			$display 		= $array['display'];
			$widget 		= $array['widget'];
			$widgetStyle 	= $widget['style'];
			$widgetCustom 	= $widget['custom'];
			$widgetDisplay 	= $widget['display'];
			$styleOutput 	= $colorBG = $colorText = $colorBGStyle = $colorTextStyle = $dataColorText = $dataColorBG = $bgImage = $bgRepeat = $bgPosition = '';

			// Widget Styling
			if($widgetStyle['color']['change']){
				$widgetColor	= $widgetStyle['color'];
				$colorBG 		= ($widgetColor['color-bg'] != 'custom') ? get_field('color_' . $widgetColor['color-bg'], 'options') : $widgetColor['color-bg-custom'];
				$colorText		= ($widgetColor['color-text'] != 'custom') ? get_field('color_' . $widgetColor['color-text'], 'options') : $widgetColor['color-text-custom'];
				$colorBGStyle 	= 'background-color: ' . $colorBG . ';';
				$dataColorText 	= ($colorText) ? ' data-color-text="' . $colorText . '"' : '';
				$dataColorBG	= ($colorBG) ? ' data-color-bg="' . $colorBG . '"' : '';
			}
			if($widgetStyle['bg']['display']){
				$widgetBG		= $widgetStyle['bg'];
				$bgImage 		= pantheon_display_post_field_image($widgetBG['image'], 'large', 'style-add', false);
				$bgRepeat		= ($bgImage && $widgetBG['image-repeat']) ? 'background-repeat: ' . $widgetBG['image-repeat'] . '; ' : 'background-repeat: repeat; ';
				$bgPosition		= ($bgImage && $widgetBG['image-position']) ? 'background-position: ' . $widgetBG['image-position'] . '; ' : 'background-position: center; ';
				$bgSize 		= ($bgImage && $widgetBG['image-size']) ? 'background-size: ' . $widgetBG['image-size'] . '; ' : 'background-size: auto; ';
			}
			if($widgetStyle['color']['change'] || $widgetStyle['bg']['display']){
				$styleOutput = ' style="' . $colorBGStyle . $colorText . $bgImage . $bgRepeat . $bgPosition . $bgSize . '"';
			}

			// Set our column size if it's not full width
			$columnPull = ( ($counter == 1) && ($order != 'start-widget') ) ? ' large-push-9' : '';
			$columnSize = ($widgetDisplay['full']) ? '' : ' large-3 flush' . $columnPull;

			// Display if: we're set to display the widget at all (we need to make sure this is an array as well, or we'll throw an error)
			if( (gettype($display['parts']) == 'array') && in_array('widget', $display['parts']) ){
				
				// Check which side of the content this widget is being displayed so we can flush it against the browser's edge
				$last 	= ($order == 'start-image') ? ' last' : '';
				$full 	= ($widgetDisplay['full']) ? ' full' : '';
?>
				<div class="type-widget columns<?= $columnSize . $full; ?>">
					<div class="widget<?= $last . $full . $vary . ' widget-' . $widgetDisplay['type']; ?>"<?= $styleOutput . $dataColorText . $dataColorBG; ?>>
						<?php
							// CUSTOM OR DEFAULT WIDGET
							//======================================
							if( ($widgetDisplay['type'] == 'custom') || ($widgetDisplay['type'] == 'default') ){
								$widgetDefault 		= ($widgetDisplay['type'] == 'default') ? true : false;
								$widgetContent 		= ($widgetDefault) ? get_field('widget_content', 'options') : $widgetCustom['content'];
								$widgetImage 		= ($widgetDefault) ? pantheon_display_post_field_image(get_field('widget_image', 'options'), 'thumbnail', 'image', false) : $widgetCustom['image']['image'];
								$widgetImageSide 	= ($widgetDefault) ? get_field('widget_image_side', 'options') : $widgetCustom['image']['side'];
								$widgetImageAlign	= ($widgetCustom['image']['align']) ? ' align-' . $widgetCustom['image']['align'] : '';
								$widgetCTA 			= ($widgetDefault) ? pantheon_display_post_cta(array('prefix' => 'widget_', 'options' => true, 'echo' => false)) : $widgetCustom['cta'];
						?>
								<div class="row">
									<?php
										// Column Pull/Push Variables 
										$columnImage = $columnContent = '';

										// Display widget image if there is one
										if($widgetCustom['image']['display'] || ($widgetDefault && get_field('widget_image_display', 'options')) ):
											
											// We need to change the order of things if its set to
											$columnImage = $columnContent = '';
											if( ((($order == 'start-image') && ($widgetImageSide == 'outer')) || (($order == 'start-widget') && ($widgetImageSide == 'inner'))) &&  $widgetDisplay['full']){
												$columnImage = ' medium-3 push-9 last' . $widgetImageAlign;
												$columnContent = ' medium-9 pull-3';
											}
											elseif($widgetDisplay['full']) {
												$columnImage = ' medium-3' . $widgetImageAlign;
												$columnContent = ' medium-9';
											}
											elseif( (($order == 'start-image') && ($widgetImageSide == 'outer')) || (($order == 'start-widget') && ($widgetImageSide == 'inner')) ){
												$columnImage = ' medium-4 push-8 last' . $widgetImageAlign;
												$columnContent = ' medium-8 pull-4';
											}
											else {
												$columnImage = ' medium-4' . $widgetImageAlign;
												$columnContent = ' medium-8';
											}
									?>
											<div class="image columns<?= $columnImage; ?>">
												<?= $widgetImage; ?>
											</div>
									<?php endif; ?>
									<div class="columns<?= $columnContent; ?>">
										<?= $widgetContent . $widgetCTA; ?>
									</div>
								</div>

						<?php
							}
							
							// SHARE WIDGET (default for intro section)
							//======================================
							elseif($widgetDisplay['type'] == 'share'){
								echo '<h3>Share This</h3>';
								global $post;
								pantheon_display_social_share('widget', 'post', $post, get_field('backdrop_content', $post->ID), get_field('backdrop_image', $post->ID));
							}

							// GALLERY
							//======================================
							elseif($widgetDisplay['type'] == 'gallery'){
								$widgetGallery 	= $widget['gallery'];
								$gallery 		= $widgetGallery['display'];

								// Featured Image
								pantheon_display_post_featured_image($gallery->ID, 'TTG Featured Image', true, 'image');
								// Title
								echo '<h3>' . $gallery->post_title . '</h3>';
								// Excerpt
								if($widgetGallery['excerpt']){
									if($gallery->post_excerpt != ''){
										echo $gallery->post_excerpt;
									}
									else {
										echo wpautop(wp_trim_words($gallery->post_content, 35, '...'));
									}
								}
								// Gallery Link
								echo '<a href="' . get_permalink($gallery->ID) . '" class="button">View Gallery</a>';

							}

							// TESTIMONIAL
							//======================================
							elseif($widgetDisplay['type'] == 'testimonial'){
								// Find our testimonials and output them
								$widgetTest = $widget['testimonial'];
								$picked 	= $widgetTest['pick'];
								$randomCat	= $widgetTest['random-cat'];
								$randomNum	= $widgetTest['random-number'];

								echo '<h3><span class="ring-box"><span class="diamond"></span><span class="ring">&#8220;</span></span>Testimonials</h3>';

								// Make this a slider if we have more than one testimonial
								if( (count($picked) > 1) || ($randomNum > 1) ){
									echo '<ul class="testimonials" data-orbit data-options="slide_number: false; timer: false; navigation_arrows: false;">';
								}

								// If set to PICKED //
								if(($widgetTest['type'] == 'pick') && $picked){
									$counter = 1;

									foreach($picked as $post):
										setup_postdata($post);

										// Testimonial Variables
										$content 	= get_field('testimonial', $post->ID);
										$author 	= get_field('testimonial_author', $post->ID);
										$location 	= get_field('testimonial_location', $post->ID);
										$comma 		= ($author && $location) ? ', ' : '';
										$credit 	= ($author || $location) ? '<p class="credit">' . $author . $comma . $location . '</p>' : '';

										// Output content; Use list item if there's more than one testimonial
										if(count($picked) > 1){
											echo '<li data-orbit-slide="testimonial-' . $counter++ . '">';
										}

										echo $content . $credit;

										if(count($picked) > 1){
											echo '</li>';
										}

									endforeach;
									wp_reset_postdata();
								}

								// If set to RANDOM //
								if(($widgetTest['type'] == 'random') && $randomCat && $randomNum){
									$grabTerms = array();

									// Get our taxonomy IDs as integers
									foreach($randomCat as $cat){
										array_push($grabTerms, $cat->term_taxonomy_id);
									}

									// Setup a query to run through these posts. We'll randomly select X number of posts from X terms
									$testimonialArgs = array( 
										'tax_query' 		=> array(
											'relation'		=> 'AND',
												array(
										        	'taxonomy' 	=> 'testimonial-types',
										        	'field' 	=> 'id',
										        	'terms' 	=> $grabTerms,
										        	'operator' 	=> 'IN'
												),
										),
										'post_type' 		=> 'testimonial',
										'posts_per_page'	=> $randomNum,
										'orderby' 			=> 'rand'
									);
									$testPosts = new WP_Query( $testimonialArgs );
									
									// The Loop
									if ( $testPosts->have_posts() ) :
										while ( $testPosts->have_posts() ) : $testPosts->the_post();
																			// Testimonial Variables
										$content 	= get_field('testimonial', $post->ID);
										$author 	= get_field('testimonial_author', $post->ID);
										$location 	= get_field('testimonial_location', $post->ID);
										$comma 		= ($author && $location) ? ', ' : '';
										$credit 	= ($author || $location) ? '<p class="credit">' . $author . $comma . $location . '</p>' : '';
										
										// Output content; Use list item if there's more than one testimonial
										if($randomNum > 1){
											echo '<li data-orbit-slide="testimonial-' . $counter++ . '">';
										}

										echo $content . $credit;

										if(count($picked) > 1){
											echo '</li>';
										}

										endwhile;
									endif;
									wp_reset_postdata();
								}

								if( (count($picked) > 1) || ($randomNum > 1) ){
									echo '</ul>';
								}

								// Display Links
								$testArchive 	= get_field('widget_testimonial_page', 'options');
								$testForm 		= get_field('widget_testimonial_form', 'options');

								if($testArchive) { echo '<a href="' . $testArchive . '" class="button more">View More Stories</a>&nbsp;'; }
								if($testForm) { echo '<a href="' . $testForm . '" class="button share">Share Your Story &raquo;</a>'; }
							}	
						?>

					</div>
				</div>
<?php
			}
		}
	}