<?php
	/*===========================================================================
	SECTION- Content Block
	===========================================================================*/
	if(!function_exists('aphrodite_section_content')){
		function aphrodite_section_content($array, $order, $counter = null){

			// Simplify our variables

			// Display Settings
			$display 		= $array['display'];
			$displayType	= $display['type'];
			$displayFill	= $display['fill'];
			$displayIsArray = (gettype($display['parts']) == 'array') ? true : false;

			// Content Types
			$content 		= $array['content'];
			$contentInfo 	= $content['content'];
			$gallery		= $content['gallery'];
			$slider 		= $content['slider'];
			$contentBG 		= $content['content']['bg']['display'];
			
			// Display Columns
			$displayImage 	= ($displayIsArray && in_array('image', $display['parts']) && ($display['type'] == 'content') && (!$contentBG)) ? true : false;
			$displayWidget 	= ($displayIsArray && in_array('widget', $display['parts'])) ? true : false;
			$displayContent = ($display['type'] == 'content') ? true : false;
			
			// Count our columns
			if($displayImage && $displayWidget) { $columnCount = 3; }
			else if( ($displayImage || $displayWidget) && ($contentBG || !$displayContent) ) { $columnCount = 4; }
			else if($displayImage || $displayWidget) { $columnCount = 2; }
			else { $columnCount = 1; }

			// Get our large grid size depending on if it needs to fill or offset empty space
			$columnSize = '';

			// Add a fill class just in case we need it for something
			if($displayFill){
				$columnSize .= ' fill';
			}
			// If set to fill and is only one column, make it full width
			if($displayFill && ($columnCount == 1) ){
				$columnSize .= ' large-12';
			}
			// If (we have three columns OR not set to fill) AND (column is just plain content)
			if( (($columnCount == 3) || !$displayFill) && $displayContent && !$contentBG ){
				$columnSize .= ' large-6';
			}
			// If (we have two columns AND set to fill space) OR (column is anything but plain content)
			if( (($columnCount == 2) && $displayFill) || ($contentBG || !$displayContent) ){
				$columnSize .= ' large-9';
			}
			// If we have an image, it needs to always be side-by-side to the content
			if($displayImage){
				$columnSize .= ' medium-9';
			}
			// If (doesn't have image and is supposed to start with image OR doesn't have widget and supposed to start with widget) AND not set to fill AND not the first section
			if( ((($order == 'start-image') && !$displayImage) || (($order == 'start-widget') && !$displayWidget)) && !$displayFill && ($columnCount != 4)){
				$columnSize .= ' large-offset-3';
			}
			// If (doesn't have image and is supposed to start with image OR doesn't have widget and supposed to start with widget) AND not set to fill AND not the first section
			if( ($counter == 1) && ((($order == 'start-image') && $displayImage) || (($order == 'start-widget') && $displayWidget)) && !$displayFill && ($columnCount == 2)){
				$columnSize .= ' medium-push-3';
			}
			// If (first section) AND (doesn't have image and is supposed to start with image OR doesn't have widget and supposed to start with widget) AND anything but plain content
			if( ($counter == 1) && ($columnCount == 4) && ((($order == 'start-image') && !$displayImage) || (($order == 'start-widget') && !$displayWidget))){
				$columnSize .= ' large-pull-3';
			}
			// If (first section) AND (image supposed to start) AND (has three columns)
			if( ($counter == 1) && ($order == 'start-image') && ($columnCount == 3) ){
				$columnSize .= ' medium-push-3 large-push-0';
			}
			// If (first section) AND (image supposed to start) AND (is fill)
			if( ($counter == 1) && ($order == 'start-image') && ($displayFill) ){
				$columnSize .= ' medium-push-3';
			}

			// Mark this as the end of the line if we don't have a block on the right (otherwise it will float right)
			$end 	= ( (($order == 'start-image') && !$displayWidget) || ($order == 'start-widget' && !$displayImage) ) ? ' end' : '';

			// Display if: widget isn't full width AND we're set to display the content at all (we need to make sure this is an array as well, or we'll throw an error)
			if( (gettype($display['parts']) == 'array') && in_array('content', $display['parts']) && !$array['widget']['display']['full'] ){

				// GALLERY
				//================================================
				if($displayType == 'gallery'){
					$post_object = $gallery['display'];
					
					if($post_object):
						$post = $post_object;
						setup_postdata($post); 

						// We're going to "cheat" the positioning of the gallery here to keep things DRY...
						// But it's all right that the gallery falls below the content on mobile regardless of which side its on
						$pull = ($order == 'start-image') ? ' pull-8' : '';
						$push = ($order == 'start-image') ? ' push-4' : '';
						$first 	= ($order == 'start-image') ? ' first' : '';
			?>
						<div class="type-content columns <?= $columnSize . $end; ?>">
							<div class="gallery row<?= $first; ?>">

								<div class="medium-8 columns<?= $push; ?>">
									<h2><?= $post->post_title; ?></h2>
									<?php
										// Output Description // 
										if($gallery['desc'] == 'full') {
											echo wpautop($post->post_content);
										}
										elseif( ($gallery['desc'] == 'excerpt') && ($post->post_excerpt != '') ) {
											echo $post->post_excerpt;
										}
										else{
											echo wpautop(wp_trim_words($post->post_content, 60, '...'));
										}
									?>
									<a href="<?= get_permalink($post->ID); ?>" class="button">View Gallery</a>
								</div>

								<div class="flush medium-4 columns<?= $pull . $first; ?>">
									<?php
										// Get gallery images
										$galleryImage = get_field('gallery_images', $post->ID);

										if($gallery['type'] == 'featured'){
											// Output Featured Image
											$first = ($order == 'start-image') ? ' first' : '';
											echo '<div class="image' . $first . '">';
											pantheon_display_post_featured_image($post->ID, 'TTG Medium Thumbnail', false, 'image');
											echo '</div>';
										}
										elseif( ($gallery['type'] == 'slideshow') && (count(get_field('gallery_images', $post->ID)) > 1) ){
											// Output our Slider
											echo '<div class="int-slider">';
											foreach($galleryImage as $image){
												echo '<div>';
												pantheon_display_post_field_image($image, 'TTG Medium Thumbnail', 'image');
												echo '</div>';
											}
											echo '</div>';
										}
										elseif( ($gallery['type'] == 'slideshow') && (count(get_field('gallery_images', $post->ID)) == 1) ){
											// If we only have one image in our gallery, no need for a slider.
											foreach($galleryImage as $image){
												pantheon_display_post_field_image($image, 'TTG Medium Thumbnail', 'image');
											}
										}
									?>
								</div>

							</div>
						</div>
			<?php 		

						if(!function_exists('aphrodite_theme_section_gallery')){
							function aphrodite_theme_section_gallery(){
								echo '<div class="flush large-4 columns">';
								if($gallery['type'] == 'featured'){
									$first = ($order == 'start-widget') ? ' first' : '';
									echo '<div class="image' . $first . '">';
									pantheon_display_post_featured_image($post->ID, 'TTG Medium Thumbnail', false, 'image');
									echo '</div>';
								}
								else {}
								echo '</div>';
							}
						}
						wp_reset_postdata();
					endif;
				}

				// PAGE SLIDER
				//================================================
				elseif( ($displayType == 'slider') && $slider) {
					// We're going to "cheat" the positioning of the gallery here to keep things DRY...
					// But it's all right that the gallery falls below the content on mobile regardless of which side its on
					$pull 	= ($order == 'start-image') ? ' pull-8' : '';
					$push 	= ($order == 'start-image') ? ' push-4' : '';
					$first 	= ($order == 'start-image') ? ' first' : '';
			?>
					<div class="type-content columns <?= $columnSize . $end; ?>">
						<div class="slider-pages row<?= $first; ?>">
							<div class="columns">
								<div class="int-slider">

									<?php
										foreach($slider['pages'] as $slide):

											// Our Post Variables
											$titleRaw 	= $slide->post_title;
											$title 		= '<h2>' . $titleRaw . '</h2>';
											$content 	= (get_field('excerpt_content', $slide->ID)) ? get_field('excerpt_content', $slide->ID) : wpautop(wp_trim_words($slide->post_content, 60, '...'));	
											$image 		= pantheon_display_post_featured_image($slide->ID, 'TTG Medium Thumbnail', null, 'image', false);
											$link		= (get_field('excerpt_link', $slide->ID)) ? '<a href="' . get_permalink($slide->ID) . '" class="button">' . get_field('excerpt_link', $slide->ID) . '</a>' : '';
											$excerptCol = ($image) ? 'medium-8' : 'medium-12';
											$excerptPush = ($image) ? $push : '';
									?>
											<div class="row">
												<div class="excerpt-content columns <?= $excerptCol . $excerptPush; ?>">
													<?= $title . $content . $link; ?>
												</div> 
												<?php if($image):?>
													<div class="excerpt-image medium-4 columns <?= $pull . $first; ?>" data-equalizer-watch>
														<?= $image; ?>
													</div>
												<?php endif; ?>
											</div>
									<?php endforeach; ?>

								</div>
							</div>
						</div>
					</div>

			<?php					
					wp_reset_postdata();
				}

				// CONTENT
				//================================================
				elseif($displayType == 'content') {
					// Background Image
					$first 		= ($order == 'start-image') ? ' first' : '';
					$contentBG 	= $contentInfo['bg'];
					$bgDisplay 	= $contentBG['display'];
					$bgImage 	= pantheon_display_post_field_image($contentBG['image'], 'large', 'style-add', false);
					$bgPosition	= $contentBG['position'];
					if($first){
						$bgPositionFull = 'left';
						switch ($bgPosition){
							case 'top': $bgPositionFull .= ' top'; break;
							case 'center': $bgPositionFull .= ' center'; break;
							case 'bottom': $bgPositionFull .= ' bottom'; break;
						}
					}
					else{
						$bgPositionFull = 'right';
						switch ($bgPosition){
							case 'top': $bgPositionFull .= ' top'; break;
							case 'center': $bgPositionFull .= ' center'; break;
							case 'bottom': $bgPositionFull .= ' bottom'; break;
						}
					}
					$bgStyle 	= ($bgDisplay) ? ' style="' . $bgImage . ' background-position: ' . $bgPositionFull . ';"' : '';
					$bgClass 	= ($bgDisplay) ? ' has-bg' : '';
			?>
					<div class="type-content columns <?= $columnSize . $end . $bgClass . $first; ?>"<?= $bgStyle; ?>>
						<div class="content">
							<?php echo $contentInfo['title'] . $contentInfo['content'] . $contentInfo['cta']; ?>
						</div>
					</div>
			<?php
				}
			}
		}
	}