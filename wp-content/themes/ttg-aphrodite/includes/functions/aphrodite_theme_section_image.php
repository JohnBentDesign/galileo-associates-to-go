<?php
	/*===========================================================================
	SECTION- Image Block
	===========================================================================*/
	if(!function_exists('aphrodite_section_image')){
		function aphrodite_section_image($array, $order, $counter = null){
			$display 	= $array['display'];
			$pullColumn = ( ($counter == 1) && ($order == 'start-image') ) ? ' medium-pull-9' : ''; 

			// Display if: if the widget isn't set to full width AND if the content section isn't a gallery AND if we're set to display the image at all (we need to make sure this is an array as well, or we'll throw an error)
			if( (gettype($display['parts']) == 'array') && in_array('image', $display['parts']) && !$array['widget']['display']['full'] && ($array['display']['type'] == 'content') && (!$array['content']['content']['bg']['display'])){
				
				// Check which side of the content this image is being displayed so we can flush it against the browser's edge
				$last = ($order == 'start-image') ? ' first' : ' ';
?>
				<div class="type-image medium-3 columns flush<?= $pullColumn; ?>">
					<div class="image<?= $last; ?>">
						<?= $array['image']; ?>
					</div>
				</div>
<?php
			}
		}
	}