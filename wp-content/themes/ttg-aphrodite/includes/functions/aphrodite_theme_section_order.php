<?php
	/*===========================================================================
	SECTION- Order
	===========================================================================*/
	if(!function_exists('aphrodite_sections')){
		function aphrodite_sections($array, $ID = null, $counter = null){
			global $displayOrder;
			global $widgetVary;

			// Simplify our variables
			$display 		= $array['display'];
			$content 		= $array['content'];
			$widgetFull 	= $array['widget']['display']['full'];
			$ID 			= ($ID) ? ' id="' . $ID . '"' : '';
			$counterClass 	= ($counter) ? ' section-' . $counter : '';
			$widgetVary 	= ( ($widgetVary == ' vary-off') && ($counter != 1) ) ? ' vary-on' : ' vary-off';

			// Figure out our ordering
			if(!$widgetFull){
				$displaySide 	= (!$display['side'] || ($display['side'] == 'default') ) ? 'default' : $display['side'];
				if( ($displaySide == 'default') && ($counter == 1) ){
					$displayOrder = 'start-image';
				}
				else{
					$displayOrder 	= ($displaySide != 'default') ? $displaySide : $displayOrder;
				}
			}
			
			// Display if: we have any content
			echo '<div class="section row' . $counterClass . ' ' . $displayOrder . '"' . $ID . '>';

				if($widgetFull){
					aphrodite_section_widget($array, $displayOrder, $counter, $widgetVary);
				}
				elseif( ($counter == 1) || (($displayOrder == 'start-widget') && ( ($content['content']['content'] != '') || ($display['type'] == 'gallery') || ($display['type'] == 'slider') ) ) ){
					aphrodite_section_widget($array, $displayOrder, $counter, $widgetVary);
					aphrodite_section_content($array, $displayOrder, $counter);
					aphrodite_section_image($array, $displayOrder, $counter);
					$displayOrder 	= ( ($counter == 1) && ($displayOrder == 'start-image')) ? 'start-widget' : 'start-image';
				}
				elseif( ($displayOrder == 'start-image') && ( ($content['content']['content'] != '') || ($display['type'] == 'gallery') || ($display['type'] == 'slider') ) ) {
					aphrodite_section_image($array, $displayOrder, $counter);
					aphrodite_section_content($array, $displayOrder, $counter);
					aphrodite_section_widget($array, $displayOrder, $counter, $widgetVary);
					$displayOrder 	= 'start-widget';
				}

			echo '</div>';

		}
	}