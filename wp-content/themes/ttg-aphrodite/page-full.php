<?php
	/*===========================================================================
	Template Name: Full Width
	=============================================================================
	Full width page with no sidebar
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>
	<div class="container">
		<div class="row">
			<article class="post full">
				<div class="columns">
					<?php 
						if(have_posts()):
							while(have_posts()): the_post();
								
								the_content();

							endwhile;
						endif;
					?>	
				</div>		
			</article>

		</div>
	</div>

<?php
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>