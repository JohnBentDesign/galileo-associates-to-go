<?php
	/*===========================================================================
	HEADER- BLOG
	=============================================================================
	Blog header with blog header and breadcrumbs
	*/

	// Don't display this if we aren't on the right types of pages
	if(is_home() || is_search() || is_single() || is_archive()):
?>
		<header class="blog">

			<div class="row">
				<div class="title medium-4 push-4 columns">
					<?php
						// What did the user search for (if search page)?
						if(is_search()){
							echo '<h3>Search Results for "' . get_search_query() . '"</h3>';
						}
						// Or what category are we currently viewing?
						else if(is_category()){
							echo '<h3>Category for "' . single_cat_title('', false) . '"</h3>';	
						}
						else if(is_tag()){
							echo '<h3>Term archive for "' . single_term_title('', false) . '"</h3>';	
						}
						else if(is_day()){
							echo '<h3>Daily archives for ' . get_the_date() . '</h3>';
						}
						else if(is_month()){ 
							echo '<h3>Monthly archives for ' . get_the_date('F, Y') . '</h3>';
						}
						else if(is_year()){
							echo '<h3>Yearly archives for ' . get_the_date('Y') . '</h3>';
						}
						else if( ($post->post_type == 'gallery') || is_post_type_archive( 'gallery' ) ){
							echo '<h3>Gallery</h3>';
						}
						else {
							echo '<h3>Blog</h3>';
						}
					?>
				</div>
				<div class="category medium-4 pull-4 columns">
					<?php 
						if( ($post->post_type == 'gallery') || is_post_type_archive( 'gallery' ) ):
							function get_terms_dropdown($taxonomies, $args){
								$myterms 	= get_terms($taxonomies, $args);
								$output 	= '<select id="cat">';
								$output 	.= '<option value="">Select Category</option>';
								foreach($myterms as $term){
									$root_url 		= get_bloginfo('url');
									$term_taxonomy	= $term->taxonomy;
									$term_slug		= $term->slug;
									$term_name 		= $term->name;
									$link 			= $root_url . '/' . $term_taxonomy . '/' .$term_slug;
									$output 		.= '<option value="' . $link . '">' . $term_name . '</option>';
								}
								$output 	.= '</select>';
								return $output;
							}

							$taxonomies = array('gallery-categories');
							$args 		= array('orderby'=>'count','hide_empty'=>true);
							echo get_terms_dropdown($taxonomies, $args);
					?>
						<script type="text/javascript">
							var dropdown = document.getElementById("cat");
							function onCatChange() {
								if ( dropdown.options[dropdown.selectedIndex].value != '' ) {
									location.href = dropdown.options[dropdown.selectedIndex].value;
								}
							}
							dropdown.onchange = onCatChange;
						</script>
					<?php
						else: 
							wp_dropdown_categories(array(
								'orderby'            	=> 'name', 
								'order'              	=> 'ASC',
								'show_count'         	=> 0,
								'hide_empty'         	=> 1, 
								'taxonomy'           	=> 'category',
								'hide_if_empty'      	=> true,
								'show_option_none'		=> 'Select Category'
							));
					?>
							<script type="text/javascript">
								var dropdown = document.getElementById("cat");
								function onCatChange() {
									if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
										location.href = "<?php echo get_option('home');?>/?cat="+dropdown.options[dropdown.selectedIndex].value;
									}
								}
								dropdown.onchange = onCatChange;
							</script>
					<?php endif; ?>
				</div>
				<div class="recent-entries medium-4 columns">
					<?php
						// Link to Blog Index
						if( ($post->post_type == 'gallery') || is_post_type_archive( 'gallery' ) ){
							echo '<a href="' . get_post_type_archive_link('gallery') . '">All Galleries</a>';
						}		
						else {
							$blogIndex = (get_option('page_for_posts')) ? get_permalink(get_option('page_for_posts')) : home_url(); 	
							echo '<a href="' . $blogIndex . '">All Entries</a>';
						}

					?>
				</div>
			</div>

		</header>
<?php endif; ?>