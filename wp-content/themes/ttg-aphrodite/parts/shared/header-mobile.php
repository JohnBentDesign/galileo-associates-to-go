<?php
	/*===========================================================================
	HEADER- MAIN
	===========================================================================*/
	// Contains the logo and main navigation
?>
	<header class="main mobile">
		
		<div class="upper row">

			<?php // LOGO // ?>
			<div class="middle medium-6 large-4 large-push-4 columns">
				<a href="<?php bloginfo('home'); ?>" class="logo">
					<?php 
						if(get_field('logo_image', 'options')){
							pantheon_display_post_field_image(get_field('logo_image', 'options'), 'medium', 'image');
						}
						else {
							bloginfo('title');
						}
					?>
				</a>
			</div>

			<div class="left-side medium-6 large-4 large-pull-4 columns">
				<div class="band"></div>

				<?php
					// MOBILE CONTACT BUTTONS
					//===================================
					$phone 		= get_field('general_phone', 'options');
					$email 		= get_field('general_email_primary', 'options');
					$location	= get_field('mobile_google_link', 'options');

					// Count how many buttons
					$counter 	= 0;
					if($phone){ ++$counter; }
					if($email){ ++$counter; }
					if($location){ ++$counter; }

					if($phone || $email || $location){
						// Figure Out Button Size
						switch($counter){
							case 1:	$columnSize = '';			break;
							case 2:	$columnSize = 'small-6';	break;
							case 3:	$columnSize = 'small-4';	break;
						}

						// Get our Buttons
						$phone 		= ($phone) 		? '<a href="tel:' . $phone . '" class="ion-ios7-telephone-outline icon columns ' . $columnSize . '"></a>' : '';
						$email 		= ($email) 		? '<a href="mailto:' . $email . '" class="ion-ios7-email-outline icon columns ' . $columnSize . '"></a>' : '';
						$location 	= ($location) 	? '<a href="' . $location . '" target="_blank" class="ion-ios7-location-outline icon columns ' . $columnSize . '"></a>' : '';

						echo '<div class="mobile-buttons row">' . $phone . $email . $location . '</div>';

					}

					// CONTACT DETAILS
					//===================================
					get_template_part('parts/shared/contact');
				?>

				<div class="social-block">
					<?php
						// SOCIAL- CONNECT
						//===================================
						pantheon_display_social_connect('header');

						// SOCIAL- SHARE
						//===================================
						pantheon_display_social_share('header', 'post', $post);
					?>
				</div>
			</div>
		</div>

		<div class="lower row">

			<div class="small-6 columns collapse">
				<?php
					// MAIN NAVIGATION
					//===================================
					if(have_rows('menu_links', 'options')):
						// We need to make a mobile dropdown navigation
						$mobileDrop = array();

						while(have_rows('menu_links', 'options')): the_row();				
							// We need to figure out our active links in the header
							$menuLink 		= get_sub_field('menu_link');
							$linkInfo 		= array('url' => $menuLink, 'name' => get_sub_field('menu_text'));

							// Push link to mobile dropdown array
							array_push($mobileDrop, $linkInfo);

							if(get_sub_field('dropdown_display')):
	 			 				$links = get_sub_field('dropdown_links');

								if($links):
									foreach($links as $p):
										$title 			= $p['title'];
										$menuLink 		= $p['link'];
										$menuLinkCustom = $p['custom_link'];
										if($menuLink){
											$linkID 		= url_to_postid($menuLink);
											$blogID 		= get_option('page_for_posts');
											$titleOutput	= ($title) ? $title : get_the_title($linkID);
											$titleOutput 	= ($linkID == 0) ? get_the_title($blogID) : $titleOutput;
											$linkInfo 		= array('url' => $menuLink, 'name' => '- ' . $titleOutput);
										}
										elseif($menuLinkCustom){
											$linkInfo 		= array('url' => $menuLinkCustom, 'name' => '- ' . $title);
										}
										// Push link to mobile dropdown array
										array_push($mobileDrop, $linkInfo);
									endforeach;
								endif;

	 						endif;
						endwhile;

						// MOBILE DROPDOWN MENU //
						echo '<select ONCHANGE="location = this.options[this.selectedIndex].value;" class="tinynav">';
						echo '<option>Menu</option>';
						foreach($mobileDrop as $link) {
							echo '<option value="' . $link['url'] . '">' . $link['name'] . '</option>';
						}
						echo '</select>';
					endif;
				?>
			</div>

			<div class="small-6 columns">
				<div class="sb-search">
					<form method="get" action="<?php bloginfo('home'); ?>/">
						<input class="sb-search-input" placeholder="Search" type="text" value="" name="s">
						<input class="sb-search-submit icon ion-search" type="submit" value="&#xf21f;">
						<span class="sb-icon-search icon ion-search"></span>
					</form>
				</div>
			</div>
		</div>

		<?php
			// BACKDROP // 
			get_template_part('parts/shared/backdrop');
		?>

	</header><?php // /header.main ?>

