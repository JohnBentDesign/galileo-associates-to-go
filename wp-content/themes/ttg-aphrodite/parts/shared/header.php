<?php
	/*===========================================================================
	HEADER- MAIN
	===========================================================================*/
	// Contains the logo and main navigation
?>
	<header class="main not-mobile">
		
		<div class="header-contents">
			<div class="upper row">

				<?php // LOGO // ?>
				<div class="middle medium-6 large-4 large-push-4 columns">
					<a href="<?php echo get_bloginfo ( 'url' ); ?>" class="logo">
						<?php 
							if(get_field('logo_image', 'options')){
								pantheon_display_post_field_image(get_field('logo_image', 'options'), 'medium', 'image');
							}
							else {
								bloginfo('title');
							}
						?>
					</a>
				</div>

				<div class="left-side medium-6 large-4 large-pull-4 columns">
					<div class="band"></div>
					<?php
						// CONTACT DETAILS
						//===================================
						get_template_part('parts/shared/contact');

						// SOCIAL- CONNECT
						//===================================
						pantheon_display_social_connect('header');

						// SOCIAL- SHARE
						//===================================
						pantheon_display_social_share('header', 'post', $post);
					?>
				</div>
				<div class="right-side medium-6 large-4 columns">
					<div class="band"></div>
					<?php
						// SHOP MENU
						//===================================
						if(class_exists('jigoshop') && get_field('shop_enable', 'options')){
							echo '<div class="shop-nav">';

							$menuFooter = array(
								'theme_location'  => 'menu-shop',
								'menu'            => 'Shop Menu',
								'container'       => 'nav',
								'container_class' => 'shop',
								'menu_class'      => 'menu',
								'depth'           => 1,
								'fallback_cb'	  => false

							);
							wp_nav_menu($menuFooter);

							$cart_quantity = sprintf(_n('%d', '%d', jigoshop_cart::$cart_contents_count), jigoshop_cart::$cart_contents_count);
							echo '<span class="cart-quantity">' . $cart_quantity . '</span>';

							echo '</div>';
						}
					?>
					<?php get_template_part('parts/shared/searchform', 'header'); ?>
				</div>
			</div>

			<div class="lower row">
				<?php
					// MAIN NAVIGATION
					//===================================
					if(have_rows('menu_links', 'options')):
						echo '<nav class="main table"><ul class="menu table-row">';

						while(have_rows('menu_links', 'options')): the_row();
							$hasDropdown = (get_sub_field('dropdown_display')) ? ' class="dropdown table-cell"' : ' class="table-cell"';
							
							// We need to figure out our active links in the header
							$menuLink 		= get_sub_field('menu_link');
							$urlToID 		= url_to_postid($menuLink);
							$pageForPosts 	= get_option('page_for_posts');
							$pageID     	= get_queried_object_id();		

							$active 		= ( ($urlToID == $post->ID) || ($pageForPosts == $pageID && $urlToID == 0) ) ? ' class="active"' : ''; 			
							echo '<li' . $hasDropdown . '>';
	 						echo '<a href="' . $menuLink . '"' . $active . '>' . get_sub_field('menu_text') . '</a>';

	 						if(get_sub_field('dropdown_display')):
	 			?>
	 							<div class="sub-menu small-12 columns">
	 								
	 									<div class="small-12 columns">
	 										<span class="band"></span>
	 									</div>
	 								

	 								<div class="row">

	 									<?php
	 										// Submenu Image //
	 										if(get_sub_field('dropdown_image')):
												$dropdownObj 	= get_sub_field('dropdown_image');
												$dropdownURL 	= $dropdownObj['sizes']['TTG Medium Thumbnail'];
												$dropdownAlt 	= $dropdownObj['alt'];
										?>
												<div class="menu-image medium-3 medium-offset-1 columns">
													<img src="<?= $dropdownURL; ?>" alt="<?= $dropdownAlt; ?>" />
												</div>
										<?php endif; ?>
	 								
	 									<?php 
	 										// Submenu Links //
	 										$columnOffset = (get_sub_field('dropdown_image')) ? ' medium-7' : ' medium-7 medium-centered';
	 									?>
	 									<div class="columns end<?= $columnOffset; ?>">
	 										<h3><?= get_sub_field('menu_text'); ?></h3>

	 										<div class="links">
		 										<?php 
													$links = get_sub_field('dropdown_links');
													if($links):
														foreach($links as $p):
															$title 			= $p['title'];
															$menuLink 		= $p['link'];
															$menuLinkCustom = $p['custom_link'];
															if($menuLink){
																$linkID 		= url_to_postid($menuLink);
																$blogID 		= get_option('page_for_posts');
																$titleOutput	= ($title) ? $title : get_the_title($linkID);
																$titleOutput 	= ($linkID == 0) ? get_the_title($blogID) : $titleOutput;
																echo '<p class="clearfix"><span class="bull">&bull;&nbsp;&nbsp;</span><a href="' . $menuLink . '">' . $titleOutput . '</a></p>';
															}
															elseif($menuLinkCustom){
																echo '<p class="clearfix"><span class="bull">&bull;&nbsp;&nbsp;</span><a href="' . $menuLinkCustom . '">' . $title . '</a></p>';
															}
														endforeach;
													endif;
												?>
											</div>
										</div>

									</div>
								</div>
				<?php
	 						endif;
	 						echo '</li>';			
						endwhile;

						echo '</ul></nav>';
					endif;
				?>
			</div>
		</div>

		<?php
			// BACKDROP // 
			get_template_part('parts/shared/backdrop');
		?>

	</header><?php // /header.main ?>

