<?php
	/*===========================================================================
	SEARCH FORM
	=============================================================================
	Search form to be used across the site
	*/
?>

	<form method="get" class="searchform" action="<?php bloginfo('home'); ?>/">
		<input type="text" size="put_a_size_here" name="s" id="s" value="Write your search and hit Enter" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;"/>
		<input type="submit" id="searchsubmit" value="Search" class="btn" />
	</form>