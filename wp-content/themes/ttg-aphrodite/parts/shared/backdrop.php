<?php
	/*===========================================================================
	BACKDROP
	===========================================================================*/
	// Contains the image that will appear behind the content.

	if( !is_front_page() ):
		// Get some IDs
		global $jigoshopShopID;
		$isBlog 		= ((is_home() && get_option('page_for_posts')) || (is_single() && ($post->post_type == 'post')) || is_category() || is_day() || is_tag() || is_month() || is_year()) ? true : false; 
		$blogID 		= get_option('page_for_posts');
		$postType 		= get_post_type();
		$isProductArch 	= ($postType == 'product') && is_archive();
		$isGallery 		= ($postType == 'gallery') || (is_post_type_archive('gallery'));

		// Narrow it down even more on where we're going to be showing this
		if(!$isBlog && !$isGallery):

			// Figure out where we're pulling our backdrop from
			if(is_post_type_archive('product' )) 						{ $useID = $jigoshopShopID;}
			elseif($isProductArch)										{ 
																			$term 			= get_query_var($wp_query->query_vars['taxonomy']);
																			$tax 			= $wp_query->query_vars['taxonomy'];
																			$termObj 		= get_term_by('slug', $term, $tax);
																			$useID 			= $termObj;
																		}
			elseif($isBlog)												{ $useID = $blogID; }
			elseif($postType == 'product') 								{ $useID = $jigoshopShopID; }
			elseif( ($postType == 'post') || ($postType == 'page') )	{ $useID = $post->ID; }
			else 														{ $useID = ''; }

			// Check what we're displaying
			$imageDisplay 	= get_field('backdrop_image_display', $useID);
			$overlayDisplay = get_field('backdrop_overlay', $useID);
			$titleDisplay 	= get_field('backdrop_title', $useID);

			// Get what we're displaying
			$imageObj = $prodImage = '';

			if(($imageDisplay != 'none') || $isProductArch){
				if(!$isProductArch || is_post_type_archive('product')){
					$imageID 		= ($imageDisplay == 'default') ? 'options' : $useID;
					$imageObj 		= get_field('backdrop_image', $imageID);
				}
				else {
					$imageGet	= jigoshop_product_cat_image($termObj->term_id);
					$prodImage	= ($imageGet && !strpos($imageGet['image'], 'placeholder.png')) ? ' style="background-image: url(' . $imageGet['image'] . ')"' : '';
					$imageObj 	= '';
				}
			}
			if($overlayDisplay != 'none'){
				$overlayID 		= ($overlayDisplay == 'default') ? 'options' : $useID;
			}
			if(!$titleDisplay){
				if(!$isProductArch || is_post_type_archive('product')){
					$title 			= '<h1>' . get_the_title($useID) . '</h1>';
					$content 		= get_field('backdrop_content', $useID);
				}
				else {
					$title 			= apply_filters('jigoshop_product_taxonomy_header', '<h1>' . wptexturize( $termObj->name ) . '</h1>');
					$content 		= wpautop($termObj->description);
				}
			}

			// Show our backdrop only if we have at least ONE of the pieces that makes it
			if( ($imageDisplay != 'none') || (!$titleDisplay) || $prodImage ):
?>
			<div class="backdrop"<?= $prodImage; ?><?php pantheon_display_post_field_image($imageObj, 'TTG Featured Image', 'style'); ?>>

				<?php if(!$titleDisplay): ?>
					<div class="table">
						<div class="table-row">
							<div class="table-cell">
								<?= $title . $content; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<?php if($overlayDisplay): ?>
					<div class="overlay"></div>
				<?php endif; ?>

			</div>
<?php 
			endif;
		endif;
	endif;

	// BACKDROP - HOMEPAGE // 
	if(is_front_page() && have_rows('home_slider')):

		echo '<div class="home-slider">';

		while(have_rows('home_slider')): the_row();
			$imageObj 		= get_sub_field('image');
			$title 			= (get_sub_field('title')) ? '<h3>' . get_sub_field('title') . '</h3>' : '';
			$content 		= (get_sub_field('content')) ? get_sub_field('content') : '';
?>
				<div<?php pantheon_display_post_field_image($imageObj, 'TTG Featured Image', 'style'); ?>>
					<?php if($title || $content || $ctaOutput): ?>
						<div class="table">
							<div class="table-row">
								<div class="slide-caption table-cell">
									<?php 
										echo $title;
										echo $content;
										pantheon_display_post_cta(array('class' => 'button tiny', 'repeater' => 'sub-field'));
									?>
								</div>
							</div>
						</div>
					<?php endif; ?>
					<div class="overlay"></div>
				</div>
<?php

		endwhile;
		echo '</div>';
	endif;
?>