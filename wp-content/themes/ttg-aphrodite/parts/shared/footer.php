<?php
	/*===========================================================================
	FOOTER- MAIN
	===========================================================================*/
	// Contains the copyright and footer navigation
?>	
	<footer class="main">

		<?php // UPPER FOOTER // ?>
		<div class="upper">
			<div class="row">

				<?php // Footer Navigation ?>
				<div class="medium-9 columns">
					<?php
						$menuFooter = array(
							'theme_location'  => 'menu-footer',
							'menu'            => 'Footer Menu',
							'container'       => 'nav',
							'container_class' => 'footer',
							'menu_class'      => 'menu',
							'depth'           => 1,
							'fallback_cb'	  => false
						);
						wp_nav_menu($menuFooter);
					?>
				</div>

				<?php 
					// Socialization
				
					// Let's figure out if we have both the connect and share buttons in the header
					$maxSocialize = ( (is_array(get_field('social_button_location', 'options')) && in_array('Header', get_field('social_button_location', 'options')) ) && ( get_field('share_button_location', 'options') && in_array('Header', get_field('share_button_location', 'options')) ) ) ? ' max-socialize' : '';
				?>
				<div class="medium-3 columns<?= $maxSocialize; ?>">
					<div class="socialize">
						<?php
							// SOCIAL- CONNECT
							//===================================
							pantheon_display_social_connect('footer');

							// SOCIAL- SHARE
							//===================================
							pantheon_display_social_share('footer', 'post', $post);
						?>
					</div>

				</div>
			</div>
		</div><?php // /.upper ?>

		<?php // LOWER FOOTER // ?>
		<div class="lower">
			<div class="row" data-equalizer>

				<?php // Logo and Address ?>
				<div class="medium-6 large-3 columns" data-equalizer-watch>

					<?php // Logo ?>
					<a href="<?php bloginfo('home'); ?>" class="logo">
						<?php 
							if(get_field('logo_image', 'options')){
								pantheon_display_post_field_image(get_field('logo_image_footer', 'options'), 'medium', 'image');
							}
							else {
								bloginfo('title');
							}
						?>
					</a>

					<div class="contact-details">
						<?php 
							// Contact
							get_template_part('parts/shared/contact');

							$contactLink = (get_field('footer_contact', 'options')) ? get_field('footer_contact', 'options') : '';
							if($contactLink){
								echo '<a href="' . $contactLink . '">Contact Us</a>';
							}
						?>
					</div>
				</div>

				<?php 
					// Hours
					$footerHours = get_field('hours', 'options');
					if($footerHours):
				?>
						<div class="hour-container medium-6 columns" data-equalizer-watch>
							<h4>Hours</h4>
							<div class="hours">
								<?php while(have_rows('hours', 'options')): the_row(); ?>
									<p><span class="day"><?= get_sub_field('day'); ?></span> <?= get_sub_field('time'); ?></p>
								<?php endwhile; ?>
							</div>
						</div>
				<?php endif; ?>

				<?php 
					// Additional Info
					$footerExtra = get_field('footer_extra', 'options');
					if($footerExtra):
				?>
						<div class="extra large-3 columns" data-equalizer-watch>
							<?= $footerExtra; ?>
						</div>
				<?php endif; ?>

			</div>
		</div>

		<?php // COPYRIGHT // ?>
		<div class="copyright row">
			<div class="columns">
				<a href="#" class="top button tiny">Back to Top</a>
				<p>&copy;<?php echo date('Y') . ' '; bloginfo('name'); ?>. Site Powered by <a href="http://techtherapytogo.com/" target="_blank">Technology Therapy To Go</a>.</p>
			</div>
		</div>

	</footer><?php // /footer.main ?>