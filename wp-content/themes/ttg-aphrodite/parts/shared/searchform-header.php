<?php
	/*===========================================================================
	SEARCH FORM
	=============================================================================
	Search form to be used across the site
	*/
?>
	<div id="sb-search" class="sb-search">
		<form method="get" action="<?php bloginfo('home'); ?>/">
			<input class="sb-search-input" placeholder="Enter your search term..." type="text" value="" name="s" id="s">
			<input class="sb-search-submit icon ion-search" type="submit" value="&#xf21f;">
			<span class="sb-icon-search icon ion-search"></span>
		</form>
	</div>