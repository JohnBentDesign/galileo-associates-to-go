		<?php
			// FOOTER // 
			get_template_part('parts/shared/footer');
			
			// WP FOOTER //	
			wp_footer();

			// CUSTOM JAVASCRIPT //
			$customScript = get_field('advanced_js', 'options');
			if($customScript){
				echo $customScript;
			}
		?>

	</body>
</html>