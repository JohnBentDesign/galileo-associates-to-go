<?php
	/*===========================================================================
	SECTION- Repeater
	===========================================================================*/
	// Output all the repeating content blocks

	// Make our master array so we can cleanly loop through it later
	$sectionsMaster = array();

	// Grab each row and output it into a clean array
	if(have_rows('section_repeater')):
		while(have_rows('section_repeater')):
			// the_row();
			$row = the_row();

			// Make a master array of all our information
			$sectionArray = array(
				// DISPLAY //
				'display' 	=> array(
					'parts' 	=> get_sub_field('section_parts'),
					'side'		=> get_sub_field('section_side'),
					'fill'		=> get_sub_field('main_fill'),
					'type'		=> get_sub_field('main_type')
				),

				// IMAGE //
				'image' 	=> pantheon_display_post_field_image(get_sub_field('image_featured'), 'TTG Medium Thumbnail', 'image', false),
				
				// CONTENT //
				'content' 	=> array(
					'content' => array(
						'title' 	=> '<h2>' . get_sub_field('main_title') . '</h2>',
						'content' 	=> get_sub_field('main_content'),
						'cta' 		=> pantheon_display_post_cta(array('prefix' => 'main_', 'echo' => false, 'repeater' => 'sub-field')),
						'bg'		=> array(
							'display' 			=> get_sub_field('main_bg_display'),
							'image'				=> get_sub_field('main_bg'),
							'position'			=> get_sub_field('main_bg_position')					
						)
					),
					'gallery' => array(
						'display' 	=> get_sub_field('main_gallery_display'),
						'desc'		=> get_sub_field('main_gallery_description'),
						'type'		=> get_sub_field('main_gallery_display_type')
					),
					'slider' => array(
						'pages' 	=> get_sub_field('main_slider_pages')
					)
				),
				// WIDGET //
				'widget' 	=> array(
					// Widget Display
					'display' 	=> array(
						'type' 		=> get_sub_field('widget_type'),
						'full'		=> get_sub_field('widget_full')
					),
					// Widget Style
					'style' 	=> array(
						// Color
						'color'		=> array(
							'change' 			=> get_sub_field('widget_background'),
							'color-bg'			=> get_sub_field('widget_color_bg'),
							'color-bg-custom' 	=> get_sub_field('widget_color_bg_custom'),
							'color-text'		=> get_sub_field('widget_color_text'),
							'color-text-custom' => get_sub_field('widget_color_text_custom'),
						),
						// Background
						'bg'		=> array(
							'display'			=> get_sub_field('widget_bg_image_display'),
							'image'				=> get_sub_field('widget_bg_image'),
							'image-repeat'		=> get_sub_field('widget_bg_image_repeat'),
							'image-position'	=> get_sub_field('widget_bg_image_position'),
							'image-size'		=> get_sub_field('widget_bg_image_size')
						)
					),
					// Widget: Custom
					'custom'	=> array(
						// Content
						'content'	=> get_sub_field('widget_content'),
						'cta'		=> pantheon_display_post_cta(array('prefix' => 'widget_', 'echo' => false, 'repeater' => 'sub-field')),
						// Image and properties
						'image'		=> array(
							'display'			=> get_sub_field('widget_image_display'),
							'image'				=> pantheon_display_post_field_image(get_sub_field('widget_image'), 'thumbnail', 'image', false),
							'side'				=> get_sub_field('widget_image_side'),
							'align' 			=> get_sub_field('widget_image_alignment')
						)
					),
					// Testimonial
					'testimonial' => array(
						'type'			=> get_sub_field('testimonial_type'),
						'pick'			=> get_sub_field('testimonial_pick'),
						'random-number'	=> get_sub_field('testimonial_random_number'),
						'random-cat'	=> get_sub_field('testimonial_random_categories')
					),
					// Gallery
					'gallery' 	=> array(
						'display'	=> get_sub_field('gallery_display'),
						'excerpt'	=> get_sub_field('gallery_description'),
						'featured'	=> get_sub_field('gallery_featured')	
					)
				)
			);

			// Push this section to the master array so we can output it later--cleaner
			array_push($sectionsMaster, $sectionArray);

		endwhile;
    endif;

    // Loop through our fancy array now with our fancy function
    $counter = 1;

    if(have_rows('section_repeater')) { echo '<div class="main">'; }
    foreach($sectionsMaster as $section){
    	$title 		= ($section['content']['content']['title']) ? $section['content']['content']['title'] : 'section-' . $counter;
    	$ID 		= sanitize_title_with_dashes($title);

    	aphrodite_sections($section, $ID, $counter++);
    }
    if(have_rows('section_repeater')) { echo '</div>'; }
?>

	