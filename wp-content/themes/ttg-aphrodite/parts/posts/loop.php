<?php
	/*===========================================================================
	POST LOOP
	=============================================================================
	Blog post loop for single.php and index.php
	*/
	
	// Figure out where we are
	$listing = (is_home() || is_archive() || is_search()) ? true : false;
	if($listing){ $postOutput = ' excerpt'; }
	else if($listing == false){ $postOutput = ' full'; }

	// THE LOOP //
	if(have_posts()){

		// If it's the blog listing page, wrap our articles in a div
		if($listing){ echo '<div class="post-list">'; }

		// Start the loop
		while(have_posts()): the_post();
			if($listing){
				$title 		= '<h2><a href="' . get_permalink() . '">' . get_the_title() . '</a></h2>';
				$content 	= wpautop(get_the_excerpt());
				$imageSize 	= 'TTG Featured Image';
			}
			elseif($listing == false){
				$title 		= '<h1>' . get_the_title() . '</h1>';
				$content 	= wpautop(get_the_content());
				$imageSize 	= 'TTG Featured Image';
			}
			$hasFeatured = (has_post_thumbnail($post->ID)) ? ' has-featured' : '';
?>
			<article class="post<?= $postOutput . $hasFeatured; ?>">

				<?php // POST HEADER // ?>
				<header>

					<?php if($listing): ?>
						<div class="divider">
							<span class="diamond"></span><span class="diamond"></span><span class="diamond"></span>
						</div>
						<span class="ring-box">
							<span class="diamond"></span>
							<time datetime="<?= get_the_date('Y-m-d'); ?>" class="ring">
								<span class="date"><?= get_the_date('j'); ?></span>
								<span class="month"><?= get_the_date('M'); ?></span>
							</time>
						</span>
					<?php endif; ?>
					<?php
						// Display Title
						echo $title;

						// Display featured image next to the excerpt if blog listing page 
						if($listing)	{ pantheon_display_post_featured_image($post->ID, $imageSize, get_permalink()); }
						else 			{ pantheon_display_post_featured_image($post->ID, $imageSize); }
					?>
					<?php if(!$listing): ?>
						<span class="ring-box">
							<span class="diamond"></span>
							<time datetime="<?= get_the_date('Y-m-d'); ?>" class="ring">
								<span class="date"><?= get_the_date('j'); ?></span>
								<span class="month"><?= get_the_date('M'); ?></span>
							</time>
						</span>
					<?php endif; ?>	

					<div class="categorizing">
						<?php
							// Display Post Categories if blog listing page
							pantheon_display_post_categories($post->ID);

							// Display Post Tags if blog listing page
							pantheon_display_post_tags($post->ID);
						?>
					</div>
		
				</header>

				<?php // POST CONTENT // ?>
				<div class="the-post">

					<div class="content">
						<?php
							if($listing)	{ the_excerpt(); }
							else 			{ the_content(); }
						?>
					</div>
				</div>

			</article><?php // /.post ?>	

			<?php
				//---------------------------------
				// COMMENTS - LIST
				//---------------------------------
				$commentCount = get_comments_number();

				if(!$listing && ($commentCount > 0)):
					/*
			?>
					<div class="comment-intro">
						<h3>Comments</h3>
						<h4><?= $commentCount; ?> Response<?php if($commentCount > 1){ echo 's'; } ?> to <?= $post->post_title; ?></h4>
					</div>

					<ol class="comment-list">
						<?php
							//Gather comments for a specific page/post 
							$comments = get_comments(array(
								'post_id'	=> $post->ID,
								'status'	=> 'approve' //Change this to the type of comments to be displayed
							));

							//Display the list of comments
							wp_list_comments(array(
								'per_page' 			=> 10,
								'reverse_top_level' => false,
								'max_depth' 		=> 2
							), $comments);
						?>
					</ol>
			<?php */ endif; ?>

			<?php
				//---------------------------------
				// COMMENTS - FORM
				//---------------------------------
				if(!$listing):
			?>
					<div class="comments">
						<?php comments_template(); ?>
					</div>
			<?php endif; ?>

<?php
		endwhile;

		// Close the blog listing div
		if($listing){ 
			echo '</div>';
			get_template_part('parts/shared/pagination');
		}
	}
	else{
		echo '<p>Sorry, but nothing matched your search criteria. Please try again with different keywords.</p>';
		get_template_part('searchform');
	}
?>