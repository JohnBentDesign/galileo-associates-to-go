<?php
/**
 * Class SassWatcher
 *
 * This simple tool compiles all .scss files in folder A to .css files (with exactly the same name) into folder B.
 * To keep things as minimal as possible, this tool compiles every X seconds, regardless of changes within the files.
 * This seems weird, but makes sense as checking for changes in the files is more CPU-extensive than simply
 * re-compiling them. SassWatcher uses scssphp, the best SASS compiler in PHP available.
 *
 * SassWatcher is not a standalone compiler, it's just a little method that uses the excellent scssphp compiler written
 * by Leaf Corcoran (https://twitter.com/moonscript), which can be found here: http://leafo.net/scssphp/ and adds
 * automatic interval-compiling to it.
 *
 * The currently supported version of SCSS syntax is 3.2.12, which is the latest one.
 * To avoid confusion: SASS is the name of the language itself, and also the "name" of the "first" version of the
 * syntax (which was quite different than CSS). Then SASS's syntax was changed to "SCSS", which is more like CSS, but
 * with awesome additional possibilities and features.
 *
 * The compiler uses the SCSS syntax, which is recommended and mostly used. The old SASS syntax is not supported.
 *
 * @see SASS Wikipedia: http://en.wikipedia.org/wiki/Sass_%28stylesheet_language%29
 * @see SASS Homepage: http://sass-lang.com/
 * @see scssphp, the used compiler (in PHP): http://leafo.net/scssphp/
 *
 * How to use this tool:
 *
 * 1. Edit $sass_watcher->watch( ... ); in the last line of this file and put your stuff in here, see the parameter
 *    list below.
 * 2. Make sure PHP can write into your CSS folder.
 * 3. Run the script:
 *    a) simple way, from browser, just enter the URL to scss-compiler.php: http://127.0.0.1/folder/php-sass-watcher.php
 *       The script will run forever, even if you close the browser window.
 *    b) PHPStorm users can run the script by right-clicking the file and selecting "Run php-sass-watcher.php".
 * 4. To stop the script, stop/restart your Apache/Nginx/etc. or press the red "stop process button in PHPStorm.
 *
 * The parameters:
 *
 *  1. relative path to your SCSS folder
 *  2. relative path to your CSS folder (make sure PHP has write-rights here)
 *  3. the compiling interval (in seconds)
 *  4. relative path to the scss.inc.php file, which is the main file of the SASS compiler used
 *     here. Download the script manually from http://leafo.net/scssphp/ or "require" it via Composer:
 *     "leafo/scssphp": "0.0.9"
 *  5. optional: how the .css output should look like. See http://leafo.net/scssphp/docs/#output_formatting for more.
 *
 * How the tool works:
 *
 * Every X seconds ALL files in the scss folder will be compiled to same-name .css files in the css folder.
 * The tool does not stop when a .scss file is broken, has syntax error or similar.
 * The tool does not compile when .scss file is broken, has syntax error or similar. It will only compile next time
 * when there's a valid scss file.
 */

// We need to be able to read our database and functions in Wordpress, so we'll find where wp-load.php is and load it
function find_wp_config_path() {
	$dir = dirname(__FILE__);
	do {
		if( file_exists($dir."/wp-config.php") ) {
			return $dir;
		}
	} while( $dir = realpath("$dir/..") );
	return null;
}
require_once( find_wp_config_path()  . '/wp-load.php' );

// In order to make this truely incredible, we need to check our colors so the theme can smartly change the color values
function check_color($hex, $modifier, $parentSame = null){
	//break up the color in its RGB components
	$hex 	= str_replace('#', '', $hex);
	$r 		= hexdec(substr($hex,0,2));
	$g 		= hexdec(substr($hex,2,2));
	$b 		= hexdec(substr($hex,4,2));

	// We're going to ignore some color values if they are too bright. It's making it difficult for our grays
	$r 		= ($r > 130) ? $r : 0;
	$g 		= ($g > 130) ? $g : 0;
	$b 		= ($b > 130) ? $b : 0;

	// Add it all together, weighting green because it can be tricky, and now we have our RGB value
	$rgb 	= $r + ($g * 1.8) + $b;

	if($parentSame){
		// If we're altering the color of something, where its parent container is the same color,
		// we need to override our logic and just make it darker or lighter
		if(($rgb > 0) && ($rgb > 0)) {
			return 'darken';
		}
		else if(($rgb == 0)) {
			return 'lighten';
		}
	}
	else if( ($modifier == 'lighten') && ( ($rgb > 0) && ($rgb <= 550) ) ){
		// Light Color being told to get lighter; Stop it
		return 'darken';
	}
	else if( ($modifier == 'darken') && ( ($rgb == 0) || ($rgb > 550) ) ){
		// Dark color being told to get darker; Stop it
		return 'lighten';
	}
	else {
		// This porridge is just right
		return $modifier;
	}
}



/*==========================================================================
CUSTOM COLORS DEFINED BY USER
==========================================================================*/
$primaryColor 		= (get_field('color_primary', 'options')) 		? get_field('color_primary', 'options') 	: '#ffffff';
$secondaryColor 	= (get_field('color_secondary', 'options')) 	? get_field('color_secondary', 'options') 	: '#565656';
$tertiaryColor 		= (get_field('color_tertiary', 'options')) 		? get_field('color_tertiary', 'options') 	: '#00aeef';
$quaternaryColor 	= (get_field('color_quaternary', 'options'))	? get_field('color_quaternary', 'options') 	: '#691c33';

$bgHeaderColor		= (get_field('bg_color_header', 'options')) 	? get_field('bg_color_header', 'options') 	: '#ffffff';
$bgHeaderOpacity 	= (get_field('bg_opacity_header', 'options'))	? get_field('bg_opacity_header', 'options')	: '.5';

$bgFooterColor 		= (get_field('bg_color_footer', 'options'))		? get_field('bg_color_footer', 'options') 	: '#ffffff';

$user_colors = '
	// Primary //
	body,
	.home .slide-caption .button, .home .orbit-bullets li.active,
	.widget .button, .widget-testimonial .diamond, .widget-testimonial .orbit-bullets li.active,
	.type-content .orbit-container .orbit-slides-container li {
		background-color: ' . $primaryColor . ';
	}
	.home .orbit-bullets li {
		background-color: rgba(' . $primaryColor . ', .50);
	}
	.widget input, .widget textarea {
		background-color: rgba(' . $primaryColor . ', .35);
	}
	.home .slide-caption h3, .home .slide-caption p,
	.category span, .recent-entries a,
	.post time span,
	input, textarea,
	.breadcrumbs p, .breadcrumbs p a,
	.backdrop h1, .backdrop p,
	.home .slide-caption .button:hover,
	.widget-testimonial .button.share, .widget .gform_wrapper .gform_footer input[type=submit]:hover, .widget .gform_wrapper div.validation_error,
	header.blog h3,
	.continue, .continue:hover,
	.sb-icon-search, .sb-search-submit, 
	.cart-quantity,
	.gallery-overlay h4, .gallery-overlay p,
	.reveal-modal .form p.alt, .reveal-modal .social a.symbol, .reveal-modal .controls a,
	.button-alt {
		color: ' . $primaryColor . ';
	}
	.widget {
		h1, h2, h3, h4, h5, p, a.symbol, a, label, .button:hover, .button.share, ol, ul, li {
			color: ' . $primaryColor . ';
		}
	}
	@media only screen and (max-width: 64em) {
		.widget.vary-on {
			.orbit-bullets li {
				background-color: ' . $primaryColor . ';
			}
			&.widget-testimonial .button.share:hover {
				color: ' . $primaryColor . ';
			}
		}
	}
	.reveal-modal .form {
		background-color: rgba(' . $primaryColor . ', .75);
	}
	.reveal-modal .details {
		h1, h2, h3, h4, h5, h6, p, span, a, ul, ol, li {
			color: ' . $primaryColor . ';
		}
	}
	footer.reveal {
		border-color: rgba(' . $primaryColor . ', .5);
	}
	.interest-form .alt {
		background-color: ' . $primaryColor . ';
		color: ' . $tertiaryColor . ';
	}
	

	// Secondary //
	input, textarea, .comment-body:after, .button,
	.widget .button:hover,
	.diamond,
	.continue:hover,
	.sb-icon-search, .sb-search-submit,
	.ilightbox-overlay,
	.cart-quantity {
		background-color: ' . $secondaryColor . ';
	}
	.comment > .children {
		background-color: rgba(' . $secondaryColor . ', .05);
	}
	.widget-testimonial .orbit-bullets li,
	.breadcrumbs {
		background-color: rgba(' . $secondaryColor . ', .5);
	}
	body, h1, h1 a, h2, h2 a, h3, h4, h5, h6, p, ul, ol, a, blockquote, dl, dt, dd, .textwidget, table, tr, td, span,
	nav .menu a,
	.social a, .social a,
	.home .slide-caption .button,
	.counter,
	.post time, .post time:before,
	.widget, .widget a.button, .widget-testimonial a.button.share:hover, .widget .gform_wrapper .gform_footer input[type=submit],
	.breadcrumbs span,
	.contact-details p a,
	#commentform label small {
		color: ' . $secondaryColor . ';
	}
	div.main .categorizing p {
		color: ' . check_color($secondaryColor, 'lighten') . '(' . $secondaryColor . ', 40%);
	}
	@media only screen and (max-width: 64em) {
		.widget.vary-on {
			background-color: ' . $secondaryColor . ';
			span.ring {
				color: ' . $secondaryColor . ';
			}
		}
	}
	#breadcrumb {
		background-color: ' . check_color($secondaryColor, 'lighten') . '(' . $secondaryColor . ', 40%);
		a {
			color: ' . $secondaryColor . ';
		}
	}
	.product-nav {
		background-color: ' . check_color($secondaryColor, 'lighten') . '(' . $secondaryColor . ', 50%);
		a {
			color: ' . $secondaryColor . ';
		}
		.current {
			background-color: ' . check_color($secondaryColor, 'lighten') . '(' . $secondaryColor . ', 25%);
			color: ' . check_color($secondaryColor, 'lighten') . '(' . $secondaryColor . ', 100%);
		}
	}


	// Tertiary //
	header.blog, input[type=submit], 
	.widget, 
	.button:hover, .slide-caption .button:hover,
	.backdrop,
	.continue,
	.reveal-modal .form .alt,
	.button-alt {
		background-color: ' . $tertiaryColor . ';
	}
	.gallery-overlay {
		background-color: rgba(' . $tertiaryColor . ', .75);
	}
	.test-list blockquote {
		border-color: ' . $tertiaryColor . ';
	}
	nav.main a:hover, nav.main a.active,
	a, .social a:hover, span.prepend,
	.counter .current,
	#respond h3 a, label small, .vcard cite.fn, .loading, .loading p, .loading em,
	.widget-testimonial span.ring,
	.categorizing a,
	.section .gallery .orbit-container .orbit-prev>span, .section .slider-pages .orbit-container .orbit-prev>span, .section .gallery .orbit-container .orbit-next>span, .section .slider-pages .orbit-container .orbit-next>span,
	button.icon:before {
		color: ' . $tertiaryColor . ';
	}
	a:hover,
	header.blog a:hover {
		color: ' . check_color($tertiaryColor, 'lighten') . '(' . $tertiaryColor . ', 20%);
	}
	.widget a:hover {
		color: ' . check_color($tertiaryColor, 'lighten', true) . '(' . $tertiaryColor . ', 10%);
	}
	@media only screen and (max-width: 40em) {
		.contact-details p a {
			color: ' . $tertiaryColor . ';
		}
		.widget.vary-on {
			.orbit-bullets li.active {
				background-color: ' . $tertiaryColor . ';
			}
		}
	}
	.reveal-modal-bg,
	.ilightbox-overlay.dark {
		background-color: rgba(' . $tertiaryColor . ', .95);	
	}
	.reveal-modal .form {
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, span, label {
			color: ' . $tertiaryColor . ';
		}
	}
	.bg-tertiary {
		background-color: ' . $tertiaryColor . ';
		h1, h2, h3, h4, h5, h6, p, ul, ol, dt, dd, span, label {
			color: ' . $primaryColor . ';
		}
	}
	

	// Quaternary //
	.customSelect,
	.button-alt:hover  {
		background-color: ' . $quaternaryColor . ';
	}
	.product-nav.bottom .pagination {
		background-color: rgba(' . $quaternaryColor . ', .5);
	}
	.max-socialize .social-connect, span.inline:first-child, header.main .band,
	footer.main .upper, footer.main .lower, .hour-container,
	.blog footer.main, .single footer.main, .archive footer.main, .diamond,
	.extra,
	header.main .tel:last-child  {
		border-color: ' . $quaternaryColor . ';
	}
	.sb-search-input::-webkit-input-placeholder, .sb-search-input:-moz-placeholder, .sb-search-input::-moz-placeholder, .sb-search-input:-ms-input-placeholder {
		color: ' . $quaternaryColor . ';
	}
	.customSelect span {
		color: ' . check_color($quaternaryColor, 'darken', true) . '(' . $quaternaryColor . ', 30%);
	}
	.product-nav.bottom {
		.current {
			background-color: ' . check_color($quaternaryColor, 'lighten') . '(' . $quaternaryColor . ', 25%);
			color: ' . $primaryColor . ';
		}
	}
	div.main .widget h3 {
		color: ' . $primaryColor . ';
	}


	// Background Header //
	nav.main, .sub-menu {
		background-color: ' . $bgHeaderColor . ';
	}
	.header-contents {
		background-color: rgba(' . $bgHeaderColor . ', ' . $bgHeaderOpacity . ');
	}

	// Background Footer //
	footer.main {
		background-color: ' . $bgFooterColor . ';
	}


	// Social Icons //
	.social-share, .social-connect {
		&.st-single-color {
			.bg-primary &, .container &, .product-summary &, .widget & {
				a:before { color: ' . $tertiaryColor . '; }
				a:hover:before { color: darken(' . $tertiaryColor . ', 10%); }
			}
			.bg-secondary &, .bg-tertiary &, .bg-quaternary &, .widget & {
				a:before { color: ' . $primaryColor . '; }
				a:hover:before { color: darken(' . $primaryColor . ', 10%); }
			}
		}
	}

	// Jigoshop //
	.jigoshop.jigoshop-checkout form.checkout {
		input, textarea {
			background-color: ' . $secondaryColor . ';
			color: ' . $primaryColor . ';
		}
	}
';

$selectors = array(
	'header' 	=> 'header.main',
	'footer' 	=> 'footer.main',
	'sidebar' 	=> 'aside.sidebar',
	'widget' 	=> '.widget',
	'post' 		=> '.the-post',
	'page' 		=> 'article.main, div.main',
	'gallery' 	=> 'article.gallery',
	'product' 	=> 'li.product',
);

$user_custom_font_style = pantheon_function_font_select($selectors);
$user_custom_text_style = pantheon_function_text_style($selectors);

$user_custom_styles = get_field('advanced_css', 'options');
/*==========================================================================*/

class SassWatcher
{
	/**
	 * Watches a folder for .scss files, compiles them every X seconds
	 * Re-compiling your .scss files every X seconds seems like "too much action" at first sight, but using a
	 * "has-this-file-changed?"-check uses more CPU power than simply re-compiling them permanently :)
	 * Beside that, we are only compiling .scss in development, for production we deploy .css, so we don't care.
	 *
	 * @param string $scss_folder source folder where you have your .scss files
	 * @param string $css_folder destination folder where you want your .css files
	 * @param int $interval interval in seconds
	 * @param string $scssphp_script_path path where scss.inc.php (the scssphp script) is
	 * @param string $format_style CSS output format, ee http://leafo.net/scssphp/docs/#output_formatting for more.
	 */
	public function watch($scss_folder, $css_folder, $scssphp_script_path, $format_style = "scss_formatter", $user_defined_colors = null, $user_defined_custom_font = null, $user_defined_custom_text = null, $user_defined_styles = null)
	{
		// load the compiler script (scssphp), more here: http://www.leafo.net/scssphp
		require $scssphp_script_path;
		$scss_compiler = new scssc();

		// set the path to your to-be-imported mixins. please note: custom paths are coming up on future releases!
		$scss_compiler->setImportPaths($scss_folder);
		// set css formatting (normal, nested or minimized), @see http://leafo.net/scssphp/docs/#output_formatting
		$scss_compiler->setFormatter($format_style);

		// get all .scss files from scss folder
		$filelist = glob($scss_folder . "*.scss");

		// step through all .scss files in that folder
		foreach ($filelist as $file_path) {
			// get path elements from that file
			$file_path_elements = pathinfo($file_path);

			// get file's name without extension
			$file_name = $file_path_elements['filename'];

			// get .scss's content, put it into $string_sass
			$string_sass = file_get_contents($scss_folder . $file_name . ".scss");

			// Add our user defined colors to the compiled code
			$string_sass .= $user_defined_colors;

			// Add our user font styles to the compiled code
			$string_sass .= $user_defined_custom_font;

			// Add our user text styles to the compiled code
			$string_sass .= $user_defined_custom_text;

			// Add our user defined styles to the compiled code (so we can save it)
			$string_sass .= $user_defined_styles;

			// try/catch block to prevent script stopping when scss compiler throws an error
			try {
				// compile this SASS code to CSS
				$string_css = $scss_compiler->compile($string_sass);
				// write CSS into file with the same filename, but .css extension
				file_put_contents($css_folder . $file_name . ".css", $string_css);
			} catch (Exception $e) {
				echo 'Gary was here; Ash is a loser!';
			}
		}
	}
}

$sass_watcher = new SassWatcher();
$sass_watcher->watch(get_template_directory() . '/stylesheets/sass/', get_template_directory() . '/stylesheets/css/', get_template_directory() . '/stylesheets/scss.inc.php', 'scss_formatter', $user_colors, $user_custom_font_style, $user_custom_text_style, $user_custom_styles);
