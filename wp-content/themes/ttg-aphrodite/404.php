<?php
	/*===========================================================================
	400
	=============================================================================
	Display for 404 pages
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');
?>

	<div class="container row">
		<div class="main large-8 large-centered columns">

			<h3>Sorry, we couldn't find what you were looking for.</h2>
			<p>Please try going back to the <a href="<?= get_bloginfo('siteurl'); ?>">homepage</a>, and if that doesn't help, try using the search box above to find what you may be looking for.</p>
			<?php get_template_part('searchform'); ?>

		</div>
	</div>
	
<?php get_template_part('parts/shared/footer', 'html'); ?>