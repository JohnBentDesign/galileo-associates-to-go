<?php
	/*===========================================================================
	INCLUDE THE INCLUDES
	===========================================================================*/
	// Advanced Custom Fields
	include('includes/advanced-custom-fields/theme-styling.php');
	include('includes/advanced-custom-fields/theme-navigation.php');
	include('includes/advanced-custom-fields/page-home.php');
	include('includes/advanced-custom-fields/page-details.php');
	include('includes/advanced-custom-fields/page-backdrop.php');
	include('includes/advanced-custom-fields/page-excerpt.php');
	include('includes/advanced-custom-fields/shop-taxonomy.php');

	// Custom Taxonomies
	include('includes/taxonomies/page-category.php');

	// Custom Functions
	include('includes/functions/aphrodite_theme_section_image.php');
	include('includes/functions/aphrodite_theme_section_content.php');
	include('includes/functions/aphrodite_theme_section_widget.php');
	include('includes/functions/aphrodite_theme_section_order.php');

	// Plugins
	include('includes/plugins/wp-mobile-detect/wp-mobile-detect.php');


	/*===========================================================================
	OPTIONS PAGES
	===========================================================================*/
	// Add Options Pages
	function aphrodite_options($settings){
		array_push($settings['pages'], 'Main Menu');
	 
		return $settings;
	}
	add_filter('acf/options_page/settings', 'aphrodite_options');



	/*===========================================================================
	ADD THEME SUPPORTS
	===========================================================================*/
	add_theme_support('post-thumbnails');
	add_theme_support('menus');



	/*===========================================================================
	CUSTOM THUMBNAIL SIZES
	===========================================================================*/
	add_image_size('TTG Featured Image', 1200, 600, false);
	add_image_size('TTG Gallery Normal Display', 1200, 9999, false);
	add_image_size('TTG Medium Thumbnail', 350, 350, true);



	/*===========================================================================
	REGISTER MENUS
	===========================================================================*/
	if(!function_exists('ttg_menus')){
		function ttg_menus() {
			register_nav_menus(array(
				'menu-footer' 		=> 'Footer Menu'
			));

			if(class_exists('jigoshop')){
				register_nav_menus(array(
					'menu-shop' 		=> 'Shop Menu'
				));
			}
		}
		add_action('init', 'ttg_menus');
	}



	/*===========================================================================
	ENQUEUE SCRIPTS AND STYLES
	===========================================================================*/
	// JAVASCRIPTS
	//---------------------------------------------------------------------------
	function ttg_scripts(){
		if(!is_admin()){
			// Plugins
			wp_enqueue_script('customselect', get_template_directory_uri() . '/javascripts/plugins/customselect.min.js', array('jquery'), '', true);
			wp_enqueue_script('slick', get_template_directory_uri() . '/javascripts/plugins/slick.min.js', array('jquery'), '', true);
			wp_enqueue_script('search', get_template_directory_uri() . '/javascripts/plugins/expandable-search.js', array('jquery'), '', true);
			wp_enqueue_script('hoverintent', get_template_directory_uri() . '/javascripts/plugins/hoverintent.min.js', array('jquery'), '', true);

			// Aphrodite Scripts
			wp_enqueue_script('aphrodite', get_template_directory_uri() . '/javascripts/aphrodite.js', array('jquery', 'foundation', 'customselect', 'slick', 'search', 'hoverintent'), '', true);
		}
	}
	add_action('wp_enqueue_scripts', 'ttg_scripts');

	// STYLESHEETS
	//---------------------------------------------------------------------------
	function ttg_styles(){
		if(!is_admin()){
			// Aphrodite Styles
			wp_enqueue_style('aphrodite', get_template_directory_uri() . '/stylesheets/css/style.css');
		}
	}
	add_action('wp_enqueue_scripts', 'ttg_styles');

	// RECOMPILES THE CSS ONLY WHEN THE OPTIONS PAGE IS SAVED
	//---------------------------------------------------------------------------
	// function ttg_compile_css( $post_id ) {
	// 	$page = htmlspecialchars($_GET["page"]);
	// 	if($page == 'acf-options-theme-styling'){
			// Compile our CSS
			include(get_template_directory() . '/stylesheets/php-sass-watcher.php');
	// 	}
	// }
	// // run after ACF saves the $_POST['fields'] data
	// add_action('acf/save_post', 'ttg_compile_css', 20);
	
