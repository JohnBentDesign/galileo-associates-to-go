<?php
	/*===========================================================================
	Template Name: Testimonial
	=============================================================================
	Testimonial listing page. Makes it easier to assign header details and to put
	into a menu.
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');

	echo '<div class="test-container row">';

	$testimonialArgs = array( 
		'post_type' 		=> 'testimonial',
 		'posts_per_page' 	=> -1,
		'order' 			=> 'DESC',
		'orderby' 			=> 'date',
	);
 
	$the_query = new WP_Query( $testimonialArgs );
	
	// THE LOOP
	if($the_query->have_posts()):
?>

		<div class="row test-list">

			<?php 
				while($the_query->have_posts()):
					$the_query->the_post();
			?>

					<div class="medium-6 columns">
						<blockquote>
							<?php
								// Variables
								$content 	= get_field('testimonial_content');
								$author 	= get_Field('testimonial_author');
								$location 	= get_Field('testimonial_location');

								if($content){
									echo '<div class="quote">' . $content . '</div>';
								}
								if($author || $location){
									echo '<cite>';
									echo $author;
									if($author && $location) { echo '&nbsp;|&nbsp;'; }
									echo $location;
									echo '</cite>';
								}
							?>
						</blockquote>
					</div>
			<?php endwhile; ?>

		</div>

<?php 
	else:
		echo '<p>There are no testimonials to display.</p>';

	endif;

	// Reset Post Data
	wp_reset_postdata();

	echo '</div>';

	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>