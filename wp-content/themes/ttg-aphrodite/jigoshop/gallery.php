<?php 
	/*===========================================================================
	PART: GALLERY
	=============================================================================
	Displays a gallery that is attached to a page
	*/
	global $post;

	// Get Info from Current Page
	$gallery 		= get_field('page_gallery');
	$galleryTitle 	= (get_field('page_gallery_title')) ? '<h2>' . get_field('page_gallery_title') . '</h2>' : '';

	// Options
	$optionsGallery = get_field('gallery_form', 'options');
	$optionsAlt 	= get_field('gallery_alt', 'options');
	
	//---------------------------------
	// POST CONTENT
	//---------------------------------
	$countImages 	= count($galleryArray);
	$counter		= 1;

		$title 			= ($image['title']) ? '<h3>' . strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $image['title']))) . '</h3>' : '';
		$caption		= $image['caption'];
		$description 	= ($image['description']) ? wpautop($image['description']) : '';
		$extra 			= (get_field('attach_add', $image['id'])) ? '<p class="extra">' . get_field('attach_add', $image['id']) . '</p>': '';
		$form 			= (get_field('attach_form', $image['id'])) ? true : false;
?>
<?php do_action('jigoshop_after_shop_loop_item_title', $post, $_product); ?>
		<div id="reveal-<?= $counter; ?>" class="reveal-modal xlarge" data-reveal>
			<div class="row">
				<?php // Featured Image // ?>
				<div class="large-6 columns">
					<?php pantheon_display_post_field_image($image, 'large'); ?>
				</div>

				<?php // Image Details // ?>
				<div class="large-6 columns">
					
					<?php // Details ?>
					<div class="details">
						<?php
							echo $extra;
							echo $title;
							echo $description;
						?>
					</div>

					<?php 
						// Form
						if($form):
						    $form = get_field('gallery_form', 'options');
						    gravity_form_enqueue_scripts($form->id, true); 
					?>
							<div class="form">
								<?php 
									// Display Form
									if($form){
										gravity_form($form->id, true, true, false, '', true, 1);
									}

									// Display break if there's also alternate contact details
									if($optionsGallery && $optionsAlt){ 
										echo '<p class="alt-break">or:</p>';
									}

									// Display Alt Contact Details
									if($optionsAlt){
										echo '<p class="alt">' . $optionsAlt . '</p>';
									}
								?>
							</div>
					<?php endif; ?>
					
					<?php // Footer ?>
					<footer class="reveal">
						<div class="row">
							<div class="social medium-6 columns">
								<?php pantheon_display_social_share('gallery', 'image', $image); ?>
							</div>
							<div class="controls medium-6 columns">
								<?php
									// Show previous button if there is a previous modal
									if($counter != 1){
										echo '<a href="#" data-reveal-id="reveal-' . ($counter - 1) . '" class="icon ion-chevron-left"></a>';
									}
									if($counter != $countImages){
										echo '<a href="#" data-reveal-id="reveal-' . ($counter + 1) . '" class="icon ion-chevron-right"></a>';
									}
								?>
								<a class="close-reveal-modal icon ion-close"></a>
							</div>
						</div>
					</footer>

				</div>
			</div>
		</div>
		<div class="medium-4 large-3 columns">
			<div class="img-zoom">
				<a href="#" data-reveal-id="reveal-<?= $counter; ?>" data-reveal>
					<?php pantheon_display_post_field_image($image, 'shop_small'); ?>
				</a>
			</div>
		</div>
<?php
		$counter++; 

?>	

