<?php
/**
 * Loop shop template
 *
 * DISCLAIMER
 *
 * Do not edit or add directly to this file if you wish to upgrade Jigoshop to newer
 * versions in the future. If you wish to customise Jigoshop core for your needs,
 * please use our GitHub repository to publish essential changes for consideration.
 *
 * @package             Jigoshop
 * @category            Catalog
 * @author              Jigoshop
 * @copyright           Copyright © 2011-2014 Jigoshop.
 * @license             GNU General Public License v3
 */

	// VARIABLES //
	global $columns, $per_page, $isSingle;
	$activeShop 			= get_field('shop_enable', 'options');
	$activeShopCat 			= get_field('shop_enable_cat', 'options');
	$activeShopCatSelect 	= get_field('shop_enable_cat_select', 'options');

	// PULL INFO FROM JIGOSHOP //
	$jigoshop_options 	= Jigoshop_Base::get_options();
	$catalogColumn		= $jigoshop_options->get_option('jigoshop_catalog_columns');
	$catalogCount 		= $jigoshop_options->get_option('jigoshop_catalog_per_page');
	$catalogHalf 		= $catalogCount / 2;
	$catalogSweetSpot	= ceil($catalogHalf);

	// SHARE LINK //
	$catalogURL			= @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
	$catalogURL			.= ( $_SERVER["SERVER_PORT"] !== 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
	$catalogURL			.= $_SERVER["REQUEST_URI"];

	// JIGOSHOP: BEFORE LOOP ACTION //
	do_action('jigoshop_before_shop_loop');

	// LOOP VARIABLES //
	$loop = 0;
	if(!isset($columns) || !$columns){
		switch (apply_filters( 'loop_shop_columns', 3 )) {
			case 1: $columnSize = 1; break;
			case 2: $columnSize = 2; break;
			case 3: $columnSize = 3; break;
			case 4: $columnSize = 4; break;
			case 5: $columnSize = 6; break;
			case 6: $columnSize = 6; break;
			case 7: $columnSize = 6; break;
			case 8: $columnSize = 6; break;
			case 9: $columnSize = 6; break;
			case 10: $columnSize = 6; break;
			default: $columnSize = 3; break;
		}
	}
	else {
		$columns;
	}
	$columns = (!isset($columns) || !$columns) ? apply_filters( 'loop_shop_columns', 3 ) : $columns;

	// SINGLE PAGE //
	if($isSingle){
		$columnSize = 3;
		$columns = 4;
	}

	//only start output buffering if there are products to list
	if(have_posts()){
		ob_start();
	}

	// PRODUCT LOOP //
	if(have_posts()):
		while(have_posts()):
			the_post();
			$_product = new jigoshop_product( $post->ID );
			$loop++;


			// PRODUCT PURCHASE ACTIVATE
			//---------------------------------
			// Figure out 
			$cartShop = false;
			$postCats = get_the_terms($post->ID, 'product_cat');

			// If the shop is just active without category selection, just activate it!
			if($activeShop && !$activeShopCat){
				$cartShop = true;
			}
			// If we have an active shop AND active category selection, we'll loop through and see if any of its categories activate it for sales
			elseif($activeShop && $activeShopCat){
				foreach ($postCats as $id => $category) {
					if(in_array($id, $activeShopCatSelect)){
						$cartShop = true;
					}
				}
			}
			// Else, we'll just make it impossible to buy
			else {
				$cartShop = false;
			}


			//---------------------------------
			// PRODUCT
			//---------------------------------
			// Display
			$displayType 	= get_field('product_display', 'options');
			// Product
			$linkAttr 		= ($displayType == 'single') ? 'href="' . get_permalink($post->ID) . '"' : 'href="#" data-reveal-id="reveal-' . $loop . '" data-reveal data-slug="#' . $slug .'"';
			// Slug
			$slug 			= $post->post_name;
			// Get Fields
			$fields 		= get_post_custom();
			// Image
			$imageThumb 	= pantheon_display_post_featured_image($post->ID, 'shop_small', false, 'image', false); 
			$imageFull 		= pantheon_display_post_featured_image($post->ID, 'large', false, 'image', false); 
			// Form
			$formDisplay 	= get_field('product_form_type');
			$formObj 		= ( ($formDisplay == 'default') || ($formDisplay == NULL) ) ? get_field('product_form', 'options') : get_field('product_form_choose');
			$formAlt 		= get_field('product_alt', 'options');
?>
			<li class="product">

				<?php 
					// PRODUCT THUMB
					//---------------------------------
				?>
				<div class="product-thumb">
					<a <?= $linkAttr; ?>>
						<?php if($imageThumb):?>
							<div class="img">
								<div class="table">
									<div class="table-row">
										<div class="table-cell">
											<?php echo $imageThumb; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endif; ?>
						<div class="details">
							<strong><?php the_title(); ?></strong>
							<?php
								if(!$isSingle):
									do_action('jigoshop_after_shop_loop_item_title', $post, $_product);
								endif;
							?>
						</div>
					</a>
				</div>


				<?php 
					// MODAL
					//---------------------------------
					if($displayType == 'pop'):
				?>
						<div id="reveal-<?= $loop; ?>" class="product-details reveal-modal xlarge" data-reveal data-slug="#<?= $slug; ?>">
							<div class="row">
							
								<?php // Featured Image // ?>
								<div class="product-image large-6 columns">
									<?= $imageFull; ?>
								</div>

								<?php // Product Details // ?>
								<div class="large-6 columns">
						
									<?php // Details ?>
									<div class="details">
										<?php
											do_action('jigoshop_after_shop_loop_item_title', $post, $_product);
											echo '<h3>' . get_the_title() . '</h3>';
											the_content();
										?>
									</div>

									<?php 
										if( ($cartShop && $hasPrice) || ($formDisplay != 'none') ):
											$hasPrice = ($_product->get_price()) ? true : false;
									?>
										<div class="form">
											<?php
												//---------------------------------
												// ACTIVE: ADD TO CART
												//---------------------------------
												if($cartShop && $hasPrice):
													do_action('jigoshop_after_shop_loop_item', $post, $_product);

												//---------------------------------
												// INACTIVE: INTEREST FORM
												//---------------------------------
												elseif($formDisplay != 'none'): 
													// Display Form
													if($formDisplay != 'none'){
														echo do_shortcode( $formObj );
													}

													if(($formDisplay != 'none') && $formAlt){
														echo '<p class="alt-break">or:</p>';
													}

													// Display Alt Contact Details
													if($formAlt){
														echo '<p class="alt">' . $formAlt . '</p>';
													}
														
												endif;
											?>
										</div>
									<?php endif; ?>
						
									<?php // Footer ?>
									<footer class="reveal">
										<div class="row">
											<div class="social medium-6 columns">
												<?php pantheon_display_social_share('product', 'post', $imageThumb, get_the_content(), null, $catalogURL . '%23' . $slug); ?>
											</div>
											<div class="controls medium-6 columns">
												<?php
													// Show previous button if there is a previous modal
													if($loop != 1){
														echo '<a href="#" data-reveal-id="reveal-' . ($loop - 1) . '" class="icon ion-chevron-left"></a>';
													}
													if($loop != $catalogCount){
														echo '<a href="#" data-reveal-id="reveal-' . ($loop + 1) . '" class="icon ion-chevron-right"></a>';
													}
												?>
												<a class="close-reveal-modal icon ion-close"></a>
											</div>
										</div>
									</footer>

								</div>
							</div>
						</div>
				<?php endif; ?>

			</li>			
<?php
		endwhile;
	endif;

	//---------------------------------
	// DISPLAY PRODUCTS
	//---------------------------------
	if($loop == 0):
		$content = '<p class="info">'.__('No products found which match your selection.', 'jigoshop').'</p>';
	else:
		$found_posts = ob_get_clean();
		$content = '<ul class="product-list small-block-grid-1 medium-block-grid-3 large-block-grid-' . $columnSize . '">' . $found_posts . '</ul>';
	endif;

	echo apply_filters( 'jigoshop_loop_shop_content', $content );
	do_action( 'jigoshop_after_shop_loop' );
