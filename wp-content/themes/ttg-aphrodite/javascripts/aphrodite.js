//---------------------------
// INITIALIZE FOUNDATION
//---------------------------
jQuery(document).foundation();


//---------------------------
// DOCUMENT READY
//---------------------------
jQuery(document).ready(function(){
	// Fade In/Out Main Menu
	//---------------------------
	function aphroditeMenuEnter() {
		if(!jQuery(this).is(':animated')){
			jQuery(this).find('.sub-menu').slideDown();
		}
	}
	function aphroditeMenuExit() {
		jQuery(this).find('.sub-menu:visible').stop().slideUp();
	}
	jQuery('.dropdown').hoverIntent(aphroditeMenuEnter, aphroditeMenuExit);


	// Apply Custom Colors to Widgets
	//---------------------------
	jQuery('.widget').each(function(){
		var colorText 	= jQuery(this).attr('data-color-text'),
			colorBG 	= jQuery(this).attr('data-color-bg');

		jQuery(this).find('p, h1, h2, h3, h4, h5, h6, a, a.button.share').css('color', colorText);
		jQuery(this).find('.ring, a.button.more').css('color', colorBG);
	});


	// Expandable Search Bar
	//---------------------------
	new UISearch( document.getElementById( 'sb-search' ) );


	// Tinynav Customize with CustomSelect
	//---------------------------
	jQuery('.tinynav').customSelect();
	jQuery('.customSelectInner').html('<span class="title">Menu</span> <span class="ion-arrow-down-b icon"></span>');


	// Blog Sticky Header
	//---------------------------
	if(jQuery(window).width() > 430){
		jQuery(window).scroll(function(e){ 
			var sticky 			= jQuery('header.blog'),
				headerHeight 	= jQuery('header.main').height(); 
			
			// If we've scrolled past the main header, we'll sticky the header
			if( (jQuery(this).scrollTop() > headerHeight) && !sticky.hasClass('sticky')){ 
				jQuery('header.blog').addClass('sticky'); 
			}
			else if(jQuery(this).scrollTop() < headerHeight) {
				jQuery('header.blog').removeClass('sticky');
			}
		});
	}


	// Back to Top
	//---------------------------
	jQuery('.top').click(function(e) {
		jQuery('body,html').animate({
			scrollTop: 0
		}, 800);
		e.preventDefault();
	});


	// Flex Video
	//---------------------------
	jQuery('iframe').each(function(){
		getIframeSrc  = jQuery(this).attr('src');

		if((getIframeSrc.indexOf('youtube') >= 0) || (getIframeSrc.indexOf('vimeo') >= 0)){
			jQuery(this).wrapAll('<div class="flex-video">');
		}
	});


	// Slick Slider Initiations
	//---------------------------
	// Homepage
	jQuery('.home-slider').slick({
		dots: true,
		lazyLoad: 'ondemand',
		infinite: true,
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: false,
		adaptiveHeight: true
	});

	// Interior Slides
	jQuery('.int-slider').slick({
		lazyLoad: 'ondemand',
		infinite: true,
		adaptiveHeight: true
	});

	// Move directional arrows inside of content area
	jQuery('.slider-pages .slick-prev, .slider-pages .slick-next').appendTo('.excerpt-content');

	// Add correct icon classes to buttons
	jQuery('.slick-prev').addClass('icon ion-ios7-arrow-back');
	jQuery('.slick-next').addClass('icon ion-ios7-arrow-forward');


});
