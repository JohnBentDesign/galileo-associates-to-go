<?php
	/*===========================================================================
	INDEX
	=============================================================================
	Default fallback style. Default page for blog viewing page
	*/

	// HEADER //
	get_template_part('parts/shared/header', 'html');

	// BLOG HEADER //
	get_template_part('parts/blog/header');	
?>

	<div class="row">

		<?php 
			// POST LOOP //
			if($post->post_type == 'gallery'){
				get_template_part('parts/posts/loop', 'gallery');	
			}
			else {
				echo '<div class="main large-7 large-centered columns">';
				get_template_part('parts/posts/loop');
				echo '</div>';
			}
		?>

	</div>

<?php
	// FOOTER //
	get_template_part('parts/shared/footer', 'html');
?>