function pantheonInfiniteScrollLoad(pageNumber) {
	jQuery.ajax({
		url: PantheonInfiniteScroll.ajaxurl,
		type:'POST',
		data: "action=infinite_scroll&page-number="+ pageNumber + PantheonInfiniteScroll.query_string,
		success: function(html){
			jQuery("#infinite-scroll-loading").toggle(false);
			jQuery("#infinite-scroll-container").append(html);
			if ( '' == html ) {
				infiniteScrollComplete = true;
				jQuery("#infinite-scroll-finished").toggle(true);
			}
		}
	});
	return false;
}

// Start by adding page two and increment from there.
var infiniteScrollCount = 2;
var infiniteScrollComplete = false;

jQuery(window).scroll(function(){
	if ( ! infiniteScrollComplete ) {
		if  (jQuery(window).scrollTop() == jQuery(document).height() - jQuery(window).height()){
			jQuery("#infinite-scroll-loading").toggle(true);
			pantheonInfiniteScrollLoad(infiniteScrollCount);
			infiniteScrollCount++;
		}
	}
});

