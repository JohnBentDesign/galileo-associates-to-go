<?php
/**
 * Additional feature for pantheon themes that adds infinite scroll functionality.
 */

/*===========================================================================
INCLUDE THE INCLUDES
===========================================================================*/

/*
 * This addition uses a new method for including code based on WordPress hooks.
 *
 * To include this addition in a theme, add the following to the themes functions.php inside
 * the after_theme_setup block.
 *
 * do_action( 'pantheon/require_addition', 'infinite-scroll', $args );
 *
 * $args for this addition is an array of $query args to be passed back as query_args in the ajax request. If using
 * infinite scroll in more than one place include info so that the correct data will be returned.
 *
 */
function action__require_additions( $addition, $args = array() ) {
	if ( 'infinite-scroll' == $addition ) {

		// build localization object
		$l10n = array(
			'ajaxurl'      => admin_url( 'admin-ajax.php' ),
			'query_string' => '&' . trim( build_query( $args ), '?' ),
		);

		// embed the javascript file that makes the AJAX request
		wp_enqueue_script( 'pantheon-infinite-scroll', plugin_dir_url( __FILE__ ) . '/infinite-scroll.js', array( 'jquery' ) );

		// declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)

		wp_localize_script( 'pantheon-infinite-scroll', 'PantheonInfiniteScroll', $l10n );
	}
}
add_action( 'pantheon/require_addition', __NAMESPACE__ . '\action__require_additions', 10, 2 );



function action__ajax_infinite_scroll () {
	$query = array(
		'paged' => isset( $_POST['page-number'] ) ? $_POST['page-number'] : 1,
	);

	query_posts( $query );
	while ( have_posts() ) : the_post();
		do_action( 'pantheon/infinite-scroll/post' );
	endwhile;

	exit;
}
add_action( 'wp_ajax_infinite_scroll',        __NAMESPACE__ . '\action__ajax_infinite_scroll' );  // for logged in user
add_action( 'wp_ajax_nopriv_infinite_scroll', __NAMESPACE__ . '\action__ajax_infinite_scroll' );  // if user not logged in
