<?php
namespace Pantheon\Addition\Event;

	/*===========================================================================
	PANTHEON ADDITION: EVENT
	===========================================================================*/
	// An additional feature for Pantheon that adds events in certain themes.
	// Events have start date, end date, and can have several intermediate dates.


	/*===========================================================================
	INCLUDE THE INCLUDES
	===========================================================================*/

	/*
	 * This addition uses a new method for including code based on WordPress hooks.
	 *
	 * To include this addition in a theme, add the following to the themes functions.php inside
	 * the after_theme_setup block.
	 *
	 * do_action( 'pantheon/require_addition', 'event' );
	 *
	 */
	function action__require_additions( $addition ) {
		if ( 'event' == $addition ) {
			// require taxonomy first to make rewrites work
			require_once( 'taxonomy-event-type.php' );
			require_once( 'post-type-event.php' );
			require_once( 'post-type-event-loc.php' );
		}
	}
	add_action( 'pantheon/require_addition', __NAMESPACE__ . '\action__require_additions' );

	/**
	 * Returns
	 * Multi-date event: Starts [Date] (until start date) / Ends [Date] (until end)
	 * Single-date event: Today/Tomorrow/[Day of Week], [Date] and [Time] / Currently showing! / Ended
	 */
	function get_upcoming_string() {
		$today = current_time( 'Ymd' );
		$tonow = date( 'His', current_time( 'timestamp' ) );

		if ( $date_summary = get_field( 'date_box_override' ) ) {
			return strip_tags(wp_kses_post( $date_summary ), '<a><br><br /><b><strong><em><i>');
		} elseif ( ( $agenda = get_field( 'agenda_items' ) ) && is_array( $agenda ) ) {

			$dates = wp_list_pluck( $agenda, 'date' );

			// if we only have one unique date then we are dealing with a one-day event
			if ( 1 === count( array_unique( $dates ) ) ) {
				$date_string = date( ' F jS', strtotime( $dates[0] ) );

				// Let's find out when this event happens
				if ( $dates[0] === $today ) { // hey cool! the event is today!
					// check if event is happening, like, really right now
					$start_times = array_map( 'strtotime', wp_list_pluck( $agenda, 'start_time' ) );
					$end_times   = array_map( 'strtotime', wp_list_pluck( $agenda, 'end_time' ) );
					if ( min( $start_times ) && date( 'His', min( $start_times ) ) < $tonow ) {
						if ( date( 'His', max( $end_times ) ) > $tonow ) {
							$date_string = '';
							$date_summary = "Currently Showing!";
						} else {
							$date_summary = "Ended";
						}
					} else {
						$date_summary = 'Today';
					}
				} elseif ( $dates[0] - 1 == $today ) { // maybe tomorrow
					$date_summary = 'Tomorrow';
				} elseif ( $dates[0] > $today ) { // or maybe sometime in the future
					$date_summary = date( 'l', strtotime( $dates[0] ) ); // date( 'l' ) gives day of week
				} else { // oops, I guess we missed this one...
					$date_summary = 'Ended';
				}

			// otherwise we probably have a multi-day event
			} elseif ( 1 < count( array_unique( $dates ) ) ) {
				$date_string = date( ' F jS', strtotime( max( $dates ) ) );

				if ( min( $dates ) > $today ) { // event has not started
					$date_summary = 'Starting';
					$date_string = date( ' F jS', strtotime( min( $dates ) ) );
				} elseif ( max( $dates ) < $today ) { // event has already ended
					$date_summary = 'Ended';
				} else { //event is in progress
					$date_summary = 'Ending';
				}
			}

			return "<span class=\"small-caps\">$date_summary</span>" . $date_string;
		}

	}

// sort event queries by start date
function action__pre_get_posts ( $query ) {
	// first, if we are in the admin or not the main_query of an archive and not ajax, bail
	switch ( true ) :
		case ( defined( 'DOING_AJAX' ) && DOING_AJAX && isset( $_POST['event-type'] ) && ! empty( $_POST['event-type'] ) ) :
			if ( 'all' !== $_POST['event-type'] ) {
				$tax_query = array(
					array(
						'taxonomy' => 'pantheon_event_type',
						'field' => 'slug',
						'terms' => $_POST['event-type'],
					)
				);
				$query->set('tax_query', $tax_query);
			}
			$query->set( 'post_type', 'pantheon_event' );
			break;
		case is_admin() :
			return;
		case ( $query->is_main_query() && is_archive() ) :
			break;
		default :
			return;
	endswitch;

	// now, handle taxonomy queries, we can assign colors for posts but we only want events on event-type archives
	if ( ! isset( $query->query_vars['post_type'] ) && isset( $query->query_vars['pantheon_event_type'] ) ) {
		$query->set( 'post_type', 'pantheon_event' );
	}

	//now if we are doing any kind of event search let's sort and filter
	if ( isset( $query->query_vars['post_type'] ) && 'pantheon_event' == $query->query_vars['post_type'] ) {
		// add a filter on pantheon_event_archive_per_page to adjust number of events on a page
		$query->set( 'posts_per_page', apply_filters( 'pantheon_event_archive_per_page', -1 ) );

		$query->set( 'orderby', 'meta_value' );
		$query->set( 'meta_key', 'pantheon_event_start_date' );
		$query->set( 'order', 'ASC' );

		$period_start = null;
		$period_end = null;

		// check for event_period_month query variable and convert to event_period_start and event_period_end
		if ( isset( $query->query_vars['event_period_month'] ) && ! empty( $query->query_vars['event_period_month'] ) ) {
			$month = $query->query_vars['event_period_month'];
			if ( is_numeric( $month ) && 6 == strlen( $month ) ) {
				$year  = substr( $month, 0, 4);
				$month = substr( $month, 4, 2);
			} elseif ( is_numeric( $month ) && $month <= 12 ) {
				$year  = date( 'Y' ) + ( $month >= date( 'm' ) ? 0 : 1 );
			} else {
				$year  = date( 'Y', strtotime( $month ) ) ?: '';
				$month = date( 'm', strtotime( $month ) ) ?: '';
			}

			$period_start = date( 'Ymd', mktime( 0, 0, 0, $month,     1, $year ) );
			$period_end   = date( 'Ymd', mktime( 0, 0, 0, $month + 1, 0, $year ) );
		}

		// if we haven't set it based on month, check for a event_period_start
		if ( ! $period_start && isset( $query->query_vars['event_period_start'] ) && ! empty( $query->query_vars['event_period_start'] )
			&& strtotime( $query->query_vars['event_period_start'] ) ) {
				$period_start = date( 'Ymd', strtotime( $query->query_vars['event_period_start'] ) );
		}

		// now do the same for event_period_end
		if ( ! $period_end && isset( $query->query_vars['event_period_end'] ) && ! empty( $query->query_vars['event_period_end'] )
			&& strtotime( $query->query_vars['event_period_end'] ) ) {
			$period_end = date( 'Ymd', strtotime( $query->query_vars['event_period_end'] ) );
		}

		$meta_query = $query->get('meta_query');
		$meta_query[] = array(
			'key'     => 'pantheon_event_end_date',
			'value'   => $period_start ?: date( 'Ymd', strtotime( '-2 days' ) ),
			'compare' => '>=',
		);
		if ( $period_end ) {
			$meta_query[] = array(
				'key'     => 'pantheon_event_start_date',
				'value'   => $period_end,
				'compare' => '<=',
			);
		}
		$query->set('meta_query',$meta_query);
	}
}
add_action( 'pre_get_posts', __NAMESPACE__ . '\action__pre_get_posts' );

function filter__query_vars( $vars ){
	$vars = array_merge( $vars, array(
		'event_period_month',
		'event_period_start',
		'event_period_end',
	));
	return $vars;
}
add_filter( 'query_vars', __NAMESPACE__ . '\filter__query_vars' );

// create post_meta field to sort events by
function action__save_post ( $post_id ) {
	if ( ( 'pantheon_event' == get_post_type( $post_id ) ) && ( $agenda = get_field( 'agenda_items', $post_id ) ) && is_array( $agenda ) ) {
		$dates = wp_list_pluck( $agenda, 'date' );
		$start_date  = min( $dates );
		$end_date = max( $dates );
	} else {
		$start_date = $end_date = 'TBD'; // this won't actually show in the UI but makes sure these events are on the list
	}
	update_post_meta( $post_id, 'pantheon_event_start_date', $start_date );
	update_post_meta( $post_id, 'pantheon_event_end_date',   $end_date );
}
add_action( 'save_post', __NAMESPACE__ . '\action__save_post' );