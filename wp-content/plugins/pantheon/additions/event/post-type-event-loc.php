<?php
namespace Pantheon\Addition\Event\Location;

	/*===========================================================================
	EVENT LOCATION CUSTOM POST TYPE
	===========================================================================*/

	function action__init() {
		$namespace = 'pantheon';
		$single    = __( 'Event Location',  $namespace );
		$plural    = __( 'Event Locations', $namespace );

		register_post_type( 'pantheon_event_loc',
			array(
				'labels' => array(
					'name'               => $plural,
					'singular_name'      => $single,
					'menu_name'          => $plural,
					'name_admin_bar'     => $single,
					'add_new'            =>          __( 'Add New',              $namespace ),
					'add_new_item'       => sprintf( __( 'Add New %s',           $namespace ), $single ),
					'new_item'           => sprintf( __( 'New %s',               $namespace ), $single ),
					'edit_item'          => sprintf( __( 'Edit %s',              $namespace ), $single ),
					'view_item'          => sprintf( __( 'View %s',              $namespace ), $single ),
					'all_items'          => sprintf( __( 'All %s',               $namespace ), $plural ),
					'search_items'       => sprintf( __( 'Search %s',            $namespace ), $plural ),
					'parent_item_colon'  => sprintf( __( 'Parent %s:',           $namespace ), $plural ),
					'not_found'          => sprintf( __( 'No %s found',          $namespace ), $plural ),
					'not_found_in_trash' => sprintf( __( 'No %s found in trash', $namespace ), $plural ),
				),
				'hierarchical'        => true,
				'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'has_archive'         => true,
				'query_var'           => true,
				'can_export'          => true,
				'rewrite'             => array( 'slug' => sanitize_key( str_replace( ' ', '-', $single ) ) ),
				'capability_type'     => 'post',
			)
		);
	}
	add_action( 'init', __NAMESPACE__ . '\action__init' );

