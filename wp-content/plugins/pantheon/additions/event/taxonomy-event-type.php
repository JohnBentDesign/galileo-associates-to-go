<?php
namespace Pantheon\Addition\Event\Type;

	/*===========================================================================
	EVENT TYPE TAXONOMY
	===========================================================================*/

	function action__init() {
		$namespace = 'pantheon';
		$single    = __( 'Event Type',  $namespace );
		$plural    = __( 'Event Types', $namespace );

		register_taxonomy( 'pantheon_event_type', array( 'pantheon_event', 'post' ), array(
			'labels' => array(
				'name'                       => $single,
				'singular_name'              => $plural,
				'search_items'               => sprintf( __( 'Search %s',                 $namespace ), $plural ),
				'popular_items'              => sprintf( __( 'Popular %s',                $namespace ), $plural ),
				'all_items'                  => sprintf( __( 'All %s',                    $namespace ), $plural ),
				'parent_item'                => sprintf( __( 'Parent %s',                 $namespace ), $single ),
				'parent_item_colon'          => sprintf( __( 'Parent %s:',                $namespace ), $single ),
				'edit_item'                  => sprintf( __( 'Edit %s',                   $namespace ), $single ),
				'update_item'                => sprintf( __( 'Update %s',                 $namespace ), $single ),
				'add_new_item'               => sprintf( __( 'Add New %s',                $namespace ), $single ),
				'new_item_name'              => sprintf( __( 'New %s',                    $namespace ), $single ),
				'separate_items_with_commas' => sprintf( __( 'Separate %s with commas',   $namespace ), $plural ),
				'add_or_remove_items'        => sprintf( __( 'Add or remove %s',          $namespace ), $plural ),
				'choose_from_most_used'      => sprintf( __( 'Choose from most used %s',  $namespace ), $plural ),
				'menu_name'                  => $plural,
			),
			'public'            => true,
			'show_in_nav_menus' => true,
			'show_ui'           => true,
			'show_tagcloud'     => false,
			'show_admin_column' => false,
			'hierarchical'      => true,
			'rewrite'           => array(
				'slug'              => 'event/' . sanitize_key( str_replace( ' ', '-', $single ) ),
				'with_front'        => false ),
			'query_var'         => true
		) );
	}
	add_action( 'init', __NAMESPACE__ . '\action__init' );
