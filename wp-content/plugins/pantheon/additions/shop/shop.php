<?php
	/*===========================================================================
	PANTHEON ADDITION: SHOP
	===========================================================================*/
	// An additional feature for Patheon that adds Jigoshop on certain themes.
	// This also includes some basic

	// Themes to Include This Addition In
	global $shopArray;
	$shopArray = array('persephone', 'hecate', 'aphrodite', 'demeter');

	// INCLUDE SHOP FILES
	//---------------------------------------------------------------------------
	if(pantheon_function_addition($shopArray)){

		// Get our functions related to the shop
		require_once('shop-functions.php');


		/*===========================================================================
		ALLOW JIGOSHOP CAPABILITIES TO EDITOR ROLE
		===========================================================================*/
		$role = get_role( 'editor' );

		$role->add_cap( 'assign_product_terms' );
		$role->add_cap( 'assign_shop_coupon_terms' );
		$role->add_cap( 'assign_shop_order_terms' );
		$role->add_cap( 'delete_others_products' );
		$role->add_cap( 'delete_others_shop_coupons' );
		$role->add_cap( 'delete_others_shop_orders' );
		$role->add_cap( 'delete_private_products' );
		$role->add_cap( 'delete_private_shop_coupons' );
		$role->add_cap( 'delete_private_shop_orders' );
		$role->add_cap( 'delete_product' );
		$role->add_cap( 'delete_product_terms' );
		$role->add_cap( 'delete_products' );
		$role->add_cap( 'delete_published_products' );
		$role->add_cap( 'delete_published_shop_coupons' );
		$role->add_cap( 'delete_published_shop_orders' );
		$role->add_cap( 'delete_shop_coupon' );
		$role->add_cap( 'delete_shop_coupon_terms' );
		$role->add_cap( 'delete_shop_coupons' );
		$role->add_cap( 'delete_shop_order' );
		$role->add_cap( 'delete_shop_order_terms' );
		$role->add_cap( 'delete_shop_orders' );
		$role->add_cap( 'edit_others_products' );
		$role->add_cap( 'edit_others_shop_coupons' );
		$role->add_cap( 'edit_others_shop_orders' );
		$role->add_cap( 'edit_private_products' );
		$role->add_cap( 'edit_private_shop_coupons' );
		$role->add_cap( 'edit_private_shop_orders' );
		$role->add_cap( 'edit_product' );
		$role->add_cap( 'edit_product_terms' );
		$role->add_cap( 'edit_products' );
		$role->add_cap( 'edit_published_products' );
		$role->add_cap( 'edit_published_shop_coupons' );
		$role->add_cap( 'edit_published_shop_orders' );
		$role->add_cap( 'edit_shop_coupon' );
		$role->add_cap( 'edit_shop_coupon_terms' );
		$role->add_cap( 'edit_shop_coupons' );
		$role->add_cap( 'edit_shop_order' );
		$role->add_cap( 'edit_shop_order_terms' );
		$role->add_cap( 'edit_shop_orders' );
		$role->add_cap( 'manage_jigoshop' );
		$role->add_cap( 'manage_jigoshop_coupons' );
		$role->add_cap( 'manage_jigoshop_orders' );
		$role->add_cap( 'manage_jigoshop_products' );
		$role->add_cap( 'manage_product_terms' );
		$role->add_cap( 'manage_shop_coupon_terms' );
		$role->add_cap( 'manage_shop_order_terms' );
		$role->add_cap( 'publish_products' );
		$role->add_cap( 'publish_shop_coupons' );
		$role->add_cap( 'publish_shop_orders' );
		$role->add_cap( 'read_private_products' );
		$role->add_cap( 'read_private_shop_coupons' );
		$role->add_cap( 'read_private_shop_orders' );
		$role->add_cap( 'read_product' );
		$role->add_cap( 'read_shop_coupon' );
		$role->add_cap( 'read_shop_order' );
		$role->add_cap( 'view_jigoshop_reports' );
	}