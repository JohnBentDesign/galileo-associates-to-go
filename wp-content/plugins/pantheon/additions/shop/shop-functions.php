<?php
	/*===========================================================================
	PANTHEON ADDITION: SHOP FUNCTIONS
	===========================================================================*/


	/*===========================================================================
	JIGOSHOP ADD SHOP INCLUDES
	===========================================================================*/
	function pantheon_action_shop_includes(){
		if(!class_exists('jigoshop')){
			return;
		}
		else {
			// Load Shop specific scripts
			require_once('advanced-custom-fields-shop-options.php');
			require_once('advanced-custom-fields-shop-product.php');
			require_once('pantheon_display_post_shop_xsell.php');
		}
	}
	add_action('after_setup_theme', 'pantheon_action_shop_includes');



	/*===========================================================================
	JIGOSHOP ID
	===========================================================================*/
	function pantheon_action_shop_get_page_id() {
		global $jigoshopShopID;

		// Sometimes Jigoshop is de-activated - do nothing without it
		if(!class_exists('jigoshop')){
			$jigoshopShopID = '';
			return;
		}
		else {
			// Get the ID of the page that is set as the catalog and make it a global variable
			$jigoshop_options 	= Jigoshop_Base::get_options();
			$jigoshopShopID 	= $jigoshop_options->get_option( 'jigoshop_shop_page_id' );
		}
	}
	add_action('after_setup_theme', 'pantheon_action_shop_get_page_id');



	/*===========================================================================
	REMOVE ACTIONS (after theme load)
	===========================================================================*/
	function pantheon_action_shop_remove_actions(){
		if(!class_exists('jigoshop')){
			return;
		}
		else {
			// Absolutely remove description tab
			remove_action('jigoshop_product_tabs', 'jigoshop_product_description_tab', 10);
			remove_action('jigoshop_product_tab_panels', 'jigoshop_product_description_panel', 10);

			// Remove Review Tab if not enabled
			$shopActive = get_field('shop_enable', 'options');
			$shopReview = get_field('shop_reviews', 'options');
			if(!$shopReview){
				remove_action('jigoshop_product_tabs', 'jigoshop_product_reviews_tab', 30);
				remove_action('jigoshop_product_tab_panels', 'jigoshop_product_reviews_panel', 30);
			}

			// Remove Wrong Thumbnail output
			remove_action('jigoshop_before_single_product_summary', 'jigoshop_show_product_images', 20);
		}
	}
	add_action('after_setup_theme', 'pantheon_action_shop_remove_actions');



	/*===========================================================================
	ADD ACTIONS
	===========================================================================*/
	// Fix the images function to actually output the correct thumbnail size
	//---------------------------------------------------------------------------
	function pantheon_action_shop_show_product_images(){
		global $_product, $post;

		echo '<div class="images">';

		do_action( 'jigoshop_before_single_product_summary_thumbnails', $post, $_product );

		$thumb_id = 0;
		if (has_post_thumbnail()) :
			$thumb_id = get_post_thumbnail_id();
			// since there are now user settings for sizes, shouldn't need filters -JAP-
			//$large_thumbnail_size = apply_filters('single_product_large_thumbnail_size', 'shop_large');
			$large_thumbnail_size = jigoshop_get_image_size( 'shop_large' );
			$image_classes = apply_filters( 'jigoshop_product_image_classes', array(), $_product );
			array_unshift( $image_classes, 'zoom' );
			$image_classes = implode( ' ', $image_classes );

			$args = array( 'post_type' => 'attachment', 'post_mime_type' => 'image', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post->ID, 'orderby' => 'menu_order', 'order' => 'asc', 'fields' => 'ids' );
			$attachments = get_posts($args);
			$attachment_count = count( $attachments );
			if ( $attachment_count > 1 ) {
				$gallery = '[product-gallery]';
			} else {
				$gallery = '';
			}

			echo '<a href="'.wp_get_attachment_url($thumb_id).'" class="'.$image_classes.'" rel="prettyPhoto'.$gallery.'">';
			// this is it. this is the only line we changed
			the_post_thumbnail('shop_large');
			echo '</a>';
		else :
			echo jigoshop_get_image_placeholder( 'shop_large' );
		endif;

		do_action('jigoshop_product_thumbnails');

		echo '</div>';
	}
	add_action('jigoshop_before_single_product_summary', 'pantheon_action_shop_show_product_images', 1);


	// Change text for products that don't have prices
	//---------------------------------------------------------------------------
	function pantheon_filter_shop_price_text($text) {
		// Sometimes Jigoshop is de-activated - do nothing without it
		if(!class_exists('jigoshop')){
			return;
		}
		else {
			$notAnnounced 	= get_field('shop_price', 'options');
			$free 			= get_field('shop_free', 'options');
			return str_replace('Price Not Announced',$notAnnounced,$text);
			return str_replace('Free',$free,$text);
		}
	}
	add_filter('jigoshop_product_get_price_html', 'pantheon_filter_shop_price_text');

