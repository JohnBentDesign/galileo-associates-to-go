<?php
	/*===========================================================================
	OPTIONS: SHOP: PRODUCT DETAILS
	===========================================================================*/

	if ( function_exists('register_field_group') ):

	register_field_group(array (
		'key' => 'group_54232c996fa05',
		'title' => '[Shop] Product Details',
		'fields' => array (
			array (
				'key' => 'field_53580155da5d5',
				'label' => '',
				'name' => '',
				'prefix' => '',
				'type' => 'message',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'message' => '<h2 style="background-color: #0074a2; padding: 5px 10px; color: #ffffff">CONTACT FORM</h2>',
			),
			array (
				'key' => 'field_5357fc330bfe3',
				'label' => 'Form Display',
				'name' => 'product_form_type',
				'prefix' => '',
				'type' => 'radio',
				'instructions' => 'Choose which form you\'d like to use. Forms are used to collect interest in a certain product. Default form is set under Options -> Product Form',
				'required' => 0,
				'conditional_logic' => 0,
				'choices' => array (
					'default' => 'Use default',
					'choose' => 'Choose form',
					'none' => 'None',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => 'default',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_5357fca30bfe4',
				'label' => 'Form Shortcode',
				'name' => 'product_form_choose',
				'prefix' => '',
				'type' => 'text',
				'instructions' => 'This form will appear alongside the product to garner more interest. Only shortcodes for forms should be added here--anything else will render an error.',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_5357fc330bfe3',
							'operator' => '==',
							'value' => 'choose',
						),
					),
				),
				'default_value' => '',
				'maxlength' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'product',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'seamless',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));

	endif;



