<?php
	/*===========================================================================
	PANTHEON ADDITION: SHOP CUSTOM CROSS SELL FUNCITON
	===========================================================================*/
	if(!function_exists('pantheon_display_post_shop_xsell')){
		function pantheon_display_post_shop_xsell($columnCount = 3) {
			global $columns, $post, $crossIDS;

			$jigoshop_options 	= Jigoshop_Base::get_options();
			$catalogColumn		= $jigoshop_options->get_option('jigoshop_catalog_columns');

			$crossIDS 	= get_post_meta($post->ID, 'crosssell_ids', true);
			$crossID 	= array();
			foreach($crossIDS as $crosses){
				if($crosses != 0){
					array_push($crossID, $crosses);
				}
			}

			if(!empty($crossID)) :
				echo '<div class="related products cross-sells">';
				$args = array(
					'post_type'				=> 'product',
					'ignore_sticky_posts' 	=> 1,
					'posts_per_page' 		=> $columnColumn,
					'orderby' 				=> 'rand',
					'post__in' 				=> $crossID
				);
				query_posts($args);
				if (have_posts()) :
					echo '<h2>'.__('You may be interested in&hellip;', 'jigoshop').'</h2>';
					jigoshop_get_template_part( 'loop', 'shop' );
				endif;
				echo '</div>';
			endif;
			wp_reset_query();
		}
	}