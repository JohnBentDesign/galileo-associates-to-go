<?php
	/*===========================================================================
	TESTIMONIAL TYPES TAXONOMY
	=============================================================================
	*/
	add_action( 'init', 'register_taxonomy_testimonial_types' );

	function register_taxonomy_testimonial_types() {

		$labels = array( 
			'name' => _x( 'Testimonial Types', 'testimonial-types' ),
			'singular_name' => _x( 'Testimonial Type', 'testimonial-types' ),
			'search_items' => _x( 'Search Testimonial Types', 'testimonial-types' ),
			'popular_items' => _x( 'Popular Testimonial Types', 'testimonial-types' ),
			'all_items' => _x( 'All Testimonial Types', 'testimonial-types' ),
			'parent_item' => _x( 'Parent Testimonial Type', 'testimonial-types' ),
			'parent_item_colon' => _x( 'Parent Testimonial Type:', 'testimonial-types' ),
			'edit_item' => _x( 'Edit Testimonial Type', 'testimonial-types' ),
			'update_item' => _x( 'Update Testimonial Type', 'testimonial-types' ),
			'add_new_item' => _x( 'Add New Testimonial Type', 'testimonial-types' ),
			'new_item_name' => _x( 'New Testimonial Type', 'testimonial-types' ),
			'separate_items_with_commas' => _x( 'Separate testimonial Types with commas', 'testimonial-types' ),
			'add_or_remove_items' => _x( 'Add or remove testimonial Types', 'testimonial-types' ),
			'choose_from_most_used' => _x( 'Choose from the most used testimonial Types', 'testimonial-types' ),
			'menu_name' => _x( 'Testimonial Types', 'testimonial-types' ),
		);

		$args = array( 
			'labels' => $labels,
			'public' => true,
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'show_tagcloud' => false,
			'show_admin_column' => false,
			'hierarchical' => true,

			'rewrite' => true,
			'query_var' => true
		);

		register_taxonomy( 'testimonial-types', array('testimonial'), $args );
	}