<?php
	/*===========================================================================
	PANTHEON ADDITION: TESTIMONIAL
	===========================================================================*/
	// An additional feature for Patheon that allows testimonials on certain themes.
	// Testimonials have content, name, and location

	// Themes to Include This Addition In
	$themeArray = array('aphrodite', 'hera', 'juno');

	// INCLUDE TESTIMONIAL FILES
	//---------------------------------------------------------------------------
	if(pantheon_function_addition($themeArray)){
		require_once('post-type-testimonial.php');
		require_once('taxonomy-testimonial-types.php');
		require_once('advanced-custom-fields-testimonial.php');
	}