<?php
	/*===========================================================================
	OPTIONS: TESTIMONIAL OPTIONS
	===========================================================================*/

	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_testimonial-post-details',
			'title' => '[Testimonial] Post Details',
			'fields' => array (
				array (
					'key' => 'field_5321c5894ee3a',
					'label' => 'Testimonial',
					'name' => 'testimonial_content',
					'type' => 'wysiwyg',
					'instructions' => 'Excerpt will be trimmed to 60 words.',
					'default_value' => '',
					'toolbar' => 'full',
					'media_upload' => 'yes',
				),
				array (
					'key' => 'field_5321c5ad4ee3b',
					'label' => 'Author',
					'name' => 'testimonial_author',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5321c2bdb9721',
					'label' => 'Location of Testimonial Author',
					'name' => 'testimonial_location',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'testimonial',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
					0 => 'the_content',
					1 => 'excerpt',
					2 => 'custom_fields',
					3 => 'discussion',
					4 => 'comments',
					5 => 'tags',
					6 => 'send-trackbacks',
				),
			),
			'menu_order' => 0,
		));
	}






