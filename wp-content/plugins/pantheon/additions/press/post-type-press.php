<?php
	/*===========================================================================
	PRESS CUSTOM POST TYPE
	=============================================================================
	*/

	add_action( 'init', 'register_cpt_press' );

	function register_cpt_press() {

		$labels = array( 
			'name' => _x( 'Press', 'press' ),
			'singular_name' => _x( 'Press', 'press' ),
			'add_new' => _x( 'Add New', 'press' ),
			'add_new_item' => _x( 'Add New Press', 'press' ),
			'edit_item' => _x( 'Edit Press', 'press' ),
			'new_item' => _x( 'New Press', 'press' ),
			'view_item' => _x( 'View Press', 'press' ),
			'search_items' => _x( 'Search Press', 'press' ),
			'not_found' => _x( 'No press found', 'press' ),
			'not_found_in_trash' => _x( 'No press found in Trash', 'press' ),
			'parent_item_colon' => _x( 'Parent Press:', 'press' ),
			'menu_name' => _x( 'Press', 'press' ),
		);

		$args = array( 
			'labels' => $labels,
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'thumbnail', 'revisions' ),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post'
		);

		register_post_type( 'press', $args );
	}