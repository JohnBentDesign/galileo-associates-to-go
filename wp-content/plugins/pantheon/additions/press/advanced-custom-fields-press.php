<?php
	/*===========================================================================
	OPTIONS: PRESS OPTIONS
	===========================================================================*/
	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_posts-press',
			'title' => '[Pantheon] Posts: Press',
			'fields' => array (
				array (
					'key' => 'field_52547596a04c5',
					'label' => 'Button Text',
					'name' => 'press_text',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_52322156d7a17',
					'label' => 'Is this press article an offsite link, a video, or a file of an article?',
					'name' => 'press_type',
					'type' => 'radio',
					'choices' => array (
						'link' => 'Link',
						'video' => 'Video',
						'file' => 'File',
					),
					'other_choice' => 0,
					'save_other_choice' => 0,
					'default_value' => '',
					'layout' => 'horizontal',
				),
				array (
					'key' => 'field_5232218dd7a18',
					'label' => 'Article/Video URL',
					'name' => 'press_url',
					'type' => 'text',
					'required' => 1,
					'conditional_logic' => array (
						'status' => 1,
						'rules' => array (
							array (
								'field' => 'field_52322156d7a17',
								'operator' => '!=',
								'value' => 'file',
							),
						),
						'allorany' => 'all',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
				array (
					'key' => 'field_523222b0d7a19',
					'label' => 'Article File',
					'name' => 'press_file',
					'type' => 'file',
					'required' => 1,
					'conditional_logic' => array (
						'status' => 1,
						'rules' => array (
							array (
								'field' => 'field_52322156d7a17',
								'operator' => '==',
								'value' => 'file',
							),
						),
						'allorany' => 'all',
					),
					'save_format' => 'url',
					'library' => 'uploadedTo',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'press',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
					0 => 'excerpt',
					1 => 'custom_fields',
					2 => 'discussion',
					3 => 'comments',
					4 => 'slug',
					5 => 'author',
					6 => 'format',
					7 => 'categories',
					8 => 'tags',
					9 => 'send-trackbacks',
				),
			),
			'menu_order' => 0,
		));
	}
