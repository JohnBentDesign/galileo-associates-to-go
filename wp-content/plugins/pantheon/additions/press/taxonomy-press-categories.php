<?php
	/*===========================================================================
	PRESS CATEGORIES TAXONOMY
	===========================================================================*/
	add_action( 'init', 'register_taxonomy_press_categories' );

	function register_taxonomy_press_categories() {

	    $labels = array( 
	        'name' => _x( 'Press Categories', 'press_categories' ),
	        'singular_name' => _x( 'Press Category', 'press_categories' ),
	        'search_items' => _x( 'Search Press Categories', 'press_categories' ),
	        'popular_items' => _x( 'Popular Press Categories', 'press_categories' ),
	        'all_items' => _x( 'All Press Categories', 'press_categories' ),
	        'parent_item' => _x( 'Parent Press Category', 'press_categories' ),
	        'parent_item_colon' => _x( 'Parent Press Category:', 'press_categories' ),
	        'edit_item' => _x( 'Edit Press Category', 'press_categories' ),
	        'update_item' => _x( 'Update Press Category', 'press_categories' ),
	        'add_new_item' => _x( 'Add New Press Category', 'press_categories' ),
	        'new_item_name' => _x( 'New Press Category', 'press_categories' ),
	        'separate_items_with_commas' => _x( 'Separate press categories with commas', 'press_categories' ),
	        'add_or_remove_items' => _x( 'Add or remove press categories', 'press_categories' ),
	        'choose_from_most_used' => _x( 'Choose from the most used press categories', 'press_categories' ),
	        'menu_name' => _x( 'Press Categories', 'press_categories' ),
	    );

	    $args = array( 
	        'labels' => $labels,
	        'public' => true,
	        'show_in_nav_menus' => true,
	        'show_ui' => true,
	        'show_tagcloud' => false,
	        'show_admin_column' => false,
	        'hierarchical' => true,

	        'rewrite' => true,
	        'query_var' => true
	    );

	    register_taxonomy( 'press_categories', array('press'), $args );
	}