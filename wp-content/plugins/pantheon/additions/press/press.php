<?php
	/*===========================================================================
	PANTHEON ADDITION: PRESS
	===========================================================================*/
	// An additional feature for Patheon that allows press on certain themes.
	// Press allows the attachment of a file or link to video or offsite article

	// Themes to Include This Addition In
	$themeArray = array();

	// INCLUDE PRESS FILES
	//---------------------------------------------------------------------------
	if(pantheon_function_addition($themeArray)){
		require_once('post-type-press.php');
		require_once('taxonomy-press-categories.php');
		// require_once('advanced-custom-fields-press.php');
	}