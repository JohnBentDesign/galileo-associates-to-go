<?php
	/*===========================================================================
	ENQUEUE SCRIPTS AND STYLES
	===========================================================================*/
	function pantheon_additions_gallery_check_section(){
		// Set $hasGallery specifically for sectional sites (Hera, Juno)
		global $post;
		$posts = get_field('page_sections', $post);

		if($posts):
			foreach($posts as $post):
				setup_postdata($post);

				while(has_sub_field('slide_type', $post->ID)){
					if(get_row_layout() == 'slide_gallery') {
						$hasGallery = true;
					}
					else {
						$hasGallery = false;
					}
				}

			endforeach;
			wp_reset_postdata();
		endif;

		if($posts){
			return $hasGallery;
		}
		else {
			return;
		}
	}

	// JAVASCRIPTS
	//---------------------------------------------------------------------------
	function pantheon_additions_gallery_scripts(){
		if(!is_admin()){
			global $post; 

			$isGallery 	= ($post->post_type == 'gallery') ? true: false;
			$gallery 	= get_field('page_gallery', $post->ID);
			$hasGallery = ($gallery[0] || pantheon_additions_gallery_check_section()) ? true : false;

			// Plugin Scripts
			if($hasGallery || $isGallery){
				wp_enqueue_script('flexslider', plugins_url('javascripts/flexslider.min.js', __FILE__), array('jquery'), '', true);
				wp_enqueue_script('iLightbox-raf', plugins_url('ilightbox/js/jquery.requestAnimationFrame.js', __FILE__), array('jquery'), '', true);
				wp_enqueue_script('iLightbox-mw', plugins_url('ilightbox/js/jquery.mousewheel.js', __FILE__), array('jquery'), '', true);
				wp_enqueue_script('iLightbox', plugins_url('ilightbox/js/ilightbox.packed.js', __FILE__), array('jquery', 'iLightbox-raf', 'iLightbox-mw'), '', true);
				wp_enqueue_script('unveil', plugins_url('javascripts/unveil.min.js', __FILE__), array('jquery'), '', true);
				wp_enqueue_script('gallery', plugins_url('javascripts/gallery.js', __FILE__), array('jquery', 'unveil', 'iLightbox'), '', true);
			}
		}
	}
	add_action('wp_enqueue_scripts', 'pantheon_additions_gallery_scripts');

	// STYLESHEETS
	//---------------------------------------------------------------------------
	function pantheon_additions_gallery_styles(){
		if(!is_admin()){
			global $post;
			
			$isGallery 	= ($post->post_type == 'gallery') ? true: false;
			$gallery 	= get_field('page_gallery', $post->ID);
			$hasGallery = ($gallery[0] || pantheon_additions_gallery_check_section()) ? true : false;

			// Plugin Stylesheets
			if($hasGallery || $isGallery){
				wp_enqueue_style('gallery', plugins_url('stylesheets/gallery.css', __FILE__));
				wp_enqueue_style('iLightbox', plugins_url('ilightbox/css/ilightbox.css', __FILE__));				
			}
		}
	}
	add_action('wp_enqueue_scripts', 'pantheon_additions_gallery_styles');