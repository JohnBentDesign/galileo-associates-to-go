/**
 * jquery.hoverdir.js v1.1.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2012, Codrops
 * http://www.codrops.com
 */
;( function( $, window, undefined ) {
	
	'use strict';

	$.HoverDir = function( options, element ) {
		this.$el = $( element );
		this._init( options );
	};

	// the options
	$.HoverDir.defaults = {
		speed : 300,
		easing : 'ease',
		hoverDelay : 0,
		inverse : false
	};

	$.HoverDir.prototype = {

		_init : function( options ) {
			
			// options
			this.options = $.extend( true, {}, $.HoverDir.defaults, options );
			// transition properties
			this.transitionProp = 'all ' + this.options.speed + 'ms ' + this.options.easing;
			// support for CSS transitions
			this.support = Modernizr.csstransitions;
			// load the events
			this._loadEvents();

		},
		_loadEvents : function() {

			var self = this;
			
			this.$el.on( 'mouseenter.hoverdir, mouseleave.hoverdir', function( event ) {
				
				var $el = $( this ),
					$hoverElem = $el.find( 'div' ),
					direction = self._getDir( $el, { x : event.pageX, y : event.pageY } ),
					styleCSS = self._getStyle( direction );
				
				if( event.type === 'mouseenter' ) {
					
					$hoverElem.hide().css( styleCSS.from );
					clearTimeout( self.tmhover );

					self.tmhover = setTimeout( function() {
						
						$hoverElem.show( 0, function() {
							
							var $el = $( this );
							if( self.support ) {
								$el.css( 'transition', self.transitionProp );
							}
							self._applyAnimation( $el, styleCSS.to, self.options.speed );

						} );
						
					
					}, self.options.hoverDelay );
					
				}
				else {
				
					if( self.support ) {
						$hoverElem.css( 'transition', self.transitionProp );
					}
					clearTimeout( self.tmhover );
					self._applyAnimation( $hoverElem, styleCSS.from, self.options.speed );
					
				}
					
			} );

		},
		// credits : http://stackoverflow.com/a/3647634
		_getDir : function( $el, coordinates ) {
			
			// the width and height of the current div
			var w = $el.width(),
				h = $el.height(),

				// calculate the x and y to get an angle to the center of the div from that x and y.
				// gets the x value relative to the center of the DIV and "normalize" it
				x = ( coordinates.x - $el.offset().left - ( w/2 )) * ( w > h ? ( h/w ) : 1 ),
				y = ( coordinates.y - $el.offset().top  - ( h/2 )) * ( h > w ? ( w/h ) : 1 ),
			
				// the angle and the direction from where the mouse came in/went out clockwise (TRBL=0123);
				// first calculate the angle of the point,
				// add 180 deg to get rid of the negative values
				// divide by 90 to get the quadrant
				// add 3 and do a modulo by 4  to shift the quadrants to a proper clockwise TRBL (top/right/bottom/left) **/
				direction = Math.round( ( ( ( Math.atan2(y, x) * (180 / Math.PI) ) + 180 ) / 90 ) + 3 ) % 4;
			
			return direction;
			
		},
		_getStyle : function( direction ) {
			
			var fromStyle, toStyle,
				slideFromTop = { left : '0px', top : '-110%' },
				slideFromBottom = { left : '0px', top : '110%' },
				slideFromLeft = { left : '-110%', top : '0px' },
				slideFromRight = { left : '110%', top : '0px' },
				slideTop = { top : '0px' },
				slideLeft = { left : '0px' };
			
			switch( direction ) {
				case 0:
					// from top
					fromStyle = !this.options.inverse ? slideFromTop : slideFromBottom;
					toStyle = slideTop;
					break;
				case 1:
					// from right
					fromStyle = !this.options.inverse ? slideFromRight : slideFromLeft;
					toStyle = slideLeft;
					break;
				case 2:
					// from bottom
					fromStyle = !this.options.inverse ? slideFromBottom : slideFromTop;
					toStyle = slideTop;
					break;
				case 3:
					// from left
					fromStyle = !this.options.inverse ? slideFromLeft : slideFromRight;
					toStyle = slideLeft;
					break;
			};
			
			return { from : fromStyle, to : toStyle };
					
		},
		// apply a transition or fallback to jquery animate based on Modernizr.csstransitions support
		_applyAnimation : function( el, styleCSS, speed ) {

			$.fn.applyStyle = this.support ? $.fn.css : $.fn.animate;
			el.stop().applyStyle( styleCSS, $.extend( true, [], { duration : speed + 'ms' } ) );

		},

	};
	
	var logError = function( message ) {

		if ( window.console ) {

			window.console.error( message );
		
		}

	};
	
	$.fn.hoverdir = function( options ) {

		var instance = $.data( this, 'hoverdir' );
		
		if ( typeof options === 'string' ) {
			
			var args = Array.prototype.slice.call( arguments, 1 );
			
			this.each(function() {
			
				if ( !instance ) {

					logError( "cannot call methods on hoverdir prior to initialization; " +
					"attempted to call method '" + options + "'" );
					return;
				
				}
				
				if ( !$.isFunction( instance[options] ) || options.charAt(0) === "_" ) {

					logError( "no such method '" + options + "' for hoverdir instance" );
					return;
				
				}
				
				instance[ options ].apply( instance, args );
			
			});
		
		} 
		else {
		
			this.each(function() {
				
				if ( instance ) {

					instance._init();
				
				}
				else {

					instance = $.data( this, 'hoverdir', new $.HoverDir( options, this ) );
				
				}

			});
		
		}
		
		return instance;
		
	};
} )( jQuery, window );


//============================
// Tabbed Galleries
//============================
jQuery('.gallery-list').each(function(){
	// Get information about our gallery
	var gallery 	= jQuery(this),
		images		= gallery.find('.gallery-image'),
		imageCount 	= images.length,
		rowCount 	= gallery.attr('data-gallery-row'),
		columnCount = gallery.attr('data-gallery-column'),
		galleryName = gallery.attr('data-gallery-slug');

	// We're only going to do this if our imageCount is more than our tabTotal, and if it's not just infinite
	if( (rowCount != 'infinite') && (rowCount != '') ){
		tabTotal 	= rowCount * columnCount;

		if((imageCount > tabTotal)){

			// Prepare the Tabs
			gallery.children().wrapAll('<div class="tabs-content"/>');

			// Sectionalize our images
			var tabContentCount = 1;
			for(var i = 0; i < imageCount; i += tabTotal) {
				images.slice(i, i + tabTotal).wrapAll('<div class="content" id="' + galleryName + '-' + tabContentCount++ + '"></div>');
			}

			// Add Tab Container
			var tabCount = gallery.find('.content').length;
			gallery.append('<dl class="tabs" data-tab />');

			// Loop through and add our actual tabs
			var tabTabCount = 1;
			for(var t = 0; t < tabCount; t += 1){
				gallery.find('.tabs').append('<dd><a data-scroll href="#' + galleryName + '-' + tabTabCount++ + '"></a></dd>')
			}

			// Add active class to first content and tab so it works all right
			gallery.find('.content:first-child, .tabs dd:first-child').addClass('active');

			// We need to reinitialize foundation because we added all this DOM load
			jQuery(document).foundation();
		}
	}
});

jQuery(document).ready(function(){
	/*==========================================================================
	GALLERY
	==========================================================================*/
	// Lazy Load on Gallery Page
	//===================================================
	if(jQuery('.gallery').length > 0){
		jQuery('img.lazy').unveil(-100, function() {
			jQuery(this).load(function() {
				this.style.opacity = 1;
			});
		});
	}

	// Initiate iLightbox
	//===================================================
	var canShare 	= jQuery('.ilightbox').attr('data-lightbox-share'),
		title 		= jQuery('.ilightbox:first-of-type').attr('data-lightbox-title'),
		thumb 		= jQuery('.ilightbox:first-of-type').attr('data-lightbox-thumb');

	// FOR GROUP LIGHTBOXES (ie. has thumbnails)
	if(canShare == 'true'){
		jQuery('.ilightbox').iLightBox({
			skin: 'dark',
			social: {
				buttons: {
					facebook: true,
					twitter: true,
					googleplus: true,
					pinterest: { 
						source: 'http://www.pinterest.com/pin/create/bookmarklet/?url={URL}&media={URL}',
						text: 'Share on Pinterest'
					}
				}
			},
			linkId: title
		});
	}
	else {
		jQuery('.ilightbox').iLightBox({
			skin: 'dark',
			linkId: title
		});
	}

	// FOR SINGLE LIGHTBOXES (no thumbnails)
	jQuery('.single-lightbox').each(function(){
		if(jQuery(this).attr('id')){
			var canShare 	= jQuery(this).attr('data-lightbox-share'),
				title 		= jQuery(this).attr('data-lightbox-title'),
				thumb 		= jQuery(this).attr('data-lightbox-thumb');

				console.log(thumb);

			if(canShare == 'true'){
				jQuery(this).iLightBox({
					skin: 'dark',
					social: {
						buttons: {
							facebook: true,
							twitter: true,
							googleplus: true,
							pinterest: { 
								source: 'http://pinterest.com/pin/create/button/?url={URL}&media=' + thumb,
								text: 'Share on Pinterest'
							}
						}
					},
					linkId: title
				});
			}
			else {
				jQuery(this).iLightBox({
					skin: 'dark',
					linkId: title
				});
			}
		}
	});


	// Smart Hover Direction
	//===================================================
	jQuery('.gallery-list .gallery-image .inner').each( function() { jQuery(this).hoverdir(); } );
});

