<?php
	/*===========================================================================
	GALLERY DISPLAY
	=============================================================================
	Display of gallery items on single gallery posts
	*/

	if(!function_exists('pantheon_display_gallery')){
		function pantheon_display_gallery($postID = null, $displayOverride = null){

				// Get Globals
				global $post;
				$postID = ($postID) ? $postID : $post->ID;


				// Get our plethora of variables
				//----------------------------------------------------------
				// Overall Setting
				$lightboxTitle 				= ' data-lightbox-title="' . sanitize_title($post->post_title) . '"';
				$galleryImages 				= get_field('gallery_images', $postID);
				$galleryType 				= ($displayOverride) ? $displayOverride : get_field('gallery_type', $postID);
				$galleryCaption 			= get_field('gallery_caption', $postID);
				$galleryRandom 				= get_field('gallery_random', $postID);
				$galleryShare 				= get_field('gallery_share', $postID);
				$galleryWidth				= (get_field('gallery_width', $postID)) ? ' full-width' : '';
				$galleryClick 				= get_field('gallery_click', $postID);
				// Lightbox Setting
				$galleryLightbox 			= ($galleryClick == 'lightbox') ? true : false;
				$galleryLightboxThumb 		= get_field('gallery_lightbox_thumb', $postID);
				$galleryLightboxShare 		= ($galleryShare) ? ' data-lightbox-share="true"' : ' data-lightbox-share="false"';
				// Normal Gallery Setting
				$galleryNormThumb 			= get_field('gallery_norm_thumb', $postID);
				$galleryNormThumbSize 		= 'TTG Medium Thumbnail';
				$galleryNormThumbUp 		= get_field('gallery_norm_thumb_up', $postID);
				$galleryNormRowUp			= get_field('gallery_norm_thumb_row', $postID);
				$galleryNormThumbSpace 		= get_field('gallery_norm_thumb_space', $postID);
				$galleryNormThumbSpaceClass	= ($galleryNormThumbSpace) ? ' no-space' : '';
				switch($galleryNormThumbUp) {
					case 2: $galleryNormThumbClass = 'medium-4 large-6' . $galleryNormThumbSpaceClass; 	break;
					case 3: $galleryNormThumbClass = 'medium-4 large-4' . $galleryNormThumbSpaceClass; 	break;
					case 4: $galleryNormThumbClass = 'medium-4 large-3' . $galleryNormThumbSpaceClass;	break;
					case 6: $galleryNormThumbClass = 'medium-3 large-2' . $galleryNormThumbSpaceClass; $galleryNormThumbSize = 'thumbnail';	break;
				}
				// Slideshow Setting
				$gallerySlideAnimation 		= get_field('gallery_slide_animation', $postID);
				$gallerySlideAuto 			= (get_field('gallery_slide_auto')) ? 'true' : 'false';
				$gallerySlideSpeed			= get_field('gallery_slide_speed', $postID);
				$gallerySlideLoop 			= (get_field('gallery_slide_loop')) ? 'true' : 'false';
				$gallerySlideNav 			= get_field('gallery_slide_nav', $postID);
				$gallerySlideCarouselNav	= get_field('gallery_slide_carousel_nav', $postID);
				$gallerySlideThumb			= get_field('gallery_slide_thumb', $postID);
				$gallerySlideNumber 		= get_field('gallery_slide_number', $postID);
				$galleryArray 				= array();
				// We'll be pushing all our variables to an array so we can reach them outside of the loop
				$imageArray 				= array();

				$i = 0;

				// Contain our "Normal" Gallery
				if($galleryType == 'normal'){ echo '<div class="gallery-list row' . $galleryWidth . '" data-gallery-row="' . $galleryNormRowUp . '" data-gallery-column="' . $galleryNormThumbUp . '" data-gallery-slug="' . sanitize_title(get_the_title()) . '">'; }
				if($galleryRandom) { shuffle($galleryImages); }

				// Loop through all the images. Weeee!
				foreach($galleryImages as $image){
					// Get our image's information
					$imageID 			= $image['id'];
					$imageAlt 			= $image['alt'];
					$imageTitle 		= $image['title'];
					$imageCaption 		= $image['caption'];
					$imageDesc			= $image['description'];

					// NORMAL DISPLAY
					//===================================
					if($galleryType == 'normal'){
						// Get our image's extended information
						$imageSize 			= ($galleryNormThumb) ? $galleryNormThumbSize : 'TTG Featured Image';
						$imageDisplaySize	= (!$galleryNormThumb) ? ' full columns' : ' thumb columns ' . $galleryNormThumbClass;
						$imageFullOutput	= pantheon_display_post_field_image($image, $imageSize, 'image', false, ' class="lazy"');

						// Get Image Overlay
						$displayTitle 		= ($imageTitle && !empty($galleryCaption) && (in_array('image', $galleryCaption))) ? '<h4>' . $imageTitle . '</h4>' : '';
						$displayCaption 	= ($imageCaption && !empty($galleryCaption) && (in_array('image', $galleryCaption))) ? '<p>' . $imageCaption . '</p>' : '';
						$displayOverlay 	= ($galleryCaption) ? '<div class="gallery-overlay"><div class="table"><div class="table-row"><div class="table-cell">' . $displayTitle . $displayCaption . '</div></div></div></div>' : '';

						// Get Image Lightbox
						$lightboxGroup		= ($galleryLightboxThumb) ? ' class="ilightbox"' : ' id="' . sanitize_title($imageTitle) . '" class="single-lightbox"';
						$lightboxGroup 		= ($galleryLightbox || $galleryNormThumb) ? $lightboxGroup : '';
						$lightboxDesc 		= ( (!empty($galleryCaption)) && (in_array('lightbox', $galleryCaption)) ) ? ' data-caption="' . $imageDesc . '"' : '';
						$lightboxOptions 	= ' data-options="thumbnail: \'' . $image['sizes']['TTG Medium Thumbnail'] . '\'"'; 
						$lightboxThumb 		= ' data-lightbox-thumb="' . $image['sizes']['TTG Medium Thumbnail'] . '"';
						$lightboxOpen 		= ($galleryLightbox) ? '<a href="' . $image['sizes']['large'] . '"' . $lightboxGroup . $lightboxDesc . $lightboxOptions . $galleryLightboxShare . $lightboxTitle . $lightboxThumb . '>' : '';
						$lightboxClose 		= ($galleryLightbox) ? '</a>' : '';
						$lightboxTitle 		= ($lightboxGroup) ? ' data-lightbox-title="' . sanitize_title($imageTitle) . '"' : $lightboxTitle;

						// External Link
						$external 			= ($galleryClick == 'external') ? true : false;
						$externalLink 		= (get_field('external_link', $imageID)) ?  get_field('external_link', $imageID) : '';
						$externalOpen 		= ($external && $externalLink) ? '<a href="' . get_field('external_link', $imageID) . '" target="_blank">' : '';
						$externalClose 		= ($external && $externalLink) ? '</a>' : '';

						// Show the 
						echo '<div class="gallery-image' . $imageDisplaySize . '"><div class="inner">';
						echo $externalOpen . $lightboxOpen . $imageFullOutput . $displayOverlay . $lightboxClose . $externalClose;
						echo '</div></div>';
					}

					// SLIDESHOW DISPLAY
					//===================================
					if($galleryType == 'slideshow'){
						// Image Information
						$imageArray[$i]['imageFull']		= $image['sizes']['TTG Featured Image'];
						$imageArray[$i]['imageLarge']		= $image['sizes']['large'];
						$imageArray[$i]['imageThumb']		= $image['sizes']['TTG Medium Thumbnail'];
						$imageArray[$i]['imageAlt'] 		= $image['alt'];
						$imageArray[$i]['imageCaption'] 	= ( (!empty($galleryCaption)) && (in_array('image', $galleryCaption)) && $imageCaption ) ? '<div class="caption">' . $imageCaption . '</div>' : '';
						$imageArray[$i]['imageDesc'] 		= ( (!empty($galleryCaption)) && (in_array('image', $galleryCaption)) && $imageCaption ) ? $imageDesc : '';

						// Lightbox Info
						$lightboxGroup		= ($galleryLightboxThumb) ? ' class="ilightbox"' : ' id="ilightbox"';
						$lightboxGroup 		= ($galleryLightbox || $galleryNormThumb) ? $lightboxGroup : '';
						$lightboxDesc 		= ( (!empty($galleryCaption)) && (in_array('lightbox', $galleryCaption)) ) ? ' data-caption="' . $imageDesc . '"' : '';
						$lightboxOptions 	= ' data-options="thumbnail: \'' . $image['sizes']['TTG Medium Thumbnail'] . '\'"'; 
						$imageArray[$i]['lightboxOpen']		= ($galleryLightbox || $galleryNormThumb) ? '<a href="' . $image['sizes']['large'] . '"' . $lightboxGroup . $lightboxDesc . $lightboxOptions . $galleryLightboxShare . $lightboxTitle . '>' : '';
						$imageArray[$i]['lightboxClose'] 		= ($galleryLightbox || $galleryNormThumb) ? '</a>' : '';

						$i++;
					}
				}

				// Contain our "Normal" Gallery
				if($galleryType == 'normal'){ echo '</div>'; }

				// OUTPUT SLIDESHOW
				if($galleryType == 'slideshow' && !empty($imageArray)):
?>
					<div id="slider" class="flexslider">
						<ul class="slides">
							<?php foreach($imageArray as $image): ?>
								<li class="gallery-image">
									<?= $image['imageCaption']; ?>
									<?= $image['lightboxOpen']; ?>
									<img src="<?= $image['imageFull']; ?>" alt="<?= $image['imageAlt']; ?>" />
									<?= $image['lightboxClose']; ?>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>

					<?php if($gallerySlideThumb): ?>
						<div id="carousel" class="flexslider">
							<ul class="slides">
								<?php foreach($imageArray as $image): ?>
									<li>
										<img src="<?= $image['imageThumb']; ?>" alt="<?= $image['imageAlt']; ?>" />
									</li>
								<?php endforeach; ?>
							</ul>
						</div>
					<?php endif; ?>

					<script>
						jQuery(window).load(function() {
							// The slider being synced must be initialized first
							<?php if($gallerySlideThumb): ?>
								jQuery('#carousel').flexslider({
									animation: 		'slide',
									controlNav: 	<?php if(!empty($gallerySlideCarouselNav) && in_array('nodes', $gallerySlideCarouselNav)) { echo 'true'; } else { echo 'false'; } ?>,
									directionNav: 	<?php if(!empty($gallerySlideCarouselNav) && in_array('direction', $gallerySlideCarouselNav)) { echo 'true'; } else { echo 'false'; } ?>,
									<?php if($gallerySlideLoop) { 'animationLoop: ' . $gallerySlideLoop . ','; } ?>
									slideshow: 		false,
									itemWidth: 		150,
									minItems: 		5,
									itemMargin: 	0,
									asNavFor: 		'#slider'
								});
							<?php endif; ?>
							jQuery('#slider').flexslider({
								controlNav: 	<?php if(!empty($gallerySlideNav) && in_array('nodes', $gallerySlideNav)) { echo 'true'; } else { echo 'false'; } ?>,
								directionNav: 	<?php if(!empty($gallerySlideNav) && in_array('direction', $gallerySlideNav)) { echo 'true'; } else { echo 'false'; } ?>,
								<?php
									if($gallerySlideAnimation) { 'animation: ' . $gallerySlideAnimation . ','; }
									if($gallerySlideSpeed) { 'slideshowSpeed: ' . $gallerySlideSpeed . ','; }
									if($gallerySlideLoop) { 'animationLoop: ' . $gallerySlideLoop . ','; }
									if($gallerySlideSpeed) { 'slideshowSpeed: ' . $gallerySlideSpeed . ','; }
									if($gallerySlideAuto) { 'slideshow: ' . $gallerySlideAuto . ','; }
								?>
								pauseOnAction: 	true,
								pauseOnHover: 	true,
								<?php if($gallerySlideNumber):?>
									start: function(){
										var slideCount 		= jQuery('#slider .slides li:not(.clone)').length,
											currentCount 	= jQuery('.flex-active-slide').index(),
											displayCount 	= (jQuery('#slider .clone').length) ? currentCount : ++currentCount;
										jQuery('#slider').append('<div class="counter"><span class="current">' + displayCount + '</span>/' + slideCount + '</div>');
									},
									after: function(){
										var currentCount = jQuery('.flex-active-slide').index(),
											displayCount = (jQuery('#slider .clone').length) ? currentCount : ++currentCount;
										jQuery('#slider .current').text(displayCount);	
									},
								<?php endif; ?>
								<?php if($gallerySlideThumb): ?>
									sync: "#carousel"
								<?php endif; ?>
							});
						});
					</script>
<?php 
				endif;
			}
		}
?>