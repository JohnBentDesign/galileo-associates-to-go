<?php
	/*===========================================================================
	PANTHEON ADDITION: GALLERY
	===========================================================================*/
	// An additional feature for Patheon that allows galleries on certain themes.
	// Gallery can be lazy load, slideshow, or thumbnail views.
	// General interiors will always remain the same, but archive views can be
	// different per theme.
	
	// Themes to Include This Addition In
	global $galleryArray;
	$galleryArray = array('demeter', 'aphrodite', 'artemis', 'artemis-alpha', 'hera', 'juno', 'persephone', 'hecate', 'hephaestus');
	
	// INCLUDE GALLERY FILES
	//---------------------------------------------------------------------------
	if(pantheon_function_addition($galleryArray)){
		require_once('enqueue-gallery.php');
		require_once('post-type-gallery.php');
		require_once('taxonomy-gallery-categories.php');
		require_once('advanced-custom-fields-gallery.php');
		require_once('functions/pantheon_display_gallery.php');
	}