<?php
	/*===========================================================================
	GALLERY CATEGORIES TAXONOMY
	===========================================================================*/
	add_action( 'init', 'pantheon_register_taxonomy_gallery_categories' );

	function pantheon_register_taxonomy_gallery_categories() {

		$labels = array( 
			'name' => _x( 'Gallery Categories', 'gallery-categories' ),
			'singular_name' => _x( 'Gallery Category', 'gallery-categories' ),
			'search_items' => _x( 'Search Gallery Categories', 'gallery-categories' ),
			'popular_items' => _x( 'Popular Gallery Categories', 'gallery-categories' ),
			'all_items' => _x( 'All Gallery Categories', 'gallery-categories' ),
			'parent_item' => _x( 'Parent Gallery Category', 'gallery-categories' ),
			'parent_item_colon' => _x( 'Parent Gallery Category:', 'gallery-categories' ),
			'edit_item' => _x( 'Edit Gallery Category', 'gallery-categories' ),
			'update_item' => _x( 'Update Gallery Category', 'gallery-categories' ),
			'add_new_item' => _x( 'Add New Gallery Category', 'gallery-categories' ),
			'new_item_name' => _x( 'New Gallery Category', 'gallery-categories' ),
			'separate_items_with_commas' => _x( 'Separate gallery categories with commas', 'gallery-categories' ),
			'add_or_remove_items' => _x( 'Add or remove gallery categories', 'gallery-categories' ),
			'choose_from_most_used' => _x( 'Choose from the most used gallery categories', 'gallery-categories' ),
			'menu_name' => _x( 'Gallery Categories', 'gallery-categories' ),
		);

		$args = array( 
			'labels' => $labels,
			'public' => true,
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'show_tagcloud' => false,
			'show_admin_column' => false,
			'hierarchical' => true,

			'rewrite' => true,
			'query_var' => true
		);

		register_taxonomy( 'gallery-categories', array('gallery'), $args );
	}