<?php
	/*===========================================================================
	STAFF CUSTOM POST TYPE
	=============================================================================
	*/

	add_action( 'init', 'register_cpt_staff' );

	function register_cpt_staff() {

		$labels = array( 
			'name' => _x( 'Staff', 'staff' ),
			'singular_name' => _x( 'Staff', 'staff' ),
			'add_new' => _x( 'Add New', 'staff' ),
			'add_new_item' => _x( 'Add New Staff', 'staff' ),
			'edit_item' => _x( 'Edit Staff', 'staff' ),
			'new_item' => _x( 'New Staff', 'staff' ),
			'view_item' => _x( 'View Staff', 'staff' ),
			'search_items' => _x( 'Search Staff', 'staff' ),
			'not_found' => _x( 'No staff found', 'staff' ),
			'not_found_in_trash' => _x( 'No staff found in Trash', 'staff' ),
			'parent_item_colon' => _x( 'Parent Staff:', 'staff' ),
			'menu_name' => _x( 'Staff', 'staff' ),
		);

		$args = array( 
			'labels' => $labels,
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'thumbnail', 'revisions' ),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => false,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post'
		);

		register_post_type( 'staff', $args );
	}