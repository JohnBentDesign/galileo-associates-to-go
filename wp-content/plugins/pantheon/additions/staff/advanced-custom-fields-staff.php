<?php
	/*===========================================================================
	OPTIONS: STAFF POST DETAILS
	===========================================================================*/

	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_staff-post-details',
			'title' => '[Staff] Post Details',
			'fields' => array (
				array (
					'key' => 'field_537c021c6d073',
					'label' => 'Job Title',
					'name' => 'staff_title',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'html',
					'maxlength' => '',
				),
				array (
					'key' => 'field_537c02336d074',
					'label' => '',
					'name' => '',
					'type' => 'message',
					'message' => '<h2 style="background-color: #0074a2; padding: 5px 10px; color: #ffffff">CONTACT DETAILS</h2>',
				),
				array (
					'key' => 'field_537cb7703aa15',
					'label' => 'Email',
					'name' => 'staff_email',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
				array (
					'key' => 'field_537c02656d075',
					'label' => 'Phone Number',
					'name' => 'staff_number',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
				array (
					'key' => 'field_537c02736d076',
					'label' => 'Extension Number',
					'name' => 'staff_extension',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
				array (
					'key' => 'field_537c02836d077',
					'label' => 'Cell Phone',
					'name' => 'staff_cell',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
				array (
					'key' => 'field_537c02946d078',
					'label' => '',
					'name' => '',
					'type' => 'message',
					'message' => '<h2 style="background-color: #0074a2; padding: 5px 10px; color: #ffffff">SOCIAL MEDIA</h2>',
				),
				array (
					'key' => 'field_537c02a76d079',
					'label' => 'Facebook URL',
					'name' => 'staff_facebook',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
				array (
					'key' => 'field_537c02b26d07a',
					'label' => 'Twitter URL',
					'name' => 'staff_twitter',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
				array (
					'key' => 'field_537c02cd6d07b',
					'label' => 'LinkedIn URL',
					'name' => 'staff_linkedin',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
				array (
					'key' => 'field_537c03727dbf3',
					'label' => 'Google+ URL',
					'name' => 'staff_google',
					'type' => 'text',
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'staff',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}





