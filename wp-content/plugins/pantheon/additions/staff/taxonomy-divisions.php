<?php
	/*===========================================================================
	STAFF DIVISIONS TAXONOMY
	===========================================================================*/
	add_action( 'init', 'register_taxonomy_divisions' );

	function register_taxonomy_divisions() {

	    $labels = array( 
	        'name' => _x( 'Divisions', 'divisions' ),
	        'singular_name' => _x( 'Division', 'divisions' ),
	        'search_items' => _x( 'Search Divisions', 'divisions' ),
	        'popular_items' => _x( 'Popular Divisions', 'divisions' ),
	        'all_items' => _x( 'All Divisions', 'divisions' ),
	        'parent_item' => _x( 'Parent Division', 'divisions' ),
	        'parent_item_colon' => _x( 'Parent Division:', 'divisions' ),
	        'edit_item' => _x( 'Edit Division', 'divisions' ),
	        'update_item' => _x( 'Update Division', 'divisions' ),
	        'add_new_item' => _x( 'Add New Division', 'divisions' ),
	        'new_item_name' => _x( 'New Division', 'divisions' ),
	        'separate_items_with_commas' => _x( 'Separate divisions with commas', 'divisions' ),
	        'add_or_remove_items' => _x( 'Add or remove divisions', 'divisions' ),
	        'choose_from_most_used' => _x( 'Choose from the most used divisions', 'divisions' ),
	        'menu_name' => _x( 'Divisions', 'divisions' ),
	    );

	    $args = array( 
	        'labels' => $labels,
	        'public' => true,
	        'show_in_nav_menus' => true,
	        'show_ui' => true,
	        'show_tagcloud' => false,
	        'show_admin_column' => false,
	        'hierarchical' => true,

	        'rewrite' => true,
	        'query_var' => true
	    );

	    register_taxonomy( 'divisions', array('staff'), $args );
	}