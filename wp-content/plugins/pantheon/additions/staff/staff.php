<?php
	/*===========================================================================
	PANTHEON ADDITION: STAFF
	===========================================================================*/
	// An additional feature for Patheon that allows staff on certain themes.
	// Staff have positions and divisions

	// Themes to Include This Addition In
	$themeArray = array('hera', 'juno');

	// INCLUDE STAFF FILES
	//---------------------------------------------------------------------------
	if(pantheon_function_addition($themeArray)){
		require_once('post-type-staff.php');
		require_once('taxonomy-divisions.php');
		require_once('advanced-custom-fields-staff.php');
	}