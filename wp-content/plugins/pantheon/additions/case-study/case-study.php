<?php
	/*===========================================================================
	PANTHEON ADDITION: CASE STUDY
	===========================================================================*/
	// An additional feature for Patheon that allows case studies on certain themes.
	// Case Study's can relate to testimonials, if testimonials are active.

	// Themes to Include This Addition In
	$themeArray = array();

	// INCLUDE CASE STUDY FILES
	//---------------------------------------------------------------------------
	if(pantheon_function_addition($themeArray)){
		require_once('enqueue-case-study.php');
		require_once('post-type-case-study.php');
		require_once('taxonomy-case-study-types.php');
		// require_once('advanced-custom-fields-case-study.php');
	}