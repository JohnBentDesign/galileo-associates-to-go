<?php
	/*===========================================================================
	CASE STUDY TYPES TAXONOMY
	=============================================================================
	*/
	add_action( 'init', 'register_taxonomy_case_study_type' );

	function register_taxonomy_case_study_type() {

		$labels = array( 
			'name' => _x( 'Case Study Types', 'case-study-types' ),
			'singular_name' => _x( 'Case Study Type', 'case-study-types' ),
			'search_items' => _x( 'Search Case Study Types', 'case-study-types' ),
			'popular_items' => _x( 'Popular Case Study Types', 'case-study-types' ),
			'all_items' => _x( 'All Case Study Types', 'case-study-types' ),
			'parent_item' => _x( 'Parent Case Study Type', 'case-study-types' ),
			'parent_item_colon' => _x( 'Parent Case Study Type:', 'case-study-types' ),
			'edit_item' => _x( 'Edit Case Study Type', 'case-study-types' ),
			'update_item' => _x( 'Update Case Study Type', 'case-study-types' ),
			'add_new_item' => _x( 'Add New Case Study Type', 'case-study-types' ),
			'new_item_name' => _x( 'New Case Study Type', 'case-study-types' ),
			'separate_items_with_commas' => _x( 'Separate case study Types with commas', 'case-study-types' ),
			'add_or_remove_items' => _x( 'Add or remove case study Types', 'case-study-types' ),
			'choose_from_most_used' => _x( 'Choose from the most used case study Types', 'case-study-types' ),
			'menu_name' => _x( 'Case Study Types', 'case-study-types' ),
		);

		$args = array( 
			'labels' => $labels,
			'public' => true,
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'show_tagcloud' => false,
			'show_admin_column' => false,
			'hierarchical' => true,

			'rewrite' => true,
			'query_var' => true
		);

		register_taxonomy( 'case-study-types', array('case-study'), $args );
	}