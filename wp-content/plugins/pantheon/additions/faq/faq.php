<?php
	/*===========================================================================
	PANTHEON ADDITION: FAQ
	===========================================================================*/
	// An additional feature for Patheon that allows FAQs on certain themes.
	// FAQ has questions and answers that expand and collapse on click

	// Themes to Include This Addition In
	$themeArray = array();

	// INCLUDE FAQ FILES
	//---------------------------------------------------------------------------
	if(pantheon_function_addition($themeArray)){
		require_once('post-type-faqs.php');
		require_once('advanced-custom-fields-faqs.php');
	}