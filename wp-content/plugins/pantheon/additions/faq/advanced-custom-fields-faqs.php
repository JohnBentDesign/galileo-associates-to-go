<?php
	/*===========================================================================
	OPTIONS: FAQS OPTIONS
	===========================================================================*/

	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_faq-post-details',
			'title' => '[FAQ] Post Details',
			'fields' => array (
				array (
					'key' => 'field_537e205bc4d74',
					'label' => 'Questions and Answers',
					'name' => 'faq_qa',
					'type' => 'repeater',
					'sub_fields' => array (
						array (
							'key' => 'field_537e254bc4d75',
							'label' => 'Question',
							'name' => 'faq_question',
							'type' => 'text',
							'column_width' => '',
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'formatting' => 'html',
							'maxlength' => '',
						),
						array (
							'key' => 'field_537e2557c4d76',
							'label' => 'Answer',
							'name' => 'faq_answer',
							'type' => 'wysiwyg',
							'column_width' => '',
							'default_value' => '',
							'toolbar' => 'full',
							'media_upload' => 'yes',
						),
					),
					'row_min' => '',
					'row_limit' => '',
					'layout' => 'row',
					'button_label' => 'Add Q&A',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'post',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
					0 => 'the_content',
					1 => 'excerpt',
					2 => 'custom_fields',
					3 => 'discussion',
					4 => 'comments',
					5 => 'featured_image',
					6 => 'categories',
					7 => 'tags',
					8 => 'send-trackbacks',
				),
			),
			'menu_order' => 0,
		));
	}




