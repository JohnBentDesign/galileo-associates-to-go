<?php
	/*===========================================================================
	FAQ CUSTOM POST TYPE
	=============================================================================
	*/
	add_action( 'init', 'register_cpt_faq' );

	function register_cpt_faq() {

		$labels = array( 
			'name' => _x( 'FAQs', 'faqs' ),
			'singular_name' => _x( 'FAQ', 'faqs' ),
			'add_new' => _x( 'Add New', 'faqs' ),
			'add_new_item' => _x( 'Add New FAQ', 'faqs' ),
			'edit_item' => _x( 'Edit FAQ', 'faqs' ),
			'new_item' => _x( 'New FAQ', 'faqs' ),
			'view_item' => _x( 'View FAQ', 'faqs' ),
			'search_items' => _x( 'Search FAQs', 'faqs' ),
			'not_found' => _x( 'No faqs found', 'faqs' ),
			'not_found_in_trash' => _x( 'No faqs found in Trash', 'faqs' ),
			'parent_item_colon' => _x( 'Parent FAQ:', 'faqs' ),
			'menu_name' => _x( 'FAQs', 'faqs' ),
		);

		$args = array( 
			'labels' => $labels,
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'thumbnail', 'revisions' ),
			// 'taxonomies' => array( 'faq-types' ),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post'
		);

		register_post_type( 'faqs', $args );
	}