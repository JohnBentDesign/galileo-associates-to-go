<?php
	/*===========================================================================
	ENQUEUE SCRIPTS AND STYLES
	===========================================================================*/
	// All of our templates will be running on Foundation. So all we need to do
	// is load it from one location. Let's keep the updating easy, huh?!
	
	// JAVASCRIPTS
	//---------------------------------------------------------------------------
	function pantheon_scripts(){
		if(!is_admin()){
			// Vendor Scripts
			wp_enqueue_script('modernizer', plugins_url('foundation/javascripts/vendor/modernizr.js', __FILE__), array('jquery'), '', false);

			// Foundation Scripts
			wp_enqueue_script('foundation', plugins_url('foundation/javascripts/foundation.min.js', __FILE__), array('jquery'), '', true);		
		}
	}
	add_action('wp_enqueue_scripts', 'pantheon_scripts');

	// STYLESHEETS
	//---------------------------------------------------------------------------
	function pantheon_styles(){
		if(!is_admin()){
			// Foundation Stylesheets
			wp_enqueue_style('foundation', plugins_url('foundation/stylesheets/foundation.min.css', __FILE__));
			wp_enqueue_style('ion', plugins_url('ion/ionicons.min.css', __FILE__));
		}
	}
	add_action('wp_enqueue_scripts', 'pantheon_styles');


	function pantheon_styles_admin() {
		if(is_admin()){
			wp_enqueue_style('pantheon-admin', plugins_url('pantheon-admin.css', __FILE__));
		}
	}
	add_action('admin_enqueue_scripts', 'pantheon_styles_admin');
