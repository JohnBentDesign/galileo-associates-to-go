<?php
	/*===========================================================================
	SOCIAL ICONS
	===========================================================================*/
	function pantheon_styles_social(){
		if(!is_admin()){

			// StackIcon Import
			$import 	= false;

			if( have_rows('social_button_styles', 'options') ):
				while( have_rows('social_button_styles', 'options') ): the_row();
					$buttonStyleSocial = get_sub_field('social_style_type');
					if($buttonStyleSocial == 'icon'){
						$import = true;
					}
				endwhile;
			endif;
			if( have_rows('share_button_styles', 'options') ):
				while( have_rows('share_button_styles', 'options') ): the_row();
					$buttonStyleShare = get_sub_field('share_button_style');
					if($buttonStyleShare == 'icon'){
						$import = true;
					}
				endwhile;
			endif;

			if( $import ){
				wp_enqueue_style('stackicons', content_url() . '/plugins/pantheon/styles/social/stackicons-social-custom.min.css');
			}

		}
	}
	add_action('wp_enqueue_scripts', 'pantheon_styles_social');