<?php
	// Remove Core Widget
	if(!function_exists('pantheon_action_remove_widget')){
		function pantheon_action_remove_widget() {
			unregister_widget('WP_Widget_Meta');
			unregister_widget('WP_Widget_Links');
			unregister_widget('WP_Widget_RSS');
			unregister_widget('WP_Widget_Tag_Cloud');
		}

		add_action( 'widgets_init', 'pantheon_action_remove_widget' );
	}