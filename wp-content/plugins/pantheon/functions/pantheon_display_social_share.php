<?php
	/*===========================================================================
	SOCIAL- SHARE
	===========================================================================*/
	if(!function_exists('pantheon_display_social_share')){
		function pantheon_display_social_share($display = false, $type = 'post', $object = null, $content = null, $image = null, $url = null){
			global $post;

			// $display = boolean; defaults to false, true displays the share widget
			// $type 	= string; defaults to 'post', also accepts 'image'
			// $object 	= object; pass an object of the post or image, defaults to global post
			// $content = string; specific content to be used as the summary--helpful for non-traditional sites that may not have the_content()
			// $image 	= object; accepts image object, specific image to be used as the image--helpful for non-traditional sites that may not have a featured image
			// $url 	= string; specific url to be used as the track back link

			// Figure out if we're allowed to display this
			$checkLoc 		= get_field('share_button_location', 'options');
			$display		= ( (!empty($checkLoc) && in_array($display, $checkLoc)) || ($display == 'widget') ) ? $display : false;
			if($object == null){
				global $post;
				$object = $post;
			}
			
			if($display != false){
				// Figure out which buttons to even show
				$buttonsArray 		= ( get_field('share_networks', 'options') ) ? get_field('share_networks', 'options') : array('facebook', 'twitter', 'google', 'linkedin', 'pinterest');
				
				// Get our info depending on object type
				if($type == 'post'){
					$title 		= $object->post_title;
					$url 		= ($url) ? $url : get_permalink($object->ID);

					// Figure out our image
					if($image != ''){
						$media = pantheon_display_post_field_image($image, 'thumbnail', 'url', false);
					}
					elseif(has_post_thumbnail($post->ID)){
						$media = pantheon_display_post_featured_image($object->ID, 'thumbnail', null, 'url', false);
					}
					elseif(get_field('share_button_image', 'options')){
						$media = pantheon_display_post_field_image(get_field('share_button_image', 'options'), 'thumbnail', 'url', false);
					}
					else {
						$media = '';
					}

					// Figure out our summary
					if($content != ''){
						$summary = strip_tags($content);
					}
					elseif($object->post_content != ''){
						$summary = wp_trim_words($object->post_content, 40, '...'); 
					}
					elseif(get_field('share_button_summary', 'options') != ''){
						$summary = get_field('share_button_summary', 'options');
					}
					else {
						$summary = '';
					}
				}
				elseif('image'){
					global $post;

					$title 		= $object['title'];
					$summary 	= wp_trim_words($object['caption'], 40, '...');
					$url 		= get_permalink($post->ID);
					$media 		= pantheon_display_post_field_image($object, 'thumbnail', 'url', false);
				}
				else {
					echo 'You must enter "post" or "image" as the object input.'; 
				}

				// Get Button Styles
				if( have_rows('share_button_styles', 'options') ):
					// We're going to have to check where we are so we only pull styles defined for that space
					$styleLocation = '';

					while( have_rows('share_button_styles', 'options') ):
						the_row();

						// STYLE PER LOCATION //
						// Check Location and Button Style Type
						$styleLocation 		= get_sub_field('share_style_locations');
						$applyStyles 		= ( $display == $styleLocation ) ? true : false;
						$linkType 			= ( $applyStyles && (get_sub_field('share_style_type') == 'text') ) ? 'text' : 'st-icon';
						
						if($applyStyles){
							if($linkType != 'text'){
								// Button Container
								$colorType			= get_sub_field('share_button_color_type');
								$colorize 			= ($colorType) ? $colorType : 'st-single-color';
								$colorizeMulti 		= ($colorType == 'st-multi-color') ? '<span></span>' : '';

								// Button Style
								$buttonType 		= ( $applyStyles && get_sub_field('share_button_style') ) ? get_sub_field('share_button_style') : 'st-shape-icon';
								$buttonSizePixel 	= ( $applyStyles && get_sub_field('share_button_size') ) ? ' font-size: ' . get_sub_field('share_button_size', 'options') . 'px;' : '';
								$buttonStyle		= ( $buttonSizePixel ) ? ' style="' . $buttonSizePixel . '"' : '';
							}
							else {
								$colorType = $colorize = $colorizeMulti = $buttonStyle = $buttonSizePixel = $buttonStyleOutput = '';
							}
						}

						$prepend 			= ( $applyStyles && get_sub_field('share_text')) ? '<span class="prepend"' . $buttonStyle . '>' . get_sub_field('share_text', 'options') . '</span>' : '';
					endwhile;
				endif;

				echo '<div class="social-share social ' . $buttonType . ' ' . $colorize . '">';

					// Put prepend text if there is any
					echo $prepend;

					// Loop through each button and output the class with the proper page URL
					foreach($buttonsArray as $button) {
						switch($button){
							case 'facebook':
								$socialURL 	= 'http://www.facebook.com/sharer.php?u=' . $url;
								break;
							case 'twitter':
								$via 		= (get_field('social_twitter', 'options')) ? '&amp;via=' . str_replace('https://twitter.com/', '', get_field('social_twitter', 'options')) : '';
								$socialURL 	= 'https://twitter.com/intent/tweet?text=' . substr($summary, 0, 110) . '...&amp;url=' . $url . $via;
								break;
							case 'google':
								$socialURL 	= 'https://plus.google.com/share?url=' . $url;
								$button 	= 'googleplus';
								break;
							case 'linkedin':
								$socialURL 	= 'https://www.linkedin.com/shareArticle?mini=true&amp;url=' .$url . '&amp;title=' . $title . '&amp;summary=' . $summary . '&amp;source=' . $url;
								break;
							case 'pinterest':
								$socialURL 	= 'http://www.pinterest.com/pin/find/?url=' . $url;
								break;
							default: 
								$socialURL	= '';
								break;
						}

						$buttonClass = ($linkType != 'text') ? ' class="st-icon-' . $button . '"' : '';

						echo '<a target="_blank" href="' . $socialURL . '"' . $buttonClass . $buttonStyle . '>' . $button . '</a>';
					}

				echo '</div>';	
			}
		}
	}
?>