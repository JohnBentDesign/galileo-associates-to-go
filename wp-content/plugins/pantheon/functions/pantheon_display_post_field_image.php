<?php
	// Advanced Custom Fields Image
	if(!function_exists('pantheon_display_post_field_image')){
		function pantheon_display_post_field_image($object, $size = 'thumbnail', $output = 'image', $echo = true, $add = null){
			// $object 	= get_field() object of image
			// $size 	= defaults to thumbnail, can be replaced by custom image sizes, blank for full size
			// $output 	= defaults to 'image', can also be set to 'style' (outputs with style attribute), 'style-add' (outputs just the CSS attribute), or 'url' (returns image url)
			// $echo 	= boolean; false returns $output instead of echoing
			// $add 	= string; add attributes to the end of something that is selected as 'image'

			// Pull our image object apart
			if($size == '' || $size == 'full'){
				$objectURL 		= (isset($object['url'])) ? $object['url'] : '';
				$objectH 		= (isset($object['height'])) ? $object['height'] : '';
				$objectW 		= (isset($object['width'])) ? $object['width'] : '';
			}
			else {
				$objectURL 		= (isset($object['sizes'])) ? $object['sizes'][$size] : '';
				$objectH 		= (isset($object['sizes'])) ? $object['sizes'][$size . '-height'] : '';
				$objectW 		= (isset($object['sizes'])) ? $object['sizes'][$size . '-width'] :'';
			}
			$objectAlt			= (isset($object['alt'])) ? $object['alt'] : '';

			// Get our mark-up based on what type we're looking for
			switch($output){
				case 'style':
					$output = ' style="background-image: url(' . $objectURL . ');"';
					break;
				case 'style-add':
					$output = ' background-image: url(' . $objectURL . ');';
					break;
				case 'url':
					$output = $objectURL;
					break;
				default:
					$output = '<img src="' . $objectURL . '" height="' . $objectH . '" width="' . $objectW . '" alt="' . $objectAlt . '"' . $add . ' />';
					break;
			}

			// Echo or Return our image
			if($object){
				if($echo){
					echo $output;
				}
				else {
					return $output;
				}
			}
		}
	}