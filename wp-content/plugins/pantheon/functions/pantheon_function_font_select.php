<?php
	// Add Font Selection to sass-watcher
	if(!function_exists('pantheon_function_font_select')){
		function pantheon_function_font_select($selectorsArray){
			/*===========================================================================
			FONT CHOICE
			===========================================================================*/
			if(have_rows('font_choice', 'options')):

				// Setup our font selector
				$cssSelector = '';

				while(have_rows('font_choice', 'options')): the_row();

					//============================
					// Font Application
					//============================
					$applications 	= get_sub_field('font_application');
					$appI 			= 1;
					$appCount 		= count($applications);

					// Font Application: open containing element
					foreach($applications as $app){
						// Check the tag so we can match the correct class for this theme
						switch($app) {
							case 'header': 		$cssSelector .= $selectorsArray['header']; break;
							case 'footer': 		$cssSelector .= $selectorsArray['footer']; break;
							case 'sidebar': 	$cssSelector .= $selectorsArray['sidebar']; break;
							case 'widget': 		$cssSelector .= $selectorsArray['widget']; break;
							case 'post': 		$cssSelector .= $selectorsArray['post']; break;
							case 'page': 		$cssSelector .= $selectorsArray['page']; break;
							case 'gallery': 	$cssSelector .= $selectorsArray['gallery']; break;
							case 'product': 	$cssSelector .= $selectorsArray['product']; break;
						}
						if($appI != $appCount){ $cssSelector .= ', '; }
						else { $cssSelector .= ' {'; }
						$appI++;
					}


						//============================
						// Font Tag
						//============================
						$tags 		= get_sub_field('font_tag');
						$tagI 		= 1;
						$tagCount 	= count($tags);

						// Font Tag: open containing element
						foreach($tags as $tag){
							$cssSelector .= $tag;
							if($tagI != $tagCount){ $cssSelector .= ', '; }
							else { $cssSelector .= ' {'; }
							$tagI++;
						}


							//============================
							// Font Change
							//============================
							$type = get_sub_field('font_type');
							$strip = array('font-family', "'", ';', ':');
							switch($type) {
								case 'Sans Serif': 		$font = get_sub_field('font_type_sanserif'); break;
								case 'Serif': 			$font = get_sub_field('font_type_serif'); break;
								case 'Monospaced': 		$font = get_sub_field('font_type_monospaced'); break;
								case 'Fantasy': 		$font = get_sub_field('font_type_fantasy'); break;
								case 'Script': 			$font = get_sub_field('font_type_script'); break;
								case 'Google Font': 	$font = str_replace($strip, '', get_sub_field('font_type_google_css')); break;
							}
							$cssSelector .= 'font-family: ' . $font . ' !important;';


						// Font Tag: close containing element
						if($tags) {
							$cssSelector .= '}';
						}


					// Font Application: close containing element
					if($applications) {
						$cssSelector .= '}';
					}

					// Google Font
					$cssSelector .= get_sub_field('font_type_google_import');

				endwhile;

				// Return all our styles
				return $cssSelector;

			endif;
		}
	}
