<?php
	// Output post categories for blog posts
	if(!function_exists('pantheon_display_post_categories')){
		function pantheon_display_post_categories($postID){
			// Display Post Categories
			$thePost = get_post($postID);
			$categories = ($thePost->post_type == 'gallery') ? get_the_terms($postID, 'gallery-categories') : get_the_category($postID);
			$output = '';

			if($categories){
				echo '<p class="categories">Categories: ';
				foreach($categories as $category) {
					if($thePost->post_type == 'gallery'){
						$output .= '<a href="' . get_term_link($category) . '">' . $category->name. '</a>, ';
					}
					else {
						$output .= '<a href="' . get_category_link($category->term_id) . '">' . $category->cat_name. '</a>, ';
					}
				}
				// echo trim($output, $separator);
				echo substr($output, 0, -2);
				echo '</p>';
			}
		}
	}