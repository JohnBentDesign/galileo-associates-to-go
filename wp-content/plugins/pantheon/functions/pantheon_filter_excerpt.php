<?php
	// Change get_the_excerpt [...] to something more useful
	if(!function_exists('pantheon_filter_excerpt')){
		function pantheon_filter_excerpt( $more ) {
			$postType = get_post_type();
			switch($postType) {
				case 'gallery': 	$text = "View All"; break;
				case 'product': 	$text = "View Details"; break;
				case 'portfolio': 	$text = "View"; break;
				default: 			$text = "Continue Reading"; break;
			}

			return '... <span class="continue-wrap"><a href="' . get_permalink() . '" class="continue">' . $text . ' &raquo;</a></span>';
		}
		add_filter('excerpt_more', 'pantheon_filter_excerpt');
	}