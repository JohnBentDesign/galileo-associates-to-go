<?php
	/*===========================================================================
	SOCIAL- CONNECT
	===========================================================================*/
	if(!function_exists('pantheon_display_social_connect')){
		function pantheon_display_social_connect($display){

			// Figure out if we're allowed to display this
			$checkLoc 		= get_field('social_button_location', 'options');
			$display		= ( (!empty($checkLoc) && in_array($display, $checkLoc)) || ($display == 'widget') ) ? $display : false;

			// Don't even show anything if we aren't supposed to
			if($display != false){

				// Let's put all of our social icons into an array
				$socialLinks = array(
					'facebook'		=> get_field('social_facebook', 'options'),
					'twitter' 		=> get_field('social_twitter', 'options'),
					'googleplus' 	=> get_field('social_google', 'options'),
					'linkedin' 		=> get_field('social_linkedin', 'options'),
					'youtube' 		=> get_field('social_youtube', 'options'),
					'vimeo' 		=> get_field('social_vimeo', 'options'),
					'pinterest' 	=> get_field('social_pinterest', 'options'),
					'instagram' 	=> get_field('social_instagram', 'options'),
					'yelp' 			=> get_field('social_yelp', 'options'),
					'foursquare'	=> get_field('social_foursquare', 'options'),
					'tumblr' 		=> get_field('social_tumblr', 'options')
				);

				// Get Button Styles
				if( have_rows('social_button_styles', 'options') ):
					// We're going to have to check where we are so we only pull styles defined for that space
					$styleLocation = '';

					while( have_rows('social_button_styles', 'options') ): the_row();
						// STYLE PER LOCATION //
						// Check Location and Button Style Type
						$styleLocation 		= get_sub_field('social_style_locations');
						$applyStyles 		= ( $display == $styleLocation ) ? true : false;
						$linkType 			= ( $applyStyles && (get_sub_field('social_style_type') == 'text') ) ? 'text' : 'st-icon';

						if($applyStyles){
							if($linkType != 'text'){
								// Button Container
								$colorType			= get_sub_field('social_button_color_type');
								$colorize 			= ($colorType) ? $colorType : 'st-single-color';
								$colorizeMulti 		= ($colorType == 'st-multi-color') ? '<span></span>' : '';

								// Button Style
								$buttonType 		= ( $applyStyles && get_sub_field('social_button_style') ) ? get_sub_field('social_button_style') : 'st-shape-icon';
								$buttonSizePixel 	= ( $applyStyles && get_sub_field('social_button_size') ) ? ' font-size: ' . get_sub_field('social_button_size', 'options') . 'px;' : '';
								$buttonStyle		= ( $buttonSizePixel) ? ' style="' . $buttonSizePixel . '"' : '';
							}
							else {
								$colorType = $colorize = $colorizeMulti = $buttonStyle = $buttonSizePixel = $buttonStyleOutput = '';
							}
						}

						$prepend 			= ( $applyStyles && get_sub_field('social_text') && ($linkType != 'text')) ? '<span class="prepend"' . $buttonStyle . '>' . get_sub_field('social_text', 'options') . '</span>' : '';
					endwhile;
				endif;


				// OUTPUT ICONS //
				echo '<div class="social-connect social ' . $buttonType . ' ' . $colorize . '">';

				// Put prepend text if there is anyway
				echo $prepend;

				foreach($socialLinks as $key => $item){
					if(!empty($item)){
						$notText 		= ($linkType != 'text') ? true : false;
						$outputText 	= ($notText) ? $colorizeMulti . $key : ucwords($key);
						$buttonClass 	= ($notText) ? ' class="st-icon-' . $key . '"' : ' class="text"';
						
						// Output the Button
						echo '<a target="_blank" href="' . $item . '"' . $buttonClass . $buttonStyle . '>' . $outputText . '</a>';

					}
				}

				echo '</div>';

			}
			
		}
	}