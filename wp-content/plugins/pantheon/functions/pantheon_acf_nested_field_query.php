<?php
/**
 * Match against ACF sub-fields (repeaters and flexible content) even deeply nested.
 * Returns post-ids or specific or all ACF fields from matching posts/CPTs.
 * todo: this function needs a really good security audit to prevent sql injection and still allow everything a regular meta query does - including handling BETWEEN
 */

if ( ! function_exists( 'pantheon_acf_nested_field_query' ) ) {
	function pantheon_acf_nested_field_query ( $search_fields, $search_criteria, $return_field = 'post_id' ) {
		global $wpdb;

		/**
		 * If we don't have at least a field and a sub-field, this function is not needed. We can
		 * just match against normal post_meta.
		 */
		if ( ! is_array( $search_fields) || count( $search_fields ) < 2 ) {
			return false;
		}

		$meta_key = implode( '_%_', array_map( 'sanitize_key', $search_fields ) );
		$meta_query = '';

		foreach ( (array) $search_criteria as $criterion ) {
			if ( is_string( $criterion) ) {
				$meta_query .= ( $meta_query ? ') OR (' : '(' ) . $meta_key . ' = ' . $criterion;
			} elseif ( is_array( $criterion ) ) {
				$value   = isset( $criterion['value']   ) ? $criterion['value']   : true;
				$key     = isset( $criterion['key']     ) ? $criterion['key']     : $meta_key;
				$cast    = isset( $criterion['cast']    ) ? "AS {$criterion['cast']} )" : "";
				$compare = ( isset( $criterion['compare'] ) && in_array( $criterion['compare'], array(
					'=', '!=', '>', '>=', '<', '<=', 'LIKE', 'NOT LIKE', 'IN', 'NOT IN',
					'BETWEEN', 'NOT BETWEEN', 'NOT EXISTS', 'REGEXP', 'NOT REGEXP', 'RLIKE'
				) ) ) ? $criterion['compare'] : '=';

				$meta_query .= ( $meta_query ? ') OR (' : '(' ) . "meta_key LIKE '$key' AND " . ( $cast ? 'CAST( ' : '' ) . "meta_value $cast $compare '$value'";
			}
		}

		$meta_query .= $meta_query ? ')' : '1';
		$rows = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE $meta_query" );

		if ( 'post_id' == $return_field ) {
			return wp_list_pluck( $rows, 'post_id' );
		}

		if ( 'raw' == $return_field ) {
			return $rows;
		}

		$search_fields = array_values( $search_fields );
		$return = array();

		foreach ( $rows as $row ) {
			preg_match_all('_([0-9]+)_', $row->meta_key, $matches);
			$meta_key = '';

			if ( isset( $matches[1] ) ) {
				foreach ( $matches[1] as $key => $match ) {
					$meta_key .= $search_fields[$key] . '_' . $match . '_';
				}

				$where = $wpdb->prepare( "WHERE post_id = %s AND meta_key LIKE %s", $row->post_id, $meta_key . '%' );
				$row_fields = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta " . $where );

				if ( 'all' == $return_field ) {
					$fields = array();

					foreach ( $row_fields as $field ) {
						$field_key = substr_replace( $field->meta_key, '', 0, strlen( $meta_key ) );
						$fields[$field_key] = $field->meta_value;
					}

					$return[$row->post_id][$match] = $fields;

				} else {
					$filtered = wp_list_filter( $row_fields, array( 'meta_key' => $meta_key . $return_field ) );
					$filtered = array_shift( $filtered );

					if( isset ( $filtered->meta_value ) ) {
						$return[$row->post_id][$match] = $filtered->meta_value;
					}
				}
			}
		}

		return $return;
	}
}