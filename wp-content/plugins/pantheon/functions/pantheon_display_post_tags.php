<?php
	// Output post tags for blog posts
	if(!function_exists('pantheon_display_post_tags')){
		function pantheon_display_post_tags($postID){
			// Display Post Tags
			$tags 	= wp_get_post_tags($postID);
			$output = '';

			if($tags){
				echo '<p class="tags">Tags: ';
				foreach($tags as $tag) {
					$output .= '<a href="' . get_term_link($tag) . '">' . $tag->name. '</a>, ';
				}
				echo substr($output, 0, -2);
				echo '</p>';
			}
		}
	}