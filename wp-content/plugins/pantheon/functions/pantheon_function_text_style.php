<?php
	// Add Font Selection to sass-watcher
	if(!function_exists('pantheon_function_text_style')){
		function pantheon_function_text_style($selectorsArray){
			/*===========================================================================
			FONT CHOICE
			===========================================================================*/
			if(have_rows('text_style', 'options')):

				// Setup our font selector
				$cssSelector = '';

				while(have_rows('text_style', 'options')): the_row();

					//============================
					// Text Application
					//============================
					$applications 	= get_sub_field('text_application');
					$appI 			= 1;
					$appCount 		= count($applications);

					// Text Application: open containing element
					foreach($applications as $app){
						// Check the tag so we can match the correct class for this theme
						switch($app) {
							case 'header': 		$cssSelector .= $selectorsArray['header']; break;
							case 'footer': 		$cssSelector .= $selectorsArray['footer']; break;
							case 'sidebar': 	$cssSelector .= $selectorsArray['sidebar']; break;
							case 'widget': 		$cssSelector .= $selectorsArray['widget']; break;
							case 'post': 		$cssSelector .= $selectorsArray['post']; break;
							case 'page': 		$cssSelector .= $selectorsArray['page']; break;
							case 'gallery': 	$cssSelector .= $selectorsArray['gallery']; break;
							case 'product': 	$cssSelector .= $selectorsArray['product']; break;
						}
						if($appI != $appCount){ $cssSelector .= ', '; }
						else { $cssSelector .= ' {'; }
						$appI++;
					}


						//============================
						// Text Tag
						//============================
						$tags 		= get_sub_field('text_tag');
						$tagI 		= 1;
						$tagCount 	= count($tags);

						// Text Tag: open containing element
						foreach($tags as $tag){
							$cssSelector .= $tag;
							if($tagI != $tagCount){ $cssSelector .= ', '; }
							else { $cssSelector .= ' {'; }
							$tagI++;
						}


							//============================
							// Text Change
							//============================
							$textEdit = get_sub_field('text_edit');

							foreach($textEdit as $edit){
								if($edit == 'Text Size'){
									$cssSelector .= 'font-size: ' . get_sub_field('text_size') . 'px;';
								}
								if($edit == 'Text Style'){
									$styles = get_sub_field('text_style');
									foreach($styles as $style){
										$cssSelector .= $style;
									}
								}
								if($edit == 'Text Color'){
									$cssSelector .= 'color: ' . get_sub_field('text_color') . ';';
								}
							}


						// Text Tag: close containing element
						if($tags) {
							$cssSelector .= '}';
						}


					// Text Application: close containing element
					if($applications) {
						$cssSelector .= '}';
					}

				endwhile;

				// Return all our styles
				return $cssSelector;
				echo $cssSelector;

			endif;
		}
	}
