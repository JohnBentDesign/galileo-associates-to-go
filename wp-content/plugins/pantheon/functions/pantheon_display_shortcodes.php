<?php
	// Add some cool shortcodes
	if(!function_exists('pantheon_display_shortcodes')){
		function pantheon_display_shortcodes($init) {
			$init['theme_advanced_buttons2_add_before'] = 'styleselect';
			$init['theme_advanced_styles'] = 'Button=button';
			return $init;
		}
		add_filter('tiny_mce_before_init', 'pantheon_display_shortcodes');
	}