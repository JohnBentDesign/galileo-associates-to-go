<?php
	// Function to determine what additions/plugins to add to a theme
	if(!function_exists('pantheon_function_addition')){
		function pantheon_function_addition($themeArray){
			// Array of themes this should be included in
			$themes = $themeArray;

			// Check against our current theme
			$parentTheme 		= wp_get_theme();
			$parentTemplate 	= $parentTheme->get('Template');
			$parentTemplateSans = strtolower(str_replace('ttg-', '', $parentTemplate));
			$currentTheme 		= wp_get_theme();

			if(in_array(strtolower($currentTheme), $themes) || in_array($parentTemplateSans, $themes)){
				return true;
			}
			else {
				return false;
			}
		}
	}