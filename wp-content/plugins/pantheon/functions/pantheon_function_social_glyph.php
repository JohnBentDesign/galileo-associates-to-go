<?php
	// Mono Social glyph output
	if(!function_exists('pantheon_function_social_glyph')){
		function pantheon_function_social_glyph($network, $style){

			$glyph 		= '';

			if($style == 'm-rounded'){
				switch($network){
					case 'facebook':					$glyph = '&#xe427;'; 	break;
					case 'twitter':						$glyph = '&#xe486;'; 	break;
					case 'googleplus':					$glyph = '&#xe439;'; 	break;
					case 'google':						$glyph = '&#xe439;'; 	break;
					case 'google-plus':					$glyph = '&#xe439;'; 	break;
					case 'linkedin':					$glyph = '&#xe452;'; 	break;
					case 'youtube':						$glyph = '&#xe499;'; 	break;
					case 'vimeo':						$glyph = '&#xe489;'; 	break;
					case 'pinterest':					$glyph = '&#xe464;'; 	break;
					case 'instagram':					$glyph = '&#xe500;'; 	break;
					case 'yelp':						$glyph = '&#xe498;'; 	break;
					case 'foursquare':					$glyph = '&#xe432;'; 	break;
					case 'tumblr':						$glyph = '&#xe485;'; 	break;
					case 'email':						$glyph = '&#xe424;'; 	break;
				}
			}
			if($style == 'm-circle'){
				switch($network){
					case 'facebook':					$glyph = '&#xe227;'; 	break;
					case 'twitter':						$glyph = '&#xe286;'; 	break;
					case 'googleplus':					$glyph = '&#xe239;'; 	break;
					case 'google':						$glyph = '&#xe239;'; 	break;
					case 'google-plus':					$glyph = '&#xe239;'; 	break;
					case 'linkedin':					$glyph = '&#xe252;'; 	break;
					case 'youtube':						$glyph = '&#xe299;'; 	break;
					case 'vimeo':						$glyph = '&#xe289;'; 	break;
					case 'pinterest':					$glyph = '&#xe264;'; 	break;
					case 'instagram':					$glyph = '&#xe300;'; 	break;
					case 'yelp':						$glyph = '&#xe298;'; 	break;
					case 'foursquare':					$glyph = '&#xe232;'; 	break;
					case 'tumblr':						$glyph = '&#xe285;'; 	break;
					case 'email':						$glyph = '&#xe224;'; 	break;
				}
			}
			if($style == 'm-regular'){
				switch($network){
					case 'facebook':					$glyph = '&#xe027;'; 	break;
					case 'twitter':						$glyph = '&#xe086;'; 	break;
					case 'googleplus':					$glyph = '&#xe039;'; 	break;
					case 'google-plus':					$glyph = '&#xe039;'; 	break;
					case 'google':						$glyph = '&#xe039;'; 	break;
					case 'linkedin':					$glyph = '&#xe052;'; 	break;
					case 'youtube':						$glyph = '&#xe099;'; 	break;
					case 'vimeo':						$glyph = '&#xe089;'; 	break;
					case 'pinterest':					$glyph = '&#xe064;'; 	break;
					case 'instagram':					$glyph = '&#xe100;'; 	break;
					case 'yelp':						$glyph = '&#xe098;'; 	break;
					case 'foursquare':					$glyph = '&#xe032;'; 	break;
					case 'tumblr':						$glyph = '&#xe085;'; 	break;
					case 'email':						$glyph = '&#xe024;'; 	break;
				}
			}
			if($style == 'text'){
				switch($network){
					case 'facebook':					$glyph = 'Facebook'; 	break;
					case 'twitter':						$glyph = 'Twitter'; 	break;
					case 'googleplus':					$glyph = 'Google+'; 	break;
					case 'google-plus':					$glyph = 'Google+'; 	break;
					case 'google':						$glyph = 'Google+'; 	break;
					case 'linkedin':					$glyph = 'LinkedIn'; 	break;
					case 'youtube':						$glyph = 'Youtube'; 	break;
					case 'vimeo':						$glyph = 'Vimeo'; 		break;
					case 'pinterest':					$glyph = 'Pinterest'; 	break;
					case 'instagram':					$glyph = 'Instagram'; 	break;
					case 'yelp':						$glyph = 'Yelp'; 		break;
					case 'foursquare':					$glyph = 'FourSquare'; 	break;
					case 'tumblr':						$glyph = 'Tumblr'; 		break;
					case 'email':						$glyph = 'Email'; 		break;
				}
			}

			return $glyph;

		}
	}