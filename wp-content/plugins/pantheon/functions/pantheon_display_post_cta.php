<?php
	// Advanced Custom Fields CTA

	// FUNCTION ASSUMPTIONS
	//-----------------------------------
	// This function assumes and requires that the custom fields for a CTA are setup as follows:
		// ($prefix)cta_display (true/false)
		// ($prefix)cta_text (text)
		// ($prefix)cta_type (radio button (internal/external as choices))
		// ($prefix)cta_internal (relationship set to return Post ID)
		// ($prefix)cta_external (text)

	if(!function_exists('pantheon_display_post_cta')){
		function pantheon_display_post_cta($args = NULL){

			// DEFAULT
			//-----------------------------------
			$cta = (object) array(
				'prefix' 		=> null, 		// string; Prefix for field, should include underscore if there is one
				'class' 		=> 'button',	// string; Class given to the link that is output
				'echo' 			=> true,		// boolean; True to echo, false to return
				'repeater' 		=> 'no', 		// string; Accepts 'no', 'row', 'sub-field'. 'No' means it's not part of a repeater field, 'row' means it is being parsed by an object, 'sub-field' means it's being looped
				'options'		=> false, 		// boolean; Check if this field is coming from the 'Options' section
				'repeaterRow' 	=> null, 		// object; Pass the row's object if it is a repeater
				'content'		=> 'text',		// string; Accepts 'text' or any other string. 'Text' uses regular cta_text information. Anything else that is passed will be what appears inside of the link
				'tax' 			=> false 		// boolean; Check if field is coming from a taxonomy
			);

			// ARGUMENTS
			//-----------------------------------
			if($args){
				foreach($args as $key => $value){
					$cta->$key = $value;
				}
			}

			// Simplify our arguments
			$prefix 	= $cta->prefix;
			$class 		= $cta->class;
			$echo 		= $cta->echo;
			$repeater 	= $cta->repeater;
			$options 	= $cta->options;
			$row 		= $cta->repeaterRow;
			$content 	= $cta->content;
			$tax 		= $cta->tax;
			$queried_object = get_queried_object();

			// Check if we're inside of a custom field
			if( ($repeater == 'sub-field') && $options){
				$ctaText 		= ( ($content == 'text') && get_sub_field($prefix . 'cta_text', 'options')) ? get_sub_field($prefix . 'cta_text', 'options') : $content;
				$ctaType 		= get_sub_field($prefix . 'cta_type', 'options');
				$ctaInternal 	= get_sub_field($prefix . 'cta_internal', 'options');
				$ctaOutput 		= ($ctaType == 'internal') ? '<a href="' . get_permalink($ctaInternal[0]) . '" class="' . $class . '">' . $ctaText . '</a>' : '<a href="' . get_sub_field($prefix . 'cta_external', 'options') . '" class="' . $class . '" target="_blank">' . $ctaText . '</a>';
			}
			elseif( ($repeater == 'sub-field') ){
				$ctaDisplay 	= get_sub_field($prefix . 'cta_display');
				$ctaText 		= ( ($content == 'text') && get_sub_field($prefix . 'cta_text')) ? get_sub_field($prefix . 'cta_text') : $content;
				$ctaType 		= get_sub_field($prefix . 'cta_type');
				$ctaInternal 	= get_sub_field($prefix . 'cta_internal');
				$ctaExternal 	= get_sub_field($prefix . 'cta_external');
				$ctaSlide 		= get_sub_field($prefix . 'cta_slide');
				switch ($ctaType) {
					case 'internal': $ctaOutput = '<a href="' . get_permalink($ctaInternal[0]) . '" class="' . $class . '">' . $ctaText . '</a>'; break;
					case 'external': $ctaOutput = '<a href="' . get_sub_field($prefix . 'cta_external') . '" class="' . $class . '" target="_blank">' . $ctaText . '</a>'; break;
					case 'slide': $ctaOutput = '<a data-scroll href="#' . sanitize_title(get_the_title($ctaSlide[0])) . '" class="' . $class . '">' . $ctaText . '</a>'; break;
				}
				$ctaOutput 		= ($ctaDisplay) ? $ctaOutput : '';
			}
			elseif( ($repeater == 'row') && !empty($row) ){
				$ctaDisplay 	= $row[$prefix . 'cta_display'];
				$ctaText 		= ( ($content == 'text') && $row[$prefix . 'cta_text']) ? $row[$prefix . 'cta_text'] : $content;
				$ctaType 		= $row[$prefix . 'cta_type'];
				$ctaInternal 	= $row[$prefix . 'cta_internal'];
				$ctaOutput 		= ($ctaType == 'internal') ? '<a href="' . get_permalink($ctaInternal[0]) . '" class="' . $class . '">' . $ctaText . '</a>' : '<a href="' . $row[$prefix . 'cta_external'] . '" class="' . $class . '" target="_blank">' . $ctaText . '</a>';
				$ctaOutput 		= ($ctaDisplay) ? $ctaOutput : '';
			}
			elseif($options){
				$ctaDisplay 	= get_field($prefix . 'cta_display', 'options');
				$ctaText 		= ( ($content == 'text') && get_field($prefix . 'cta_text', 'options')) ? get_field($prefix . 'cta_text', 'options') : $content;
				$ctaType 		= get_field($prefix . 'cta_type', 'options');
				$ctaInternal 	= get_field($prefix . 'cta_internal', 'options');
				$ctaOutput 		= ($ctaType == 'internal') ? '<a href="' . get_permalink($ctaInternal[0]) . '" class="' . $class . '">' . $ctaText . '</a>' : '<a href="' . get_field($prefix . 'cta_external', 'options') . '" class="' . $class . '" target="_blank">' . $ctaText . '</a>';
				$ctaOutput 		= ($ctaDisplay) ? $ctaOutput : '';
			}
			elseif($tax){
				$ctaDisplay 	= get_field($prefix . 'cta_display', $queried_object);
				$ctaText 		= ( ($content == 'text') && get_field($prefix . 'cta_text', $queried_object)) ? get_field($prefix . 'cta_text', $queried_object) : $content;
				$ctaType 		= get_field($prefix . 'cta_type', $queried_object);
				$ctaInternal 	= get_field($prefix . 'cta_internal', $queried_object);
				$ctaOutput 		= ($ctaType == 'internal') ? '<a href="' . get_permalink($ctaInternal[0]) . '" class="' . $class . '">' . $ctaText . '</a>' : '<a href="' . get_field($prefix . 'cta_external', $queried_object) . '" class="' . $class . '" target="_blank">' . $ctaText . '</a>';
				$ctaOutput 		= ($ctaDisplay) ? $ctaOutput : '';
			}
			else {
				$ctaDisplay 	= get_field($prefix . 'cta_display');
				$ctaText 		= ( ($content == 'text') && get_field($prefix . 'cta_text')) ? get_field($prefix . 'cta_text') : $content;
				$ctaType 		= get_field($prefix . 'cta_type');
				$ctaInternal 	= get_field($prefix . 'cta_internal');
				$ctaExternal 	= get_field($prefix . 'cta_external');
				$ctaSlide 		= get_field($prefix . 'cta_slide');
				switch ($ctaType) {
					case 'internal': 	$ctaOutput = isset($ctaInternal[0]) ? '<a href="' . get_permalink($ctaInternal[0]) . '" class="' . $class . '">' . $ctaText . '</a>' : ''; break;
					case 'external': 	$ctaOutput = '<a href="' . get_field($prefix . 'cta_external') . '" class="' . $class . '" target="_blank">' . $ctaText . '</a>'; break;
					case 'slide': 		$ctaOutput = isset($ctaSlide[0]) ?'<a data-scroll href="#' . sanitize_title(get_the_title($ctaSlide[0])) . '" class="' . $class . '">' . $ctaText . '</a>' : ''; break;
				}
				$ctaOutput 		= ($ctaDisplay) ? $ctaOutput : '';
			}

			// Echo or Return our image
			if($ctaOutput){
				if($echo){
					echo $ctaOutput;
				}
				else {
					return $ctaOutput;
				}
			}
		}
	}