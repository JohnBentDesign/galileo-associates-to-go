<?php
	// Featured Images
	if(!function_exists('pantheon_display_post_featured_image')){
		function pantheon_display_post_featured_image($postID, $size = 'thumbnail', $link = null, $output = 'image', $echo = true){
			// $postID 	= post ID of the current post or the post you'd like the featured image from
			// $size 	= defaults to thumbnail, can be replaced by custom image sizes
			// $link 	= boolean; check to wrap in provided link
			// $ouput 	= defaults to 'image', can also be set to 'style' (outputs with style attribute), 'style-add' (outputs just the CSS attribute), or 'url' (returns image url)
			// $echo 	= boolean; true echoes, false returns

			if(has_post_thumbnail($postID)):
				$imageID 	= get_post_thumbnail_id($postID);
				$imageOrg 	= wp_get_attachment_image_src($imageID, $size);
				$imageAlt 	= get_post_meta($imageID , '_wp_attachment_image_alt', true);
				$linkOpen 	= ($link) ? '<a href="' . $link . '">' : '';
				$linkClose 	= ($link) ? '</a>' : '';

				// If it's a gif, we'll just leave it alone (in case it animates)
				if(strpos($imageOrg[0], '.gif') !== false) {
					$size = 'full';
				}

				// Put it all together
				$imageOutput = $linkOpen . '<img src="' . $imageOrg[0] . '" alt="' . $imageAlt . '" height="' . $imageOrg[2] . '" width="' . $imageOrg[1] . '" class="featured-image" />' . $linkClose;

				// Figureo out our
				switch($output) {
					case 'url':
						$outputImage = $imageOrg[0];
						break;
					case 'image':
						$outputImage = $imageOutput;
						break;
					case 'style':
						$outputImage = ' style="background-image: url(' . $imageOrg[0] . ')"';
						break;
					case 'style-add':
						$outputImage = ' background-image: url(' . $imageOrg[0] . ')';
						break;
				}

				// Return or Echo
				if($echo){
					echo $outputImage;
				}
				else {
					return $outputImage;
				}
			endif;
		}
	}