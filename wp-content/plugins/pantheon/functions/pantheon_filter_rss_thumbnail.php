<?php
	// Add Thumbnail to RSS Feed
	if(!function_exists('pantheon_filter_RSS_thumbnail')){
		function pantheon_filter_RSS_thumbnail($content) {
			global $post;
			if(has_post_thumbnail($post->ID)) {
				$content = '<div>' . get_the_post_thumbnail( $post->ID, 'medium' ) . '</div>' . $content;
			}
			return $content;
		}
		add_filter('the_excerpt_rss', 'pantheon_filter_RSS_thumbnail');
		add_filter('the_content_feed', 'pantheon_filter_RSS_thumbnail');
	}