<?php
	// Remove Automatic Linking on Images added from media
	if(!function_exists('pantheon_action_remove_auto_image_links')) {
		function pantheon_action_remove_auto_image_links() {
			$image_set = get_option('image_default_link_type');
			
			if ($image_set !== 'none') {
				update_option('image_default_link_type', 'none');
			}
		}
		add_action('admin_init', 'pantheon_action_remove_auto_image_links', 10);
	}
