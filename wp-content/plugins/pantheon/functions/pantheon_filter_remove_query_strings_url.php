<?php
	// Remove Version Query String from URLs
	if(!function_exists('pantheon_filter_remove_query_strings_url')){
		function pantheon_filter_remove_query_strings_url( $src ){
			$parts = explode( '?ver', $src );
			return $parts[0];
		}
		add_filter( 'script_loader_src', 'pantheon_filter_remove_query_strings_url', 15, 1 );
		add_filter( 'style_loader_src', 'pantheon_filter_remove_query_strings_url', 15, 1 );
	}