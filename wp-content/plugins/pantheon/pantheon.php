<?php
	/**
	 * Plugin Name: Pantheon
	 * Plugin URI: http://technologytherapy.com
	 * Description: TTG2GO Theme Framework
	 * Version: 1.0
	 * Author: Technology Therapy
	 * Author URI: http://technologytherapy.com
	 */

	// White Label
	require_once('white-label/white-label.php');

	// Plugins
	require_once('activate-plugins/class-tgm-plugin-activation.php');
	require_once('activate-plugins/pantheon-plugins.php');

	// Functions
	require_once('functions/pantheon_function_addition.php');
	require_once('functions/pantheon_function_social_glyph.php');
	require_once('functions/pantheon_function_font_select.php');
	require_once('functions/pantheon_function_text_style.php');

	require_once('functions/pantheon_display_post_categories.php');
	require_once('functions/pantheon_display_post_featured_image.php');
	require_once('functions/pantheon_display_post_field_image.php');
	require_once('functions/pantheon_display_post_cta.php');
	require_once('functions/pantheon_display_post_tags.php');
	require_once('functions/pantheon_display_social_connect.php');
	require_once('functions/pantheon_display_social_share.php');
	require_once('functions/pantheon_display_shortcodes.php');

	require_once('functions/pantheon_filter_excerpt.php');
	require_once('functions/pantheon_filter_rss_thumbnail.php');
	require_once('functions/pantheon_filter_remove_query_strings_url.php');

	require_once('functions/pantheon_action_remove_auto_image_links.php');
	require_once('functions/pantheon_action_remove_widget.php');

	// ACF Functions
	require_once( 'functions/pantheon_acf_nested_field_query.php' );

	// Options (ACF)
	require_once('advanced-custom-fields/widget-custom.php');
	require_once('advanced-custom-fields/company.php');
	require_once('advanced-custom-fields/theme-font-choice.php');
	require_once('advanced-custom-fields/theme-font-style.php');
	require_once('advanced-custom-fields/theme-social.php');
	require_once('advanced-custom-fields/theme-admin.php');

	// Styles
	require_once('styles/enqueue-defaults.php');	
	require_once('styles/social/social.php');

	// Widgets
	require_once('widgets/widget-call-to-action.php');
	require_once('widgets/widget-custom.php');	
	require_once('widgets/widget-social.php');
	require_once('widgets/widget-contact.php');	
	require_once('widgets/widget-hours.php');
	require_once('widgets/widget-login.php');		

	// Additions
	require_once('additions/gallery/gallery.php');
	require_once('additions/case-study/case-study.php');
	require_once('additions/faq/faq.php');
	require_once('additions/press/press.php');
	require_once('additions/staff/staff.php');
	require_once('additions/testimonial/testimonial.php');
	require_once('additions/shop/shop.php');
	require_once('additions/event/event.php');
	require_once('additions/infinite-scroll/infinite-scroll.php');

	// don't test for existence of function until all plugins have been loaded
	function pantheon__action__plugins_loaded () {

		// Add Options Pages
		if(function_exists('acf_add_options_page')) {
			// We might have special option pages depending on theme, so let's figure out what we're using
			$parentTheme 		= wp_get_theme();
			$parentTemplate 	= $parentTheme->get('Template');
			$parentTemplateSans = strtolower(str_replace('ttg-', '', $parentTemplate));
			$currentTheme 		= wp_get_theme();

			// Add Option Pages
			acf_add_options_page();
			acf_add_options_sub_page('Company Info');
			acf_add_options_sub_page('Theme Styling');

			// Aphrodite
			if(strtolower($currentTheme) == 'aphrodite'){
				acf_add_options_sub_page('Main Menu');
			}
		}

		// If ACF doesn't exist or is deactivated, load a shim
		if( ! function_exists( 'get_field' ) ) {
			require_once( 'advanced-custom-fields/api-shim.php' );
		}

	}
	add_action( 'plugins_loaded', 'pantheon__action__plugins_loaded' );


	// load any acf field groups saved as json
	function pantheon_acf_json_load_point( $paths ) {

		// append path
		$paths[] = plugin_dir_path( __FILE__ ) . 'advanced-custom-fields/';


		// return
		return $paths;

	}
	add_filter('acf/settings/load_json', 'pantheon_' . 'acf_json_load_point');
