<?php
	/*===========================================================================
	WHITE LABELING
	===========================================================================*/

	// Replace WordPress login logo with your own
	add_action('login_head', 'pantheon_custom_login_logo');
	function pantheon_custom_login_logo() {
		$logoFull = plugins_url('images/ttg-logo.png' , __FILE__);

		echo '<style type="text/css">.login h1 a { background-image:url(' . $logoFull . ') !important; height: 72px !important; width: 239px !important; background-size: auto !important; padding-bottom: 0 !important; }</style>';
	}

	// Replace Wordpress Dashboard Logo and Turn off Menu
	add_action('admin_head', 'pantheon_custom_admin_logo');
	function pantheon_custom_admin_logo() {
		$logoIcon = plugins_url('images/ttg-couch.png' , __FILE__);

		echo '<style type="text/css">
			#wpadminbar #wp-admin-bar-wp-logo>.ab-item .ab-icon:before { content: ""; }
			#wp-admin-bar-wp-logo { background-image: url(' . $logoIcon . ') !important; background-size: auto !important; background-repeat: no-repeat !important; background-position: center !important; background-position: 0 0;}
			#wpadminbar #wp-admin-bar-wp-logo.hover>.ab-item { background-image: url(' . $logoIcon . ') !important; background-size: auto !important; background-repeat: no-repeat !important; background-position: center !important; background-position: 0 0; }
		</style>';
        echo '
        	<script type="text/javascript">
	        	jQuery(document).ready(function(){
					jQuery("#wp-admin-bar-wp-logo .ab-sub-wrapper").remove();
					jQuery("#wp-admin-bar-wp-logo a").attr("href", "http://technologytherapy.com/");
				});
			</script>
		';
	}

	// Change the Footer Link
	add_filter('admin_footer_text', 'pantheon_modify_footer_admin');
	function pantheon_modify_footer_admin(){
		echo '<span id="footer-thankyou">Powered by <a href="http://technologytherapy.com/" target="_blank">Technology Therapy To Go</a>.</span>';
	}