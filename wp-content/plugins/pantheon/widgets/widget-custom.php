<?php
	/*===========================================================================
	CUSTOM WIDGET
	===========================================================================*/
	class pantheon_widget_custom_content extends WP_Widget {

		// CONSTRUCT
		//-----------------------------
		function __construct() {
			$widget_ops = array(
				'classname' => 'widget-custom-content', 
				'description' => __('A custom widget that displays a title, content, image, link, and/or background color.')
			);
			$control_ops = array();
			parent::__construct('pantheon_custom', __('[PANTHEON] Custom Content Widget'), $widget_ops, $control_ops);
		}


		// OUTPUT
		//-----------------------------
		function widget( $args, $instance ) {
			extract($args);

			// Break out our variables for output
			$widgetID 			= "widget_" . $args["widget_id"];
			
			$widgetTitle 		= (get_field('widget_title', $widgetID)) ? '<h3 class="widget-title">' . get_field('widget_title', $widgetID) . '</h3>' : '';
			$widgetText 		= (get_field('widget_content', $widgetID)) ? get_field('widget_content', $widgetID) : '';

			$widgetImage 		= (get_field('widget_image', $widgetID)) ? pantheon_display_post_field_image(get_field('widget_image', $widgetID), $size = 'large', $output = 'image', $echo = false, $add = ' class="widget-image"') : '';
			$widgetImageOutput 	= (get_field('widget_image_link', $widgetID)) ? '<a href="' . get_field('widget_image_link', $widgetID) . '">' . $widgetImage . '</a>' : $widgetImage;
			$widgetImagePos 	= get_field('widget_image_position', $widgetID);
			$widgetImageAbove	= ($widgetImagePos == 'above') ? $widgetImageOutput : '';
			$widgetImageBelow 	= ($widgetImagePos == 'below') ? $widgetImageOutput : '';

			$widgetBGChange 	= get_field('widget_bg_change', $widgetID);
			$widgetBGTemp		= ($widgetBGChange == 'template') ? get_field('widget_bg_color', $widgetID) : '';
			$widgetBGCustom		= ($widgetBGChange == 'custom') ? ' style="background-color: ' . get_field('widget_bg_custom', $widgetID) . '"' : '';
			$widgetBGCustomHas 	= ($widgetBGChange == 'custom') ? ' hasBG' : '';

			$widgetCTADisplay 	= get_field('widget_cta_display', $widgetID);
			$widgetCTATitle 	= get_field('widget_cta_text', $widgetID);
			$widgetCTALink		= get_field('widget_cta_link', $widgetID);
			$widgetCTAType 		= get_field('widget_cta_button', $widgetID);
			$widgetCTAButton 	= ($widgetCTAType == 'button') ? ' button' : '';
			$widgetCTAOutput 	= ($widgetCTADisplay) ? '<a href="' . $widgetCTALink . '" class="widget-cta' . $widgetCTAButton . '">' . $widgetCTATitle . '</a>' : '';

			$widgetDivID 		= ($widgetTitle) ? ' id="' . sanitize_title($widgetTitle) . '"' : '';
?>
			<div class="widget widget-custom" <?= $widgetDivID; ?>>
				<div class="<?= $widgetBGTemp . $widgetBGCustomHas; ?>" <?= $widgetBGCustom; ?>>
					<?php echo $widgetImageAbove; ?>
					<?php if($widgetTitle || $widgetText || $widgetCTAOutput): ?>
						<div class="content">
							<?php
								// Output the content
								echo $widgetTitle . $widgetText . $widgetCTAOutput;
							?>	
						</div>
					<?php endif; ?>
					<?php echo $widgetImageBelow; ?>
				</div>
			</div>
<?php
		}


		// UPDATE
		//-----------------------------
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			return $instance;
		}


		// FORM CREATE
		//-----------------------------
		function form( $instance ) { }
	}
	add_action('widgets_init', create_function('', 'return register_widget("pantheon_widget_custom_content");'));