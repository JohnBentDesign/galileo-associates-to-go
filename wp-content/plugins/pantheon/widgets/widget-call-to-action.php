<?php
	/*===========================================================================
	CUSTOM WIDGET
	TODO: Remove this completely!!!!
	===========================================================================*/
	class pantheon_widget_custom extends WP_Widget {

		// CONSTRUCT
		//-----------------------------
		function __construct() {
			$widget_ops = array(
				'classname' => 'widget-custom', 
				'description' => __('A custom widget that displays a title, content, image, link, and/or background color.')
			);
			$control_ops = array();
			parent::__construct('pantheon_custom', __('[PANTHEON] Custom Widget'), $widget_ops, $control_ops);
		}


		// OUTPUT
		//-----------------------------
		function widget( $args, $instance ) {
			extract($args);

			// Break out our variables for output
			$widgetTitle 	= ($instance['widget-title']) ? '<h3 class="widget-title">' . $instance['widget-title'] . '</h3>' : '';
			$widgetText 	= ($instance['widget-text']) ? wpautop($instance['widget-text']) : '';
			$widgetImage 	= ($instance['widget-image']) ? '<img src="' . $instance['widget-image'] . '" class="widget-image" alt="Widget Image" />' : '';
			$widgetLinkText = ($instance['widget-link-text']) ? $instance['widget-link-text'] : 'Read More &raquo;';
			$widgetLink		= ($instance['widget-link']) ? '<a href="' . $instance['widget-link'] . '" class="button">' . $widgetLinkText . '</a>' : '';
			$widgetBG 		= ($instance['widget-bg']) ? ' has-bg' : '';
			$widgetID 		= ($widgetTitle) ? ' id="' . sanitize_title($widgetTitle) . '"' : '';
?>
			<div class="widget widget-custom <?= $widgetBG; ?>" <?= $widgetID; ?>>
				<?php
					// Output the content
					echo $widgetTitle . $widgetImage . $widgetText . $widgetLink;
				?>
			</div>
<?php
		}


		// UPDATE
		//-----------------------------
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			$allowedTags = '<br><a><b><strong><em><i><p><h4><h5><img><span>';

			// Update our instance
			$instance['widget-title'] 		= strip_tags($new_instance['widget-title'], $allowedTags);
			$instance['widget-text'] 		= strip_tags($new_instance['widget-text'], $allowedTags);
			$instance['widget-image'] 		= strip_tags($new_instance['widget-image']);
			$instance['widget-link'] 		= strip_tags($new_instance['widget-link']);
			$instance['widget-link-text'] 	= strip_tags($new_instance['widget-link-text'], $allowedTags);
			$instance['widget-bg'] 			= isset($new_instance['widget-bg']);

			return $instance;
		}


		// FORM CREATE
		//-----------------------------
		function form( $instance ) {
			$instance = wp_parse_args((array) $instance, array( 
				'widget-title' 		=> '',
				'widget-text' 		=> '',
				'widget-image' 		=> '',
				'widget-link' 		=> '',
				'widget-link-text' 	=> '',
				'widget-bg' 		=> '',
			));

			$allowedTags = '<br><a><b><strong><em><i><p><h4><h5><img><span>';

			$widgetTitle 		= strip_tags($instance['widget-title'], $allowedTags);
			$widgetText 		= strip_tags($instance['widget-text'], $allowedTags);
			$widgetImage 		= strip_tags($instance['widget-image']);
			$widgetLink 		= strip_tags($instance['widget-link']);
			$widgetLinkText 	= strip_tags($instance['widget-link-text'], $allowedTags);
			$widgetBG 			= strip_tags($instance['widget-bg']);
?>
			<h3>Custom Widget</h3>
			<p><em>Widget contents will appear in the order you see below.</em></p>
			
			<p>
				<label for="<?php echo $this->get_field_id('widget-title'); ?>"><?php _e('Widget Title:'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('widget-title'); ?>" name="<?php echo $this->get_field_name('widget-title'); ?>" type="text" value="<?php echo esc_attr($widgetTitle); ?>" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('widget-image'); ?>"><?php _e('Widget Image:'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('widget-image'); ?>" name="<?php echo $this->get_field_name('widget-image'); ?>" type="text" value="<?php echo esc_attr($widgetImage); ?>" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('widget-text'); ?>"><?php _e('Widget Text:'); ?></label>
				<textarea class="widefat" rows="16" id="<?php echo $this->get_field_id('widget-text'); ?>" name="<?php echo $this->get_field_name('widget-text'); ?>"><?php echo esc_attr($widgetText); ?></textarea>
				<span><em>Line breaks will be automatically wrapped in paragraphed tags</em></span>
			</p>

			<hr />

			<h3>Button</h3>
			<p><em>If you leave this area blank, then a button will not be displayed. If custom text is not filled out, it will default to "Read More &raquo;"</em></p>
	
			<p>
				<label for="<?php echo $this->get_field_id('widget-link'); ?>"><?php _e('Call to Action URL:'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('widget-link'); ?>" name="<?php echo $this->get_field_name('widget-link'); ?>" type="text" value="<?php echo esc_attr($widgetLink); ?>" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('widget-link-text'); ?>"><?php _e('Call to Action Button Text:'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('widget-link-text'); ?>" name="<?php echo $this->get_field_name('widget-link-text'); ?>" type="text" value="<?php echo esc_attr($widgetLinkText); ?>" />
			</p>

			<hr />

			<h3>Background</h3>
			<p>
				<input id="<?php echo $this->get_field_id('widget-bg'); ?>" name="<?php echo $this->get_field_name('widget-bg'); ?>" type="checkbox" <?php checked(isset($instance['widget-bg']) ? $instance['widget-bg'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('widget-bg'); ?>"><?php _e('Change background to stand out from other widgets'); ?></label>
			</p>
<?php
		}
	}
	add_action('widgets_init', create_function('', 'return register_widget("pantheon_widget_custom");'));