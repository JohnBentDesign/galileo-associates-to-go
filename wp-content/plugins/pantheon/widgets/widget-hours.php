<?php
	/*===========================================================================
	CUSTOM WIDGET
	===========================================================================*/
	class pantheon_widget_hours extends WP_Widget {

		// CONSTRUCT
		//-----------------------------
		function __construct() {
			$widget_ops = array(
				'classname' => 'widget-hours', 
				'description' => __('A custom widget that displays all hours information under Options &rarr; Company Info.')
			);
			$control_ops = array();
			parent::__construct('pantheon_hours', __('<i class="fa fa-line-chart"></i>[PANTHEON] Hours Widget'), $widget_ops, $control_ops);
		}


		// OUTPUT
		//-----------------------------
		function widget( $args, $instance ) {
			extract($args);

			// Break out our variables for output
			$widgetTitle 	= ($instance['widget-title']) ? '<h3 class="widget-title">' . $instance['widget-title'] . '</h3>' : '';
			$widgetText 	= ($instance['widget-text']) ? wpautop($instance['widget-text']) : '';
			$widgetBG 		= ($instance['widget-bg']) ? ' has-bg' : '';
			$widgetID 		= ($widgetTitle) ? ' id="' . sanitize_title($widgetTitle) . '"' : '';
?>
			<div class="widget widget-hours <?= $widgetBG; ?>" <?= $widgetID; ?>>

				<?php echo $widgetTitle . $widgetText; ?>
				
				<div class="hours">
					<?php 
						while(have_rows('hours', 'options')): the_row();
							$day 	= get_sub_field('day');
							$time 	= get_sub_field('time');

							if($day && $time):
					?>
								<p><span class="day label secondary"><?= get_sub_field('day'); ?></span> <time><?= get_sub_field('time'); ?></time></p>
					<?php 
							endif;
						endwhile;
					?>
				</div>

			</div>
<?php
		}


		// UPDATE
		//-----------------------------
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			$allowedTags = '<br><a><b><strong><em><i><p><h4><h5><img><span>';

			// Update our instance
			$instance['widget-title'] 		= strip_tags($new_instance['widget-title'], $allowedTags);
			$instance['widget-text'] 		= strip_tags($new_instance['widget-text'], $allowedTags);
			$instance['widget-bg'] 			= isset($new_instance['widget-bg']);

			return $instance;
		}


		// FORM CREATE
		//-----------------------------
		function form( $instance ) {
			$instance = wp_parse_args((array) $instance, array( 
				'widget-title' 		=> '',
				'widget-text' 		=> '',
				'widget-bg' 		=> '',
			));

			$allowedTags = '<br><a><b><strong><em><i><p><h4><h5><img><span>';

			$widgetTitle 		= strip_tags($instance['widget-title'], $allowedTags);
			$widgetText 		= strip_tags($instance['widget-text'], $allowedTags);
			$widgetBG 			= strip_tags($instance['widget-bg']);
?>
			<p><em>This widget displays all information filled out under</em> Options &rarr; Company Info.</p>
			
			<p>
				<label for="<?php echo $this->get_field_id('widget-title'); ?>"><?php _e('Widget Title:'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('widget-title'); ?>" name="<?php echo $this->get_field_name('widget-title'); ?>" type="text" value="<?php echo esc_attr($widgetTitle); ?>" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('widget-text'); ?>"><?php _e('Widget Text:'); ?></label>
				<textarea class="widefat" rows="16" id="<?php echo $this->get_field_id('widget-text'); ?>" name="<?php echo $this->get_field_name('widget-text'); ?>"><?php echo esc_attr($widgetText); ?></textarea>
				<span><em>Line breaks will be automatically wrapped in paragraphed tags</em></span>
			</p>

			<hr />

			<h3>Background</h3>
			<p>
				<input id="<?php echo $this->get_field_id('widget-bg'); ?>" name="<?php echo $this->get_field_name('widget-bg'); ?>" type="checkbox" <?php checked(isset($instance['widget-bg']) ? $instance['widget-bg'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('widget-bg'); ?>"><?php _e('Change background to stand out from other widgets'); ?></label>
			</p>
<?php
		}
	}
	add_action('widgets_init', create_function('', 'return register_widget("pantheon_widget_hours");'));