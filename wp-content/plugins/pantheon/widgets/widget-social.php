<?php
	/*===========================================================================
	SOCIAL WIDGET
	===========================================================================*/
	class pantheon_widget_social extends WP_Widget {

		// CONSTRUCT
		//-----------------------------
		function __construct() {
			$widget_ops = array(
				'classname' => 'widget_social', 
				'description' => __('A widget that will display either social connect or social share buttons')
			);
			$control_ops = array();
			parent::__construct('ttg_social', __('[PANTHEON] Social Share/Connect'), $widget_ops, $control_ops);
		}


		// OUTPUT
		//-----------------------------
		function widget( $args, $instance ) {
			extract($args);

			// Break out our variables for output
			$widgetTitle 	= $instance['social-title'];
			$widgetClass 	= ($instance['social-type'] == 'social-type-share') ? ' social-share' : ' social-connect';
			$widgetBG 		= ($instance['widget-bg']) ? ' has-bg' : '';
			$widgetID 		= ($widgetTitle) ? ' id="' . sanitize_title($widgetTitle) . '"' : '';
?>
			<div class="widget widget-social <?= $widgetClass . $widgetBG; ?>" <?= $widgetID; ?>>
				<?php
					// Title
					if($widgetTitle){
						echo '<h3 class="widget-title">' . $widgetTitle . '</h3>';	
					}
					// Share
					if($instance['social-type'] == 'social-type-share'){ 
						pantheon_display_social_share('widget', 'post');
					}
					// Connect
					else if($instance['social-type'] == 'social-type-connect'){
						pantheon_display_social_connect('widget');
					}
				?>
			</div>

<?php
		}


		// UPDATE
		//-----------------------------
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			$allowedTags = '<br><a><b><strong><em><i><p><h4><h5><img><span>';

			$instance['social-title'] 	= strip_tags($new_instance['social-title'], $allowedTags);
			$instance['social-type'] 	= strip_tags($new_instance['social-type']);
			$instance['widget-bg'] 		= isset($new_instance['widget-bg']);

			return $instance;
		}


		// FORM CREATE
		//-----------------------------
		function form( $instance ) {
			$instance = wp_parse_args((array) $instance, array( 
				'social-title' => '',
				'social-type' => '',
				'widget-bg' 		=> '',
			));

			$allowedTags = '<br><a><b><strong><em><i><p><h4><h5><img><span>';

			$socialTitle 	= strip_tags($instance['social-title'], $allowedTags);
			$socialType 	= strip_tags($instance['social-type']);
			$widgetBG 		= strip_tags($instance['widget-bg']);
?>
			<p>Apply social share/connect styles to <strong>widget</strong> under <strong><em>Options &rarr; Theme Styling &rarr; Social Media</em></strong>.</em></p>

			<h3>Title</h3>
			<label for="<?php echo $this->get_field_id('social-title'); ?>"><?php _e('Widget Title:'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('social-title'); ?>" name="<?php echo $this->get_field_name('social-title'); ?>" type="text" value="<?php echo esc_attr($socialTitle); ?>" /></p>

			<h3>Social Button Type</h3>
			<p>
				<label for="<?php echo $this->get_field_id('social-type-connect'); ?>">
					Connect:&nbsp;&nbsp;
					<input class="" id="<?php echo $this->get_field_id('social-type-connect'); ?>" name="<?php echo $this->get_field_name('social-type'); ?>" type="radio" value="social-type-connect" <?php if($socialType === 'social-type-connect'){ echo 'checked="checked"'; } ?> />
					&nbsp;&nbsp;&nbsp;&nbsp;
				</label>
				<label for="<?php echo $this->get_field_id('social-type-share'); ?>">
					Share:&nbsp;&nbsp;
					<input class="" id="<?php echo $this->get_field_id('social-type-share'); ?>" name="<?php echo $this->get_field_name('social-type'); ?>" type="radio" value="social-type-share" <?php if($socialType === 'social-type-share'){ echo 'checked="checked"'; } ?> />
				</label>
			</p>

			<hr />
			
			<h3>Background</h3>
			<p>
				<input id="<?php echo $this->get_field_id('widget-bg'); ?>" name="<?php echo $this->get_field_name('widget-bg'); ?>" type="checkbox" <?php checked(isset($instance['widget-bg']) ? $instance['widget-bg'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('widget-bg'); ?>"><?php _e('Change background to stand out from other widgets'); ?></label>
			</p>

<?php
		}
	}
	add_action('widgets_init', create_function('', 'return register_widget("pantheon_widget_social");'));