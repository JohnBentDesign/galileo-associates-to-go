<?php
	/*===========================================================================
	CUSTOM WIDGET
	===========================================================================*/
	class pantheon_widget_contact extends WP_Widget {

		// CONSTRUCT
		//-----------------------------
		function __construct() {
			$widget_ops = array(
				'classname' => 'widget-contact', 
				'description' => __('A custom widget that displays all contact information under Options &rarr; Company Info.')
			);
			$control_ops = array();
			parent::__construct('pantheon_contact', __('[PANTHEON] Contact Widget'), $widget_ops, $control_ops);
		}


		// OUTPUT
		//-----------------------------
		function widget( $args, $instance ) {
			extract($args);

			// Break out our variables for output
			$widgetTitle 	= ($instance['widget-title']) ? '<h3 class="widget-title">' . $instance['widget-title'] . '</h3>' : '';
			$widgetText 	= ($instance['widget-text']) ? wpautop($instance['widget-text']) : '';
			$widgetBG 		= ($instance['widget-bg']) ? ' has-bg' : '';
			$widgetID 		= ($widgetTitle) ? ' id="' . sanitize_title($widgetTitle) . '"' : '';

			// Get Contact Information!
			$addressOne 		= get_field('general_address_one', 'options');
			$addressTwo 		= get_field('general_address_two', 'options');
			$addressCity 		= get_field('general_address_city', 'options');
			$addressState 		= get_field('general_address_state_abbr', 'options');
			$addressZip 		= get_field('general_address_zip', 'options');
			$phone 				= get_field('general_phone', 'options');
			$phoneFree			= get_field('general_phone_free', 'options');
			$fax 				= get_field('general_fax', 'options');
			$emailPrimary 		= get_field('general_email_primary', 'options');
			$emailSecondary 	= get_field('general_email_secondary', 'options');
?>
			<div class="widget widget-contact <?= $widgetBG; ?>" <?= $widgetID; ?>>

				<?php
					// Make an array with our details so we don't show empty containers
					global $contactDetails;
					$contactDetails = array($addressOne, $addressTwo, $addressCity, $addressState, $addressZip, $phone, $phoneFree, $fax, $emailPrimary, $emailSecondary);

					if(!empty($contactDetails)):

						echo $widgetTitle . $widgetText;

						echo '<div class="vcard contact-details">';

								// ADDRESS //
								if($addressOne || $addressTwo || $addressCity || $addressState || $addressZip){
									echo '<div class="adr">';

									// Address: Company Name
									echo '<p class="fn org">' . get_bloginfo('title') . '</p>';
									// Address: Street
									if($addressOne) { echo '<p class="street-address">' . $addressOne . '</p>'; }
									if($addressTwo) { echo '<p class="extended-address">' . $addressTwo . '</p>'; }
									// Address: Locale
									if($addressCity || $addressState || $addressZip) {
										echo '<p class="locale">';
										if($addressCity) 									{ echo '<span class="locality">' . $addressCity . '</span>'; }
										if($addressCity && ($addressState || $addressZip)) 	{ echo ', '; }
										if($addressState) 									{ echo '<span class="region">' . $addressState . '</span> '; }
										if($addressZip) 									{ echo '<span class="postal-code">' . $addressZip . '</span>'; }
										echo '</p>';
									}

									echo '</div>';
								}

								// NUMBERS //
								if($phone || $phoneFree || $fax){
									echo '<div class="numbers">';

									if($phone) 		{ echo '<p class="tel"><span class="prepend">Phone:</span>' . $phone . '</p>'; }
									if($phoneFree) 	{ echo '<p class="tel" type="work"><span class="prepend">Toll Free:</span>' . $phoneFree . '</p>'; }
									if($fax)		{ echo '<p class="tel" type="fax"><span class="prepend">Fax:</span>' . $fax . '</p>'; }

									echo '</div>';
								}

								// EMAILS //
								if($emailPrimary || $emailSecondary){
									echo '<div class="emails">';

									if($emailPrimary) 	{ echo '<a href="mailto:' . $emailPrimary . '">' . $emailPrimary . '</a>'; }
									if($emailSecondary) { echo '<a href="mailto:' . $emailSecondary . '">' . $emailSecondary . '</a>'; }

									echo '</div>';
								}
						
						echo '</div>';

					endif;
				?>

			</div>
<?php
		}


		// UPDATE
		//-----------------------------
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			$allowedTags = '<br><a><b><strong><em><i><p><h4><h5><img><span>';

			// Update our instance
			$instance['widget-title'] 		= strip_tags($new_instance['widget-title'], $allowedTags);
			$instance['widget-text'] 		= strip_tags($new_instance['widget-text'], $allowedTags);
			$instance['widget-bg'] 			= isset($new_instance['widget-bg']);

			return $instance;
		}


		// FORM CREATE
		//-----------------------------
		function form( $instance ) {
			$instance = wp_parse_args((array) $instance, array( 
				'widget-title' 		=> '',
				'widget-text' 		=> '',
				'widget-bg' 		=> '',
			));

			$allowedTags = '<br><a><b><strong><em><i><p><h4><h5><img><span>';

			$widgetTitle 		= strip_tags($instance['widget-title'], $allowedTags);
			$widgetText 		= strip_tags($instance['widget-text'], $allowedTags);
			$widgetBG 			= strip_tags($instance['widget-bg']);
?>
			<p><em>This widget displays all information filled out under</em> Options &rarr; Company Info.</p>
			
			<p>
				<label for="<?php echo $this->get_field_id('widget-title'); ?>"><?php _e('Widget Title:'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('widget-title'); ?>" name="<?php echo $this->get_field_name('widget-title'); ?>" type="text" value="<?php echo esc_attr($widgetTitle); ?>" />
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('widget-text'); ?>"><?php _e('Widget Text:'); ?></label>
				<textarea class="widefat" rows="16" id="<?php echo $this->get_field_id('widget-text'); ?>" name="<?php echo $this->get_field_name('widget-text'); ?>"><?php echo esc_attr($widgetText); ?></textarea>
				<span><em>Line breaks will be automatically wrapped in paragraphed tags</em></span>
			</p>

			<hr />

			<h3>Background</h3>
			<p>
				<input id="<?php echo $this->get_field_id('widget-bg'); ?>" name="<?php echo $this->get_field_name('widget-bg'); ?>" type="checkbox" <?php checked(isset($instance['widget-bg']) ? $instance['widget-bg'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('widget-bg'); ?>"><?php _e('Change background to stand out from other widgets'); ?></label>
			</p>
<?php
		}
	}
	add_action('widgets_init', create_function('', 'return register_widget("pantheon_widget_contact");'));