<?php
	/*===========================================================================
	LOGIN WIDGET
	===========================================================================*/
	class pantheon_widget_login extends WP_Widget {

		// CONSTRUCT
		//-----------------------------
		function __construct() {
			$widget_ops = array(
				'classname' => 'widget_meta', 
				'description' => __( "Log in/out links")
			);
			$control_ops = array();
			parent::__construct('ttg_login', __('[PANTHEON] Login'), $widget_ops, $control_ops);
		}


		// OUTPUT
		//-----------------------------
		function widget( $args, $instance ) {
			extract($args);

			// Break out our variables for output
			$widgetTitle 	= ($instance['widget-title']) ? '<h3 class="widget-title">' . $instance['widget-title'] . '</h3>' : '';
			$widgetBG 		= ($instance['widget-bg']) ? ' has-bg' : '';
			$widgetID 		= ($widgetTitle) ? ' id="' . sanitize_title($widgetTitle) . '"' : '';
?>
			<div class="widget widget-login <?= $widgetBG; ?>" <?= $widgetID; ?>>
				<?php
					// Output the content
					echo $widgetTitle;
					echo '<div class="button">';
					wp_loginout();
					echo '</div>';
				?>
			</div>
<?php
		}


		// UPDATE
		//-----------------------------
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;

			$allowedTags = '<br><a><b><strong><em><i><p><h4><h5><img><span>';

			$instance['widget-title'] 		= strip_tags($new_instance['widget-title'], $allowedTags);
			$instance['widget-bg'] 			= isset($new_instance['widget-bg']);

			return $instance;
		}


		// FORM CREATE
		//-----------------------------
		function form( $instance ) {
			$instance = wp_parse_args((array) $instance, array( 
				'widget-title' 		=> '',
				'widget-bg' 		=> '',
			));

			$allowedTags = '<br><a><b><strong><em><i><p><h4><h5><img><span>';

			$widgetTitle 		= strip_tags($instance['widget-title'], $allowedTags);
?>
			<h3>Custom Widget</h3>
			<p><em>Widget contents will appear in the order you see below.</em></p>
			
			<p>
				<label for="<?php echo $this->get_field_id('widget-title'); ?>"><?php _e('Widget Title:'); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id('widget-title'); ?>" name="<?php echo $this->get_field_name('widget-title'); ?>" type="text" value="<?php echo esc_attr($widgetTitle); ?>" />
			</p>
			<h3>Background</h3>
			<p>
				<input id="<?php echo $this->get_field_id('widget-bg'); ?>" name="<?php echo $this->get_field_name('widget-bg'); ?>" type="checkbox" <?php checked(isset($instance['widget-bg']) ? $instance['widget-bg'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('widget-bg'); ?>"><?php _e('Change background to stand out from other widgets'); ?></label>
			</p>
<?php
		}
	}
	add_action('widgets_init', create_function('', 'return register_widget("pantheon_widget_login");'));
