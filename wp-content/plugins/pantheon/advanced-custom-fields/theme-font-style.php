<?php
	/*===========================================================================
	OPTIONS: TEXT STYLING
	===========================================================================*/

// don't test for existence of function until all plugins have been loaded
function pantheon_font_style__action__plugins_loaded () {

	if( function_exists('register_field_group') ):

	register_field_group(array (
		'key' => 'group_54232c9a7aca9',
		'title' => 'Text Styling',
		'fields' => array (
			array (
				'key' => 'field_52f4ffe9d78fa',
				'label' => '',
				'name' => '',
				'prefix' => '',
				'type' => 'message',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '<p>When applying colors, keep your brand in mind. Colors should not clash with your logo or with each other. Keep to only a few consistent colors. If you need help coming up with color combinations, using a site like <a href="http://www.colorcombos.com/" target="_blank">ColorCombos</a> could be a great reference.</p>
	<p><strong>NOTE:</strong> All styles selected will be shared across all of the tags you select to edit. Not paying attention to this detail could resolve in unexpected results.</h4></p>
	<p><strong>NOTE 2:</strong> These styles apply <em>broadly</em> to the theme and can be aggressive at times. Additional specificity through CSS overriding might be necessary.</p>',
			),
			array (
				'key' => 'field_52f3dec1c3850',
				'label' => 'Text Style',
				'name' => 'text_style',
				'prefix' => '',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Text Style',
				'min' => 0,
				'max' => 0,
				'sub_fields' => array (
					array (
						'key' => 'field_52f3e25d5ddf5',
						'label' => 'Text Tag',
						'name' => 'text_tag',
						'prefix' => '',
						'type' => 'checkbox',
						'instructions' => 'Select as many tags you\'d like to apply to these styles. Next to the description is the HTML tag that corresponds to it.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => 35,
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'h1' => 'Heading (h1)',
							'h2' => 'Heading (h2)',
							'h3' => 'Heading (h3)',
							'h4' => 'Heading (h4)',
							'h5' => 'Heading (h5)',
							'h6' => 'Heading (h6)',
							'p' => 'Paragraph (p)',
							'ol' => 'Ordered List (ol)',
							'ul' => 'Unordered List (ul)',
							'li' => 'List Item (li)',
							'blockquote' => 'Block Quote (blockquote)',
							'label' => 'Form Label (label)',
							'input' => 'Form Input (input)',
							'input[type=submit]' => 'Submit Button (input[type="submit"])',
							'strong' => 'Bold (strong)',
							'em' => 'Italic (em)',
							'dt' => 'Definition Term (dt)',
							'dd' => 'Definition Details (dd)',
							'code' => 'Code (code)',
							'a' => 'Link (a)',
							'.cta' => 'Call to Action (a.cta)',
							'.button' => 'Button Link (a.button)',
							'nav a' => 'Navigation Link (nav a)',
						),
						'default_value' => array (
						),
						'layout' => 'horizontal',
					),
					array (
						'key' => 'field_53a1b422fec31',
						'label' => 'Text Application',
						'name' => 'text_application',
						'prefix' => '',
						'type' => 'checkbox',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'header' => 'Header',
							'footer' => 'Footer',
							'sidebar' => 'Sidebar',
							'widget' => 'Widget',
							'post' => 'Blog Posts',
							'page' => 'Page/Section',
							'gallery' => 'Gallery Posts',
							'product' => 'Product',
						),
						'default_value' => array (
						),
						'layout' => 'horizontal',
					),
					array (
						'key' => 'field_52f524abb8b2d',
						'label' => 'What do you wish to edit?',
						'name' => 'text_edit',
						'prefix' => '',
						'type' => 'checkbox',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'Text Size' => 'Text Size',
							'Text Style' => 'Text Style',
							'Text Color' => 'Text Color',
						),
						'default_value' => array (
						),
						'layout' => 'horizontal',
					),
					array (
						'key' => 'field_52f522abbeeed',
						'label' => 'Text Size',
						'name' => 'text_size',
						'prefix' => '',
						'type' => 'number',
						'instructions' => 'Font sizes should be based on pixels.',
						'required' => 1,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_52f524abb8b2d',
									'operator' => '==',
									'value' => 'Text Size',
								),
							),
						),
						'wrapper' => array (
							'width' => 20,
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => 0,
						'max' => '',
						'step' => '.01',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_52f522489df1c',
						'label' => 'Text Style',
						'name' => 'text_style',
						'prefix' => '',
						'type' => 'checkbox',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_52f524abb8b2d',
									'operator' => '==',
									'value' => 'Text Style',
								),
							),
						),
						'wrapper' => array (
							'width' => 20,
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'font-style: normal;' => 'Style- Normal',
							'font-style: italic;' => 'Style- Italic',
							'font-style: underline;' => 'Style- Underline',
							'font-weight: normal;' => 'Weight- Normal',
							'font-weight: bold;' => 'Weight- Bold',
							'text-transform: uppercase;' => 'Variant- All Caps',
							'font-variant: small-caps;' => 'Variant- Small Caps',
							'text-transform: capitalize;' => 'Variant- Capitalize Every Word',
							'text-transform: lowercase;' => 'Variant- Lowercase Every Word',
						),
						'default_value' => array (
						),
						'layout' => 'vertical',
					),
					array (
						'key' => 'field_52f3e2ff5ddf6',
						'label' => 'Text Color',
						'name' => 'text_color',
						'prefix' => '',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_52f524abb8b2d',
									'operator' => '==',
									'value' => 'Text Color',
								),
							),
						),
						'wrapper' => array (
							'width' => 20,
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
					),
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-theme-styling',
				),
			),
		),
		'menu_order' => 10,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));

	endif;

}
add_action( 'plugins_loaded', 'pantheon_font_style__action__plugins_loaded' );



