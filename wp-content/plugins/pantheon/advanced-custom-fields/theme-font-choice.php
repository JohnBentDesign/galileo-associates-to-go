<?php
	/*===========================================================================
	OPTIONS: FONT CHOICE
	===========================================================================*/

// don't test for existence of function until all plugins have been loaded
function pantheon_font_choice__action__plugins_loaded () {

	if( function_exists('register_field_group') ):

	register_field_group(array (
		'key' => 'group_54232c9a191f6',
		'title' => 'Font Select',
		'fields' => array (
			array (
				'key' => 'field_52f4f5dfb1efd',
				'label' => '',
				'name' => '',
				'prefix' => '',
				'type' => 'message',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '<p>It is <strong>highly</strong> suggested that you keep to 2-3 font choices for the <em>entire</em> site. Also, keeping away from "display" fonts for normal paragraph text (ie. Copperplate) will allow for better readability of your content. Following these guidelines helps to keep design integrity and to properly serve information to your viewers.</p>
	<p><strong>NOTE:</strong> These styles apply <em>broadly</em> to the theme and can be aggressive at times. Additional specificity through CSS overriding might be necessary.</p>',
			),
			array (
				'key' => 'field_52f5026d9ed19',
				'label' => 'Font Choice',
				'name' => 'font_choice',
				'prefix' => '',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Font Choice',
				'min' => 0,
				'max' => 0,
				'sub_fields' => array (
					array (
						'key' => 'field_52f5026d9ed1a',
						'label' => 'Text Tag',
						'name' => 'font_tag',
						'prefix' => '',
						'type' => 'checkbox',
						'instructions' => 'Next to the description is the HTML tag that corresponds to it',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => 75,
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'h1' => 'Heading (h1)',
							'h2' => 'Heading (h2)',
							'h3' => 'Heading (h3)',
							'h4' => 'Heading (h4)',
							'h5' => 'Heading (h5)',
							'h6' => 'Heading (h6)',
							'p' => 'Paragraph (p)',
							'span' => 'Span (span)',
							'ol' => 'Ordered List (ol)',
							'ul' => 'Unordered List (ul)',
							'li' => 'List Item (li)',
							'blockquote' => 'Block Quote (blockquote)',
							'label' => 'Form Label (label)',
							'input' => 'Form Input (input)',
							'input[type=submit]' => 'Submit Button (input[type="submit"])',
							'strong' => 'Bold (strong)',
							'em' => 'Italic (em)',
							'dt' => 'Definition Term (dt)',
							'dd' => 'Definition Details (dd)',
							'code' => 'Code (code)',
							'a' => 'Link (a)',
							'.cta' => 'Call to Action (a.cta)',
							'.button' => 'Button Link (a.button)',
							'nav a' => 'Navigation Link (nav a)',
						),
						'default_value' => array (
						),
						'layout' => 'horizontal',
					),
					array (
						'key' => 'field_53a1ab88227ad',
						'label' => 'Font Application',
						'name' => 'font_application',
						'prefix' => '',
						'type' => 'checkbox',
						'instructions' => 'Select as many locations as you wish for the font styles to apply to. Some options may not be applicable depending on your theme.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'header' => 'Header',
							'footer' => 'Footer',
							'sidebar' => 'Sidebar',
							'widget' => 'Widget',
							'post' => 'Blog Posts',
							'page' => 'Page/Section',
							'gallery' => 'Gallery Posts',
							'product' => 'Product',
						),
						'default_value' => array (
						),
						'layout' => 'horizontal',
					),
					array (
						'key' => 'field_52f5026d9ed1b',
						'label' => 'Font Type',
						'name' => 'font_type',
						'prefix' => '',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'Sans Serif' => 'Sans Serif',
							'Serif' => 'Serif',
							'Monospaced' => 'Monospaced',
							'Fantasy' => 'Fantasy',
							'Script' => 'Script',
							'Google Font' => 'Google Font',
						),
						'default_value' => array (
						),
						'allow_null' => 1,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'placeholder' => '',
						'disabled' => 0,
						'readonly' => 0,
					),
					array (
						'key' => 'field_52f5026d9ed1c',
						'label' => 'Sans Serif',
						'name' => 'font_type_sanserif',
						'prefix' => '',
						'type' => 'select',
						'instructions' => 'A font will only load if the user\'s native computer has the font already installed in their system. This is a list of sans serif fonts that are installed by default on many computer systems. To view the fonts, <a href="http://cssfontstack.com/" target="_blank">click here</a>.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_52f5026d9ed1b',
									'operator' => '==',
									'value' => 'Sans Serif',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'Arial, Helvetica Neue, Helvetica, sans-serif' => 'Arial',
							'Arial Black, Arial Bold, Gadget, sans-serif' => 'Arial Black',
							'Arial Narrow, Arial, sans-serif' => 'Arial Narrow',
							'Arial Rounded MT Bold, Helvetica Rounded, Arial, sans-serif' => 'Arial Rounded MT Bold',
							'Avant Garde, Avantgarde, Century Gothic, CenturyGothic, AppleGothic, sans-serif' => 'Avant Garde',
							'Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif' => 'Calibri',
							'Candara, Calibri, Segoe, Segoe UI, Optima, Arial, sans-serif' => 'Candara',
							'Century Gothic, CenturyGothic, AppleGothic, sans-serif' => 'Century Gothic',
							'Franklin Gothic Medium, Franklin Gothic, ITC Franklin Gothic, Arial, sans-serif' => 'Franklin Gothic Medium',
							'Futura, Trebuchet MS, Arial, sans-serif' => 'Futura',
							'Geneva, Tahoma, Verdana, sans-serif' => 'Geneva',
							'Gill Sans, Gill Sans MT, Calibri, sans-serif' => 'Gill Sans',
							'Helvetica Neue, Helvetica, Arial, sans-serif' => 'Helvetica',
							'Impact, Haettenschweiler, Franklin Gothic Bold, Charcoal, Helvetica Inserat, Bitstream Vera Sans Bold, Arial Black, sans serif' => 'Impact',
							'Lucida Grande, Lucida Sans Unicode, Lucida Sans, Geneva, Verdana, sans-serif' => 'Lucida Grande',
							'Optima, Segoe, Segoe UI, Candara, Calibri, Arial, sans-serif' => 'Optima',
							'Segoe UI, Frutiger, Frutiger Linotype, Dejavu Sans, Helvetica Neue, Arial, sans-serif' => 'Segoe UI',
							'Tahoma, Verdana, Segoe, sans-serif' => 'Tahoma',
							'Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif' => 'Trebuchet MS',
							'Verdana, Geneva, sans-serif' => 'Verdana',
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'placeholder' => '',
						'disabled' => 0,
						'readonly' => 0,
					),
					array (
						'key' => 'field_52f5026d9ed1d',
						'label' => 'Serif',
						'name' => 'font_type_serif',
						'prefix' => '',
						'type' => 'select',
						'instructions' => 'A font will only load if the user\'s native computer has the font already installed in their system. This is a list of serif fonts that are installed by default on many computer systems. To view the fonts, <a href="http://cssfontstack.com/" target="_blank">click here</a>.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_52f5026d9ed1b',
									'operator' => '==',
									'value' => 'Serif',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'Baskerville, Baskerville Old Face, Hoefler Text, Garamond, Times New Roman, serif' => 'Baskerville',
							'Big Caslon, Book Antiqua, Palatino Linotype, Georgia, serif' => 'Big Caslon',
							'Bodoni MT, Didot, Didot LT STD, Hoefler Text, Garamond, Times New Roman, serif' => 'Bodoni MT',
							'Book Antiqua, Palatino, Palatino Linotype, Palatino LT STD, Georgia, serif;' => 'Book Antiqua',
							'Calisto MT, Bookman Old Style, Bookman, Goudy Old Style, Garamond, Hoefler Text, Bitstream Charter, Georgia, serif' => 'Calisto MT',
							'Cambria, Georgia, serif' => 'Cambria',
							'Didot, Didot LT STD, Hoefler Text, Garamond, Times New Roman, serif' => 'Didot',
							'Garamond, Baskerville, Baskerville Old Face, Hoefler Text, Times New Roman, serif' => 'Garamond',
							'Georgia, Times, Times New Roman, serif' => 'Georgia',
							'Goudy Old Style, Garamond, Big Caslon, Times New Roman, serif' => 'Goudy Old Style',
							'Hoefler Text, Baskerville old face, Garamond, Times New Roman, serif' => 'Hoefler Text',
							'Lucida Bright, Georgia, serif' => 'Lucida Bright',
							'Palatino, Palatino Linotype, Palatino LT STD, Book Antiqua, Georgia, serif' => 'Palatino',
							'Perpetua, Baskerville, Big Caslon, Palatino Linotype, Palatino, URW Palladio L, Nimbus Roman No9 L, serif' => 'Perpetua',
							'Rockwell, Courier Bold, Courier, Georgia, Times, Times New Roman, serif' => 'Rockwell',
							'Rockwell Extra Bold, Rockwell Bold, monospace' => 'Rockwell Extra Bold',
							'TimesNewRoman, Times New Roman, Times, Baskerville, Georgia, serif' => 'Times New Roman',
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'placeholder' => '',
						'disabled' => 0,
						'readonly' => 0,
					),
					array (
						'key' => 'field_52f5026d9ed1e',
						'label' => 'Monospaced',
						'name' => 'font_type_monospaced',
						'prefix' => '',
						'type' => 'select',
						'instructions' => 'A font will only load if the user\'s native computer has the font already installed in their system. This is a list of monospaced fonts that are installed by default on many computer systems. To view the fonts, <a href="http://cssfontstack.com/" target="_blank">click here</a>.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_52f5026d9ed1b',
									'operator' => '==',
									'value' => 'Monospaced',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'Andale Mono, AndaleMono, monospace' => 'Andale Mono',
							'Consolas, monaco, monospace' => 'Consolas',
							'Courier New, Courier, Lucida Sans Typewriter, Lucida Typewriter, monospace' => 'Courier New',
							'Lucida Console, Lucida Sans Typewriter, Monaco, Bitstream Vera Sans Mono, monospace' => 'Lucida Console',
							'Lucida Sans Typewriter, Lucida Console, Monaco, Bitstream Vera Sans Mono, monospace' => 'Lucida Sans Typewriter',
							'Monaco, Consolas, Lucida Console, monospace' => 'Monaco',
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'placeholder' => '',
						'disabled' => 0,
						'readonly' => 0,
					),
					array (
						'key' => 'field_52f5026d9ed1f',
						'label' => 'Fantasy',
						'name' => 'font_type_fantasy',
						'prefix' => '',
						'type' => 'select',
						'instructions' => 'A font will only load if the user\'s native computer has the font already installed in their system. This is a list of fantasy fonts that are installed by default on many computer systems. To view the fonts, <a href="http://cssfontstack.com/" target="_blank">click here</a>.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_52f5026d9ed1b',
									'operator' => '==',
									'value' => 'Fantasy',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'Copperplate, Copperplate Gothic Light, fantasy' => 'Copperplate',
							'Papyrus, fantasy' => 'Papyrus',
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'placeholder' => '',
						'disabled' => 0,
						'readonly' => 0,
					),
					array (
						'key' => 'field_52f5026d9ed20',
						'label' => 'Script',
						'name' => 'font_type_script',
						'prefix' => '',
						'type' => 'select',
						'instructions' => 'A font will only load if the user\'s native computer has the font already installed in their system. This is a list of script fonts that are installed by default on many computer systems. To view the fonts, <a href="http://cssfontstack.com/" target="_blank">click here</a>.',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_52f5026d9ed1b',
									'operator' => '==',
									'value' => 'Script',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array (
							'Brush Script MT, cursive' => 'Brush Script MT',
						),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'placeholder' => '',
						'disabled' => 0,
						'readonly' => 0,
					),
					array (
						'key' => 'field_53d14749c8b91',
						'label' => '',
						'name' => '',
						'prefix' => '',
						'type' => 'message',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_52f5026d9ed1b',
									'operator' => '==',
									'value' => 'Google Font',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'message' => 'Adding a Google Font is a little more complex, so listen up!

	<ol>
	<li>Browse to <a href="https://www.google.com/fonts" target="_blank">Google Fonts</a></li>
	<li>Search for a font you\'d like to use and click "Add to Collection"</li>
	<li>With <strong>only one</strong> font in your collection, click the button that says "Use" in the bar at the bottom of your screen</li>
	<li>Make sure to choose <strong>only one</strong> font style (ie. Light, regular, or bold--but ONLY ONE!)</li>
	<li>Click the @import tab and copy the information into the field below</li>
	<li>Copy the text from the "Integrate the fonts into your CSS" section into the field below</li>
	<li>Save!</li>
	<li>Repeat this process for other variations or other font families you wish to add</li>
	</ol>',
					),
					array (
						'key' => 'field_53a1adf1ebbfd',
						'label' => 'Google Font @import',
						'name' => 'font_type_google_import',
						'prefix' => '',
						'type' => 'text',
						'instructions' => 'Copy and paste the entire contents of the @import tab text here (ie. <em>@import url(http://fonts.googleapis.com/css?family=Open+Sans);</em> )',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_52f5026d9ed1b',
									'operator' => '==',
									'value' => 'Google Font',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_53d1465727965',
						'label' => 'Google Font CSS',
						'name' => 'font_type_google_css',
						'prefix' => '',
						'type' => 'text',
						'instructions' => 'Copy and paste the entire contents of the section that requests you to integrate the font into your CSS. (ie. <em>font-family: \'Open Sans\', sans-serif;</em> )',
						'required' => 0,
						'conditional_logic' => array (
							array (
								array (
									'field' => 'field_52f5026d9ed1b',
									'operator' => '==',
									'value' => 'Google Font',
								),
							),
						),
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-theme-styling',
				),
			),
		),
		'menu_order' => 10,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));

	endif;

}
add_action( 'plugins_loaded', 'pantheon_font_choice__action__plugins_loaded' );




