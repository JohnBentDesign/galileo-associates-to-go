<?php
	/*===========================================================================
	OPTIONS: ADMIN ONLY
	===========================================================================*/

// don't test for existence of function until all plugins have been loaded
function pantheon_theme_admin__action__plugins_loaded () {

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_54232c9b464fd',
	'title' => 'Administrator Only',
	'fields' => array (
		array (
			'key' => 'field_5390a987d734a',
			'label' => 'CSS',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_537226570a771',
			'label' => 'Custom CSS',
			'name' => 'advanced_css',
			'type' => 'textarea',
			'instructions' => 'Add custom CSS that will be added to the theme here. Sass is allowed!',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => 40,
			'new_lines' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5390a98fd734b',
			'label' => 'Javascript',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_5509e56683173',
			'label' => 'Header Javascript',
			'name' => 'advanced_js_header',
			'type' => 'textarea',
			'instructions' => 'Add custom Javascript that will be added to the theme\'s header here.		
	Note: a script tag around your code is MANDATORY.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => 20,
			'new_lines' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_537e296ac1bf1',
			'label' => 'Footer Javascript',
			'name' => 'advanced_js',
			'type' => 'textarea',
			'instructions' => 'Add custom Javascript that will be added to the theme\'s footer here.		
	Note: a script tag around your code is MANDATORY.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => 20,
			'new_lines' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5390a998d734c',
			'label' => 'Shop',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_5390a7ff5a1ef',
			'label' => 'Enable shopping cart?',
			'name' => 'shop_enable',
			'type' => 'true_false',
			'instructions' => 'Leave unchecked if you only want to use the shopping cart plugin to display products.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
		),
		array (
			'key' => 'field_53bd9ad9807bc',
			'label' => 'Enable Product Reviews?',
			'name' => 'shop_reviews',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_5390a7ff5a1ef',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
		),
		array (
			'key' => 'field_53c412ad26cbe',
			'label' => 'Enable Related Products?',
			'name' => 'shop_related',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
		),
		array (
			'key' => 'field_5390a8185a1f0',
			'label' => 'Enable shopping cart ONLY for certain categories?',
			'name' => 'shop_enable_cat',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_5390a7ff5a1ef',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
		),
		array (
			'key' => 'field_5390a8335a1f1',
			'label' => 'Product Category Selections',
			'name' => 'shop_enable_cat_select',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array (
				array (
					array (
						'field' => 'field_5390a7ff5a1ef',
						'operator' => '==',
						'value' => '1',
					),
					array (
						'field' => 'field_5390a8185a1f0',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'product_cat',
			'field_type' => 'checkbox',
			'allow_null' => 0,
			'load_save_terms' => 0,
			'return_format' => 'id',
			'multiple' => 0,
			'add_term' => 1,
			'load_terms' => 0,
			'save_terms' => 0,
		),
		array (
			'key' => 'field_5398b967131cb',
			'label' => '404',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_5398b972131cc',
			'label' => 'Choose which page to pull content from',
			'name' => '404_content',
			'type' => 'relationship',
			'instructions' => 'This will pull all the information from the page you choose, including featured image, content, and any other stylistic options the theme has to offer. If the theme has a sidebar-less template, the 404 page will follow that template.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'id',
			'post_type' => array (
				0 => 'page',
			),
			'taxonomy' => array (
			),
			'filters' => array (
				0 => 'search',
				1 => 'post_type',
			),
			'max' => 1,
			'elements' => array (
				0 => 'featured_image',
				1 => 'post_type',
			),
			'min' => 0,
		),
		array (
			'key' => 'field_55bbc0212dba5',
			'label' => 'Development',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_55bbc0632dba6',
			'label' => 'Turn off "on save" CSS compiling',
			'name' => 'dev_compile',
			'type' => 'true_false',
			'instructions' => 'This turns off the "compile on options page save" functionality. This should be UNCHECKED on a live environment.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-theme-styling',
			),
			array (
				'param' => 'current_user_role',
				'operator' => '==',
				'value' => 'administrator',
			),
		),
	),
	'menu_order' => 10000,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

endif;

}
add_action( 'plugins_loaded', 'pantheon_theme_admin__action__plugins_loaded' );

